# David Bohm e notas sobre interpretações quânticas
David Bohm fez uma teoria sensata da mecânica quântica, sem as loucuras de Heisenberg e de Bohr. Bohm parte da linha de que Einstein e Schrödinger estavam certos (Einstein dizia que a mecânica quântica fazia toda a natureza parecer contínua mesmo quando não era; note, Einstein não ignorava a existência de objetos contínuos na realidade).

A teoria de Bohm foi atacada por todos os lados, tanto porque ele era marxista quanto porque os copenhaguistas eram muito dogmáticos. A teoria de Bohm é forte o suficiente para a mecânica quântica clássica, mas é mais que isso, ela é mais forte.

Experimentos mostravam a trajetória das partículas, como a teoria de Bohm previu. Na mecânica quântica de Copenhagen não existe caminho, a partícula só tem posição na hora que você "mede", seja lá que raios signifique medir, não há uma definição formal, nem uma especificação de uma função de medida nem em espaços métricos. O ato de medir é tomado como algo elementar, tanto que há uma discussão sobre isso.
>>O problema da medição e do observador é o problema de onde a medição começa e termina, e onde o observador começa e termina. Considere meus óculos, por exemplo: se eu os tirar agora, a que distância devo colocá-los antes que sejam parte do objeto e não do observador? Existem problemas como esse desde a retina, passando pelo nervo óptico, até o cérebro, e assim por diante. Eu acho que - quando você analisa essa linguagem em que os físicos caíram, que a física trata dos resultados das observações - você descobre que na análise ela se evapora e nada muito claro está sendo dito.<br/>
>>– <cite>John Bell</cite><br/>
>
>Há outra objeção óbvia, também levantada por Bell [46, p. 34]: “O que exatamente qualifica alguns sistemas físicos para desempenhar este papel de ‘medidor’?” E o que aconteceu antes que os humanos existissem? Ou talvez antes que os laboratórios modernos fossem criados? E o que dizer das vastas partes do universo onde não há observadores? As leis da física deixam de se aplicar lá ou no passado? Como pode uma teoria física, que é a mais fundamental de todas, que trata de átomos e partículas elementares, e se aplica em princípio a todo o universo, exigir para sua própria formulação algo tão contingente como certas manipulações (“medições”) feitas durante o nos últimos 100 anos por alguns membros de uma espécie particular, Homo sapiens, vivendo em um determinado planeta em algum lugar do cosmos?

Na citação de Bell, ele se pergunta o quão longe o óculos deve estar dele para que seja considerado um objeto, separado do observador, e essa questão do observador dá muitos paradoxos, como o [amigo de Wigner](https://en.m.wikipedia.org/wiki/Wigner%27s_friend).

A interpretação de Copenhagen virou um culto, não é uma interpretação equivocada, o livro mostra passo a passo, com as citações de Bohr e de Heisenberg. Em última instância, a escola de Copenhagen diz que a lua não existe enquanto você não a observa. Isso não é só uma piada. Se você analisar as consequências de Copenhagen, dá nisso.

Gato de Schrödinger era uma crítica a esse sentido.

Não é uma metáfora, é um experimento de pensamento onde Schrödinger transforma um caso microscópico em macroscópico, porque a justificativa era "no mundo microscópico é assim". Seguem a linha do Dirac e do Feynman. O experimento de Schrödinger pode parecer ruim, porque observar não é olhar e medir é necessariamente interagir, mas há uma pegadinha: isso já foi levado a tão extremos que existem interpretações onde se fala que a consciência humana é que colapsa a função de onda, e essa interpretação não é de nada mais e nada menos do que de Von Neumann (até os gênios erram). Essa interpretação é a interpretação de Von Neumann-Wigner.

Einstein tem uma outra versão (paradoxo EPR), onde ele transforma um problema microscópico em macro.

Mas pode ficar tranquilo que nenhum físico vai defender isso hoje na academia, todo mundo ignora, fingem que não tem nada de errado. Mas a interpretação de Copenhagen, levada ao extremo implica nisso. Todo mundo segue a linha do Feynman.

Em especial, a teoria quântica de campos não abandonou a interpretação copenhaguiana. Eles não comentam sobre interpretações. Copenhagen é a interpretação padrão, e interpretações são importantes, elas têm implicações.

Por exemplo, a teoria de Bohm previu algo que nenhuma outra teoria previu: a trajetória dos elétrons no experimento de dupla fenda. Isso era algo impensável, porque pela própria teoria você nem pode se perguntar isso.

Teoria quântica de campos ainda tem todos os conceitos da mecânica quântica. Há a questão da superposição de estados e colapso, ou seja, cai nesse mesmo problema que ninguém quer mexer mais. A discussão de interpretação já durou décadas e nunca teve um consenso.

Enfim, enquanto a mecânica quântica e teoria quântica de campos têm um aspecto muito idealista e até aparentemente mágico, a teoria de Bohm quebra toda essa magia e transforma em uma teoria materialista (ainda que, de forma limitada; tenho um amigo que diz que se Bohm tivesse usado matemática construtivista, todos os idealismos restantes na teoria de Bohm seriam jogados pela janela), infelizmente não tem um maior desenvolvimento, pouca gente se interessou e foi muito mal falada.

Fontes:
- Making Sense of Quantum Mechanics (Jean Bricmont)
- The Quantum Theory of Motion (Peter Holland)
