# Os limites da física moderna
O Big Bang apenas nos diz que as galáxias estão se dispersando, mas não diz o que há em volta delas (tampouco é uma teoria que explica a origem do Universo, em sentidos hegelianos e de teoria-M, digamos, o Big Bang apenas explica o universo, mas não o Universo), e na verdade, existem regiões no espaço em que o movimento é diferente, também existem grandes espaços sem matéria, e não sabemos o porquê. A maior parte do universo conhecido é espaço livre, no qual não se viu até poeira, e temos o mistério da matéria escura: não sabemos o que tem além dela e nem mesmo o que ela é ou o que significa.

Apesar da tecnologia atual conseguir bastante coisa, ainda é muito limitada. Existem muitos objetos enormes que ainda são invisíveis para ela, dependendo da distância, mas o fato de todo o espaço ser mergulhado em um campo de força que parece ter mais ou menos a mesma intensidade em todos os pontos, e o fato dele ter elasticidade inesgotável e produzir uma enorme quantidade de partículas virtuais, nos sugere que o espaço é de fato infinito.

Se o espaço não é infinito, então o que significa esse campo de força que se estende por todo o espaço que pudemos observar? Qual seria a fonte dele? Por que ele parece ter harmônicas com potência igual em todas as frequências que puderam ser medidas? Por que parece que o espaço possui energia infinita sem momento em cada milímetro cúbico dele? E o que é a energia, afinal? Por que é que parece que tudo que existe é feito de energia?

A energia física pode parecer algo próximo de nós, porque estamos usando eletricidade o dia todo, vemos carros, aviões no céu, etc, mas para os pesquisadores, a natureza da energia ainda é um mistério.

Nós não sabemos o que vai acontecer, até onde nosso conhecimento chega, o que sabemos é que estamos em uma fase muito jovem do universo, no qual as estrelas estão se multiplicando. Daqui várias centenas de bilhões de anos, prevemos que elas irão esgotar o combustível, colapsar e quase todas virar buracos negros, depois não sabemos o que virá. Talvez o Universo se torne completamente ausente de luz, mas não temos certeza. Podemos ter uma maravilhosa surpresa em um futuro muito distante.

Segundo a matemática, não existe nada impedindo a energia de realizar o processo reverso e fazer a entropia do universo "voltar no tempo", tanto é que a temperatura negativa é possível e já foi produzida em laboratório, mas nós não sabemos o que seria necessário para fazer a energia fluir contra o sentido termodinâmico comum em larga escala ou naturalmente.

Talvez, para salvar a vida no universo depois da morte de todas as estrelas, o ser humano ou a descendência da nossa espécie tenha de construir o Jardineiro Universal.

Ainda não sabemos o que realmente acontece dentro dos buracos negros. O mais próximo que conseguimos chegar de reaver alguma informação deles é medindo as ondas gravitacionais que eles emitem.

A viagem no tempo é possível no nosso universo sem paradoxos, mas existe um problema: não tem como controlar onde você vai parar. Você simplesmente parte, e não tem nenhuma garantia de que vai chegar inteiro, ou "onde" vai parar.

Também não sabemos porque podemos ver tanta matéria bariônica no universo, mas não conseguimos ver praticamente nada de anti-matéria, também não sabemos porque a anti-matéria que fazemos em laboratório dura tão pouco e desaparece. Ela deveria ser tão estável quanto a matéria bariônica, mas ela desmancha sozinha. É estranho demais.

Também não sabemos como funciona a refração negativa, ou a anti-gravidade, só sabemos que ela funciona e que o objeto sob anti-gravidade reage como se a gravidade funcionasse ao contrário para ele, mas não sabemos se o objeto distorce o espaço ou se existe um "gráviton".
