# Redução da jornada de trabalho
Os socialistas de hoje em dia querem reduzir a jornada para 6 horas. Diminuir as horas da jornada de trabalho sem diminuir os salários permite adicionar mais um turno de trabalho. Em 24 horas cabem 3 turnos de 8 horas, e também cabem 4 turnos de 6 horas, ou seja, em várias profissões seria possível empregar mais uma pessoa.

Atualmente as empresas contratam 3 pessoas + 1 folguista para manter um posto de 24 horas, com um turno de 6 horas elas contratariam 4 pessoas + 1 folguista. Imagine o impacto na economia, poder do dia para a noite aumentar o número de vagas em 25% mantendo ou até aumentando os salários e tendo mais produtividade porque os empregados não estão exaustos. É um número mais baixo porque não são todas as profissões que iriam aumentar vagas em caso de redução do turno.

Segundo estudos, os trabalhadores brasileiros só rendem em média 40% do que poderiam render, porque eles se desgastam muito. Com mais horas livres e mais dinheiro circulando, a qualidade de vida do trabalhador aumentaria rapidamente em todo o país.

Nossos empresários são muito teimosos, seria necessário o Estado intervir. Não adiantaria só mexer nas horas do turno, seria preciso mexer nas leis da CLT também.
