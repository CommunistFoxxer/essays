# Sociedade do cansaço
>A sociedade do desempenho é a mesma do cansaço.<br/>
>– <cite>Byung Chul-Han</cite>

Vivemos em uma sociedade do cansaço. O neoliberalismo vende que somos poderosos e que tudo podemos. Essa ilusão nos leva a construção de ideias inalcançáveis que, ao não serem atingidos, desmoronam como um castelo de cartas. A culpa, antes um afeto socialmente partilhado, agora é privatizada, você que pode tudo é também responsável por tudo. É a destruição da dimensão social da vida, reduzindo nossa existência a nós mesmos. O resultado é a implosão do eu no abismo depressivo. Não é por um acaso que hoje a depressão é, como diz Maria Rita Kehl, "a água que afunda a nau dos bem adaptados". A depressão é, no neoliberalismo, tecnologia de controle, sintoma do mal-estar social e a prova da falência de nosso modo de vida que se alimenta da saúde psíquica humana.

![](images/burnout_society_charge.png)

Leituras adicionais:
- Sociedade do cansaço (Byung Chul-Han)
- A nova razão do mundo (Dardot e Laval)
- Comum: ensaio sobre a revolução no século XXI (Laval)
