# O que são classes sociais?
Observei que muita gente não entende o que é classe social.
>Como assim um empresário que faz 10 ou 20 mil por mês é parte da classe trabalhadora?

Existem as classes sociais fundamentais, e existem as estratificações. O que define a classe social é uma relação de poder específica, e as diferentes camadas de desigualdade de acesso econômico dentro das classes sociais são as estratificações.

Na ótica marxista, nossa sociedade atual possui duas classes: classe burguesa e classe trabalhadora. A relação de poder que separa essas classes é o controle sobre as matérias-primas, meios de transformação, terras ou ter muito dinheiro (ter capital): os meios de produção.

Se você possui o poder concreto de controlar quando, como e o que será feito das matérias primas, das fábricas, de grandes porções de terras ou de grandes quantias de dinheiro do país, e não precisa trabalhar para ter sustento material: você está na classe burguesa.

E se você precisa trocar sua força de trabalho com outras pessoas para ter dinheiro, e se parar de trabalhar seu padrão de qualidade de vida cai (seja imediatamente ou em algum momento): você está na classe trabalhadora.

A divisão da sociedade em classes é uma consequência dos papéis que as pessoas conseguem assumir dentro dos circuitos de produção material da sociedade. Se você controla plenamente esses circuitos, você é burguês, e se você depende deles, é trabalhador. E dentro das classes, as pessoas não possuem todas o mesmo nível de controles e de acesso à produção material. Essa desigualdade de poderes cria as estratificações.

Por exemplo, na classe burguesa nós temos "o topo da pirâmide": grandes banqueiros, grandes empresários (donos de empresas com fluxos milionários/bilionários de capital), grandes industriais, latifundiários, rentistas.

Temos burgueses que trabalham para o "topo da pirâmide" e portanto possuem um nível de poder menor: diretores e alta gerência, consultores, secretários executivos, advogados dos burgueses mais poderosos, donos de empresas que prestam serviços para as empresas mais poderosas.

Também temos militares de alta patente (oficialato, generalato), políticos de alto escalão, juízes, desembargadores, presidentes e funcionários de alto nível de empresas estatais, ocupantes privilegiados de cargos de indicação. É outra estratificação da burguesia.

E na classe trabalhadora nós também temos estratificações. Por exemplo: prestadores de serviços autônomos ou profissionais de carreiras bem remuneradas como médicos, advogados de escritório, engenheiros, arquitetos; funcionários estatais concursados.

>Essa gente pra mim é rica

Eles têm muito dinheiro e conforto, mas são o que nós acostumamos a chamar de classe média. Eles não possuem controle sobre os meios de produção da sociedade, mas vivem melhor que a maioria. Eles fazem muito dinheiro porém o que os posiciona dentro da classe trabalhadora é a relação que eles possuem com os circuitos da produção material. Eles vendem sua expertise e força de trabalho na forma de serviços para trocar por dinheiro.

O proprietário de 100 casas de aluguel, o franqueado de uma marca de mini-mercados ou de restaurantes de fast-food, também são estratificação da classe trabalhadora. Não dependem tanto da própria força de trabalho mas sua relação com os meios de produção também é dependente. Temos estratificações que as pessoas acham mais fácil aceitar que são da classe trabalhadora, como os comerciários de bairros pobres/médios, vendedores de lojas, empregados domésticos, entregadores de aplicativos, pedreiros, esteticistas.

A classe média é instrumental na manutenção da exploração capitalista sobre a sociedade justamente por criar essa profunda divisão dentro da classe trabalhadora. Sua existência desafia o entendimento de quem não possui o ferramental teórico marxista.
