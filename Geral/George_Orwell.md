# George Orwell
George Orwell era um delator anticomunista que escreveu uma lista de pessoas que suspeitava serem comunistas (ou simpatizantes do comunismo) e a entregou à unidade de propaganda anticomunista do governo do Reino Unido, o Departamento de Pesquisa de Informações. A lista incluía pessoas como John Steinbeck e o incrível Paul Robeson, que Orwell vergonhosamente descreveu como sendo “muito anti-branco”. Na verdade, a lista está repleta de comentários racistas feitos por Orwell sobre as pessoas que ele delatou. Também na lista de delatores de Orwell estava Charlie Chaplin, cujo nome Orwell comentou como "Judeu?". Os detalhes da lista de Orwell podem ser encontrados em 'Who Paid the Piper?: The CIA and the Cultural Cold War', de Frances Stonor Saunders.

Leia "Atirando em um elefante" e me diga se não é um imperialista chorão e humilhado culpando as pessoas que está tentando subjugar sob a autoridade britânica por sua própria violência. O quanto de "Shooting an Elephant", com menções a "evil-spirited little beasts", é fictício? Ou no próprio "Burmese Days", cuja premissa é que os nativos são corruptos e inferiores?

![](images/burmese_days.png)

Constatava "ilusório" manter a independência da Irlanda sem "auxílio e proteção" da Inglaterra.

![](images/notes_on_nationalism.png)

Orwell relacionava absurdos como a resistência à colonização ou ações por independência em Irlanda, Escócia ou País de Gales com movimentos reacionários como o fascismo. Já que mencionei absurdos: tanto jovem como velho, Orwell falava corriqueiramente em "anglofobia", "anti-inglês", "anti-branquitude" - de que condenou o grande comunista negro Paul Robeson.

Sugerindo uma ligação orgânica entre o nacionalismo republicano irlandês e o fascismo, à ultrajante afirmação de que a Irlanda devia sua "proteção" à Grã-Bretanha. O sentimento anti-irlandês de Orwell não é algo que vejo mencionado com tanta frequência. Em uma típica mentalidade colonialista "progressista", ele se separa dos racistas vulgares, mas diz praticamente as mesmas coisas em palavras diferentes.

Orwell em essência era defensor assíduo do Império da Inglaterra, "through and through".

![](images/shooting_an_elephant.png)

Os pensamentos racistas de George Orwell sobre os habitantes de Marrakesh:
>Quando você anda por uma cidade como esta... quando você vê como as pessoas vivem, e ainda mais como facilmente morrem, é sempre difícil acreditar que você está caminhando entre os seres humanos.

O tom de todo aquele ensaio ostensivamente crítico do império revela o próprio racismo profundo de Orwell:
>Eles eram senegaleses, os negros mais negros da África, tão negros que às vezes é difícil ver o paradeiro de seus pescoços os cabelos começa.

E sua aversão ao colonizado. Há também em "Burmese Days" ou "Down and Out in Paris and London", representações racistas de povos colonizados pela Inglaterra - indianos, mianmarenses, irlandeses - além de anti-semitismo flagrante. Cumpre lembrar que Orwell serviu como oficial de polícia em Mianmar colonizada pela Inglaterra.

Criticar o comunismo com 1984, Revolução dos Bichos, Arquipélago Gulag e Chicken Little (todas obras de ficção) tem o mesmo peso que apoiar o monarquismo com O Senhor dos Anéis. Orwell nunca pôs os pés na URSS, nem fez pesquisas aprofundadas sobre como era realmente a vida na União Soviética. Um auto-proclamado socialista democrático, mais tarde na vida ele até traiu seus companheiros “camaradas” e escreveu uma lista de comunistas em potencial para o MI5.

Revolução dos Bichos nada mais era do que uma polêmica anti-soviética destinada a vender exemplares no Ocidente e irritar os comunistas no Oriente. Além disso, muitas das alegorias do livro são simplesmente representações insuficientes ou exageradas do sistema soviético. Um bom exemplo é a construção do moinho eólico, que supostamente representa a industrialização soviética. Uma imagem mais precisa seria se eles construíssem 5 moinhos de vento, dobrassem a produção da fazenda e movessem os animais dos celeiros para os apartamentos (uma nota rápida, a afirmação anterior era apenas para enfatizar, não preste atenção a números específicos). A representação do “totalitarismo” soviético é amplamente exagerada (ver origens do grande expurgo por Arch Getty e Outra visão de Stalin por Ludo Martens). Em conclusão, a Revolução dos Bichos não merece mais um lugar nas discussões sérias sobre o comunismo do que o Sniper Americano pertence nas discussões sobre o capitalismo.

Fontes:
- [Stuck on the Flypaper by Frances Stonor Saunders](https://www.lrb.co.uk/the-paper/v37/n07/frances-stonor-saunders/stuck-on-the-flypaper)
- [The Complete Works of George Orwell | Marrakech](http://george-orwell.org/Marrakech/0.html)
- [The New Irishman | George Orwell on Ireland](http://brianjohnspencer.blogspot.com/2014/10/george-orwell-on-ireland.html)
- [Anti Imperialist Action Ireland | Unmasking St George](https://anti-imperialist-action-ireland.com/blog/2020/05/19/unmasking-st-george)
