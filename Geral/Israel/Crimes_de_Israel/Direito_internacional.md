# Direito internacional
[Apartheid contra palestinos](http://web.archive.org/web/20210520054352/https://en.wikipedia.org/wiki/Israel_and_the_apartheid_analogy).

O Direito Internacional (Protocolo Adicional I às Convenções de Genebra de 1949, Resolução 37/43 da ONU) determina que os palestinos têm o direito legal de uma luta armada para resistir à ocupação.

**Fontes:** [Protocolo I](http://web.archive.org/web/20210520054352/https://treaties.un.org/doc/publication/unts/volume%201125/volume-1125-i-17512-english.pdf), [Convenções de Genebra (PDF)](http://web.archive.org/web/20210520054352/https://treaties.un.org/doc/publication/unts/volume%201125/volume-1125-i-17512-english.pdf), [Resolução da ONU 37/43 (PDF)](http://web.archive.org/web/20210520054352/https://www.google.com/url?sa=t&source=web&rct=j&url=https://undocs.org/pdf%3Fsymbol%3Den/A/RES/37/43&ved=2ahUKEwi2jrGYp6jmAhXEbSsKHdCyAqUQFjAAegQIAhAB&usg=AOvVaw3XITjNzYRnxin4S4JRtzRt&cshid=1575885696424).

A construção de assentamentos por Israel em territórios ocupados como a Cisjordânia e Jerusalém Oriental são ilegais sob o direito internacional, violando o Artigo 49 da Quarta Convenção de Genebra de 1949, bem como o Estatuto de Roma, art. 8 (2)(b)(viii) que afirma: "A Potência Ocupante não deve deportar ou transferir partes de sua própria população civil para o território que ocupa.".

**Fontes:** [Wiki](http://web.archive.org/web/20210520054352/https://en.m.wikipedia.org/wiki/International_law_and_Israeli_settlements), [ONU](http://web.archive.org/web/20210520054352/https://www.un.org/press/en/2016/sc12657.doc.htm), [Lei Internacional sobre Ocupação](http://web.archive.org/web/20210520054352/https://books.google.com.sg/books?id=lcVOlb0wefwC&pg=PA140&redir_esc=y#v=onepage&q&f=false), [A.J](http://web.archive.org/web/20210520054352/https://interactive.aljazeera.com/aje/palestineremix/phone/settlement.html), [Amnesty](http://web.archive.org/web/20210520054352/https://www.amnesty.org/en/latest/campaigns/2019/01/chapter-3-israeli-settlements-and-international-law/), [Amnesty](http://web.archive.org/web/20210520054352/https://www.amnestyusa.org/lets-be-clear-israels-long-running-settlement-policy-constitutes-a-war-crime/), [B'Tselem](http://web.archive.org/web/20210520054352/https://www.btselem.org/international_law), [YouTube](http://web.archive.org/web/20210520054352/https://youtu.be/Mnf0w9UuV4s).

A destruição de casas por Israel em territórios ocupados, como a Cisjordânia e Jerusalém Oriental são ilegais ao abrigo do Estatuto de Roma, bem como violam o Artigo 53 da Quarta Convenção de Genebra de 1949, que afirma: "Qualquer destruição pela Potência Ocupante de bens imóveis ou pessoais pertencentes individual ou coletivamente a particulares... é proibida, exceto quando tal destruição for tornada absolutamente necessária por operações militares."

**Fonte:** [Quarta Convenção de Genebra (PDF)](http://web.archive.org/web/20210520054352/https://www.google.com/url?sa=t&source=web&rct=j&url=https://www.un.org/en/genocideprevention/documents/atrocity-crimes/Doc.33_GC-IV-EN.pdf&ved=2ahUKEwjfifTV77_mAhXp4HMBHUZwD9sQFjANegQIARAB&usg=AOvVaw1inV-CGU9ExFOyNuIJzAoc).

O Artigo 7, Seção 1 (d) dos Estatutos de Roma declara 'Deportação ou transferência forçada de população' um Crime contra a Humanidade.

**Fonte:** [Estatuto de Roma](http://web.archive.org/web/20210520054352/https://legal.un.org/icc/statute/99_corr/cstatute.htm).

>Nós alugamos casa após casa... não terminamos, vamos para o próximo bairro e depois vamos para mais<br/>
>– <cite>Yonatan Yosef</cite> (porta-voz dos colonos israelenses)

**Fonte:** [Reddit](http://web.archive.org/web/20210520054352/https://www.reddit.com/r/Palestine/comments/mvkadx/israeli_settlers_filmed_stealing_homes_from/).

>Os árabes terão que ir, mas é preciso um momento oportuno para que isso aconteça, como uma guerra.<br/>
>– <cite>Ben Gurion</cite>, Primeiro Ministro de Israel (1937)

**Fonte:** [The Ethnic Cleansing of Palestine](http://web.archive.org/web/20210520054352/https://books.google.com.sg/books?id=Xhq9DwAAQBAJ&pg=PT50&lpg=PT50&dq=The+Arabs+will+have+to+go,+but+one+needs+an+opportune+moment+for+making+it+happen,+such+as+a+war&source=bl&ots=s9S-XcK-Ko&sig=ACfU3U1uKBmf04j3cwPEyKe1y9Gd7FrAfA&hl=en&sa=X&ved=2ahUKEwj97IjV6svwAhXYbn0KHS7BDT8Q6AEwEXoECAwQAg).

>Não há lugar para duas nações nesta terra, e não há outra solução além da expulsão de árabes palestinos para outros países vizinhos. Nem mesmo uma aldeia ou uma família deve permanecer nesta terra.<br/>
>– <cite>Yosef Weitz</cite> (Diretor do Departamento de Terras e Florestas, Fundo Nacional Judaico, 1940)

**Fontes:** Birth of the Palestinian Refugee Problem p. 27, Expulsion of the Palestinians p. 131.

Lista de Leis Discriminatórias contra Palestinos em Israel e Territórios Ocupados:
- [Adalah | Discriminatory Laws in Israel](https://www.adalah.org/en/law/index)
- [AlJazeera | Five ways Israeli law discriminates against Palestinians](https://www.aljazeera.com/news/2018/7/19/five-ways-israeli-law-discriminates-against-palestinians)
- [Relatório PDF HRW](https://www.hrw.org/sites/default/files/reports/iopt1210webwcover_0.pdf)
