# Países socialistas antes do socialismo
Não existe essa de "restaurar a democracia" em países socialistas, todos esses países socialistas, com exceção da Tchecoslováquia, não eram uma democracia antes do socialismo. 

**Rússia**

A Rússia era um regime racista e autocrático controlada por um rei (czar), era chamada de "cativeiro dos povos", pois todos os povos bárbaros e "negros" (definição para qualquer um que não tenha a mesma cor de pele que os europeus - os irlandeses só foram ser considerados brancos no final do século 19) que a Europa não gostava (por racismo) viviam lá, oprimidos por uma única etnia grão-russa (não existe um "russo" em abstrato, o que haviam eram 53 etnias vivendo em conflito sob o mesmo "cativeiro" dominado pelos grão-russos).

**Polônia**

A Polônia era uma ditadura fascista controlada por Pilsudski (hoje retratado como "herói nacional da Polônia") com seus campos de concentração.

**Albânia**

A Albânia era um protetorado fascista italiano desde 1927 e ao final da Segunda Guerra Mundial tinha menos de 20% de alfabetização.

**Cuba**

Cuba era uma ditadura financiada pelos EUA, era o paraíso dos ricos com enormes cassinos, mas a pobreza e a desigualdade aumentavam cada vez mais. Tudo o que se via em Havana eram prostitutas, crianças pedindo esmola e pequenos furtos. Os afrodescendentes tinham sua religião perseguida e viviam sob condições de escravidão, alguns cubanos nativos trabalhavam sem ganhar um mínimo centavo por isso.

**Letônia**

Ulmanis chegou ao poder como resultado do golpe em 15 de maio de 1934 (aliás, o presidente nominal não ligou para o golpe), todos os partidos políticos foram dissolvidos, o Seim foi abolido, os governos locais foram parcialmente liquidados, o a constituição foi encerrada, muitas publicações impressas foram encerradas, reuniões e manifestações foram proibidas. Mais de 2.000 pessoas foram detidas, algumas das quais enviadas para um campo de concentração em Liepaja.

Na edição de junho da revista "Aizsargs", junto com muitos elogios dirigidos a K. Ulmanis, foi feita uma tentativa de substanciar a necessidade de "liderança" na Letônia com referência à experiência fascista italiana.

O artigo, que glorificava o regime de Benito Mussolini, era adornado com o título: “O líder do povo e o sentido do líderismo. Especialmente as pequenas nações precisam de líderes poderosos e corajosos.“ Depois do golpe de 15 de maio, Ulmanis frequentemente se referia a Mussolini e ao regime fascista, vendo nele um modelo a seguir nos assuntos de governo, na política social e na formação da imagem de um governante forte - “vadonis”. 

As inovações ideológicas foram realizadas na forma de um sistema de "câmeras" como órgãos de supervisão sobre associações de comerciantes, industriais, artesãos, produtores agrícolas e representantes de "profissões livres" (bem como a imagem restaurada dos sindicatos de trabalhadores), monopolização da produção e do comércio nas mãos de letões "confiáveis" (o próprio Ulmanis praticou o recrutamento de seu séquito não por qualquer habilidade, mas por devoção banal), nacionalização forçada, expulsão do mercado de representantes de nacionalidades "não titulares", até a proibição de exercerem determinadas atividades.

Na esfera econômica, as medidas da “letonização” afetaram gravemente os judeus letões - muitos deles foram privados de suas autorizações de trabalho como advogados e médicos, não podiam mais ser proprietários de serrarias e empresas de marcenaria, fábricas de têxteis foram requisitadas, etc. Também foi estritamente restringida a participação no volume de negócios de "não-arianos" com a Alemanha, eles até enviaram pessoas para monitorar a implementação dessas instruções.

O Ministro das Relações Exteriores da República da Lituânia, Vilhelms Munters, em uma conversa com o embaixador alemão em Riga Ulrich von Kotze em maio de 1939, enfatizou: "É esse antissemitismo silencioso que dá bons resultados, que o povo em geral entende e concorda."

A retórica do “líder”, porém, não andou de mãos dadas com represálias sangrentas - ele preferiu criar condições de vida insuportáveis ​​para os insatisfeitos, praticou prisões, encarceramento e expulsão de oposicionistas do país, monitorou sua atuação na emigração com os ajuda de agentes da Polícia Política, e privou-os da cidadania.

Como resultado do governo categoricamente habilidoso de Ulmanis e amigos, descobriu-se o seguinte:
>No diário de J. de Boss, o vice-chefe da missão francesa em Riga, pode-se encontrar as seguintes entradas: “Fala-se muito sobre a propaganda comunista em Latgale, onde uma tendência pronunciada para a autonomia foi observada desde um certo tempo” (23 de setembro de 1939); no subúrbio de Riga, em Moscou, "os trabalhadores exigem a chegada dos russos e uma mudança no sistema estatal" (29 de setembro); durante a demonstração do filme "Alexander Nevsky" nos cinemas de Riga, episódios com teutões quebrados foram acompanhados de aplausos estrondosos (23 de novembro). Em abril de 1940, uma onda de prisões de ativistas clandestinos de esquerda varreu a Letônia.

**Lituânia**

Smetona chegou ao poder com a ajuda de um golpe militar ocorrido em 17 de dezembro de 1926. Os organizadores do golpe foram o general Povilas Plehavičius, o general Kazis Ladiga, o coronel Vladas Skorupskis e vários oficiais influentes.

Eles frequentemente conversavam com os líderes da "União dos Tautínicos" Antanas Smetona e Augustin Voldemaras. Logo após o golpe, surgiram campos de concentração e prisões em massa de ativistas de esquerda e líderes sindicais progressistas no país. Mais de 350 membros do Partido Comunista Lituano, mais de 130 membros do Komsomol, cerca de 60 sindicalistas, 35 ativistas de organizações de esquerda foram presos.

Quatro líderes do Partido Comunista Lituano foram executados - Karolis Pozhela, Kazis Gedris, Juozas Greifenbergeris, Rapolas Charnas e Fayvusas Abramavičius e Pius Glovackas foram substituídos por prisão perpétua. Muitos de seus outros associados receberam longas sentenças de prisão que variam de 5 a 15 anos. Alguns foram enviados para o campo de concentração de Varney. Quase toda a oposição foi presa, mesmo os moderados (como Andrius Domashavicius e Pranas Dovidaitis).

Durante o reinado do regime de Smetona, uma sociedade imobiliária foi formada na Lituânia. Cerca de 2% dos habitantes da Lituânia eram muito ricos: altos oficiais, grandes políticos, banqueiros, grandes kulaks, a nova nobreza. Cerca de 15% dos habitantes da Lituânia pertenciam à classe média, enquanto o resto vivia abaixo da linha de pobreza.

Em 1935-1936, uma greve de camponeses estourou em Suwalkia: a polícia atirou em três pessoas, várias ficaram gravemente feridas (duas delas morreram no hospital). Em seguida, 6 organizadores da greve foram executados. Centenas de participantes no levante, incluindo alguns membros do Partido Comunista Lituano e membros do Komsomol, foram parar em prisões.

Smetona diferia de todos os líderes bálticos da época com sentimentos particularmente calorosos pela Alemanha. A Alemanha ofereceu à Lituânia seu protetorado, prometendo anexar as regiões de Vilna e Grodno da Polônia à Lituânia. Nem é preciso dizer que Kaunas se alegrou com a oportunidade de devolver a antiga capital da Lituânia, Vilnius, com a ajuda dos alemães, e até de anexar mais de 25 mil km² do território da atual Bielorrússia.

Portanto, Kaunas e Berlim condenaram a Polônia à morte à revelia quase 6 meses antes da assinatura do Pacto Molotov-Ribbentrop. O pagamento pela cumplicidade com os "verdadeiros arianos" neste ato deveria ter sido a transformação da Lituânia em uma espécie de "protetorado da Boêmia e da Morávia", onde o Gauleiter Reinhardt Heydrich declarou que "os tchecos não têm nada a ver aqui" (no entanto, o que é engraçado, eles receberam quase metade dos territórios atuais após a introdução das tropas soviéticas).

**Estônia**

Em janeiro de 1934, Konstantin Päts assumiu como primeiro-ministro na qualidade de ancião do estado. Temendo a vitória inevitável do partido Vaps nas próximas eleições e usando os poderes quase ditatoriais conferidos pela nova constituição, em 12 de março de 1934, juntamente com Johan Laidoner, que novamente liderou o exército estoniano, encenou um golpe de estado. O golpe militar estabeleceu um regime autoritário e declarou estado de emergência. Päts foi declarado o Protetor do Estado da Estônia.

Em outubro de 1934, a Assembleia Nacional foi dissolvida. O período que se iniciou, denominado "era do silêncio", foi caracterizado pelo colapso da democracia parlamentar, do regime autoritário e do fortalecimento do nacionalismo estoniano. O país era realmente governado por um triunvirato constituído pelo presidente (Konstantin Päts), o comandante-chefe do exército (Johan Laidoner) e o ministro do Interior (Kaarel Eenpalu). Em março de 1935, um sistema de partido único foi introduzido na Estônia.

Todos os partidos políticos foram proibidos, em vez deles foi criado o único partido no poder - a União da Pátria. O Parlamento não se reuniu de 1934 a 1938. Em preparação para o referendo, uma circular secreta do governo para as autoridades executivas locais ordenou:
>Aqueles que são conhecidos por poderem votar contra a Assembleia Nacional não devem ser autorizados a votar... devem ser entregues imediatamente às mãos da polícia.

Em 50 dos 80 círculos eleitorais, nenhuma eleição foi realizada. No entanto, para dar uma imagem mais democrática ao seu governo democrático, 4 anos após a ditadura aberta, Päts organizou a eleição de si mesmo como presidente. Päts iniciou pessoalmente uma campanha em grande escala para a estonização de nomes e sobrenomes. Como resultado da campanha de estonização, em 1940, quase 250.000 cidadãos do país tinham seu sobrenome estonizado. Mas a economia da Estônia sob Päts era diferente do resto dos estados bálticos.
>A economia do país, especialmente sua indústria, passou por um período de rápido crescimento. Na segunda metade da década de 1930, a produção industrial começou a crescer (até 14% ao ano). Em 1938, a participação da indústria na renda nacional havia chegado a 32%. A participação dos produtos industriais nas exportações da Estônia aumentou de 36% no final da década de 1920 para 44% no final da década de 1930. Novas empresas foram criadas, tecnologias de produção foram aprimoradas. Em 1939, a produção de xisto betuminoso atingiu 2 milhões de toneladas; foram produzidas 181 mil toneladas de óleo de xisto e 22,5 mil toneladas de gasolina de xisto. As indústrias têxtil, química e alimentar, metalmecânica, marcenaria, papeleira, turfa e mineração de fosfato foram de grande importância para a economia do país. A agricultura foi desenvolvida.

No entanto, nem tudo é tão rosa como pode parecer, na verdade, todo o setor privado da Estônia estava nas mãos de capital estrangeiro. E por causa do sucesso categórico de tal sistema em 1938, foram criados "campos para desistentes" - campos para trabalhos forçados de desempregados. Havia regime de prisão, jornada de trabalho de 12 horas e cânones. Nos “campos de desocupados” foram presos por um período de 6 meses a 3 anos todos “cambaleando sem trabalho e sem meios de subsistência”

**Hungria**

Hungria era completamente fascista aliada dos Nazistas na Segunda Guerra Mundial.

**Romênia**

Romênia era completamente fascista aliada dos Nazistas na Segunda Guerra Mundial.

**Bulgária**

Bulgária era completamente fascista aliada dos Nazistas na Segunda Guerra Mundial.

**Vietnã**

No Vietnã, se um francês cometesse alguma injustiça ou crime contra um vietnamita, ele não era nem ao menos julgado, e constantemente haviam estupros e assédios por parte dos franceses, um povo que nem ao menos podia construir uma escola sem os franceses destruírem. No Vietnã do Sul, os EUA proibiu o budismo.

**China**

A China antes da revolução era o país mais pobre do mundo, havia crises de fome constantemente, era um país bem grande, que vivia sob o terror de um imperador autocrático que perseguia quem demandasse por reformas, e isso só piorou depois da Guerra do Ópio. Os chineses tinham que comer cascos de árvore e vender seus filhos para pagarem os donos de terra britânicos. Por anos, haviam bairros em Hong Kong proibidos para nativos, somente ingleses podiam se assentar lá.

**Coreia Popular**

A Coreia sofreu uma das piores injustiças de todas, são muitas, os japoneses realmente não ligavam se os coreanos eram humanos. Tentaram abolir até o idioma coreano, matavam qualquer um que dissesse uma ÚNICA palavra em coreano. Vilas e aldeias eram constantemente saqueadas, mulheres eram estupradas, havia tortura e massacre generalizado, etc.

Qual democracia eles querem restaurar da China? Subordinada aos americanos, perda de soberania nacional e ataque a tudo de importante que os chineses conquistaram? Qual seria a democracia da Coreia? Se subordinar aos americanos e ao Japão que até hoje nunca pediu desculpas aos asiáticos e até hoje não abandonou a bandeira fascista imperial?
