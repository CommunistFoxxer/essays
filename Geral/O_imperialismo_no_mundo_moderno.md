# O imperialismo no mundo moderno
Há uma noção de que o capitalismo é o estado natural das coisas (não é, o capitalismo existe há apenas algumas centenas de anos) no mundo e que tudo sempre funcionou assim mais ou menos. É claro que qualquer exame completo da história mostra que isso não é verdade - todas as muitas nações do mundo tiveram suas próprias maneiras de se desenvolver, crescer e existir no mundo. No entanto, o imperialismo europeu se propôs a garantir "a maneira como o mundo funciona", se tornando a maneira que os estados mais poderosos da Europa querem, e o capitalismo será instalado com violência e força sob a mira de uma arma. Você pode ver esta expansão violenta e esmagamento forçado de qualquer autodeterminação nacional por potências americanas/europeias na maior parte do mundo: a América massacrando um continente para roubá-lo para si (e, em seguida, reivindicando as ilhas vizinhas - mesmo aquelas na metade do mundo), a Europa colonizando e saqueando a África, as várias empresas de comércio da Índia nas ilhas do Atlântico e do Pacífico, as Guerras do Ópio na China, os franceses transformando o Vietnã em suas próprias plantações de escravos, a Índia britânica, nativos australianos - quase não sobrou nenhum lugar do mundo.

A maior parte deste processo é bastante óbvio e permanece muito claro até cerca de 1918 a 1945, onde é obstruído por dois enormes conflitos (a Primeira Guerra Mundial foi um conflito de Capitalismo Financeiro vs Capitalismo Industrial - cujo sistema viria a dominar o globo, e a Segunda Guerra Mundial foi em grande parte uma guerra imperialista no que diz respeito ao Ocidente - não foi travada (pelo Ocidente) para impedir o genocídio, nem mesmo para honrar tratados - Alemanha e Japão estavam se intrometendo em território britânico e americano - esse era o problema). De acordo com a mitologia tácita do Ocidente, o imperialismo basicamente parou após a 2ª Guerra Mundial, e os europeus pararam de invadir outros países para roubar seus recursos e instalar regimes fantoches porque isso era flagrantemente ruim agora aos olhos do mundo que viu a destruição que Hitler forjou.

Claro, isso nunca impediu europeus e americanos de continuarem a pilhagem sem fim do planeta - apenas os incomodava fazer isso da maneira como antes (lembre-se, o capitalismo é o sistema da coerção). Um dos benefícios do capitalismo é que todas as antigas reivindicações de terras e propriedades (especialmente aquelas sobre os meios de produção) permitem que você devolva à governança de uma nação ao seu povo (por exemplo, EUA) sem realmente devolver às partes importantes do país (por ex, Congo) - aqueles podem permanecer sob propriedade europeia ou americana. Ou fortemente conectado a contratos de negócios europeus/americanos que favorecem fortemente a realização de riqueza no lado euro/americano da fronteira. Portanto, a fábrica continua produzindo, mas os produtos vão para a América ou Europa, junto com grande parte dos lucros. Mas por que qualquer governo concordaria com um acordo como esse, tão prejudicial para seu próprio povo? O que o país ganha em troca? Bem, em grande parte, o pagamento da dívida - muitas vezes dívidas duvidosas, colocadas em prática pelas mesmas potências europeias e americanas durante suas ocupações. Ou dívidas francamente maliciosas e enganosas que foram feitas sem interesse de concessão de crédito, mas por pura usura (isso costumava ser um crime uma vez, e condenado pela Igreja Católica). E como os juros compostos são uma força tão poderosa, essas dívidas continuam a crescer mais rápido do que a capacidade de pagamento do país.

Em muitos casos, os países do terceiro mundo pagaram suas principais dívidas muitas vezes, mas continuam a cair cada vez mais fundo porque os juros crescem mais rápido do que a economia permite. E porque o dinheiro devido é a instituições americanas/europeias, eles conseguem definir os termos de tal arranjo, e ao invés de desenvolver a economia de uma nação a custo, eles optam por despojá-la de seus recursos, até devorando a capacidade econômica de desenvolver a economia. Então, por que as nações não resistem ativamente a esse tipo de coisa? Bem, as tentativas pacíficas de fazê-lo foram facilmente cooptadas, suprimidas e, quando não o foram, foram totalmente destruídas. Isto é o que significa grande parte da intervenção americana (e europeia) ao redor do globo - eliminar violentamente aqueles que desafiariam a classe capitalista. Claro, a resistência genuína a esta opressão emerge dela - estes foram/são os movimentos comunistas dos séculos 20 e 21 ao redor do globo - nações que se recusam a participar neste jogo desonesto e optam por um modelo alternativo de desenvolvimento e para o seu próprio povo. Vietnã e Cuba não se tornaram comunistas porque o alcance soviético foi capaz de se estender de alguma forma ao redor do mundo e eles tiveram que fazer o que Moscou lhes disse. O Vietnã se tornou comunista porque eles descobriram isso e não queriam ser escravos da França até o fim dos tempos. Cuba tornou-se comunista porque a América instalou um ditador brutal sobre eles para extrair a pouca riqueza que eles tinham para enriquecer a riqueza que os americanos investiram na ilha. O Irã era uma democracia próspera na década de 1950, por que a América, "o campeão da democracia" os derrubaria para instalar um ditador? Porque eles ousaram desafiar os interesses comerciais americanos. É para isso que servem o enorme Exército militar americano - é por isso que o orçamento militar dos EUA nunca parou de aumentar quando os soviéticos foram embora.

Estes são os seus milhares e milhares de bombardeios americanos, assassinatos, golpes, intervenções, etc. em todo o mundo - "lutar contra o comunismo" não é uma luta contra os opressores soviéticos do mal - "lutar contra o comunismo" é sobre esmagar qualquer nação ou grupo que pensa não devem ser forçados a jogar pelas regras americanas/europeias que instalaram-se à força no seu país. As mais de 900 bases militares americanas em todo o mundo não estão lá "para ajudar na proteção contra os malfeitores." Tampouco existem bases para atuar como "polícia mundial", mantendo os criminosos à distância. As mais de 900 bases são as armas, muito literais e muito reais, apontadas para as cabeças de todos os países do mundo, armadas e prontas para disparar no momento em que qualquer elemento ou instituição significativa nesses países fique significativamente fora de linha com os interesses comerciais americanos e ameaçando interromper o processo de extração em curso e interminável - até que toda a riqueza de uma nação seja extraída dele, para o duvidoso benefício dos consumidores da classe média americana e para o benefício significativo de um punhado de investidores já imensamente ricos e proprietários. Há uma razão muito real pela qual os únicos lugares a se desenvolver a partir do terceiro mundo na era do imperialismo são os estados clientes americanos na Ásia, funcionando como uma extensão do Império, ou aqueles que são, ou pelo menos foram, comunistas, e lutaram ativamente para se libertarem do jugo europeu/americano.

É por isso que você vê comunistas "radicais" apoiando ativamente regimes como a Coreia do Norte ou Assad na Síria - não porque Assad seja uma figura comunista heróica ou porque a Coreia do Norte é o modelo para o comunismo - mas porque essas nações têm o direito de dizer não à determinação empresarial americana de suas terras e de seu povo - um direito de não ser depenado até o osso de seus recursos e seu trabalho para fornecer mais lixo e aterro para o consumo americano, de modo que alguns bilionários que fizeram todo o processo acontecer possam obter um pouco mais de lucro.

Só ver o caso dos camponeses na Inglaterra pré-revolução industrial. A produção não servia para ser levada ao mercado, mas para consumo próprio. O mercado só vai se desenvolver quando o governo toma as terras e cede a burguesia para o desenvolvimento das fábricas, isso cria uma mão de obra, que passa a trabalhar não pelo excedente, mas pelo dinheiro. O excedente vai ser lançado ao público, sendo precificado e comparado aos outros produtos — isso é o mercado. E, por fim, vai ser trocado pelo dinheiro ganho do trabalhador com as fábricas. É fácil entender.

Pela ótica da Teoria dos Sistemas Sociais de Niklas Luhmann, é possível entender o capitalismo como um acoplamento dominante do subsistema econômico em relação a todos os demais subsistemas sociais. Nesse acoplamento, o capitalismo mais parasita é o que controla os subsistemas. Some a isso o fato de que no capitalismo o subsistema econômico é regido por um programa de mais-valor: toda decisão deve distinguir entre o que acumula (capital) e o gasto (trabalho). O regime de acumulação significa maximizar essa distinção por um algoritmo maxmin.

O mercado para se desenvolver precisa expandir a sua produção e escoar mercadorias cada vez mais. O imperialismo serviu para isso: criar colônias de consumo, que serviram para o escoamento do excedente produzido e expansão das empresas da metrópole.

A dialética do colonialismo: enquanto a França comemorava a vitória soviética sobre o nazismo, o Estado francês produziu mais um [massacre colonial](https://en.wikipedia.org/wiki/Sétif_and_Guelma_massacre) na Argélia. O Ocidente, enquanto construção política e histórica, é um câncer que precisa morrer para o bem de toda humanidade.

Repitam comigo: o colonialismo não é uma relação de dominação social entre raças. O colonialismo é uma relação social de dominação cujo o resultado é a raça. A raça é um resultado que aparece como pressuposto através da biologização de uma relação social.

>O que eu gosto mais é que toda crise é cheia de oportunidades.<br/>
>– <cite>Jorge Paulo Lemann</cite>

>Eu dirigi Cuba do sexto andar da embaixada dos Estados Unidos. O trabalho dos cubanos era cultivar açúcar e calar a boca.<br/>
>– <cite>Earl Smith</cite>

>Eu me pergunto se as políticas da década de 1980 de atacar a inflação pressionando a economia e os gastos públicos foram uma medida para atacar os trabalhadores. Aumentar o desemprego era uma forma muito desejável de reduzir a força da classe trabalhadora. O que foi arquitetado - em termos marxistas - foi uma crise do capitalismo que recriou um exército industrial de reserva de trabalho e permitiu aos capitalistas obterem altos lucros desde então.<br/>
>– <cite>Alan Budd</cite>

>Mesmo com a remoção do presidente do cargo do FBI ou CIA, não podem resolver o problema. O capitalismo americano, baseado na exploração dos pobres, com sua motivação fundamental na ganância pessoal, simplesmente não pode sobreviver sem a força - sem uma polícia secreta. A discussão é com o capitalismo e é o capitalismo que deve ser combatido, com sua CIA, FBI e outras agências de segurança entendidas como manifestações lógicas e necessárias da determinação de uma classe dominante em reter poder e privilégio.<br/>
>– <cite>Phillip Agee</cite>

>A CIA está claramente do lado errado, ou seja, do lado capitalista. Eu aprovo as atividades da KGB, as atividades comunistas em geral. Entre as atividades exageradas que a CIA inicia e as atividades mais modestas da KGB, não há absolutamente nenhuma comparação.<br/>
>– <cite>Phillip Agee</cite>

>A campanha de Nixon em 1968, e a Casa Branca de Nixon depois disso, tiveram dois inimigos: a esquerda antiguerra e os negros. Você entende o que estou dizendo? Sabíamos que não poderíamos tornar ilegal ser contra a guerra ou contra os negros, mas, ao fazer com que o público associasse os hippies à maconha e os negros à heroína e, em seguida, criminalizando ambos fortemente, poderíamos perturbar essas comunidades. Poderíamos prender seus líderes, invadir suas casas, interromper suas reuniões e difamá-los noite após noite no noticiário noturno. Sabíamos que estávamos mentindo sobre as drogas? Claro que sim.<br/>
>– <cite>John Ehrlichman</cite>

>Interferimos nas eleições de outros países no interesse da democracia.<br/>
>– <cite>James Woolsey</cite>

>Eu estava no East End de Londres (um bairro da classe trabalhadora) ontem e participei de uma reunião de desempregados. Escutei os discursos selvagens, que eram apenas um grito de 'pão! pão!' e no caminho de volta para casa refleti sobre a cena e fiquei mais do que nunca convencido da importância do imperialismo. Minha querida ideia é uma solução para o problema social, ou seja, para salvar os 40 milhões de habitantes do Reino Unido de uma guerra civil sangrenta, nós, estadistas coloniais, devemos adquirir novas terras para liquidar o excedente populacional, para fornecer novos mercados para os bens produzidos nas fábricas e minas. O Império, como eu sempre disse, é uma questão de pão com manteiga. Se você quer evitar a guerra civil, você deve se tornar imperialista.<br/>
>– <cite>Cecil Rhodes</cite>

>A ajuda externa é um método pelo qual os Estados Unidos mantêm uma posição de influência e controle em todo o mundo, e sustentam muitos países que definitivamente entrariam em colapso ou passariam para o bloco comunista.<br/>
>– <cite>John F. Kennedy</cite>

>Tenho a honra de informar que o assessor jurídico da United Fruit Company aqui em Bogotá afirmou ontem que o número total de grevistas mortos pelas autoridades militares colombianas durante os recentes distúrbios atingiu entre quinhentos e seiscentos; enquanto o número de soldados mortos era um.
>
>[...]
>
>Tenho a honra de informar que o representante da United Fruit Company em Bogotá me disse ontem que o número total de grevistas mortos pelos militares colombianos ultrapassou 1000.<br/>
>– <cite>Embaixador americano em Bogotá</cite>

>Posso ser influenciado pelo que me parece justiça e bom senso; mas a guerra de classes me encontrará ao lado da burguesia educada.<br/>
>– <cite>John Maynard Keynes</cite>

>Uma vitória do comunismo seria muito mais perigosa para os Estados Unidos do que uma vitória do fascismo.<br/>
>– <cite>Robert Taft</cite>

>O que nós na América chamamos de terroristas são, na verdade, grupos de pessoas que rejeitam o sistema internacional.<br/>
>– <cite>Henry Kissinger</cite>

>É um ato de insanidade e humilhação nacional ter uma lei que proíbe o presidente de ordenar assassinato.<br/>
>– <cite>Henry Kissinger</cite>

>Os militares são apenas animais burros e estúpidos para serem usados como peões na política externa.<br/>
>– <cite>Henry Kissinger</cite>

>Eu considero uma mulher que traz um filho a cada dois anos mais lucrativa do que o padrinho da fazenda. [...] O que ela produz é um acréscimo de capital.<br/>
>– <cite>Thomas Jefferson</cite>

>Eu não entendo o escrúpulo sobre o uso de gás. Sou fortemente a favor do uso de gás venenoso contra tribos incivilizadas.<br/>
>– <cite>Winston Churchill</cite>

>Eu não admito que um grande mal foi feito aos índios vermelhos da América, ou ao povo negro da Austrália, pelo fato de que uma raça mais forte, uma raça de grau superior, entrou e tomou o seu lugar.<br/>
>– <cite>Winston Churchill</cite>

>[Os nativos americanos] não tinham nenhum direito à terra e não havia razão para ninguém conceder-lhes direitos que eles não conceberam e não estavam usando. Pelo que eles estavam lutando, se eles se opuseram homens brancos neste continente? Por seu desejo de continuar uma existência primitiva, seu "direito" de manter parte da terra intocada, sem uso e nem mesmo como propriedade, basta manter todos fora para que você viva praticamente como um animal, ou talvez algumas cavernas acima dele. Qualquer pessoa branca que trouxesse o elemento da civilização tinha o direito de dominar este continente.<br/>
>– <cite>Ayn Rand</cite>

>Dê-me o controle do suprimento de dinheiro de uma nação, e não me importo com quem faz suas leis.<br/>
>– <cite>Amschel Rothschild</cite>

>Defendemos a manutenção da propriedade privada. Devemos proteger a livre iniciativa como o mais expediente, ou melhor, a única ordem econômica possível.<br/>
>– <cite>Adolf Hitler</cite>

>Com ou sem voz, as pessoas sempre podem ser levadas à licitação dos líderes. Isso é fácil. Tudo o que você precisa fazer é dizer a eles que estão sendo atacados e denunciar os pacificadores pela falta de patriotismo e por exporem o país ao perigo. Funciona da mesma forma em qualquer país.<br/>
>– <cite>Hermann Goering</cite>

>Se vemos que a Alemanha está ganhando, devemos ajudar a Rússia, e se a Rússia está ganhando, devemos ajudar a Alemanha e, dessa forma, deixá-los matar o máximo possível.<br/>
>– <cite>Harry S. Truman</cite>

>Saberemos que nosso programa de desinformação estará completo quando tudo o que o público americano acreditar for falso.<br/>
>– ex-diretor da CIA <cite>William Casey</cite>

>É muito bom que as pessoas do país não entendam nosso sistema bancário e monetário, pois se entendessem, acredito que haveria uma revolução antes de amanhã de manhã.<br/>
>– <cite>Henry Ford</cite>

>Temos cerca de 50% da riqueza mundial, mas apenas 6,3% de sua população. Nessa situação, não podemos deixar de ser objeto de inveja e ressentimento. Nossa verdadeira tarefa no período vindouro é conceber um padrão de relacionamento que nos permita manter essa posição de disparidade. Para fazer isso, teremos que dispensar todo sentimentalismo e devaneios; e nossa atenção terá de ser concentrada em todos os lugares em nossos objetivos nacionais imediatos. Devemos deixar de falar em objetivos vagos e irreais como os direitos humanos, a elevação da qualidade de vida e a democratização. Não está longe o dia em que teremos que lidar com conceitos diretos de poder. Quanto menos formos prejudicados por slogans idealistas, melhor.<br/>
>– <cite>George F. Kennan</cite>

>Se e quando nós, como populistas e libertários, abolirmos o estado de bem-estar em todos os seus aspectos, e os direitos de propriedade e o livre-mercado triunfarem mais uma vez, muitos indivíduos e grupos previsivelmente não gostarão do resultado final. Nesse caso, os grupos étnicos e outros que podem estar concentrados em ocupações de baixa renda ou de menor prestígio, guiados por seus mentores socialistas, irão, previsivelmente, levantar o grito de que o capitalismo de livre mercado é mau e "discriminatório" e que, portanto, o coletivismo é necessário para restabelecer o equilíbrio. Nesse caso, o argumento da inteligência se tornará útil para defender a economia de mercado e a sociedade livre de ataques ignorantes ou egoístas. Resumidamente; a ciência racialista propriamente não é um ato de agressão ou cobertura para a opressão de um grupo sobre outro, mas, ao contrário, uma operação em defesa da propriedade privada contra agressões de agressores.<br/>
>– <cite>Murray Rothbard</cite>

>Talvez seja desnecessário dizer que essa doutrina [da liberdade individual] deve ser aplicada apenas aos seres humanos na maturidade de suas faculdades. Não estamos falando de crianças ou de jovens abaixo da idade que a lei pode definir como masculinidade ou feminilidade. Aqueles que ainda estão em um estado que requer os cuidados de outras pessoas, devem ser protegidos contra suas próprias ações, bem como contra danos externos. Pela mesma razão, podemos deixar de considerar aqueles estados atrasados ​​da sociedade em que a própria raça pode ser considerada como estando fora da idade. As dificuldades iniciais no caminho do progresso espontâneo são tão grandes que raramente há escolha de meios para superá-las; e um governante cheio de espírito de aperfeiçoamento é garantido no uso de quaisquer expedientes que atinjam um fim, talvez de outra forma inatingível. O despotismo é uma forma legítima de governo no trato com os bárbaros, desde que o fim seja o seu aperfeiçoamento e os meios justificados para realmente realizar esse fim. A liberdade, como princípio, não se aplica a nenhum estado de coisas anterior à época em que a humanidade se tornou capaz de ser aprimorada por uma discussão livre e igualitária. Até então, não há nada para eles a não ser obediência implícita a um Akbar ou a Carlos Magno, se tiverem a sorte de encontrar um. Mas tão logo a humanidade tenha alcançado a capacidade de ser guiada para seu próprio aperfeiçoamento pela convicção ou persuasão (um período há muito alcançado em todas as nações de que nos interessamos aqui), compulsão, seja na forma direta ou na forma de dores e as penalidades pelo seu descumprimento, já não é admissível como meio para o seu próprio bem, e apenas justificável para a segurança de outrem.<br/>
>– <cite>John Stuart Mill</cite>

>Os fanáticos pobres de espírito deram a este país e ao mundo liberdade de expressão, liberdade de pensamento e ação e liberdade religiosa.<br/>
>– <cite>William Hearst</cite>

>Você fornece as fotos e eu fornecerei a guerra.<br/>
>– <cite>William Hearst</cite>

>Eu não descontaria 1917 e 1937, duas vezes a elite foi destruída, mais a guerra, mais as políticas pró-trabalhadores regulares no pós-guerra - em nosso país há uma grande habilidade em nos suprimir (a elite), isso resultou na Rússia como um país de lixo genético.<br/>
>– <cite>Ksenia Sobchak</cite>

Também há uma [entrevista](https://dgibbs.faculty.arizona.edu/brzezinski_interview) interessante de Brzezinski.
