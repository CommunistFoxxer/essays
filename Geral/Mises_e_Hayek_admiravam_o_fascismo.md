# Mises e Hayek admiravam o fascismo
Mises era um elitista com tendências fascistas, apoiava o colonialismo, era simpatizante do regime apartheid da África do Sul, grande contribuidor do regime austrofascista (conselheiro de Dolfuss), simpatizava com o fascismo europeu e quando o fascismo entrou em decadência, ele disse que o fascismo era coisa de socialista. Tenha apenas em mente, que o liberalismo e o fascismo não são incompatíveis. Fun fact: Como resultado das políticas econômicas de Mises, a Aústria entrou em crise econômica e social, o que facilitou sua anexação pela Alemanha Nazista. Algumas frases de Mises:
>A constatação de que (os fascistas) ainda não puderam desvencilhar-se de modo tão cabal como os bolcheviques, russos, de qualquer consideração por noções e ideias liberais e por tradicionais preceitos éticos, deve ser atribuída, tão somente, ao fato de que os fascistas atuam em países nos quais a herança intelectual e moral de milhares de anos de civilização não pode ser destruída em um piscar de olhos e não entre povos bárbaros de ambos os lados dos Urais, cuja relação com a civilização nunca foi mais do que a de habitantes predadores da floresta e do deserto, acostumados a se envolverem, de tempos em tempos, em pilhagem de terras civilizadas, na caça à sua presa.

E ainda:
>Não se pode negar que o fascismo e movimentos semelhantes, visando ao estabelecimento de ditaduras, estejam cheios das melhores intenções e que sua intervenção, até o momento, salvou a civilização europeia. O mérito que, por isso, o fascismo obteve para si estará inscrito na história.

Já Hayek mandava seus alunos (chicago boys) serem os conselheiros econômicos do regime fascista de Pinochet, e eles sabiam que haviam mortes, torturas e perseguições acontecendo nas ruas para impor suas políticas econômicas (chamadas de neoliberais). Hayek disse sobre o fascismo:
>Prefiro um ditador liberal do que uma democracia sem liberalismo

Outro também:
>Se eu pudesse viver sob um regime fascista, eu não tenho nenhuma dúvida que eu preferiria viver em um aquele dirigo pelos ingleses ou americanos do que qualquer outra

Há referências científicas sobre a relação Hayek-Pinochet. Algumas menos críticas, mas se você fuçar nos Journals de história do pensamento científico, você acha coisas do tipo dele [chilicando](https://econstor.eu/bitstream/10419/149724/1/chope-wp-2014-12.pdf) porque publicaram uma charge comparando Jaruzelski ("ditador" polonês) ao Pinochet.

No melhor dos cenários para o velho bigode, a posição dele é que era um [absurdo](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.673.1002&rep=rep1&type=pdf) criticar o Pinochet sem no mesmo parágrafo ter uma crítica mais efusiva ao comunismo e a URSS. Nós sabemos o que é isso.

Há um [artigo](http://eprints.lse.ac.uk/63318/1/__lse.ac.uk_storage_LIBRARY_Secondary_libfile_shared_repository_Content_Caldwell,%20B_Hayek%20and%20Chile_Cladwell_Hayek%20and%20Chile_2015.pdf) que faz um apanhado bem legal sobre essa questão. Além disso, Hayek tinha uma visão mais instrumental da democracia, o que o levou a defender coisas como "ditadura transicional", como [Burczak critica](https://www.tandfonline.com/doi/abs/10.1080/09538259.2014.932069?tab=permissions&scroll=top&).
