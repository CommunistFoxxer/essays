# As sanções
As sanções americanas estão matando as pessoas e ninguém está falando disso! O que são sanções? São um ato de guerra econômica usada pelo governo americano para impedir governos e empresas de mandarem recursos para países que os EUA considera uma ameaça ou uma ditadura. Sanções são frequentemente chamadas de embargos ou bloqueios.

**Venezuela**

80,000 venezuelanos sofrem de HIV, 16,000 tem câncer, e 4 milhões sofrem de diabetes e hipertensão. Apesar disso, a Venezuela está proibida de receber ajuda médica e remédios de companhias farmacêuticas por causa das restrições das sanções americanas.

**Coreia do Norte**

Ela está localizada em uma área onde o clima não é apropriado para a maioria dos tipos de agricultura, então o país precisa de ajuda externa para que seus 25 milhões de habitantes tenham o que comer. As sanções americanas bloqueiam os recursos de chegarem à Coreia do Norte. Isso deixa a Coreia sem nenhuma escolha a não ser produzir sua própria pasta de dente, comida, roupas, materiais escolares, etc. E custa bastante dinheiro para fazer isso ser possível!

O Conselho de Segurança das Nações Unidas, a proposta dos Estados Unidos e com o apoio da China (depois, com sinais de apoios da China para retirada das sanções) e da Rússia, aprovou sanções contra a Coreia do Norte durante muitos anos.

Em especial, vamos analisar as de março de 2016, não incluindo as sanções anteriores ou após essa data. Há que recordar que as sanções aplicam-se em consequência do programa espacial. Se bem que na imprensa ocidental pode ler-se continuamente "teste de míssil". Todos os países têm reconhecido que a Coreia do Norte colocou um satélite em órbita. 

Inclusive o Kwangmyongsong-4, como os outros satélites, foram registrados de acordo com a "Convenção Relativa ao Registro de Objetos Lançados no Espaço Cósmico". Mas de acordo com os Estados Unidos, a colocação em órbita de um satélite se faz com um foguete que, hipoteticamente, também poderá ser utilizado para transportar ogivas nucleares. Por conseguinte, a Coreia do Norte não testou um míssil nessa época (coisa que sim fazem a Rússia, a Índia, Israel ou Estados Unidos), mas na verdade desenvolve uma tecnologia pacífica que, hipoteticamente, poderia ter uma dupla finalidade: Civil e militar. Só no caso da Coreia do Norte se faz esta interpretação. Nenhum outro país do mundo tem limitado o seu programa espacial por possíveis duplos usos dos foguetes. Isto viola, claramente, os tratados internacionais que permitem em igualdade a livre exploração do espaço por todo país.

A nova resolução, numerada como 2270, restringe o comércio externo norte-coreano. Proíbe todas as principais exportações norte-coreanas: Carvão, ferro, minério de ferro, ouro, titânio, vanádio e terras raras, A China alterou ligeiramente o texto original dessas sanções, mas parece que não, justamente, para proteger a Coreia. No caso do carvão (Coreia é o principal fornecedor estrangeiro de carvão para a China). As sanções proíbem o comércio "em caso de suspeita de que derivam os ganhos para o programa de armamento". O texto é tão ambíguo que permite uma arbitrariedade completa: China decide, em função das suas necessidades de abastecimento e da "obediência" norte-coreana às suas políticas, se a Coreia está "derivando ganhos" ou não pouco importa.

Na verdade, em uma economia como a norte-coreana, onde todas as empresas são de propriedade estatal sob uma economia planificada, qualquer fundo obtido por qualquer meio, de forma direta ou indireta, está suportando os custos de um investimento em outro setor da economia, incluindo o exército. Após esta arbitrariedade, o que se esconde é um bloqueio comercial completo, com possíveis exceções no caso da Coreia se submeter à injustiça de ser a única nação sem direito a um programa espacial. Há uma segunda "curiosidade": As sanções proíbem completamente as exportações coreanas de terras raras. É curioso, porque o maior exportador do mundo de terras raras é a China e o seu principal concorrente, por depósitos confirmados, é a Coreia do Norte. A China apaga com um simples gesto a um concorrente comercial, sob a desculpa do programa espacial.

A Resolução 2270 obriga a todos os países a inspecionar os navios que tenham como destino ou origem a Coreia do Norte, atrasando assim o comércio norte-coreano. Isso para qualquer tipo de produto, roupa, comida, remédio etc...

Isso gera custos completamente desnecessários, fazendo com que os países evitem negociações comerciais com a Coreia do Norte. Essa resolução impactou inclusive a importação de grãos que o país fazia do Camboja. Proíbe também que barcos ou aviões estrangeiros sejam utilizados pela Coreia do Norte e proíbe que a Coreia do Norte compre petróleo ou derivados do petróleo, à exceção de "fins humanitários". Novamente, uma proibição ambígua condicionada a submissão política. Não se precisa do petróleo para que as ambulâncias possam circular? É "Humanitário", um comboio transportar passageiros ou beneficia o programa de armamento? E se transportar medicamentos? É também proibida a atividade dos bancos e instituições financeiras norte-coreanas no estrangeiro.

E esse é o motivo pelo qual você não pode enviar 5 centavos para um norte-coreano.

**Cuba**

Você já imaginou por que Cuba tem construções antigas com tinta descascando da parede? Porque toda empresa que envia suporte material à Cuba é multada pelos EUA. Cuba também é proibida de comprar remédios que tratam a leucemia.

**Iraque**

Na década 2000, os EUA não só invadiu o país por causa de armas de destruição em massa que nunca existiram, como matou milhões de inocentes iraquianos. As sanções no Iraque causavam a morte de uma criança a cada 12 minutos.

**O que você pode fazer?**

Os países mencionados aqui não são fracassos econômicos: Eles são atacados economicamente! Uniões trabalhistas em Sacramento, São Francisco, Cleveland e Minesotta se juntaram em um movimento e estão pedindo pelo fim das sanções! Seu trabalho é primeiramente espalhar essa mensagem amplamente. E quando as pessoas falarem sobre a pobreza em países atacados, corrija elas!

Para ajudar financeiramente, o grupo ANTICONQUISTA coloca seu dinheiro no bolso de grupos revolucionários latino-americanos. Por favor, apoie eles: https://www.patreon.com/anticonquista.

**Fun fact**: As sanções raramente prejudicam os líderes que são rotulados como ameaça ou ditador. As sanções prejudicam principalmente os civis, profissionais de saúde, deficientes, mulheres grávidas e filhos.

**Fun fact**: Em 1976. o Secretário de Estado americano Henry Kissinger sugeriu o fim das sanções contra Cuba e a criação relações diplomáticas somente se Cuba parasse de apoiar forças anticoloniais no sudeste da África. Cuba recusou e foi punida por isso desde então!

**Fun fact**: Em 1996, Bill Clinton colaborou com o senador norte-americano Jesse Helms para tornar efetiva a Lei Helms-Burton. A lei reforçou as sanções contra Cuba. Jesse Helms é o mesmo senador que apoiou a segregação durante o movimento pelos direitos civis!

Irã e Arábia Saudita entre os maiores produtores de petróleo de alta qualidade. Ambos são países capitalistas. Por que Arábia Saudita é um país rico e o Irã é um país pobre? Embargo. A Inglaterra, que na época era a maior potência do mundo, não aguentou nem 1 ano de bloqueio continental.
