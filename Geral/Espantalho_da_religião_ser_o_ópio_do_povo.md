# Espantalho da religião ser o ópio do povo
"A religião é o ópio do povo" não é uma frase exclusiva de Marx, e ela vem desde Kant, Feuerbach e Hegel. É uma expressão em latim. Aqui a frase completa de Marx:
>O homem, que na realidade fantástica do céu, onde procurava um super-homem, encontrou apenas o reflexo de si mesmo, já não será tentado a encontrar apenas a aparência de si, o inumano, lá onde procura e tem de procurar sua autêntica realidade.
>
>Este é o fundamento da crítica irreligiosa: o homem faz a religião, a religião não faz o homem. E a religião é de fato a autoconsciência e o autossentimento do homem, que ou ainda não conquistou a si mesmo ou já se perdeu novamente. Mas o homem não é um ser abstrato, acocorado fora do mundo. O homem é o mundo do homem, o Estado, a sociedade. Esse Estado e essa sociedade produzem a religião, uma consciência invertida do mundo, porque eles são um mundo invertido.
>
>A miséria religiosa constitui ao mesmo tempo a expressão da miséria real e o protesto contra a miséria real. A religião é o suspiro da criatura oprimida, o ânimo de um mundo sem coração, assim como o espírito de estados de coisas embrutecidos. Ela é o ópio do povo.
>
>A supressão da religião como felicidade ilusória do povo é a exigência da sua felicidade real. A exigência de que abandonem as ilusões acerca de uma condição é a exigência de que abandonem uma condição que necessita de ilusões.
>
>A crítica da religião é, pois, em germe, a crítica do vale de lágrimas, cuja auréola é a religião.
>
>A crítica arrancou as flores imaginárias dos grilhões, não para que o homem suporte grilhões desprovidos de fantasias ou consolo, mas para que se desvencilhe deles e a flor viva desabroche. 
>
>A crítica da religião desengana o homem a fim de que ele pense, aja, configure a sua realidade como um homem desenganado, que chegou à razão, a fim de que ele gire em torno de si mesmo, em torno de seu verdadeiro sol. A religião é apenas o sol ilusório que gira em volta do homem enquanto ele não gira em torno de si mesmo. Portanto, a tarefa da história, depois de desaparecido o além da verdade, é estabelecer a verdade do aquém.
>
>A tarefa imediata da filosofia, que esta a serviço da história, é, depois de desmascarada a forma sagrada da autoalienação humana, desmascarar a autoalienação nas suas formas não sagradas. A crítica do céu transforma-se, assim, na crítica da terra, a crítica da religião, na crítica do direito, a crítica da teologia, na crítica da política.<br/>
>[...]
>O sofrimento religioso é ao mesmo tempo a expressão do sofrimento real e um protesto contra o sofrimento real. A religião é o suspiro da criatura oprimida, o coração de um mundo sem coração e a alma de condições desalmadas. É o ópio do povo.

É fácil atacar tudo fazendo [espantalho](https://pt.wikipedia.org/wiki/Fal%C3%A1cia_do_espantalho).
