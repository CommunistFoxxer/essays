# Contra-história do liberalismo
Alguns dos principais pensadores liberais escravagistas:
- John Locke (filósofo)
- John Calhoun (vice-presidente dos EUA por 3 vezes, e grande estadista)
- John Dalberg Acton (historiador e conselheiro político britânico)
- William Gladstone (liberal britânico, deputado do parlamento, líder do partido liberal britânico e primeiro-ministro britânico por 4 vezes)
- barão Malouet (administrador colonial francês, fazendeiro e político)
- clube do Hotel Massiac (grupo político da revolução francesa escravagista-colonial contra-revolucionário)
- Francis Lieber (filósofo alemão, jurista e político)
- George Washington (líder político e militar) 
- Francis Hutcheson (professor de Adam Smith e filósofo)
- François Boucher (pintor francês, idealizador do "Rococó")
- Benjamin Franklin, que era fã de William Blackstone (um dos fundadores dos EUA)
- Edmund Burke (político britânico e influenciador de filósofos posteriores, tais como Immanuel Kant e Denis Diderot)
- Thomas Hutchison (legalista e político americano)
- William Blackstone (jurista, político, causador da guerra dos 7 anos e maior figura central na justificação da expansão britânica)

William Stanley Jevons foi um dos três grandes nomes da "Revolução Marginalista" ao lado de Menger e Walras. Ele é considerado um dos fundadores da Economia Neoclássica, escola que estrutura o pensamento econômico liberal até os dias de hoje. Jevons, um dos pais do liberalismo econômico moderno, pensava o seguinte sobre os negros:
>Um homem de raça inferior, um negro, por exemplo, aprecia menos as posses e detesta mais o trabalho; seus esforços, portanto, param, logo, um pobre selvagem se contentaria em recolher os frutos quase gratuitos da natureza, se fossem suficientes para dar-lhes sustento; é apenas a necessidade física que o leva ao esforço. O homem rico na sociedade moderna está aparentemente suprido com tudo que ele desejar e, no entanto, freqüentemente trabalha por mais sem cessar.

Fora quase todos os filósofos iluministas com exceção de Rousseau (John Locke, Adam Smith, Hegel, Immanuel Kant, David Hume, barão de Montesquieu, etc). Aprofundando mais, John Locke era escravagista, Adam Smith era colonialista, a maior parte dos liberais da época também apoiaram o Apartheid e o Jim Crow (inclusive, Hitler [se inspirou](https://www.newyorker.com/magazine/2018/04/30/how-american-racism-influenced-hitler) no Jim Crow, como relata no Mein Kampf, e disse que o que os americanos faziam era muito cruel até mesmo para ele, Hitler também dizia que faria do Volga o seu Mississipi (onde os índios americanos foram massacrados)). Winston Churchill cometeu genocídio de fome na Índia e atribuiu as mortes aos próprios indianos, além de ter copiado o discurso de Goebbels em seu discurso de 1946 em Missouri (Goebbels quem inventou o termo "cortina de aço"). Margaret Thatcher era a favor do Apartheid e Ronald Reagan reprimiu diversos movimentos anti-segregacionistas.

John Stuart Mill (criador do utilitarismo) por exemplo, vai dizer que Calhoun e Acton não eram liberais porque defendiam posições racistas e colonialistas. Mas se você for analisar bem, eles têm posições econômicas liberais. Liberalismo não é questão filosófica, é uma questão de ideal econômico. Ser ou não escravagista não te faz um não-liberal. Inclusive, consigo citar 2 liberais antigos que são contra o escravagismo: Andrew Fletcher (retratado em "O Patriota") e James Burgh.

Tanto que o pai do liberalismo é escravagista. Isso faz dele um não-liberal? Aliás, John Millar, Samuel Johnson, Foner e Granville Sharp são abolicionistas que basicamente foram bem importantes para a história.

Granville Sharp citou a luta de classes mais de 50 anos antes de Marx, e dizia que preferia viver na comunidade despótica (como eles chamavam na época, das tais liberdades consideradas como aberração, tal como centralização, direitos dos trabalhadores, classes iguais e fim da escravidão) do que na chamada comunidade livre dita por liberais que de livre não tinha nada.
