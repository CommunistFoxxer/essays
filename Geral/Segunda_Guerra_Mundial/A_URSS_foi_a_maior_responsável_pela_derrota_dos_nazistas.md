# A URSS foi a maior responsável pela derrota dos nazistas
O Exército Vermelho, oficialmente "Exército Vermelho dos Operários e dos Camponeses", foi o exército da União Soviética, criado pelos bolcheviques em 1918 para defender o país durante a Guerra Civil Russa. Durante a Segunda Guerra Mundial, o Exército Vermelho lutou contra **200 divisões nazistas**. Em comparação, os Estados Unidos e o Reino Unido enfrentaram 10 divisões nazistas. O Exército Vermelho foi responsável por abater **9 de cada 10 soldados nazistas** tombados durante a Segunda Guerra Mundial. **80% de todas as batalhas** travadas pela Alemanha Nazista foram contra o Exército Vermelho. A União Soviética foi o país que mais contribuiu para a derrota dos nazistas, empregando o **maior contingente de combatentes** (2,6 vezes mais do que todos os outros aliados somados). Também foi o país que mais sofreu baixas: **27 milhões de soviéticos foram mortos** durante a guerra, o equivalente a quase 14% da população do país. O mito do "general inverno" não se sustenta, os alemães não saíram de um país tropical, ignorando que o inverno acontece em 1/4 do ano, que os soldados soviéticos também morreram congelados, as armas emperraram, a visão ficou ruim, os tanques atolaram, as bombas até congelaram, etc e não nevou só do lado alemão. As perdas nazistas na área do Volga-Don-Stalingrado foram de 1,5 milhão de homens, 3.500 tanques, 12.000 canhões e 3.000 aviões de caça.

Quase toda a Europa invadiu a URSS, e mesmo assim, ela conseguiu eliminar a ameaça fascista com triunfo.

![](images/anti-soviet_operation.jpg)

Winston Churchill, em sua mensagem a Joseph Stalin, de 27 de setembro de 1944, escreveu:
>O Exército russo arrancou as entranhas da máquina militar alemã...

No dia 28 de abril de 1942, Franklin D. Roosevelt afirmou em discurso à nação americana:
>As tropas russas destruíram e continuam destruindo mais tropas, aviões, tanques e armas de nossos inimigos, do que todas as outras nações unidas

O lend-lease também não foi o maior determinante para a vitória soviética. Eles receberam em sua maioria, veículos de transportes, que não eram necessários, e doaram grande parte das armas para o Exército Britânico como era previsto no contrato, mesmo que os britânicos não necessitassem das armas.

![](images/land-lease.png)

Fora que o lend-lease só foi assinado em Março de 1941, e os suprimentos enviados à URSS [só chegaram no final de 41 e 1942](https://www.tandfonline.com/doi/abs/10.1080/13518046.2018.1521360?forwardService=showFullText&tokenAccess=ydb4MpRst9ecpGy2yCvk&tokenDomain=eprints&doi=10.1080%2F13518046.2018.1521360&doi=10.1080%2F13518046.2018.1521360&doi=10.1080%2F13518046.2018.1521360&target=10.1080%2F13518046.2018.1521360&journalCode=fslv20&).
