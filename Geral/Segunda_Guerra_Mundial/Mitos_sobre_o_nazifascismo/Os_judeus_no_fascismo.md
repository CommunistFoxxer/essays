#;Os judeus no fascismo
O propósito desta secção não é apontar nenhuma balela ou teoria da conspiração antissemita, mas mostrar a hipocrisia de muitos fascistas antissemitas. Portanto, vamos começar.

Alguns nacionalistas e fascistas hoje em dia tem uma certa aversão aos judeus ou então são abertamente antissemitas. Porém a princípio, alguns líderes de movimentos nacionalistas europeus não tinham esse mesmo ideal. Exemplo disso é a Espanha e a Itália. Existiam judeus dentro do Partido Nacional Fascista. O jornal italiano pró-Mussolini intitulado de “minha bandeira” foi fundado por um Judeu. 230 judeus italianos participaram da marcha sobre Roma.

Muitos judeus militaram no fascismo desde os seus primórdios, os quais, com a própria presença, eram de certo modo um “penhor” da fidelidade e do patriotismo de seus correligionários e um “freio” à consolidação de um verdadeiro anti-semitismo. Não se pode esquecer que o fascismo recebeu ajuda material de muitos judeus, tais como Giuseppe Toeplitz, diretor geral da Banca Commerciale Italiana, que se demitiu em 1934, e Bonaldo Stringher, governador da Banca d’Italia.

Na marcha sobre Roma em 1922, participaram (ou pelo menos receberam o atestado de que haviam participado), 230 judeus e, nessa mesma data, estavam inscritos no PNF (Partido Nacional Fascista), ou no Partido Nacionalista (que convergiram para formar o partido fascista em março de 1923), cerca de 750.

Contrariando o que foi afirmado, no artigo publicado em 1920, Judeus, Bolchevismo e Sionismo Italiano, Mussolini escreve:
>O bolchevismo não é, como se acredita, um fenômeno judaico. É verdade, porém, que o bolchevismo conduzirá à ruína total os judeus da Europa Oriental. Este perigo enorme e talvez imediato é claramente percebido pelos judeus de toda a Europa. É fácil prever que a decadência do bolchevismo na Rússia será seguida de um pogrom de incríveis proporções. [...] na Itália não se faz absolutamente nenhuma diferença entre judeus e não judeus, em todos os campos, da religião à política, às armas, à economia... A nova Sião, os judeus italianos a têm aqui, nesta nossa adorável terra, que afinal muitos deles defenderam heroicamente com o próprio sangue.

Como referido em um artigo:
>O Fascisti cresceu tão rapidamente que dentro de dois anos, ele transformou-se na Partido Nacional Fascista em um congresso em Roma. Também em 1921, Mussolini foi eleito para o Câmara dos Deputados pela primeira vez. Nesse meio tempo, de cerca de 1911 até 1938, Mussolini tinha vários assuntos com o autor judeu e acadêmica Margherita Sarfatti, chamada de "Mãe judaica do fascismo" na época.<br/>
>[...]<br/>
>Apesar do fascismo italiano variou suas posições oficiais de corrida da década de 1920 a 1934, o fascismo italiano ideologicamente originalmente não discriminar a comunidade judaica italiana: Mussolini reconheceu que um pequeno contingente vivera ali "desde os dias dos reis de Roma "e deve" permanecer intacta". Havia até mesmo alguns judeus no Partido Nacional Fascista, como Ettore Ovazza que em 1935 fundou o jornal fascista judaica La Nostra Bandiera ("Nossa Bandeira").

Membros do partido fascista italiano que também eram judeus era Ettore Ovazza e Guido Jung.

Primo de Rivera (líder da Falange espanhola) também deu cidadania espanhola para os judeus sefarditas que haviam sido expulsos alguns séculos antes.

Fontes:
- [Judaismo Italiano | Mussolini e os Judeus ](http://hebraismoitaliano.blogspot.com/2011/03/mussolini-e-os-judeus.html)
- [Wikipedia For Schools | Benito Mussolini](https://www.classicistranieri.com/wikipediaforschoolspt/wp/b/Benito_Mussolini.htm)
