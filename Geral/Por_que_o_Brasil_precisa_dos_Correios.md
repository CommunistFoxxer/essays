# Por que o Brasil precisa dos Correios?
A seguir, uma compilação dos principais argumentos para manter os Correios estatizado.
- 20 milhões de cartas e encomendas entregues diariamente
- Único operador logístico que chega em todos os 5.570 municípios
- Em 60% deles, também são o >único representante da União<
- Inclusão bancária
- Documentos
- Distribuição de vacinas
- Operador logístico de doações em caso de catastrofes
- Logística de urnas eletrônicas, ENEM e livros didáticos da educação pública
- Logística internacional para milhares de pequenos e médios empresários brasileiros venderem pro mundo
- Entrega em todo o Brasil: "Para se chegar ao arquipélago de Bailique, no Amapá, o carteiro leva 12 horas de barco saindo de Macapá. Já para ir de Manaus a Manicoré, são quatro dias de viagem pelo rio Amazonas."
- Os correios têm autonomia orçamentária: não precisam de dinheiro do contribuinte nem do governo para operar. Ao contrário, sua demanda operacional só aumenta.

Os Correios têm o papel social inestimável de levar aos brasileiros dignidade, cidadania e acesso a serviços públicos.
