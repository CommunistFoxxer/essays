# O nazismo/racismo no black metal
Euronymous disse uma vez em 1993: "Todas as bandas norueguesas de black metal são nacional-socialistas" em uma carta a Paul Joseph Watson, do Reino Unido. O famoso NSBM.

Começamos da premissa que sim, o Black Metal (principalmente o norueguês) é supremacista branco. Vamos começar com o Darkthrone.

**Darkthrone**

Nos seus primeiros álbuns, A Blaze in the Northern Sky e Under a Funeral Moon, eles fazem muitas referências a um passado idealizado, eles falam sobre "o norte", "castelos", "inverno", "o céu do norte", "nuvens de melancolia", os apelos a um passado idealizado e ao eurocentrismo.

Na contra-capa do Transilvanian Hunger de 1994 está literalmente escrito "Norsk Arisk Black Metal" que basicamente se traduz como "Black Metal norueguês ariano" e também havia saído o comunicado na época da banda: 
>Gostaríamos de afirmar que a Transilvanian Hunger está além de qualquer crítica. Se alguém tentar criticar este LP, ele deve ser completamente patrocinado por seu comportamento obviamente judeu.

Claro que isso não caiu nada bem, e eles retiraram a frase do álbum, além de pedirem desculpas formalmente (a pedido da gravadora deles), e disseram que usaram "Arisk" para significar "verdadeiro" ou "puro" e que "judeu" era a gíria da juventude norueguesa para "idiota".

Varg Vikernes escreveu a letra de 4 das músicas do álbum Transilvanian Hunger. Em Until The Light Take Us, Fenriz chamou-o de "cara legal". Na época do lançamento do Transilvanian Hunger, Varg ainda não tinha se auto-proclamado racista e supremacista branco. Em 1995, Darkthrone lançou o Panzerfaust. Até aí tudo bem, exceto que a divisão Panzer era a divisão de tanques blindados da Wehrmacht, o exército nazista alemão (e a divisão Panzer também já foi citada pela banda Marduk, mas vamos continuar com Darkthrone por enquanto). A divisão Panzer era o regimento de tanques blindados do Terceiro Reich, usado para deportar e incinerar mais de 6.000.000 de homens, mulheres e crianças judeus nas câmaras de gás de Auschwitz e de outros lugares.

Fenriz em 1994, para a revista terrozier: 
>Aqui na Noruega é quase ilegal ser racista, mas se você é anti-racista, é muito bom e não achamos isso certo. Pensamos que deveria ser legal ser racista e que deveria ser quase tão ilegal ser anti-racista.

**Emperor**

Você não precisa procurar muito para encontrar evidências de racismo, sexismo, homofobia e anti-semitismo na banda Emperor, mas irei citar alguns fatos aqui. Emperor foi fundado em 1991 pelo vocalista Ihsahn, guitarrista Samoth e baterista Faust. O baterista Trym Torson ocasionalmente substitui Faust na bateria.

Trym torson toca em uma banda chamada "Zyklon", enquanto Ihsahn toca violão na banda "Zyklon-B", o que até aí não tem nenhum problema, exceto que zyklon-b era um pesticida a base de ácido cianídrico, cloro e nitrogênio. Este composto foi escolhido por proporcionar - com eficiência - uma morte rápida. Exatamente isso que era utilizado nas câmaras de gás em diversos campos de concentração com o objetivo de exterminar em massa os judeus e outros inimigos da Alemanha Nazista.

Mas retornando ao Emperor: lembram do Faust? Ele foi condenado por assassinar um homem gay, o esfaqueou fatalmente em Lillehammer, Noruega, em 21 de Agosto de 1992. Palavras do Faust sobre o assassinato:
>Esse homem se aproximou de mim - ele estava obviamente bêbado e obviamente um viado [...] era óbvio que ele queria ter algum contato. Então ele me perguntou se poderíamos [...] ir para a floresta. então eu concordei, porque já havia decidido que queria matá-lo.

Na época, Faust recebeu muito apoio, do Hellhammer do Mayhem, da banda Antekhrist e de muitos outros músicos de Black Metal, por esse crime hediondo e homofóbico. Faust, assim como Samoth, tocou em "Zyklon" e "Zyklon-B".

**Gorgoroth**

O próprio nome é racista, afinal o nome em si vem da obra de Tolkien, em O Senhor Dos Anéis, onde todo mundo é um homem branco, exceto os inimigos, mas isso é assunto para outra hora.

Vamos começar com Infernus, o guitarrista e fundador da banda Gorgoroth. Na época do Black Metal Inner Circle, o mesmo afirmou que era um "bom amigo" do racista neo-nazista Varg Vikernes, da banda. Ele disse que até participou de seus julgamentos em 1994. Ele também escreveu que "anti-racista é uma palavra de código para anti-branco", algo que os supremacistas brancos costumam dizer para defender seu ódio e intolerância.

Gorgoroth não é publicamente uma banda NSBM mas o vocalista Gaahl disse que "apoiou Adolf Hitler" em uma entrevista de 1995. Em uma entrevista de Gaahl, ele diz que na época se associou a grupos de extrema-direita da noruega, mas que entre os jovens era tudo relacionado a "gangues", e que as pessoas se juntavam a elas para não apanhar.

Gaahl hoje, é homossexual assumido e diz que: "Sem dúvida houve mudanças e evolução em minha maneira de pensar. Sou uma pessoa diferente hoje."

O baterista Frost do Gorgoroth também tocou na banda Zyklon-B, o novo baterista de Gorgoroth tocou na banda Dark Funeral e na abertamente banda NSBM Dissection, cujo vocalista Jon Nödtveidt foi condenado pelo assassinato de um imigrante gay.

**Dissection**

Em 1997, Jon Nödveidt foi preso pelo assassinato do imigrante homossexual Joseff Medour. O crime ficou muito conhecido na Suécia como “o assassinato no parque Keillers”, dando origem a um filme e um livro.

Quando saiu da prisão em 2004 após cumprir pena pelo crime, Jon disse:
>Por respeito à família da vítima, não quero discutir o que aconteceu. a única coisa que quero dizer é que não tenho orgulho disso.

Quando acusados de serem uma banda nazista, Nodveidt disse que o dissection é “apenas” nacionalista e racista. Então relaxa pessoal, essa banda não é nazista, só racista mesmo!

**Watain**

Watain foi fundada por Erik Danielsson, Håkan Jonsson e Pelle Forsberg. A primeira demo já lançada por Watain em 1998 é extremamente explícita: é chamada de "foda-se seu Deus judeu", cheia de referências às câmaras de gás do Terceiro Reich e do crematório nazista.

Eles também estão familiarizados com a saudação nazistas em seus shows. Ele foi afastado da banda, e Erik Danielsson disse:
>O gesto na foto foi feito em tom de brincadeira, tanto quanto o que temos a dizer sobre isso. Mas para acabar com esse absurdo cansativo, o guitarrista em questão decidiu se afastar por um período de para evitar discussões.

Outras bandas nazistas/racistas incluem:
- Absurd
- Satanic Warmaster
- Nokturnal Mortum

Muitas outras bandas de Black Metal, como Dimmu Borgir e Nargaroth, se esforçam para esconder seus laços terríveis com o NSBM sob o pretexto de “liberdade de expressão” ou “racismo hipster”.

Fontes:
- [whiplash.net | Gorgoroth: As orientações sexuais e políticas de Gaahl](https://whiplash.net/materias/entrevistas/079372-gorgoroth.html)
