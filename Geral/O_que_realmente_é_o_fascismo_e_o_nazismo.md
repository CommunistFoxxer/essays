# O que realmente é o fascismo e o nazismo
O fascismo é nada mais e nada menos que:
1) Anti-socialismo e contra-revolucionarismo (reacionarismo)
2) Darwinismo social e racismo
3) Colonialismo

Já o nazismo é o fascismo com o colonialismo aplicado contra seu próprio povo (no caso de Hitler, contra os próprios europeus). Formas mais comuns de fascismo também incluem o corporativismo. Quando há uma crise do capitalismo, a tendência é a população ser cada vez mais socialista, então o fascismo aparece mais frequentemente atrelado às crises do capitalismo financiado pelas potências imperialistas para acabar com as esquerdas.
