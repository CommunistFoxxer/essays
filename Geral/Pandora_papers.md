#;Pandora papers
Pandora Papers expõe vários líderes latino-americanos. Os documentos do Pandora revelam uma estrutura financeira que beneficia os mais ricos. Três presidentes em exercício, 11 fora do governo e 90 políticos latino-americanos de alto escalão estão na lista.

O Consórcio Internacional de Jornalistas Investigativos (ICIJ) é uma colaboração ambiciosa envolvendo mais de 600 jornalistas e 150 veículos de comunicação de diferentes países. Sua última investigação foi revelada neste fim de semana: os Pandora papers, uma série de vazamentos que revelam os segredos financeiros de 35 líderes mundiais, mais de 330 políticos e funcionários e uma lista global de fugitivos e golpistas.

Os documentos secretos expõem as negociações offshore do rei da Jordânia; os presidentes da Ucrânia, Quênia e Equador; o primeiro-ministro da República Tcheca e o ex-primeiro-ministro britânico Tony Blair, entre outros. Os arquivos também detalham as atividades financeiras de mais de 130 bilionários da Rússia, Estados Unidos, Turquia e outras nações. Três presidentes latino-americanos em Pandora.

Entre os políticos latino-americanos expostos na investigação, estão três presidentes em exercício: o chileno Sebastián Piñera, o equatoriano Guillermo Lasso e o dominicano Luis Abinader. Também participam 11 ex-presidentes, como os colombianos César Gaviria e Andrés Pastrana; o peruano Pedro Pablo Kuczynski; o hondurenho Porfirio Lobo; o paraguaio Horacio Cartes; e os panamenhos Juan Carlos Varela, Ricardo Martinelli e Ernesto Pérez Balladares.

De acordo com os documentos que vazaram, Lasso tem laços com 10 empresas "offshore" e trustes no Panamá e nos Estados Unidos. Registros mostram que ele transferiu ativos para dois trustes em Dakota do Sul em dezembro de 2017, três meses depois que o parlamento equatoriano proibiu funcionários públicos de deter ativos em paraísos fiscais. 

Por sua vez, o presidente Sebastián Piñera tem entre seus negócios offshore, a venda do projeto de mineração Dominga, nas Ilhas Virgens Britânicas (BVI), operação que envolveu o empresário e um de seus amigos históricos, Carlos Alberto "Choclo" Délano. O presidente negociou a venda do polêmico megaprojeto de mineração em um paraíso fiscal. 

Esse acordo foi feito em dezembro de 2010 — quase nove meses após o primeiro mandato de Piñera como presidente — por 138 milhões de dólares, a ser pago em três parcelas. Mas o contrato de venda continha uma contingência: o último pagamento não seria feito se fossem tomadas medidas que impedissem "irrevogavelmente" um projeto de mineração em estudo, por exemplo, a criação de uma reserva natural.

Horas depois das revelações dos Pandora papers, o Governo do Chile [emitiu](https://media.elmostrador.cl/2021/10/2021.10.03-COMUNICADO-DE-PRENSA-PRESIDENCIA-DE-LA-REPUBLICA.pdf) um comunicado destacando-se de todas as acusações em relação aos negócios do presidente Piñera.

Luis Abinader, o presidente dominicano, está vinculado a duas empresas panamenhas: Littlecot Inc. e Padreso SA, criadas antes de a Abinader assumir a presidência do país em 2020. Os arquivos indicam que as ações eram inicialmente 'ao portador', mecanismo que permitia ocultar a identidade dos proprietários. 

O Pandora papers revela ainda que o ministro da Economia do Brasil, Paulo Guedes, já foi um dos acionistas e diretores do Dreadnoughts International Group, empresa constituída nas Ilhas Virgens Britânicas em 2014. Guedes criou a empresa com a ajuda do Trident Trust. Os arquivos vazados listam sua filha como acionista conjunta e co-diretora, e sua esposa como acionista. A Dreadnoughts estava ativa nas Ilhas Virgens Britânicas em agosto de 2021.

Na Argentina, os documentos trazem à luz os nomes de Jaime Durán Barba, consultor político que catapultou Mauricio Macri à presidência em 2015; Zulema Menem, filha do ex-presidente Carlos Menem; e o falecido Daniel Muñoz, ex-secretário do ex-presidente Néstor Kirchner.

No México, os documentos apontam para mais de 3.000 pessoas, entre as quais se destacam os empresários mais ricos do país: o magnata da mineração Germán Larrea; a herdeira do grupo de cervejas Modelo, María Asunción Aramburuzabala; e Olegario Vázquez Aldir.

Os documentos da Pandora papers - 2,94 terabytes com 11,9 milhões de registros - coletam informações sobre 27.000 empresas e 29.000 beneficiários finais, mais do que o dobro dos identificados nos Panama papers.
