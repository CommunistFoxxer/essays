# Notas sobre revisionismo
O termo "revisionismo", usado de forma isolada, não quer dizer nada. Todo rompimento com teorias anteriores é uma forma de revisionismo. A questão não deve ser essa, deve ser se a revisão em questão retrata a realidade. Quando Marx estabelece a teoria do valor-trabalho marxista, o que ele fez foi um revisionismo em relação à TVT de Adam Smith. Ele mudou definições e parâmetros centrais na TVT original. Por definição Marx foi um revisionista. Ele estava errado? Não. Mao Zedong foi um revisionista. Ele representou um rompimento na estratégia marxista-leninista de focar a organização e práxis comunista entre os operários, e ao invés disso, organizou uma revolução no campo. Mesmo com todos os teóricos anteriores a ele falando que o operariado era a classe mais avançada. Foi um afastamento um rompimento, com a teoria marxista-leninista anterior até então. Por definição é um revisionismo. Qualquer mínima adaptação que você faz do marxismo à realidade histórica onde você está inserido, é um revisionismo.

Não existe desenvolvimento da teoria marxista sem revisionismo. Não existe rompimento, não existe dialética, não existe antítese, não existe síntese, não existe nem marxismo sem revisionismo.

O revisionismo, usado como crítica isolada, não quer dizer nada. É apenas um moralismo irracional, sem a devida análise e compreensão dos fatos e da realidade.
>Toda a ciência seria supérflua se a forma fenomênica e a essência das coisas coincidissem diretamente.<br/>
>– <cite>Karl Marx</cite>
