# Artes marciais
Kempo: é eficiente em chutes, mobilizações e golpes explosivos. É bom também para treinar o condicionamento físico e se manter em forma!

Shotokan: te torna uma verdadeira máquina de matar. Te ensina a matar alguém em menos de 2 golpes. É bem explosiva.

Kung Fu: exige muito treino, leva anos para se aperfeiçoar, mas vale a pena, você se torna um ser quase invencível!

Krav Maga: luta de rua, em ambiente real, uma combinação de várias outras modalidades (incluindo MMA) com as melhores práticas, atingindo de forma eficiente os pontos vitais para imobilizar o inimigo!

Taekwondo: é pouco eficiente, não recomendo muito, mas é basicamente sobre golpes na parte inferior do inimigo. Funciona contra pessoas comuns, mas ineficaz contra outros praticantes de outras artes marciais.

Jiu Jitsu: é eficiente e prático, mas principalmente desenhado para ser eficiente na auto-defesa.

Muay Thai: eu recomendo ao invés de Taekwondo, é bom para se preparar fisicamente, e é bem diverso e intenso!
