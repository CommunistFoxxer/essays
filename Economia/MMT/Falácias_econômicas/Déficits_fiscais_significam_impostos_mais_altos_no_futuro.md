# Déficits fiscais significam impostos mais altos no futuro
Os impostos servem a muitos propósitos (reduzir o poder de compra do setor privado, criar demanda monetária, reduzir o consumo de bens e serviços prejudiciais, como o tabaco, e assim por diante), mas nenhum desses propósitos está relacionado ao financiamento de gastos do governo.

Em um sistema monetário fiduciário, em que a moeda não tem valor intrínseco, o governo precisa transferir bens e serviços reais do setor não-governamental para o setor governamental para facilitar seu programa econômico e social. Neste contexto, uma função primordial da tributação é promover ofertas de bens e serviços por particulares ao governo em troca dos fundos necessários para extinguir passivos fiscais.

O ponto crucial é que os recursos necessários para pagar as obrigações fiscais são fornecidos ao setor não governamental por meio de gastos do governo. Assim, os gastos do governo, se suficientes, proporcionam o trabalho remunerado, o que elimina o desemprego gerado pelos impostos.

Ao privar o setor não-governamental de poder de compra, os impostos atenuam a demanda agregada, de modo que o governo pode criar um espaço de recursos real não inflacionário para acomodar os gastos públicos. 

É importante ressaltar que cada geração pode escolher livremente o nível de tributação que paga, pois determina, por meio do processo político, o tamanho do governo e o espaço real de recursos que utilizará. Os déficits fiscais do passado nunca precisam ser pagos pela geração atual ou por qualquer geração futura. Os progressistas deveriam referir-se ao dinheiro “público” em vez do dinheiro dos “contribuintes”. Os contribuintes não financiam os gastos do governo. Os impostos são necessários para reduzir a capacidade do setor não-governamental de comandar bens e serviços reais para que o governo possa utilizá-los.
