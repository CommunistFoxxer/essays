# O governo enfrenta a mesma restrição de orçamento que uma família
Muitas das metáforas amplamente utilizadas, como as da Tabela abaixo, buscam equiparar diretamente os gastos do governo com os de um chefe de família individual imprudente (gastando dinheiro com dívidas de cartão de crédito, deixando de pagar a hipoteca, deixando de cuidar dos filhos). A analogia do orçamento familiar é falsa.

![](images/mmt_fallacies.png)

O governo não tem um imenso cofre onde ele guarda o dinheiro dos impostos e depois paga pelo nosso serviço. Quando o governo paga um beneficiário, por exemplo, ele emite ordem de pagamento em nome do beneficiário do gasto e o Banco Central credita o saldo na conta de Reservas Bancárias do banco do destinatário. Isso é chamado de base monetária.

As famílias usam a moeda e devem financiar seus gastos. Um governo soberano emite a moeda e deve gastá-la antes que possa, posteriormente, tributar ou tomar empréstimos. Um governo emissor de moeda nunca pode ter suas receitas restritas em um sentido técnico e pode sustentar déficits indefinidamente sem risco de solvência. A analogia do orçamento familiar não se aplica a um governo emissor de moeda. Nossa própria experiência de orçamento pessoal não gera percepções relevantes quando analisamos as despesas e receitas do governo. Portanto, uma narrativa alternativa deve destacar as características especiais do monopólio monetário do governo.
