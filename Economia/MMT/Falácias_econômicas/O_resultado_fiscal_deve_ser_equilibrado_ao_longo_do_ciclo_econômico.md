# O resultado fiscal deve ser equilibrado ao longo do ciclo econômico
Reconhecer que o resultado fiscal é endógeno significa que o governo não pode atingir um determinado resultado porque as mudanças nos gastos privados (por exemplo) podem frustrar quaisquer esforços feitos pelo governo para atingir essa meta. A estratégia responsável para um governo é permitir que seu saldo fiscal se ajuste ao nível de gasto líquido necessário para alcançar o pleno emprego, dada as decisões de gasto do setor não-governamental, independentemente do estado do ciclo econômico.

As contas nacionais nos dizem que, para uma nação com déficit externo, uma 'regra de equilíbrio do orçamento' equivale a exigir que o setor privado doméstico registre um déficit de igual magnitude ao déficit externo. É improvável que seja uma estratégia sustentável. Além disso, uma estratégia fiscal anticíclica não exige que o governo atinja um superávit. O conceito de anticíclica se refere mais corretamente à direção da mudança do que ao nível do resultado fiscal.

O governo não deve aumentar seus gastos líquidos discricionários se a economia já estiver em plena capacidade e estiver satisfeita com o mix de gastos privados. Essa expansão seria pró-cíclica. Mas uma orientação fiscal que resulte em um déficit estável é desejável onde o déficit externo é estável e o setor privado doméstico está economizando em geral.

As regras fiscais definidas em termos de dívida pública ou proporções do déficit dificilmente serão consistentes com a gestão responsável da política fiscal.
