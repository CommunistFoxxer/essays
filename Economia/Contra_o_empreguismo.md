# Contra o empreguismo
Lógico que empregos são fundamentais pra construção de um país, o ponto não é achar que eles não são importantes, assim como falar de PIBismo não é dizer que crescimento não importa. O problema vai mais fundo. É muito relevante ter uma análise crítica sobre a geração de empregos num país para não achar que um país com muitos empregados é necessariamente um país bom. Precisamos medir a qualidade dos empregos e como eles potencializam as "capacidades" da população.

Em Roma não havia desemprego estrutural, tampouco no Brasil colonial ou na Londres do século XIX. Ter muito emprego é fulcral, mas insuficiente. Nossa preocupação não deve se restringir apenas ao fim dos empregos com novas tecnologias, mas aos níveis do emprego hoje.
