# Prova econométrica de que o socialismo é melhor
Primeiro temos que definir o que é socialismo e o que é capitalismo para que possamos primeiramente falar deles. Eu vou usar a definição da Stanford, que diz que existem alguns pontos para definir o que é capitalismo e o que é socialismo. Estes são os pontos do capitalismo:
1. A maior parte dos meios de produção é de propriedade e controle privado.
2. As pessoas possuem legalmente sua força de trabalho.
3. Os mercados são o principal mecanismo de alocação de insumos e resultados da produção e de determinação de como o excedente produtivo das sociedades é usado, incluindo se e como é consumido ou investido.
4. Há uma divisão de classes entre capitalistas e trabalhadores, envolvendo relações específicas (por exemplo, seja de barganha, conflito ou subordinação) entre essas classes e moldando o mercado de trabalho, a empresa e o processo político mais amplo.
5. A produção é principalmente orientada para a acumulação de capital (ou seja, a produção econômica é principalmente orientada para o lucro, e não para a satisfação das necessidades humanas).

O socialismo transforma o 1 em 1*, isto é, 1* é onde a maior parte dos meios de produção é de propriedade e controle social. Em relação ao 2, mantém-se pois o socialismo admite que os trabalhadores têm sua própria força de trabalho. O 3 é frequentemente debatido e é abordado pelo socialismo de mercado. O ponto 4 e 5 são abolidos quando se fala do socialismo. A definição de socialismo então ficará como está frequentemente abordado no meio de discussão, como "doutrina política e econômica que prega a coletivização dos meios de produção e de distribuição, mediante a supressão da propriedade privada e das classes sociais."

Vou usar alguns conceitos para os cálculos, mas primeiro vou explicá-los: Temos um conjunto de indivíduos `N`, de uma dada sociedade e uma quantidade determinada de entrada (input) disponível, que assumimos ser `1`, mas pode variar entre outros números. Qualquer número não-negativo (0 + números naturais)

![](images/latex/equation_1.jpg)

de quantidade de entrada em uma cadeia produtiva (`aⁱ` = uma certa quantidade de material, seja capital, matéria-prima, matéria subsidiária, insumo), é transformada em um montante (acréscimo de trabalho) de riqueza representado pela quantidade vezes o perfil de produtividade

![](images/latex/equation_2.jpg)

ou seja

![](images/latex/equation_3.jpg)

Nesta dada sociedade, os valores da distribuição das quantias são distribuídos por `a¹` (quantia número 1), `a²` (quantia número 2) e assim sucessivamente, criando o somatório (soma agregada) de números não negativos. $`[\sum_a^i = 1] \Leftrightarrow \sum_i=0^1 a^i`$ (`aⁱ` varia entre `0` e `1`).

Então, a cada entrada de quantidade `aⁱ`, uma riqueza correspondente é adicionada pelo valor acrescentado, de

![](images/latex/equation_4.jpg)

onde `P` é o perfil de produtividade (o trabalho). E o resultado correspondente desta riqueza gerada é divida pelo número de indivíduos da sociedade. Modelamos um sistema econômico como uma regra que especifica para cada produtivo perfil de identidade um perfil de riqueza viável `(w¹, w², ...)`. Observe que esta noção de um sistema econômico tem não especifica a quantidade de riqueza que cada indivíduo produz; o mesmo perfil de riqueza pode ser alcançado por diferentes distribuições de insumos entre os indivíduos. Temos outra definição, que é o sistema econômico: Um sistema econômico é uma função `F` que atribui a todos os programas de produtividade arquivo `(λ¹, λⁿ, ...)` um perfil patrimonial viável `F(λ¹, λⁿ, ...)`. Para qualquer perfil de produtividade `(λ¹, λⁿ, ...)`, um perfil de números não negativos é um perfil de riqueza viável `(w¹, w², ...)`.

Se para algum perfil de distribuição de entrada `(a¹, a², ...)` de números não negativos com,

![](images/latex/equation_5.jpg)

temos

![](images/latex/equation_6.jpg)

ou seja, uma somatória (`i=1`) de `a¹` (quantia número 1) temos `1`, e na outra somatória (`i=1`) temos um perfil de riqueza viável que é igual a somatória (`i=1`) de `a¹` (quantia número 1) perfil de produtividade¹. Se `F (λ¹, λⁿ, ...) = (w¹, w², ...)` então escrevemos `F(λ¹, λⁿ, ...) = wⁱ`, a riqueza atribuída ao indivíduo `i` pela regra `F` dado o perfil de produtividade `(λ¹, λⁿ, ...)`. Observe que um sistema econômico é uma regra que especifica como a riqueza é distribuída para cada perfil de produtividade possível `(λ¹, λⁿ, ...)`, não apenas para um perfil específico. A distribuição dos insumos determina a riqueza total produzida.

A riqueza máxima é obtida atribuindo toda a entrada aos indivíduos com a maior produtividade (se não houver dois indivíduos com a mesma produtividade, isso significa que toda a entrada é atribuída ao indivíduo mais produtivo). O igual a distribuição da entrada entre todos os indivíduos é normalmente ineficiente. Vamos a um exemplo:

Suponha uma sociedade de dois indivíduos com os seguintes perfis de produtividade:
- `λ¹= 3` e `λ² = 2`

A riqueza total produzida é 

![](images/latex/equation_7.jpg)

ou seja, a somatória da riqueza viável (`wⁱ`) é igual a somatória de `aⁱ` (quantidade de entradas `i` em uma cadeia produtiva) perfil de produtividade `i`.

Neste caso

![](images/latex/equation_8.jpg)

Mas

![](images/latex/equation_9.jpg)

Substituindo isso em nossa equação original:

![](images/latex/equation_10.jpg)

Obviamente, a maior riqueza produzida será se `a¹ = 1`

![](images/latex/equation_11.jpg)

Portanto

![](images/latex/equation_12.jpg)

Então, conforme afirmado anteriormente "A riqueza máxima é obtida atribuindo todos a contribuição para os indivíduos com a maior produtividade", também "A distribuição igual da entrada entre todos os indivíduos é tipicamente ineficiente". Vamos mostrar por que esse é o caso.

Note que eu não defini o que caracteriza a eficiência, mas pode-se entender intuitivamente. 

![](images/latex/equation_13.jpg)

Então

![](images/latex/equation_14.jpg)

Simplificando: a distribuição de entradas `a¹` e `a²` (quantidade de entradas 1 e 2) tem o mesmo número de entradas e insumos. Se a somatória da riqueza viável (`wⁱ`) é igual a entrada a¹ λ¹ (quantidade de entradas 1, perfil de produtividade 1) + a² λ² (quantidade de entradas 2, perfil de produtividade 2) é igual a 1/2 λ¹ (um meio, perfil de produtividade 1) + 1/2 λ² (um meio, perfil de produtividade 2) = 1/2 (λ¹ + λ²) (ou seja, um meio com dois perfis de produtividade somados) é igual a 1/2 (3 + 2) (um meio, três mais dois, que é os valores dos perfis de produtividade), portanto `5/2` onde a metade é `2.5` e não `3`. 

Uma alternativa para distribuir a riqueza produzida pelos indivíduos é permitir que cada indivíduo mantenha a quantidade que ele produz.

No outro extremo, o quantidade total de riqueza produzida é distribuída igualmente entre os indivíduos. O socialismo permite entradas ao mais produtivo e a igualdade da riqueza. A entrada é dividida igualmente entre os indivíduos com o maior produtividade e a riqueza são divididas igualmente entre todos os indivíduos. Ou seja, para qualquer perfil de produtividade `(λ¹, λⁿ, ...)` temos

![](images/latex/equation_15.jpg)

para cada indivíduo `i`, isso é: Para cada perfil de produtividade nós vamos ter a regra `F` elevado ao `i` (indivíduo) que é igual ao máximo do perfil de produtividade. Por exemplo: 1, 2, 3, o máximo é 3, portanto extraímos 3. 

Vamos em um exemplo para aplicar o que eu disse: A entrada é dividida igualmente entre os indivíduos com a maior produtividade e cada indivíduo recebe uma quantidade de riqueza proporcional para sua produtividade. Ou seja, para qualquer perfil de produtividade `(λ¹, λⁿ, ...)` temos

![](images/latex/equation_16.jpg)

para cada indivíduo `i`.

Simplificando: para cada perfil de produtividade, vamos ter a função `F` elevado a `i` (indivíduo) que é igual ao máximo dos perfis de produtividade dividido por $`Σ^n_{i=1}`$.

Vou fazer um exemplo rápido para tentarmos entender melhor: Imagine uma sociedade com três indivíduos com o seguinte perfil de produtividade:
- `(λ¹, λ², λ³) = (3, 6, 9)`

No socialismo vamos ter:

![](images/latex/equation_17.jpg)

Portanto

![](images/latex/equation_18.jpg)

No capitalismo vamos ter:

![](images/latex/equation_19.jpg)

Portanto

![](images/latex/equation_20.jpg)

Resumindo: No socialismo, a produtividade dos trabalhadores se alinha com a média da produtividade, enquanto no capitalismo cada indivíduo tem a sua produtividade alterada, incluindo aqueles que melhor trabalham, mas que no final, a produtividade é a mesma, só muda o resultado de cada trabalhador. 

Após dar o exemplo, vou definir agora o que é eficiente. O que é eficiência? Um sistema econômico é eficiente se, para cada perfil de produtividade, não-perfil patrimonial viável diferente do gerado pelo sistema é melhor para alguns indivíduos e não pior para nenhum indivíduo, ou seja, um sistema econômico é eficiente se para cada perfil de produtividade `(λ¹, λⁿ, ...)` haja que cada perfil se alinhe com a média da produtividade, não sendo pior e nem melhor para nenhum indivíduo a média.

Um sistema econômico de função `F` é eficiente se para cada perfil de produtividade `(λ¹, λⁿ, ...)` não há perfil de riqueza viável `(w¹, w², ...)` para o qual `wⁱ ≥ Fⁱ(λ¹, λⁿ, ...)` para todo `i = 1, ..., n`, com pelo menos uma desigualdade estrita.

Um sistema econômico é eficiente se e somente se, para cada perfil de produtividade, a soma das riquezas que o sistema atribui aos indivíduos é igual ao máximo riqueza total que pode ser produzida, isso é, cada perfil de produtividade, a soma das riquezas (ex: `4 + 4`) é igual ao máximo da própria riqueza (pressupondo que o máximo é `8`, se cada indivíduo detêm de `4` da riqueza). Um sistema econômico é simétrico se para qualquer perfil de produtividade em que dois indivíduos têm a mesma produtividade, eles recebem a mesmo fortuna. Um sistema econômico é monótono por si só quando a produtividade de um indivíduo aumenta, sua riqueza não diminui, isso é, um sistema econômico de função F é monótono em sua própria produtividade se para cada perfil de produtividade `(λ¹, λ^n, ...)`, todo indivíduo `i`, e todo número `x > 0` (`x` é o número somado para aumentar a produtividade), temos `Fⁱ(λ¹, λ^n, λⁱ + x, ...) ≥ Fⁱ(λ¹, λⁿ, λⁱ, ...)`. Um sistema econômico é monótono em outros produtividades de terceiros, se houver aumento na produtividade de um indivíduo não machucar outro indivíduo. Todas essas propriedades parecem intuitivamente desabilitáveis. Não faz parecem ser um grande problema para julgar os sistemas econômicos com base nesses critérios.

Claro, alguém poderia argumentar que eles são arbitrários, mas eles não parecem arbitrários, pelo menos intuitivamente. E no final do dia, pode-se argumentar (eu não acho muito com sucesso) que todas as propriedades nas quais escolhemos julgar um sistema econômico são fundamentalmente arbitrários. Mas você tem que começar de algum lugar! Então, vamos caracterizar o socialismo:

O socialismo é o único sistema econômico que é eficiente, simétrico, monótono por si só produtividade e monótono nas produtividades dos outros é socialismo, de acordo com para o qual toda a riqueza é produzida pelos indivíduos com a maior produtividade e a riqueza total são divididas igualmente entre todos os indivíduos, ou seja, ele é o único sistema que detêm das quatro características que defini.

Vamos verificar se o socialismo satisfaz as quatro características (eficiência e simetria descarta visibilidades):

Monótono na própria produtividade:

![](images/latex/equation_21.jpg)

portanto

![](images/latex/equation_22.jpg)

Então

![](images/latex/equation_23.jpg)

desde que

![](images/latex/equation_24.jpg)

Portanto

![](images/latex/equation_25.jpg)

Isso é, `Fⁱ(λ¹, λⁿ, λⁱ + x, ...) > Fⁱ(λ¹, λⁿ, λⁱ, ...)`

Resumindo: Fⁱ(λ¹, λⁿ, λⁱ, ...) é menor que `Fⁱ(λ¹, λⁿ, λⁱ + x, ...)`, porque x > 0. Esse cálculo conclui que a produtividade do Fⁱ aumentou mas sem sua riqueza diminuir, portanto, verificado que o socialismo possui esta característica.

Monótono na produtividade de terceiros:

![](images/latex/equation_26.jpg)

para dois indivíduos `i` e `j`.

Logo

![](images/latex/equation_27.jpg)

Obs: No cálculo `Σw^i = a¹ λ¹ + a² λ² = a¹3 + (1 - a¹)2 = a¹3 + 2 - 2 = a¹`, eu sei que ficou estranho, então:

![](images/latex/equation_28.jpg)

E por isso `Σw^i = a¹ + 2`, então, `1 + 2 = 3`, `Σw^i = 3`.

Caso você se interesse, há um livro explicando tudo isso, se chama Models in Microeconomic Theory.