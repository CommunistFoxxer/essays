# Nem tudo é escasso
Dizer que os recursos são escassos é diferente de dizer que eles são finitos. O conceito da escassez na economia neoclássica se dá no sentido que todos os fatores (terra, capital, trabalho e etc) estão sendo plenamente empregados. É diferente de dizer que eles não são infinitos. Só porque existe desemprego involuntário de trabalhadores (não há escassez), não quer dizer que há uma quantidade infinita deles.

A economia neoclássica supõe que os recursos estão plenamente empregados. Peguemos novamente o desemprego. Só porque as pessoas estão dispostas a trabalhar, não quer dizer que haverá demanda para tanto. Qual a garantia de que haverá o uso deles (até a escassez)? E tem o pior. Na outra ponta a turma da Lei de Say que é contra políticas de distribuição de renda. Alegam que é necessário crescimento para reduzir pobreza e que desigualdade não é problema. Mas se os recursos são escassos crescimento é limitado e sobra as políticas de distribuição de renda.

E pior, os recursos são escassos a que níveis? Já produzimos o suficiente para ninguém mais no mundo passar fome. O problema é de distribuição. Por outro lado, não ha como produzir suficientemente para que todos tenham o mesmo estilo de vida que um estadunidenses.

A escassez não é um processo natural mas sim decorrente de um processo volitivo, consciente e alguns casos especulativo, com fins de maximização dos lucros. O problema não é haver ou não escassez, mas sim o fato de que essa definição sugere que economia é a ciência que lida de forma técnica com alocação eficiente de recursos e assume que eficiência é critério objetivo e neutro e relega para 2° plano os conflitos distributivos.

Energia solar é escassa? Distribuição de informação hoje é escassa? Transportes, se tivermos energia renovável não-escassa, seria escasso?
