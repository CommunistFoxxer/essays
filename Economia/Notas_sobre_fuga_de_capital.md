# Notas sobre fuga de capital
O argumento da "fuga de capital" é complicado, porque não leva em consideração dois fatores:
1. Tirar do país e levar para outro implica em maior taxação.
2. Medidas de governança global que começam a ser implantadas contra a evasão fiscal. É um argumento em defesa do (1) bilionário, repetido pelas outras classes sociais.

Se quer fugir de imposto, vai para onde? Países ricos? Lá taxam. Para país pobre ou paraiso fiscal? Onde vai achar consumidor? Como conseguir mercado, e do zero, com empresas locais já estabelecidas? Vai levar as fábricas/latifúndios/estabelecimentos onde? Dentro da mala? Fugir de imposto compensaria para apenas um tipo de rico: o que especula, de capital improdutivo, parasita, que vive da renda do seu dinheiro, então basta mudar de país e seguir especulando. Se é por falta de adeus: vá, desgraçado! Pode fugir até para o inferno, não fará falta!

Não houve fuga de capital produtivo na França. Quem tem empresa estabelecida por lá, que emprega, ficou por lá. Agora rentista safado foge mesmo, isso não é novidade. Perdemos nada com isso. E larguem da muleta de [Gerard Depardieu](https://www1.folha.uol.com.br/mundo/2013/03/1239308-o-mito-da-fuga-fiscal-e-o-caso-de-gerard-depardieu.shtml), é apenas um ator escroto que produz nada.

No Paraguai, houve fuga de capital, mas isso só demonstra que o país possui uma economia frágil, pouco complexa, sem indústria, sem mercado interno forte, baseada em latifúndio e capital improdutivo. Não há nada que prenda um rico por lá, principalmente os que especulam.

Sobre a DHL, ela pertence à estatal Deutsch Post; uma modalidade de privatização muito praticada pelos países ricos. Funciona assim: o Estado "privatiza", mas sem abrir mão de ser o maior acionista, ou seja, o dono, na prática.
