# Curiosidades sobre a escola austríaca
Ao contrário do que o mises.org tenta fazer parecer, a Escola Austríaca é muito dividida. A linha que o Instituto Mises segue é a linha rothbardiana, mas existem diversas outras, como a hayekiana.

Menger uma vez disse que a teoria do capital de Böhm-Bawerk foi o maior erro já cometido (Mises, Epistemological Problems of Economics, pp. 177). Mises diz que os 2 erraram e nunca entenderam a própria teoria, porque eles se baseavam ainda na teoria do valor objetivo (Schumpeter, History of Economic Analysis, pp. 814). Vindo de Schumpeter, pode-se duvidar, mas Endres em seu livro "Neoclassical Microeconomic Theory: The Founding Austrian Vision" mostra que há diversas diferenças entre a teoria do capital de ambos, então é bem provável que seja verdade (pp. 146-148).

Richard von Mises era muito diferente do seu irmão mais velho, Ludwig, e inclusive não se davam muito bem. Matemático, engenheiro mecânico e estatístico, teve contribuições fundamentais nesse campo. Mas epistemologicamente é que se diferenciava do seu irmão: foi expoente do neopositivismo e membro da primeira geração do Círculo de Viena, junto com Otto Neurath (oponente socialista de Mises na discussão sobre o cálculo econômico, criador do cálculo em espécie).

Ludwig von Mises era próximo de Weber, e teve muita influência do mesmo (principalmente na questão do cálculo econômico).

Mark Blaug defende que há 2 fases de Mises, a segunda onde Mises cria a sua famosa praxeologia.

Existem conflitos acerca da interpretação da praxeologia, entre Rothbard e Machlup, e evidências sugerem que Machlup esteja mais certo. Além desses conflitos, contra Mises, ainda há a reconstrução lakatosiana de Rizzo e a retórica da economia de McCloskey (ao mesmo tempo em que Boettke critica a Escola Austríaca). Sem contar que Hayek e Kirzner tentaram escapar da abordagem misesiana e tentaram desenvolver uma abordagem mais empírica.

Hayek foi aluno de Mises e Kelsen, se referia a Schumpeter como "professor", era primo de segundo grau de Wittgenstein, amigo de Karl Popper e Frege e próximo de Keynes e Russell.

Quando Friedman apresentou seu modelo macroeconômico com a taxa natural de desemprego, Hayek o acusou de usar terminologia comunista, uma vez que a inflação era resultado de conflitos distributivos.

Até hoje, hayekianos e austríacos mais clássicos brigam, porque Hayek interpretou Menger mal, a visão de Menger não era próxima do Teorema da Regressão, mas próxima do Alchian, e a sua moeda não era 100% endógena (isso é evidente em "Geld"), ou seja, que para a moeda ter suas três características essenciais seria necessário um "validador" externo, que historicamente são sacerdotes ou ourives estatais (como os tesoureiros dos zigurates babilônicos).

Schumpeter foi tesoureiro de uma nobre família do Egito. Durante esse período fez a façanha de reduzir a conta desta família, enquanto engordava sua própria conta.

Keynes e Schumpeter já fizeram swing, e Wittgenstein foi na lua de mel de Keynes, há rumores com Ramsey, e há um [artigo](https://jstor.org/stable/3217460) que surfa um pouco nessa questão.
