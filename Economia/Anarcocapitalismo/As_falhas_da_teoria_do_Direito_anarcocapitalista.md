# As falhas da teoria do Direito anarcocapitalista
A teoria da justiça privada anarcocapitalista é muito deficiente. Como se processarão crimes se o acusado não concordar com o tribunal privado? Eles poderão citar um exemplo da idade média ou um livro de Rothbard ou Hayek, que são muito mais de filosofia do que de criminologia, que basicamente invertem o ônus da prova para o suposto agressor, ou gente tipo Walter Kaufman, que falam até em escravidão como pena.

Repetem basicamente sistema da idade média, tentando incluir um conceito de capitalismo pueril no conjunto. Nenhuma proposta se sustenta, mesmo com toda boa vontade, em uma sociedade complexa com alguns milhares de habitantes, quanto mais com vários milhões.

Esses sistemas não conseguem dar uma resposta eficiente para questões básicas envolvendo legitimidade e competência, por exemplo. Um exemplo que vemos todo dia na nossa sociedade:
- B furta um objeto de A, na área X. 
- 'A' pretende processar 'B' para ser ressarcido e penalizá-lo. 
- 'A' é cliente de um tribunal e 'B' de outro.

Qual vai julgar? A contrata uma força policial executiva, B contrata outra, qual tem legitimidade para agir? Portanto, as perguntas a serem respondidas são:
1. Quem determina o tribunal? Qual o limite de atuação do tribunal?
2. Quem executa a pena? Se A e B contratam seguranças diferentes, elas lutam entre si? Qual a legítima?
3. Qual o sistema jurídico que será utilizado?

Uma resposta comum seria uma analogia entre crimes internacionais. Porém, a analogia é falha, pois em crimes internacionais não existe competição por jurisdição, cada Estado possuí legitimidade para processar e julgar. Pode haver conflitos de competência, mas não depende da escolha do acusado.

Repare, essa resposta é uma contradição, se baseia no Direito do Estado em sua forma mais concreta, ao que os anarcocapitalistas abominam em todas as suas formas.

Mas se não depende da escolha do acusado ou acusador, então depende do quê? Do tribunal contratado? Qual a legitimidade de um tribunal contratado por A para impor algo para B? E se, ignorando a legitimidade, houver um conflito positivo de competência, se os dois tribunais se considerarem competentes para o julgamento? Não faz nenhum sentido, um país tem legitimidade para impor sua jurisdição sobre os jurisdicionados e o local do crime - na maioria das vezes - define quem é competente. Se não houver um órgão centralizador, nada impede que dois tribunais se digam competentes para julgar.

Não existe administração de justiça sem responder de quem é a jurisdição, essa é a função inicial do Estado (antes do Estado moderno). Quando diz-se que a resposta é a dos direitos internacionais públicos, está legitimando o Estado, porque é com base no contrato social que ele pode definir esse tipo de regra.

Ou seja, não existe analogia entre Direito Internacional se não aceitar o pacto social. Os Estados fazem acordos entre si e impõe aos cidadãos, isso não depende da aceitação de ninguém. E mesmo que os tribunais de A e B fizessem esse acordo, A e B poderiam rescindir seus contratos.
