# Islândia medieval e o anarcocapitalismo
Alguns anarcocapitalistas afirmam que a Islândia medieval era o exemplo empírico de um "quase ancapistão", ou mais conscientemente, de que era um ótimo exemplo de lei policêntrica com uma justiça privada e descentralizada. Será que é mesmo? Vamos analisar primeiramente o que era a Islândia medieval.

A Islândia era uma ilha que ninguém queria invadir. Sem demanda. Em qualquer local com maior demanda e recursos, o exemplo já não vale mais. Houve inúmeras guerras e todo tipo de conflitos externos e internos por poder e recursos.

O que reinava na Islândia era o Althing (que era derivado da lei comum germânica), que foi um governo com parlamento, eleições, partidos, etc, um dos mais antigos por sinal. As [leis](https://en.wikipedia.org/wiki/Úlfljótr) vieram da Noruega, e privilegiaram certos escandinavos. O cristianismo virou obrigatório por lei forçada ao povo.

Em uma época tão arcaica e medieval, numa ilha isolada com pouca população, onde terras foram "achadas livres", só existiam fazendeiros (donos) e o resto era escravo. Nada de relações capitalistas. Por atividades descentralizadas e as terras serem livres sem precisarem comprar, poderíamos até mesmo chamar de comunismo (pelo menos um comunismo primitivo) ou anarquismo, exceto pelo fato de existência de escravos e disputas econômicas privadas constantes.

Há a ilusão fantasiosa dos anarcocapitalistas pelo uso de exemplos de terras que eram vazias e gratuitas, ou então invadidas. Épocas medievais e rudimentares. Ou seja, se não achar uma terra livre e gratuita como o caso da Islândia, já invalida a viabilidade de tudo. O período já se iniciou não só com terras livres e gratuitas (que é insustentável num mundo moderno), mas também com escravos já trazidos de fora desde o início.

Eles endeusam os godar chieftains, mas minimizam o fato que os próprios foram quem "por mérito inicial" acabaram criando situações de conflitos até chegar no oligopólio, falta de competição e coerção.

Os godar chieftains acabaram tendo um poder maior de coerção onde todo mundo dependia deles. Não era nada voluntário, mas era  uma influência sob "medo e ganância", e acabou como qualquer coisa acaba num sistema "livre" com incentivos a ganhos privados: com muita coerção, monopólio, dominação, leis punitivas, conflitos, tudo pelos próprios chieftains, que enfraqueceram a região como um todo, até que voluntáriamente ou não, o povo decidiu pela proteção, leis, ordem e incentivos de se submeterem a monarquia norueguesa.
>Os anos anteriores à assinatura do acordo foram marcados por conflitos civis na Islândia (a chamada Era dos Sturlungs), quando o rei norueguês tentava exercer sua influência por meio dos clãs da família islandesa, principalmente os Sturlungs. Gissur Þorvaldsson, um vassalo do rei, trabalhou como seu agente no assunto.
>
>De acordo com as disposições do acordo, os islandeses deveriam pagar impostos do rei norueguês, mas em troca eles deveriam receber um código de leis, garantia de paz e transporte confiável e transporte marítimo entre a Noruega e a Islândia. Noruegueses e islandeses receberam direitos iguais nos países uns dos outros. As leis da Comunidade islandesa foram atualizadas e um livro de leis chamado Jónsbók foi publicado em 1281. Sob o domínio norueguês, os laços comerciais entre os dois países aumentaram e o assentamento da Islândia se expandiu.
>
>Várias explicações possíveis foram oferecidas para a sucumbência dos chefes islandeses à coroa norueguesa:<br/>
>- Eles estavam cansados da guerra e acreditavam que uma aliança com o Rei levaria a uma paz duradoura.<br/>
>- O medo de que o rei embargasse a Islândia, a menos que eles jurassem lealdade a ele.
>- O apoio da Igreja à causa do Rei de anexar a Islândia.<br/>
>- Chefes islandeses fazendo acordos com o rei para anexar a Islândia em troca de servir como seus cortesãos.<br/>
>- Chefes islandeses rendendo seus chefes na esperança de que eles logo os governassem como feudos.<br/>
>- Os islandeses não conheciam as idéias de soberania e não aderiam aos tipos modernos de nacionalismo.<br/>
>- O poder real era uma força política muito mais forte do que a Comunidade da Islândia.<br/>

Godar é um sistema hierárquico de justiça centralizada. Byock, a quem Friedman faz referência, descreve um sistema único e central de 36 juízes nomeados pelos chefes locais. Ele diz que era mais lateral, menos hierárquico, do que a igreja medieval e o sistema feudal, mas que era basicamente um sistema republicano ou representativo, em que o povo primeiro escolheu os chefes, então os chefes nomearam esta "corte suprema" de juízes. Além disso, uma das instituições mais populares da antiga lei da Islândia era o duelo. Os duelos foram eventualmente banidos, também um movimento de cima para baixo por um chefe que tinha muito poder. Foi nessa época (cerca de 1000-1100) que o sistema central de juízes e árbitros privados começou a surgir. No entanto, eles também foram amplamente impopulares em comparação com o duelo e muito controversos, porque as pessoas simplesmente comprariam o depoimento de testemunhas. No que diz respeito ao "melhor exemplo de funcionamento" da lei policêntrica, a maneira como Byock a descreve soa como se fosse na verdade bastante corrupta e pouco funcional, a ponto de as pessoas preferirem duelos ou julgamento por provação.

Em última instância, a única coisa que diferia a Islândia medieval dos outros estados feudais era que o Althing era o meio pelo qual os godi resolviam seus problemas.

O uso privado e individualista de tudo causou a extinção macro de recursos usados de forma nada estratégica quando vista de forma macro como um todo.
>Na época dos primeiros colonos, suspeita-se que aproximadamente 40% da Islândia era coberta por florestas naturais de madeira de bétula. Essa porcentagem foi rapidamente esgotada pelos recém-chegados, que rapidamente utilizaram o material para construir navios, casas e fazendas.
>
>Árvores que não eram usadas para construção eram queimadas para se aquecer. Em um século, acredita-se que a Islândia foi totalmente desmatada. Isso teria consequências sobre a arabilidade do solo que perdura até os dias de hoje.

Apesar do que pensam os anarcocapitalistas, havia sim impostos, propriedade pública, a agricultura era baseada no sistema de bens comuns (a terra agrícola real não era propriedade, mas era compartilhada publicamente, produzida nas aldeias e distribuída), regulamentação do mercado e algo parecido com o bolsa-família no Brasil.

Há um [artigo](https://tandfonline.com/doi/abs/10.1016/0304-4181%2882%2990015-X) sobre o sistema de "previdência social" da Islândia medieval.

O Hreppur também existia na época, a propriedade pública mais antiga da história da Islândia, uma herança dos colonos Vikings.
