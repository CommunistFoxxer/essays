# Respostas às críticas dos estudos empíricos
Os economistas franceses Jonathan Nitzan e Shimshon Bichler [criticaram](http://bnarchives.yorku.ca/308/2/20101200_cockshott_nitzan_bichler_testing_the_ltv_exchange_web.htm) o trabalho de Cockshott e Cottrell sobre a TVT, argumentando que eles tinham meramente correlacionado dois valores monetários, em vez de examinar o tempo de trabalho abstrato como tal. Eles também argumentaram que a correlação entre o tempo de trabalho e o preço de mercado é espúria, com base no uso indevido da análise de regressão. Cockshott e Cottrell responderam a esses argumentos em um [artigo](https://ideas.repec.org/a/zbw/espost/157802.html) de 2014, no qual argumentam que:
>As contas salariais setoriais fornecem uma proxy razoável para o tempo de trabalho socialmente necessário de Marx... os números do tempo de trabalho estão disponíveis para alguns países e foram usados na literatura, embora também seja possível retirar dados de tempo de trabalho das folhas de pagamento, dados os salários médios por setor.

Eles também argumentam que Nitzan e Bichler estão errados ao afirmar que a correlação entre tempo de trabalho e preço de mercado é meramente espúria, fornecendo uma crítica da análise estatística usada para chegar a esta conclusão:
>A correlação supostamente correta é de fato inválida, quebrando as regras da análise dimensional, enquanto uma boa quantidade de evidências auxiliares apóiam a validade da descoberta de uma relação próxima entre preços e valores - uma descoberta que também pode ser expressa sem recurso à correlação. Interessantes questões em aberto permanecem sobre a relação preço-valor, mas se é um caso de correlação espúria não é uma delas.

Outra crítica ao trabalho de Cockshott e Cottrell foi feita pelo marxista Andrew Kliman. Em um [artigo](https://academic.oup.com/cje/article-abstract/26/3/299/1711462) de 2002 no Cambridge Journal of Economics, Kliman afirma que:
>Depois de controlar as variações no tamanho da indústria que produzem ‘correlação espúria’, não encontro nenhuma evidência confiável de que os valores relativos tenham qualquer influência sobre os preços relativos.

Ele continua:
>Os valores acabam não sendo melhores preditores de preços do que qualquer outra variável aleatória com a mesma distribuição de probabilidade.

Cockshott e Cottrell responderam em um [artigo](https://academic.oup.com/cje/article-abstract/29/2/309/1704978) de 2005, também no Cambridge Journal of Economics, no qual argumentaram que:
>As técnicas de correção estatística de Kliman envolvem dividir pelo sinal para deixar o ruído.

Como eles dizem:
>Os resultados da simulação de Kliman - que parecem ser prejudiciais à ideia de uma correlação substancial de preço e valor entre as indústrias - podem ser replicados e explicados dentro de nossa estrutura. A correlação em questão desaparece sob a escala ou deflação de preço e valor de Kliman, porque ele está, na verdade, "dividindo pelos dados", sendo os custos da indústria, na prática, os dados de origem para o cálculo dos valores da indústria.

Para aqueles que estão interessados, uma resposta adicional de Kliman pode ser encontrada [aqui](https://academic.oup.com/cje/article-abstract/29/2/317/1704980). Em contraste com o seu trabalho e com o de Cockshott e Cottrell, um [artigo](https://www.tandfonline.com/doi/abs/10.2753/PKE0160-3477280209) de 2005 no Journal of Post-Keynesian Economics argumenta que todo o argumento é impossível de julgar, devido à dificuldade de testar empiricamente a correlação entre o valor-trabalho e o preço de mercado. Os autores observam que:
>Os resultados de Kliman estão em nítido contraste com os de toda a literatura sobre testes empíricos da teoria do valor-trabalho.

No entanto, eles afirmam que:
>Todas as medidas empíricas da correlação preço-valor - e, portanto, todos os resultados estatísticos derivados de tais medidas, incluindo o de Kliman - não têm sentido.

Deixo ao leitor a decisão de quem venceu os debates mencionados; de qualquer forma, é importante que todos os marxistas levem em consideração os dois lados da questão, de modo que, qualquer que seja nossa opinião, estejamos bem versados na evidência empírica, bem como nas considerações teóricas relevantes.
