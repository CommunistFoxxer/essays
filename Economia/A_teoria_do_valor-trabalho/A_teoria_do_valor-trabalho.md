# A teoria do valor-trabalho
O papel de qualquer teoria do valor é entender e definir o que determina o preço das commodities no mercado. Para tanto, tentaremos analisar o que é exatamente que dá valor aos produtos que vendemos.

Marx seguiu uma longa tradição de economistas (Adam Smith e David Ricardo sendo suas maiores inspirações) onde se distinguia o preço de valor, pois, você poderia estar pagando por algo bem mais do que esta coisa vale. O trabalho gera algo que tem valor. Trabalho é qualquer atividade humana, no entanto, ele tem que gerar algo que tem utilidade - i.e, valor-uso (utilidade, que é subjetivo e qualitativo) - para sustentar a reprodução social humana. **Então você ir para uma floresta e dançar com um machado não refuta a TVT.**

Mas o que é o valor em si? O valor é algo que:
1) Tem valor-uso (utilidade, que é subjetiva).
2) Foi feito com trabalho.

Ou seja, o pão tem um valor.

Uma característica do valor é ter um valor-troca (que é o aspecto quantitativo do valor), que é uma expressão do valor.

Existem valores-uso (algo com utilidade) que não têm valor (ou seja, que não foi feito com trabalho)? Sim, é o caso da terra. Então como a terra não tem valor, é impossível haver um valor-troca que expresse aquele valor. Portanto, seu valor-troca (e consequentemente o preço) é puramente renda capitalizada, então terra não é capital, e seu preço não é transferido para o valor final das mercadorias, como acontece com o capital constante.

O Tempo Socialmente Necessário de Trabalho (TSNT), segundo a TVT, é a medida do valor, a quantidade de valor. Isso significa que **o TSNT não é valor**. Marx dedicou o capítulo 1 inteiro d'O Capital para destruir este mito. Mas qual a diferença? Imagine que eu pergunte para um físico o que é gravidade e ele responda `G . M . m / r²`, mas isso não explica o que é a gravidade, apenas como medí-la, Newton não explicou o que era a gravidade e nem como ela ocorria, sobrou então para Einstein.

Outra questão é que Marx realiza uma operação bem conhecida de reduzir os valores de troca das mercadorias a algo que é comum a elas em relação ao qual representam um mais ou um menos, e conclui que “se deixarmos de lado o valor de uso do corpo das mercadorias, apenas uma propriedade lhes subtrairá: a de serem produtos do trabalho” (p. 46; edição do século XXI). E mais tarde afirma que não se trata de um determinado trabalho produtivo, mas de um “trabalho humano indiferenciado”, ou seja, “trabalho abstratamente humano” (p. 47).

Marx explica filosoficamente a dualidade de "valor-uso" e "valor": a dualidade do trabalho. Por um lado o trabalho concretiza algo útil, concreto, um valor-uso, e por outro lado, ele dispende energia, músculos, gera valor.

Uma crítica comum é que eu posso costurar uma roupa em 10 horas e a vizinha pode costurar em 1000 horas, mas venderemos o mesmo preço. Mas o tempo socialmente necessário de trabalho é uma média. Logo a minha vizinha terá que se adequar ao tempo socialmente necessário de trabalho, caso contrário, ela perderá clientes.

Como valor é medido pela quantidade de trabalho socialmente necessário, quanto mais eficiente for uma sociedade capitalista, menos seus produtos irão valer, isso é uma contradição fulcral do capitalismo, chamado de TRPF (Tendency of the Rate of Profit to Fall).

E a arma que matou Van Gogh? Ela refuta as leis da TVT? Não, porque as leis da TVT se aplicam somente à mercadorias (produtos que têm o mesmo valor que semelhantes). Marx analisará como definir leis para esta arma no volume II d'O Capital, sobre equalização de preços.

E a torta de lama? Quem comeria ela? Note, ela não tem valor-uso, então as leis observadas por Marx não cabem a ela.

E o paradoxo do diamante? Simples: ele é escasso, e diamante é difícil de ser processado, leva muito trabalho. Trechos do livro de David Ricardo:
>Adam Smith observou que
>>“a palavra valor tem dois significados diferentes, expressando, algumas vezes, a utilidade de algum objeto particular, e, outras vezes, o poder de comprar outros bens, conferido pela posse daquele objeto. O primeiro pode ser chamado valor de uso; o outro, valor de troca. As coisas que têm maior valor de uso”, continua ele, “têm frequentemente pequeno ou nenhum valor de troca; e, ao contrário, as que têm maior valor de troca têm pequeno ou nenhum valor de uso”.
>
>A água e o ar são extremamente úteis; são, de fato, indispensáveis à existência, embora, em circunstâncias normais, nada se possa obter em troca deles. O ouro, ao contrário, embora de pouca utilidade em comparação com o ar ou com a água, poderá ser trocado por uma grande quantidade de outros bens.<br/>
>A utilidade, portanto, não é a medida do valor de troca, embora lhe seja absolutamente essencial. Se um bem não fosse de um certo modo útil — em outras palavras, se não pudesse contribuir de alguma maneira para a nossa satisfação —, seria destituído de valor de troca, por mais escasso que pudesse ser, ou fosse qual fosse a quantidade de trabalho necessária para produzi-lo.<br/>
>Possuindo utilidade, as mercadorias derivam seu valor de troca de duas fontes: de sua escassez e da quantidade de trabalho necessária para obtê-las.<br/>
>Algumas mercadorias têm seu valor determinado somente pela escassez. Nenhum trabalho pode aumentar a quantidade de tais bens, e, portanto, seu valor não pode ser reduzido pelo aumento da oferta. Algumas estátuas e quadros famosos, livros e moedas raras, vinhos de qualidade peculiar, que só podem ser feitos com uvas cultivadas em terras especiais das quais existe uma quantidade muito limitada, são todos desta espécie. Seu valor é totalmente independente da quantidade de trabalho originalmente necessária para produzi-los, e oscila com a modificação da riqueza e das preferências daqueles que desejam possuí-los.

E o paradoxo da água no deserto? Simples: levar água para o deserto custa bastante trabalho, pois não há água por perto. O valor varia em cada lugar, isso se chama custo de conveniência local. Fora que a TVT não nega a oferta e demanda, mas não reduz tudo a ela. A oferta e demanda flutua os preços a partir da base que é o valor.

Lembrando que o preço é somente uma expressão da forma-valor.

Marx também refutou a Lei de Say com os esquemas de reprodução e sua fórmula geral.

![](images/schemes_of_reproduction_marx.jpg)

E por último, Marx não acreditava no mito do escambo, ele apenas concluiu dedutivamente como o valor era formado através de um leilão de multi-iteração, similar à teoria dos jogos moderna.

E para analisar a TVT, Marx também analisa fenômenos assim gerados por ela: exploração do trabalho abstrato, alienação do trabalho e fetichismo da mercadoria.

Marx analisa primeiramente o valor-trabalho sob uma ótica até "pseudocientífica" (apriorismo - prova por necessidade lógica) nos primeiros capítulos, ele toma algumas suposições para que possa construir a lei do valor de baixo para cima (individualismo metodológico). Mas logo nos próximos capítulos, abandona esta metodologia, então o valor de troca e valor de uso posteriormente já não aparecem metafisicamente.

O livro do Rubin sobre teoria do valor-trabalho é muito bom, ele foi considerado o maior intérprete da TVT de Marx.

Linha do tempo:
Adam Smith - desenvolve a TVT<br/>
David Ricardo - adiciona o valor-uso, resolve o paradoxo do diamante, analisa o lucro e analisa empiricamente a validade da TVT<br/>
Karl Marx - resolve a questão ricardiana do valor ser tempo de trabalho socialmente necessário, analisa fenômenos empíricos da lei do valor (contradição fulcral do capitalismo), estuda porque propriedades naturais não devem ser levadas em conta, a redução ao fator comum, dualidade do valor (e outros desenvolvimentos filosóficos), resolve a questão ricardiana sobre preço ser baseado no valor (preço é baseado no valor-troca) e desenvolve os esquemas de reprodução do capital, partindo de um ponto de vista a longo-prazo (oferta)

Além de termos um desenvolvimento similar à Hayek em Grundrisse e nos escritos econômicos de Engels, porém os dois tinham uma visão historicamente determinada da ordem espontânea, ou seja, quando você mudava de sistema econômico, mudava-se também as leis econômicas. Recomendo o [artigo](https://www.independent.org/publications/tir/article.asp?id=1466) do professor Michael Munger mostrando que boa parte dos insights que os economistas modernos têm sobre instituições extrativistas, rent seeking, political cycles, captura institucional, fatores capital-trabalho, desemprego natural e efeitos pol da desigualdade podem ser encontrados em Marx. Em outro [artigo](https://voxeu.org/article/marx-and-modern-microeconomics?utm_source=dlvr.it), Bowles explora como Marx foi pioneiro no estudo de informações assimétricas e relações principal-agent, e não Hayek, inclusive, há [artigos](https://stumblingandmumbling.typepad.com/stumbling_and_mumbling/2012/11/marx-vs-coase-experimental-evidence.html) comparando a abordagem de contratos e relações principal-agent de Marx e Coase. Marx adiantou toda uma discussão sobre a não neutralidade da moeda, do progresso técnico e da lógica acumulativa do capital, além de resolver o problema da teoria do valor trabalho.

Mais coisas irão ser explicadas nas respostas às críticas neoclássicas e austríacas.

Fontes:
- [Game Theory through Examples | Auction](http://www.eprisner.de/MAT109/AnalysisAuction2.html)
- [Game Theory for Strategic Advantage](https://documentcloud.adobe.com/link/review?uri=urn:aaid:scds:US:a51dff25-09ed-49fd-8d18-19531022f966)
