# O complexo industrial carcerário americano
A função sistêmica primária do sistema de “justiça” criminal dos EUA é controlar e criminalizar os negros, pardos e pobres, ao mesmo tempo que atende aos interesses das corporações e dos ricos. Cada pessoa regularmente infringe as leis (consciente ou inconscientemente) - cerca de três vezes por dia, em média.

No entanto, as comunidades em particular e as leis específicas que são fortemente policiadas, processadas e criminalizadas seguem linhas altamente racistas e classistas. E as comunidades e leis que são fortemente policiadas, processadas e criminalizadas não são nem as mais mortais, nem as mais prejudiciais, nem o maior custo financeiro para a sociedade. O único traço comum é que eles controlam e criminalizam os negros, pardos e pobres.

Tomemos, por exemplo, o roubo de salários, pelo qual não queremos dizer o roubo inerente de salários equitativos ao capitalismo. Está falhando em pagar até mesmo o mínimo pateticamente exigido pelas leis trabalhistas.

Roubo de salário é o maior tipo de roubo: maior do que todos os roubos de rua, banco e loja juntos! Então, literalmente, a maior categoria de roubo, conduzida por CEOs e executivos principalmente contra a classe trabalhadora, não justifica policiamento ou criminalização.

Seus bairros não são policiados de maneira desproporcional. Não há mandados de segurança para esses "criminosos". E os banqueiros que fraudaram ilegalmente milhões de pessoas, fazendo com que 10 milhões de pessoas perdessem suas casas? Nenhum acabou na prisão. A maior categoria individual de fraude fiscal é o 1%, custando ao governo bilhões em prejuízos. Isso se soma às milhares de brechas fiscais legais que só beneficiam os ricos. Essa fraude fiscal ilegal é amplamente não-policiada.

As corporações regularmente violam as já fracas leis ambientais, causando danos incomensuráveis ​​à saúde, à vida e ao meio ambiente em nossas comunidades e no planeta. Quando foi a última vez que você viu a polícia arrombar a porta de uma dessas empresas e começar a prender os responsáveis?

Existem numerosos outros exemplos de crimes cometidos por empresas e ricos, onde as vítimas são principalmente os pobres e a classe trabalhadora - e há um claro viés sistêmico de não policiamento e não criminalização desses 'crimes'. A 'justiça' O sistema também é altamente racializado com comunidades de cor desproporcionalmente policiadas, desproporcionalmente presas e desproporcionalmente processadas por crimes desproporcionalmente menores. Lembre-se, a pessoa média quebra três leis por dia.

Cada camada da ‘guerra contra as drogas’ é altamente racial: desde os tipos de drogas com as penas mais severas até os bairros visados. Em bairros ricos e brancos, o consumo de drogas é tratado como um problema de saúde. Em bairros de pessoas de cor isso é tratado como crime.

Por exemplo, o uso de maconha é quase o mesmo em todas as linhas raciais. No entanto, cada nível do sistema de 'justiça' trata o uso de maconha pelos afro-americanos de maneira radicalmente diferente: taxas de detenção mais altas, taxas de acusação mais altas e taxas de encarceramento mais altas.

Enquanto as corporações e os ricos transportam trilhões de capitais através das fronteiras para paraísos fiscais ilegais (resultando em trilhões em receitas fiscais perdidas), esses "criminosos" brindam suas mimosas sem se preocupar com as repercussões, ao mesmo tempo que o ICE aterroriza e traumatiza as comunidades de imigrantes. Os problemas do sistema de "justiça" criminal não se referem a algumas "maçãs podres". Os problemas são a criminalização sistêmica de negros, pardos e pobres, ao mesmo tempo que atende aos interesses das corporações e dos ricos.
