# Contribuições de Mao
Antes de Mao, o consenso da maioria dos exércitos era de que toda campanha de guerra deveria durar o menor tempo possível, e ser o mais violenta possível. Mao ressucitou métodos antigos de guerra e adaptou à realidade dele. Uma combinação de táticas de guerra defensiva com guerra de atrito.

A União Soviética usou um sistema inspirado nesse durante a Segunda Guerra durante algum tempo.
