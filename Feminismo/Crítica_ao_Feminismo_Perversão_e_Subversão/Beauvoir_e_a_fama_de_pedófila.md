# Beauvoir e a fama de pedófila
Como já falei dos relacionamentos de Beauvoir com todas as alunas que a Campagnolo citou no livro, agora falarei sobre como a Campagnolo contextualiza como Beauvoir ganhou fama de pedofilia (página 172):
>Aconteceu em 1977, quando o Tribunal de Versailles acusou três homens, por atentado ao pudor contra menores de 15 anos e os condenou a três anos de prisão. Logo depois... "Uma petição pera a libertação dos três criminosos foi assinada por 69 intelectuais e publicada no jornal Le Monde".

De fato houve uma petição assinada por 69 intelectuais, nessas assinaturas estavam Beauvoir e Sartre, entretanto a petição era para definir uma idade judicial para consenso sexual, Beuavoir queria reduzir a idade de consenso para 13 anos, essa petição não foi para libertar os criminosos da prisão.

Além do mais, é normal não apoiarmos essa atitude de Beauvoir, mas isso não a torna pedófila, para acusá-la de pedofilia, é necessário que haja fatos e embasamentos históricos de que ela de fato abusou de alguém - o que historicamente falando, não tem.
