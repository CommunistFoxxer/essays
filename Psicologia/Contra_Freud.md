# Contra Freud
Freud [falsificou](https://www.revistadelibros.com/freud-era-un-mentiroso-un-estudio-de-han-israls/) o resultado de várias de suas aplicações clínicas. Ele descobriu o inconsciente e desenvolveu conceitos fundamentais, apenas falsificou a natureza produtiva do inconsciente ao colocar nele a imagem do teatro, de uma peça de representação que se repete sempre com os mesmos atores, embora sob papéis diferentes.

Deleuze e Guattari puderam ver que o inconsciente nada tem a ver com o teatro dos Filhos Edipizados, do Papa-Mama e do Espírito Santo (muito dialético), mas que se tratava de uma fábrica: produção desejante.

Pois bem, surge a pulsão de morte (Thanatos) ao nível da espécie e espera explicar até a Guerra Mundial desta forma (idealismo). Ele também é muito pessimista, fala mal do marxismo por sua concepção ingênua "otimista" do ser humano e vê a revolução como algo inútil. Dá para perceber a influência de Schopenhauer e de sua alergia positivista por tudo o que é revolucionário. Ele se contenta com tudo se desenvolvendo "normalmente" (sexualidade, civilização) para dar-lhes uma boa saída e aliviar as neuroses por meio da sublimação (pelas atividades intelectuais).

É mais o fato de colocar o instinto de morte como algo mais original do que o instinto de vida. Nas palavras nietzschianas, é como se o ressentimento fosse o primeiro princípio vital, como se o homem reativo fosse o próprio homem (quando não passa de um resultado histórico).

Na verdade, ele não bebeu de Nietzsche, ele o descobriu no final da vida, apenas disse que [havia chegado a conclusões semelhantes](http://www.scielo.org.co/pdf/ccso/v15n29/v15n29a13.pdf) (mas no fundo a semelhança tem mais a ver com a formação schopenhaueriana e dialética de cada um).

Freud é fascinado por Nietzsche, mas valoriza a realidade de uma maneira radicalmente diferente. Freud é um positivista que deseja fazer da psicologia uma ciência estrita; Nietzsche denuncia as ciências do século 19 por sua tendência de ver a vida do lado reativo.

Minha tese: a psicanálise é uma dialética hegeliana sem a parte revolucionária.
