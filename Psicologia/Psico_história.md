# Psico-história
A história é uma narrativa baseada numa evidência, que você pode construir interpretando as relações que ocorreram entre o corpo do gato e o ambiente, e essa interpretação depende do seu conhecimento e condicionamentos prévios, no momento da análise e construção da narrativa.

Você pode argumentar comigo que, devido o problema de pessoas diferentes poderem interpretar um mesmo acontecimento de formas diferentes, que cada uma delas pode escrever uma história diferente do gato, digamos assim, mas você há de concordar comigo que a maioria das pessoas é forçada à concordar com um certo aproximado conjunto de condicionamentos se elas compartilharem da mesma cultura, e que a cultura é uma entidade que evolui como uma resposta do substrato biológico humano operando em grupo frente à realidade material. Ou seja, a cultura é o resultado de uma evolução, e graças à esse resultado do qual você faz parte, você pode interpretar acontecimentos em um vídeo e construir uma história

Eu também posso argumentar que você pode escolher ignorar certos aspectos dos acontecimentos e buscar princípios de interpretação que você possa utilizar em uma análise para derivar previsões, ou seja, você pode derivar um método de construir histórias utilizando o método científico para selecionar quais aspectos dos acontecimentos devemos levar em conta para fazer previsões verificáveis.

>Para a esmagadora maioria dos gatos que filmamos, descobrimos que aqueles gatos que podiam brincar com caixas de papelão regularmente demonstravam menos estresse e maior sociabilidade com seres humanos de fora do círculo familiar.

Essa conclusão é totalmente verificável experimentalmente. Se você pode construir uma história verificável, quer dizer que existem aspectos da realidade que se impõem nas histórias que você pode fazer ao observar o que acontece nela.

Quer dizer que você pode anular aspectos subjetivos da narrativa e compartilhar uma história com pessoas de outras culturas, se elas se disporem à assumir um espírito de aprendizado, quer dizer que uma história pode carregar um caráter concreto.

Se uma história pode carregar um caráter concreto, quer dizer que você pode achar uma explicação objetiva para os traumas de um gato e estimar o erro da sua história, para procurar por fatores que você não considerou antes. Você pode estimar os conteúdos mentais de uma pessoa com base em uma história. Você pode estimar comportamentos de uma pessoa com base em uma história.
