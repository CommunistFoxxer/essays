# Método universal externo e Spinoza
Para se ter um método externo como universal, ele teria que ter a realidade como um todo como objeto, ou seja, para isso ser verdade, teria que haver uma ciência de tudo que existe, o que não existe. Não existe método separado do objeto, caso contrário, poderia-se fazer sociologia como física e como lógica, uma vez que são objetos diferentes. Se existe um método em geral, ou seja universal, ele está fora do objeto, porque ele é universal.

E Spinoza tem um argumento muito engenhoso sobre o porquê de não ser necessário encontrar um método que determine o Método (de conhecimento das coisas). Ele começa fazendo uma analogia: quando falamos de instrumentos humanos, por exemplo, um martelo é usado para fabricar outras coisas. Mas para fabricar um martelo, precisamos de um outro martelo. E assim você teria um processo infinito. O ciclo se encerra porque podemos inicialmente fazer instrumentos rudimentares com nossas próprias mãos ("instrumentos nativos"). O intelecto tem também uma análoga "força nativa" em que ele faz para si elementos intelectuais, e com eles faz obras intelectuais mais elaboradas.

Para explicar isso, ele distingue uma ideia do seu ideado. Quando temos um círculo, por exemplo, sabemos que o círculo tem a propriedade de que todos os pontos que pertencem a ele serem equidistantes de um ponto que seria o seu centro. Mas a «ideia de círculo» não tem essa propriedade (ela nem é um conjunto de pontos), sim uma ideia. Ele elucida isso com dois conceitos: essência formal e essência objetiva. Para o exemplo do círculo, a ideia de círculo é a essência objetiva do círculo.

A essência do círculo está presente no círculo, mas não é o próprio círculo. A essência do círculo só está formalmente no círculo. A ideia é objetivamente a própria essência do círculo: ela tem objetivamente em si o que o círculo tem formalmente. Ou seja, a essência formal do círculo é o objeto da sua essência objetiva (ideia).

O ponto é que, assim como um círculo tem existência objetiva (é real, pelo menos enquanto objeto matemático, mas aqui poderia funcionar para coisas corpóreas também), a ideia de círculo também tem, enquanto é uma ideia que existe. Então, por causa disso, ela pode ser objeto de uma outra ideia (a ideia da ideia de círculo). E a ideia da ideia de círculo tem objetivamente o que a ideia de círculo tem formalmente.

Você pode repetir isso infinitamente tendo a ideia da ideia da ideia de círculo, etc.

O ponto aqui é que para que eu inteligir a essência do círculo, eu não preciso inteligir antes a ideia de círculo, muito menos a ideia da ideia de círculo. O que acontece é exatamente o contrário. Para eu saber a essência da ideia de círculo (a ideia da ideia), eu preciso antes saber a essência do círculo (a ideia). "Para eu saber que sei, preciso antes saber".

E é aí que entra o Método. Ele é a via para que as próprias essências objetivas sejam buscadas ordenadamente. O Método é o conhecimento reflexivo (a ideia da ideia), e serve para distinguir uma ideia verdadeira. Assim não se tem um Método sem uma ideia, porque não se tem ideia da ideia sem ter uma ideia.

Com isso a gente já tem uma parte do método: basicamente, deve existir em nós uma verdade que seja inata, e ao ser inteligida, inteligimos também a diferença entre essa verdade e as outras percepções. Sabendo a verdade dessa percepção, a gente coibe as outras que não sabemos, e com ela temos a nossa potência de inteligir, e o resto do método vai consistir em trazer regras certas para que as outras coisas sejam conhecida conforme a norma da ideia verdadeira.

Essa é uma das justificativas para a dialética, que tanto resolve o problema dos universais (já que a dialética é um método interno inerente de cada coisa) e é justificado pelo argumento spinozista pela necessidade de seguir o próprio automovimento das coisas.
