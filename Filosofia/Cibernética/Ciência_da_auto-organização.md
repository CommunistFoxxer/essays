# Ciência da auto-organização
A ideia de auto-organização é que para que haja organização (associações, ligações, combinações, comunicação, etc), é preciso que haja interações: para que haja interações, é preciso que haja encontros, para que haja encontros, é preciso que haja desordem (agitação, turbulência).

A ordem e a organização são inconcebíveis sem interações. Nenhum corpo, nenhum objeto, pode ser concebido fora das interações que o constituíram e das interações nas quais participa necessariamente.

Toda interação gera desordem, estrago, turbulência, desperdício, estrago, irreversibilidade. Há mais dispersão produzida nos encontros do que organização, por isso o universo é tão criativo. A organização não é o contrário da desordem, eis a lei da entropia.

Prigogine em 1977 recebeu um Nobel de química por seu trabalho em 1955, sobre estruturas dissipativas. A questão que Prigogine queria atacar era a ideia da termodinâmica que o universo é somente desordem e que caminharia continuamente para a desordem, o que não explica as 500 milhões de galáxias organizadas no universo (não negando que um dia elas possam criar desordem, mas da catástrofe ela nasceu e na catástrofe ela morrerá). Prigogine descobriu que da desordem [isto é, do calor dissipado] de sistemas químicos, nasce uma nova estrutura. Não podíamos mais falar da desordem sem falar da ordem e do nascimento de uma nova ordem por meio da desordem.

Prigogine faz uma comparação de seu trabalho com a convecção de Ryleigh-Bénard. Quando aquecemos líquidos em pequenas temperaturas, o calor é transmitido pela condução térmica. Quando aquecemos líquidos em grandes temperaturas, o calor é transmitido por convecção térmica. A convecção térmica acontece quando o calor se organiza de forma crescente ou decrescente no sistema, e a convecção natural ocorre quando o calor se auto-organiza, sem influência externa (como bolhas e padrões formados em sopas). Bénard observa que convecções naturais formam padrões regulares [isto é, auto-organizados] por causa do calor [ou seja, da entropia - isto é, da dissipação do calor -, surge a ordem].

Prigogine também compara sua teoria com os padrões de Turing, que afirma que ao espalhar 2 substâncias químicas (que formam formas - morfogênicos) em um sistema, uma delas se tornará a ativadora (dominante) e a outra se tornará a inibidora, formando assim os padrões [ordem] naturais.

Em 1959, em seu livro "A order from noise principle", Foerster mostra que a auto-organização é construída da própria desordem e adiciona a ideia de não-linearidade ao conceito de sistema (superaditividade).

Na década de 70, René Thom descobre que toda forma nasce de uma catástrofe.

Em 2021, Parisi ganhou um Nobel por seu trabalho de 1979, onde mostra que sistemas atômicos cujos spins podem ficar "frustrados" por não acharem um par ideal (vidros de spin), tendem mesmo assim a se organizar, encontrando a segunda melhor posição. O que parecia ser desordenado e aleatório, era na verdade completamente ordenado e tinha um padrão oculto. Trabalhos anteriores também afirmam que tudo tem um padrão oculto, conferido pela teoria algoritmica da informação de Chaitin e pela mecânica quântica deBroglie-Bohm.

Atlan desenvolve o princípio do acaso organizador e introduz a noção de organização no conceito de sistema.
