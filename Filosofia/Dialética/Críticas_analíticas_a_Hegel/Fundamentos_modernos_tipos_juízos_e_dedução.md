# Fundamentos modernos: tipos, juízos e dedução
A filosofia analítica estava certa ao pedir uma base formal de raciocínio filosófico, a fim de tornar possível o progresso intelectual genuíno. Naquela época, a lógica de predicados e a teoria dos conjuntos eram o estado da arte em relação aos fundamentos da matemática e, portanto, foi isso que a filosofia analítica adotou e desenvolveu. Mas, desde então, houve muito progresso nos fundamentos da matemática, como teoria dos tipos intuicionista, teoria dos tipos de homotopia, etc.

Consequentemente, o ponto de partida original da filosofia analítica precisa ser reexaminado. Embora a lógica de predicado não tenha como dar um sentido formal à "Lógica" de Hegel, para a teoria dos tipos a situação é bastante diferente.
