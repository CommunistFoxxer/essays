# Determinações formais do Ser
Usando isso e com base na tese de Lawvere, somos levados a investigar objetivamente o seguinte:

Começando com a teoria dos tipos de homotopia nua e adicionando modalidades adjuntas a ela, qual é a natureza das novas proposições que podem ser provadas com essas modalidades, portanto, qual é a nova “qualidade” dos tipos na presença desses novos axiomas.
| Ser | Existência | Realidade |
|---  |---         |---        |
|     |            | $`\Re`$   |
|     |            | ⊥         |
|     | ∫          | $`\Im`$   |
| ∅   | ♭          | $`\&`$    |
| ⊥   | ⊥          |           |
| *   | ♯          |           |

Com um implicando o outro.
| Símbolo | Nome                              |
|---      |---                                |
| ∫       | modalidade de forma               |
| ♭       | modalidade plana                  |
| ♯       | modalidade aguda                  |
| $`\Re`$ | modalidade de redução             |
| ℑ       | modalidade de forma infinitesimal |
| $`\&`$  | modalidade plana infinitesimal    |
