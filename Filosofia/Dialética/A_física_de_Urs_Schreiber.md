# A física de Urs Schreiber
Segundo Schreiber argumenta, Hegel na verdade estava o tempo todo falando sobre Teoria das Cordas, só que como ele estava sozinho e não sabia expressar o que ele queria realmente desenvolver. A teoria dele ficou obscurecida por anos. E é aí que entra William Lawvere. Lawvere é um marxista especializado em teoria das categorias e tem trabalho de formalização do trabalho de Hegel para matemática.

Schreiber em 2010 redescobriu topoi coesivos, e um amigo dele falou sobre Lawvere, então ele começou a ler Lawvere, e conversando com David Corfield, ele descobriu que Lawvere dava muita atenção a Hegel, mas ele tinha a ideia que todo mundo tem de Hegel de ser um filósofo irrelevante, prolixo e etc. Então David Corfield ficou falando tanto de Hegel que o Schreiber decidiu olhar os trabalhos filosóficos de Hegel e Lawvere e se apaixonou por Hegel e teve ótimos insights do mesmo em física.

O objetivo de Hegel era fazer da filosofia uma ciência empírica - uma ciência cujas observações são a introspecção absoluta do logos em si, mas ciência empírica apesar de tudo, daí o título: "Ciência da Lógica".
>Ajudar a trazer a filosofia para mais perto da forma de ciência - aquela meta onde ela pode deixar de lado o nome do amor ao conhecimento e ser conhecimento real - é isso que eu coloquei diante de mim.<br/>
>– <cite>Hegel</cite>

Não há um único erro em Science of Logic, ao contrário, ele começa caracterizando com bastante precisão a "oposição inicial", mas depois se torna cada vez mais nebuloso à medida que continua escalando o processo.

A ciência da lógica de Hegel requer um processo de observação, argumento e prova para estabelecer a verdade, assim como a ciência empírica moderna. Este processo é o "Proceß" ou "Weg des Wissens" ou "Bewegung des Seins". É por isso que Ciência da Lógica é escrita como um silogismo de silogismos...

Aplicando a formalização de Lawvere, podemos até mesmo verificar Hegel, podemos verificar o processo previsto na "Doutrina do Ser" até a segunda camada de resoluções de dualidades, da mesma forma para a [Doutrina da Essência](https://ncatlab.org/nlab/show/Science%20of%20Logic#OverviewOfTheFormalizationProcess).

Ainda assim, existem insights na "Filosofia da Natureza" de Hegel (quando ele percebeu que não viveria o bastante para fazer um estudo completo sobre). Por exemplo, ele vê que [o espaço e o tempo se unem à matéria](https://ncatlab.org/nlab/show/Science+of+Logic#DieMechanik) (isso em 1817) assim como nos GUTs ultravioleta completos modernos.

Schreiber diz que para subirmos um nível, devemos resolver as contradições dos níveis atuais via suprassunção:
- No nível 0 temos a contradição entre "ser" e "não-ser" (note, não são uma contradição, mas contrários), Ser e Nada
- No nível 1, isso é resolvido pelo processo de "devir", e aí temos a contradição "devir" (contínuo) e "não-devir" (discreto) (relacionado à forma)
- No nível 2, isso é resolvido via infinitesimal e temos a contradição étale (pense nele como o finito - não é somente isso, mas é didático pensar assim) e infinitesimal (relativo a redução)
- No nível 3, isso é resolvido pelo reônomo (sistema onde o tempo é uma variável explícita) e bosônico (relativo a bósons) (relacionado a fermiônicos - relativo a férmions)

E não há uma quarta camada, pois aí você adquire a identidade.

E a gente resolve este processo de passar da física para uma prova formal (Proceß) no estilo Ciência da Lógica com:
- Geometria discreta (nível 0)
- Geometria coesiva (nível 1)
- Geometria diferencial (nível 2)
- Supergeometria (nível 3)

Então a partir da teoria dos tipos básica, Schreiber vai resolvendo esses sistemas, pela teoria dos tipos de homotopia (primeira lei do pensamento: A = A), teoria dos topos de homotopia (autorreflexão interna (universo univalente)), teoria homotópica coesiva (resolve oposições iniciais), teoria homotópica elástica (resolve o contínuo/discreto), teoria homotópica sólida (resolve finito/infinitesimal), supergeometria superior (modelo "fiel"), supergeometria lorentziana (representa a reonomia e forma uma cobertura universal) e por último supergravidade com branas-M2/M5 (resolve a torre de Whitehead).
