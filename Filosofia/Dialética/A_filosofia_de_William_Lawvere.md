# A filosofia de William Lawvere
Em linguagem simples, o lema de Yoneda é uma prova de que objetos são determinados simplesmente (e unicamente) por seu conjunto de relações com os outros objetos. Diz que você não pode obter mais de um objeto do que aquilo que você coloca nele. Isso tem aplicações filosóficas e ontológicas bem fortes, significa que as coisas só existem porque elas formam relação com outra coisa. Por exemplo, "mulher" só existe por causa do "homem", sem "homem", faria mais sentido chamar de "pessoa" ou "humana". Ou seja, não existem objetos independentes do universo. Objetos só existem por causa da relação que ele estabelece no universo. O lema de Yoneda diz que os objetos são totalmente determinados por mapas de todos os objetos. No entanto, às vezes podemos fazer melhor. Por exemplo, Yoneda também diz que os pré-feixes são totalmente determinados por mapas de representáveis e os esquemas são totalmente determinados por mapas de esquemas afins e assim por diante. É isso que forma a base da teoria das categorias: é uma teoria descritiva sobre como o mundo é, como o mundo se comporta, inclusive fazendo grandes descobertas, como aquela que diz que tipos em programação, lógica intuicionista e matemática construtivista são equivalentes!

William Lawvere é um marxista que tem extensas obras sobre a dialética, materialismo e Hegel, principalmente com sua conexão com a matemática, especialmente com a teoria das categorias. Além de trabalhos em física. Lawvere acredita que o materialismo influenciou bastante a evolução da teoria das categorias moderna, e que a mesma influenciará na filosofia da dialética emprestando uma forma precisa com modelos matemáticos discutíveis para distinções filosóficas, como:
- geral vs. particular
- objetivo vs. subjetivo
- ser vs. se tornar
- espaço vs. quantidade
- igualdade vs. diferença
- quantitativo vs. qualitativo

Lawvere começa seu esboço citando que Hegel faz uma distinção entre a lógica subjetiva, que envolve conceitos, juízos e dedução (lógica matemática), e a lógica objetiva que se preocupa com algo como o logos, um tipo de ontologia ou metafísica, não diferente do sistema ontológico de deduções de Spinoza. E por essa lógica objetiva ser diferente da lógica de predicados, levou a uma rejeição do idealismo objetivo por parte de Russell e a filosofia analítica em si, pois não era matematizável na época.

Lawvere diz que construções universais na lógica categórica e na teoria de topos, como adjunções, devem ser pensadas como a encarnação formal da lógica objetiva de Hegel. A lógica subjetiva é descrita por Lawvere como falha porque em cada mente, ela muda o "universo de discurso" (significado auto-evidente), e é até irônico que Wittgenstein criticava o mesmo, mas ainda era cético à lógica objetiva. Antes da teoria das categorias, pelo menos uma discussão sistemática das leis dessas transformações objetivas foi dada por Bourbaki, que discutiu como uma estrutura poderia ser deduzida de outra.

Segundo a interpretação de Lawvere, teoria das categorias é formada de categorias e dentro dessas categorias há objetos, podemos pensar então nas categorias como sendo a "teoria" e os objetos como sendo os "modelos". Por outro lado, Lawvere também responde à dicotomia geral e particular: uma categoria é abstrata (geral) e um objeto é concreto (particular).

Como já dizia Bernard Bosanquet:
>Um mundo ou cosmos é um sistema de membros, de modo que cada membro, sendo em hipótese distinto, contribui, no entanto, para a unidade do todo em virtude das peculiaridades que constituem a sua distinção. E o ponto importante para nós no momento é a diferença de princípio entre um mundo e uma classe. São necessários todos os tipos para fazer um mundo; uma classe é essencialmente de um único tipo. Em uma palavra, a diferença é que o princípio último de unidade e comunidade é plenamente exemplificado no primeiro, mas apenas superficialmente no último. O princípio último, podemos dizer, é a igualdade no outro; generalidade é mesmice apesar do outro; universalidade é igualdade por meio do outro.

Em sua busca para axiomatizar os conceitos de espaço e coesão, Lawvere, inspirado pela teoria dos tipos de homotopia, propôs uma representação matemática da relação de Aufheben dentro da teoria de topos ou teoria das categorias de maneira mais geral, junto da lógica categórica.

De acordo com Hegel, o ser puro é o oposto de nada cuja unidade é o devir puro. Lawvere tem uma interpretação matemática disso com modalidades adjuntas em monads idempotentes. Há uma notação específica para representar "ser" em categorias (essas chamadas de categoria do ser). De acordo com o Science of Logic de Lawvere, “tornar-se” é a unidade dos opostos do nada e do ser. O reverso está "cessando". O Aufheben de ambos é o Dasein ("ser puro"). Lawvere vê o espaço como sendo o contrário de quantidade. Para Lawvere, coesão significa como pontos no espaço são mantidos juntos, e ele a usou para formalizar a lógica objetiva via lógica categórica pela axiomática de topos coesos, ou seja, pela teoria do tipo modal equipada com modalidade de forma e modalidade plana.

Como dizia Hegel (mais na página de Hegel na Encyclopedia of the Philosophical Sciences):
>Elasticidade é toda a coesão.

Além de ter inspirações em Hegel, Lawvere cita bastante Grassmann e as vezes Marx! Lawvere inventou a lógica categórica e introduziu as teorias de Lawvere como uma forma teórica de categoria para descrever as teorias algébricas finitárias. Ele generalizou os topos de Grothendieck em topos elementares, revolucionando os fundamentos da matemática; nesse sentido, ele desenvolveu a base da teoria dos conjuntos estruturais chamada ETCS. Ele também introduziu e trabalhou em geometria diferencial sintética como base para geometria diferencial e equações de movimento na física contínua. Mais tarde, ele introduziu a noção de topos coesos como uma base mais geral da geometria.

Frases de Lawvere:
>Os conceitos de categorias, funtores, homomorfismos, funtores adjuntos e assim por diante fornecem um rico começo para o projeto de tornar explícita a lógica objetiva, mas provavelmente há muito mais a ser descoberto.

**Fun fact**: Delta de Dirac é equivalente ao conjunto hom. Hom(x, y) corresponde a um delta com pico em x como uma função de y, int f(y) delta_x(y) dy é somente f(x) segundo o lema de Yoneda, que é exatamente o que o delta de Dirac é.
