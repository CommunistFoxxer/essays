# Os judeus e a interpretação bíblica
Os judeus têm uma coisa que os cristãos não têm: o Talmude. A tradição oral que decodifica as mensagens secretas da Torá. O texto bíblico foi escrito por iniciados, então para entender completamente o significado das alegorias do texto é preciso ter a chave oral. E os judeus são a mais antiga sociedade secreta ainda em operação.

Gematria é interessante porque é um segundo nível de segredo no texto bíblico. O primeiro nível é o da tradição oral. O segundo nível é o da leitura dos números que as palavras em hebraico formam quando se aplica as técnicas da gematria, existe um texto escrito dentro do texto, usando números, onde cada palavra e cada letra na Torá foram calculadas para produzir os números corretos.

Esse é outro motivo pelo qual eles precisavam fazer cópias exatas do texto sagrado. Se você mudasse as palavras ou refazesse as frases, o texto secreto em números se perderia, por isso eles usam um Yad. Quando um judeu estuda a Torá, ele precisa usar um método especial que envolve usar esse apontador para não pular nem uma letra. Eles levam a sério o "não tirar nem adicionar nenhuma letra nas palavras de Deus" mas isso na verdade tem origem na gematria das sociedades secretas judaicas.

O terceiro e último nível só é passado entre sumos-sacerdotes. Essa técnica de três níveis de segredo não era exclusiva dos judeus, os egípcios também usavam ela oficialmente, os hieróglifos têm uma leitura literal (cada um é uma sílaba), uma leitura alegórica (cada um é uma alegoria à ser decodificada por uma tradição oral) e uma meta-leitura (uma compreensão subjacente às alegorias que depende de uma explicação mais detalhada).

Antes de explicar mais detalhes, recomendo ler sobre o [Midrash](https://pt.wikipedia.org/wiki/Midrash) na Wikipedia para aprender um conceito do ponto de vista judaico. Uma coisa é você ler as palavras no texto e tomar o significado daquelas palavras literalmente, outra coisa é você ler o texto com olhos de alguém que está aguardando mensagens secretas, outra coisa é você ler o texto com olhos de alguém que está pensando academicamente.

Vamos pegar um versículo da Bíblia: [Gênesis cap. 2 versículo 19](https://bibliaonline.com.br/acf/gn/2).

O versículo:
>Havendo, pois, o Senhor Deus formado da terra todo o animal do campo, e toda a ave dos céus, os trouxe a Adão, para este ver como lhes chamaria; e tudo o que Adão chamou a toda a alma vivente, isso foi o seu nome.

Se você for um crente comum, vai ler isso sem prestar atenção, superficialmente. Provavelmente vai imaginar Adão passeando pelo Éden e conhecendo as plantas e os animais enquanto uma voz dos céus fala com ele. Se você for um iniciado, você vai notar uma questão importante:
>Havendo, pois, o Senhor Deus formado da terra todo o animal do campo, e toda a ave dos céus, os trouxe a Adão
>
>os trouxe a Adão

O texto bíblico disse que Deus trouxe os animais para Adão ver

Agora, outro versículo. [Gênesis cap. 3, versículo 8](https://bibliaonline.com.br/acf/gn/3)
>E ouviram a voz do Senhor Deus, que passeava no jardim pela viração do dia; e esconderam-se Adão e sua mulher da presença do Senhor Deus, entre as árvores do jardim.

Novamente, o crente vai imaginar coisas aleatórias. O iniciado vai observar isso aqui:
>E ouviram a voz do Senhor Deus, que passeava no jardim pela viração do dia
>
>a voz do Senhor Deus, que passeava no jardim

O texto bíblico literalmente diz que Deus estava passeando pelo jardim e que tinha uma voz, logo depois de dizer que Deus estava trazendo coisas para Adão ver. Ou seja, o Deus do Gênesis tinha corpo e aparência de ser humano.

A Bíblia fala de 3 deuses:
- O deus do Gênesis
- O deus que vai de Melquisedeque até Jesus
- O deus do Apocalipse

Se eu disser isso para um crente comum, ele vai achar isso estranho, vai discutir... Talvez ele até aceite imaginar que Deus aparecia para Adão e Eva como uma pessoa humana por alguma questão misteriosa. Mas o crente comum não vai entender que dizer que Deus tinha corpo de ser humano é uma alegoria que carrega outro significado em si.

Se você ler a Bíblia com olhos inocentes, você não vai se perguntar se existe um significado mais profundo em dizer que Deus tinha corpo humano, ou se perguntar se existe um significado mais profundo no fruto da árvore do conhecimento do que é bem e mal. Gênesis não é uma cosmologia comum, é um texto escrito por uma sociedade secreta. Para você entender a cosmologia que os autores realmente tinham em mente, você precisa ser um iniciado.

Eles esconderam a cosmologia verdadeira dentro de uma falsa. Por exemplo, o fato da palavra Deus existir no português tem um significado oculto. Deus não é a Natureza, a Natureza é a criação de Deus, mas o problema é que Deus não criou nada.

Se eu vier para você com um brinquedo artesanal, você provavelmente vai me perguntar se fui eu que fiz ele. Vamos supor que eu responda: "ninguém fez esse brinquedo, ele surgiu do nada". Você vai acreditar em mim? Não vai... porque pelo seu raciocínio causal, você vai concluir que o brinquedo foi feito por alguém.

Daí vamos supor que eu começo à meter filosofia no meio e você fala:
>O universo é muito mais complexo do que um brinquedo, com certeza teve um autor.

Daí eu respondo:
>Isso seria verdade se o universo existisse. O universo é Nada.

Percebeu que eu escrevi a palavra Nada com letra maiúscula? No português, quando se usa letra maiúscula em uma palavra, é porque ela é um substantivo. Eu não usei a palavra nada-adjetivo, usei a palavra Nada-substantivo.

O Nada é uma determinada coisa com determinadas características. É uma ausência que não é ausente.

Tem outra questão que surge quando se fala do vir-a-ser: A natureza de Deus. Se você pesquisar a origem da palavra Deus, vai descobrir que significa "brilhante". Deus é uma fonte de Luz infinita (Luz com letra maiúscula mesmo, porque não é a luz física comum).

A interpretação bíblica é biologia em linguagem poética, lições militares, lições de ética, códigos para construir templos com as proporções, história da descoberta da matemática, etc. A invenção da astrologia tem tudo à ver com essa história. A Astrologia, quando foi criada na antiguidade, na verdade era uma área de estudo rigorosa, ela foi criada para aprimorar os conhecimentos matemáticos através da análise minuciosa do comportamento dos astros no céu. O astrolábio, o calendário, a semana de 7 dias, as horas e minutos, foram criadas pelos astrólogos.

Se você acreditar em espíritos, a Goétia será real para você, porque você vai se induzir à imagem subconscientemente. Vai confundir sua imaginação com seus sentidos e enlouquecer. Eu não entendo essa palavra da mesma forma que o senso comum expressa. Espírito para os iniciados da antiguidade, é meramente a junção do que nós chamamos de programa, com o que chamamos de hardware e energia elétrica. Ou seja, é o estado da "energia fazendo o programa acontecer em um artefato físico".

Alma significa apenas "um corpo vivo", então a alma (um corpo vivo) possui um espírito (energia vital em operação). É por isso que a Bíblia diz que quando uma alma morre, ela retorna ao pó, mas o espírito retorna à Deus: o corpo se desfaz, e a energia vital volta para a natureza. É biologia básica em linguagem poética. Isso também tem a ver com a ressuscitação.

Mas naquela época os homens sábios não conseguiam comunicar esse conhecimento às pessoas (veja, por exemplo, Hegel, que foi influenciado pelo filósofo e ocultista Scheiling). Quando eles tentavam, eram recebidos com desprezo e até violência. Então eles se acostumaram à criar sistemas de sociedades secretas para ensinar esse tipo de coisa apenas para outras pessoas que fossem também inteligentes.
