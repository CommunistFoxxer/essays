# Relativismo cultural e etnocentrismo
Laraia chamou atenção para o fato de que todos os povos, em diferentes tempos e lugares, sempre foram atraídos por costumes diferentes daqueles que conhecem. 

Lévi-Strauss dizia que o tabu do incesto (a proibição do casamento com certos do membros da sociedade) formaram as primeiras sociedades, assim colocando os membros em contato com outras sociedades e assim surgindo a civilização. 

Em muitos textos, no entanto, é possível ver europeus chamando índios de promíscuos, casando-se com primos e primas. No entanto, tempos depois, descobrimos que sociedades indígenas seguem regras de casamento extremamente elaboradas. Essa visão, que enxerga outras culturas pela lente de sua própria cultura, é etnocentrista.

O filósofo Montaigne, mesmo com um olhar preconceituoso, foi um relativista cultural, pois, ao refletir sobre o canibalismo ritual tupinambá, produziu um estranhamento de sua própria sociedade:
>Não me parece excessivo julgar bárbaros tais atos de crueldade, mas que o fato de condenar tais defeitos não nos leve à cegueira acerca dos nossos. Estimo que é mais bárbaro comer um homem vivo do que o comer depois de morto; e é pior esquartejar um homem entre suplícios e tormentos e o queimar aos poucos, ou entregá-lo a cães e porcos, a pretexto de devoção e fé, como não somente o lemos mas vimos ocorrer entre vizinhos nossos conterrâneos.

O etnocentrismo é acompanhado por um reforço de identidade do eu, pois produz uma distinção hierarquizada entre o grupo do eu e os demais. Não é à toa que vários grupos indígenas definem a si mesmos como “os humanos”, “os perfeitos”. Os gregos definiam todos os outros povos com quem mantinham contato como “bárbaros”, ou seja, aqueles que estavam fora da civilização.

Said nos mostra como os EUA produziram uma imagem etnocentrista de árabe. O mesmo acontece com os índios. Quando estudamos a figura do índio nas escolas, somos ensinados a respeito de um índio genérico, cujos atributos são os valores que nossa sociedade projeta sobre a imagem do outro. Ao observarmos a imagem do indígena nos livros escolares, notamos que ele aparece infantilizado ao ser recorrentemente projetado na figura do curumim (criança indígena). A mesma ideia do indígena como criança foi uma das bases da catequização: o indígena não é mau, mas ingênuo, desconhece a tecnologia, o trabalho árduo e, portanto, beneficia-se do contato com o colonizador.

Ao nos depararmos com as diferenças, que podem ser as mais variadas (éticas, estéticas, religiosas etc), ocorre um esforço para darmos sentido ao diferente - este é o momento da tradução. No etnocentrismo, a tradução se dá ao interpretarmos as coisas do outro de nosso ponto de vista.

Geralmente, as diferenças entre os neopentecostais são apagadas, e todos são homogeneizados sob nomes como evangélicos ou crentes, e ao se noticiarem os casos de perseguição aos grupos de matriz africana, muitos consideram que todos os neopentecostais são coniventes com tais ações, mas, no interior do cenário evangélico, vários grupos se manifestam publicamente em repúdio a esse tipo de atitude.

A proibição dos bailes nada mais foi do que uma ideia presente no senso comum que associa automaticamente os morros à criminalidade. Quem mora nos morros sabe disso, pois frequentemente denuncia arbi trariedades cometidas pela polícia. A marginalização e a estigmatização de grupos sociais são uma expressão poderosa do etnocentrismo.

Ao relativizarmos o que estamos vendo, não procuramos justificar o que outros grupos estão fazendo, mas entender por que estão se com portando daquela maneira.

O evolucionismo foi o principal fio condutor do pensamento antropológico no final do século XIX e começo do século XX. De acordo com o evolucionismo, a humanidade passaria por estágios de evolução que iam do primitivo, passando pela barbárie, até chegar à civilização (patriarcal, monogâmica, centrada nos Estados Nacionais e expansão capitalista). As demais - aquelas sociedades tribais encontradas nas colônias - estavam em estágios anteriores de evolução social; logo, caberia aos colonizadores ajudá-las em seu desenvolvimento.

Para os evolucionistas, as sociedades tribais eram exemplos vivos das sociedades primitivas e poderiam auxiliar na compreensão sobre como a sociedade ocidental chegou ao estágio “máximo” de civilidade. Alguns dos precursores da antropologia se dedicaram a essa tarefa. James Frazer escreveu o famoso livro O Ramo de Ouro. Nessa obra, o autor discutiu de que forma a magia (primitiva) era a antecessora da ciência, por pretender compreender e controlar fenômenos naturais de forma equivocada.

De modo semelhante, o norte-americano Lewis Morgan (1818-1881) publicara, duas décadas antes, a obra A Sociedade Antiga, tratando da evolução das relações sociais com base no parentesco. Segundo ele, o matriarcado, sociedade comandada pelas mulheres, teria sido a primeira forma de organização social, na qual a promiscuidade generalizada reinava e o pai das crianças era desconhecido. A monogamia e o controle social masculino derivariam, portanto, de formas mais elaboradas de organização social.

As teorias evolucionistas, embora tenham tido o mérito de consolidar a ideia de unidade lógica da espécie, não se descolaram daquele olhar etnocêntrico, de acordo com o qual o modelo de sociedade superior é o que se vivencia no dia a dia. 

No início do século XX, as críticas ao evolucionismo começaram a ganhar corpo com o desenvolvimento de trabalhos de campo. Um dos primeiros pesquisadores a realizar estudos prolongados em outra sociedade foi Franz Boas. Valendo-se do conceito de cultura, Boas criticou a história conjectural do evolucionismo. De acordo com esse estudioso, não seria possível estabelecer uma história universal para as sociedades humanas, pois cada cultura apresentava desenvolvimento e história próprios. A evolução deveria ser compreendida no interior de cada cultura, de forma independente.

Para Radcliffe-Brown, era necessário preocupar-se com a organização social tal como ocorria no momento do trabalho de campo. Não era possível confiar na memoria dos informantes para o registro do passado. Como sociedades tribais não tinham documentos escritos, a pesquisa histórica era inviabilizada.

Malinowski também questionou o estudo da história, afirmando que cabia ao pesquisador de campo compreender a totalidade da sociedade estudada e a relação de cada uma das partes (economia, política, parentesco, etc) como um todo.

A publicação de Argonautas do Pacífico Ocidental, em 1922, foi um marco na história da antropologia, tendo sido responsável por atrair vários adeptos ao método e formar gerações de pesquisadores de campo. Malinowski chamou atenção para três pontos centrais:
1. Os diferentes aspectos da vida social não podem ser estudados de forma isolada.
2. O pesquisador não pode confiar totalmente naquilo que o nativo fala, pois há uma diferença entre o que eles dizem que fazem e o que eles realmente fazem.
3. É preciso entender que, por mais ilógicas que pareçam certas atitudes aos olhos do observador, elas encerram algum tipo de coerência.

O livro Argonautas, que é descritivo e detalha a vida dos habitantes das Ilhas Trobriand, aponta para a necessidade de imersão no universo do outro, para que seja possível conhecê-lo e compreendê-lo. Além disso, esclarece que desvendar outras realidades não é uma tarefa fácil, uma vez que a vida social é complexa e composta por várias camadas.

Na etnografia, não bastaria participar de momentos solenes, pois as atividades corriqueiras e pueris também indicariam dimensões importantes da vida social. O convívio prolongado com a sociedade estudada ficou conhecido como observação participante. 

Segundo Roberto da Matta, o trabalho de campo compreende uma série de movimentos, os quais implicam:
1. Separação e rompimento - É necessário deixar a vida cotidiana para imergir na realidade do grupo social com o qual se vai trabalhar.
2. Imersão - Significa entrar na realidade do grupo e vivenciá-la nos seus aspectos mais cotidianos. Trata-se de um período ambíguo, pois o antropólogo está afastado de sua sociedade, mas não pertence totalmente ao grupo que está sendo estudado. Aqui, o exótico é transformado em familiar.
3. Retorno - Ao voltar para casa, é preciso dedicar-se ao trabalho de escrita; porém, o período de ausência deve ter sido capaz de modificar o olhar do pesquisador sobre a própria sociedade, dando-lhe ferramentas para questioná-la. O familiar, assim, torna-se exótico.

Segundo Radcliffe-Brown, a coesão social deveria ser mantida a todo custo, e as instituições sociais tinham como função manter essa coesão. Desse modo, o foco de Brown residiu nos processos de estabilidade social, função de todas as instituições. Ao supor ser possível chegar a um modelo de estabilidade, o autor acreditava também ser viável interpretar a totalidade de uma sociedade. Seus discípulos sustentaram a ênfase na coesão social com base no estudo dos rituais, cuja principal finalidade era dissolver tensões sociais, reforçando os laços de solidariedade.

Edward Evans-Pritchard ao estudar a bruxaria entre os Azande (povo da África Central), ressaltou que o etnógrafo deve atentar para o que é importante ao grupo estudado. Conforme o autor, não era seu interesse estudar bruxaria, mas a organização política desse povo. Entretanto, os Azande não falavam de outra coisa, e a bruxaria se impôs como objeto de reflexão. Percebemos que, em trabalho de campo, os temas se impõem e nem sempre o que era planejado pelo antropólogo sai como esperado. Mesmo assim, Pritchard observou a importância da linguagem oferecida pela bruxaria para a resolução de conflitos e disputas. Evans-Pritchard foi um dos primeiros antropólogos a chamar a atenção para o fato de que as sociedades não eram tão coesas e estáveis quanto queriam Malinowski e Radcliffe-Brown.

Da mesma forma, Max Gluckman salientou, em sua obra Rituais de Rebelião no Sudeste da África, que havia tensões na sociedade suázi que se expressavam por meio dos rituais.

Edmund Leach lançou a obra Sistemas Políticos da Alta Birmânia, na qual mostrou que a instabilidade social não estava contida na esfera dos rituais, mas era vivenciada na práxis, na vida social e se manifestava na organização política.

Os avanços da antropologia revelavam que a totalidade social de que falavam os primeiros pesquisadores de campo era mais um modelo analítico para interpretar a realidade. Com Geertz, a validade desse modelo foi questionada, sob a ideia de que a análise do antropólogo é sempre incompleta, pois a realidade não está plenamente acessível. Ao afirmar que o antropólogo realiza uma leitura de segunda mão, incompleta, com base nas interpretações que seus interlocutores têm da própria realidade, Geertz propôs substituir a ideia de observação participante por interpretação. A visão interpretativa abriu caminho à emergência da antropologia pós-moderna, que é crítica quanto à neutralidade da disciplina e ao seu alcance para a compreensão da realidade.

Um dos maiores problemas para a antropologia pós-moderna era a escrita etnográfica. Era como se a questão envolvendo a interpretação residisse num mero problema de tradução. James Clifford discutiu a produção da autoridade etnográfica como um problema que surge da relação “estar lá, escrever aqui”. A realidade seria, portanto, alcançada por meio de uma experiência (subjetiva) do antropólogo em campo; seria como se ele, e apenas ele, tivesse acesso àquele conhecimento.

Para Roy Wagner, não se tratava de um problema de escrita, mas de criação. A realidade não poderia ser apreendida em sua totalidade porque o antropólogo, ao fazer sua etnografia, interpreta a realidade com base em seu próprio sistema simbólico. Na relação entre os dois sistemas simbólicos - o seu e o do outro -, o que se produz é algo novo. A etnografia seria, ainda que pautada em métodos rigorosos, uma espécie de invenção.
