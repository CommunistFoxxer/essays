# Políticas públicas
Podemos definir o papel social da família em termos de:
- Expectativas em relação ao ideal de família (no caso, família nuclear), por exemplo: cuidados, proteção, aprendizado dos afetos, construção de identidades e vínculos relacionais de pertencimento, capazes de promover melhor qualidade de vida a seus membros e efetiva inclusão social na comunidade. No entanto, essas expectativas são possibilidades, não garantias. A família vive num contexto que pode ser tanto fortalecedor quanto esfacelador de suas possibilidades e potencialidades.
- A família assumiu formas bastante amplas desde a Roma antiga até sociedades não-ocidentais, afastadas, insulares e indígenas. Ela é uma forma social que se organiza-desorganiza-reorganiza. Como dizia Afonso & Figueiras, “É preciso enxergar na diversidade não apenas os pontos de fragilidade, mas também a riqueza das respostas possíveis encontradas pelos grupos familiares, dentro de sua cultura, para as suas necessidades e projetos”.

Durante o Estado de bem-estar keynesiano, a família perdeu sua centralidade, pois quem garantia a integração sócioeconômica era o Estado. O indivíduo promovido a cidadão só podia trilhar seu caminho dependente do Estado e do trabalho, e não mais nas chamadas sociabilidades comunitárias e familiares. Isso no embalo de uma nova urbanização com promessas de novas sociabilidades planetárias, a chamada globalização.

Os milagres econômicos que o keynesianismo trouxe foram indiscutíveis, com conquistas sociais, científicos e tecnológicos (CTS). Foi chamado de A Época de Ouro do capitalismo, depois substituído por um neoliberalismo feroz em estado de crise permanente. No entanto, essa realidade de milagres econômicos não alcançou o Terceiro Mundo, sendo recluso somente a países do centro do sistema (Sul Global).

Com o enfraquecimento da família, surgem novas demandas populares e pseudopopulares aproveitadoras, pedindo que a família volte a ter um papel central na organização da política social. Como narra Claude Martin, em muitos países europeus é discutido a hipótese de remeter a família ou para as redes de integração primária um certo número de serviços e encargos que outrora eram cobertos pela despesa pública.

Nos países latinos, cria-se um conceito que reflete muito a nossa cultura, que é o welfare mix, a partilha de responsabilidades entre sociedade civil ("terceiro setor") e Estado. No Brasil, as redes de solidariedade e sóciofamiliares nunca foram descartadas. Elas são para a camada popular a sua condição de resistência e sobrevivência. São possibilidades de maximização de rendimentos, apoios, afetos, relações para obter emprego, moradia, saúde, proteção, inclusão social, etc.

Fala-se menos em hospital e mais em internação domiciliar, médico de família, cuidador domiciliar, agentes comunitários de saúde, etc. Fala-se menos de escola integral e mais em jornada educacional de tempo integral, com tempo parcial conjugada a outros programas e serviços que ampliem o desenvolvimento infanto-juvenil com uma tarefa socializante. Fala-se em impor limites, autoridade na socialização e educação dos jovens, com a família em parceria com a escola participando dos projetos educacionais e comunidade presente na escola. Fala-se em agentes que assessoram grupos comunitários na montagem de empreendimentos geradores de trabalho e renda, animadores culturais e implementadores de projetos coletivos destinado a melhoria da qualidade de vida da comunidade. Fala-se em microempreendimento familiar.

Fala-se em ONGs prestadoras de serviços sociais. As respostas institucionais às necessidades sociais são desacreditadas. A família adquire papel fundamental nos serviços públicos, como a escola, unidade básica de saúde, hospitais, abrigos, etc. É introduzir pactos com a família para que esta participe das ações e projetos movidos por esses serviços. É na família que começa os serviços de proteção e inclusão social: a unidade mais básica de uma sociedade.

A escola, os clubes esportivos, os acampamentos, etc permitiram a emergência de sujeitos como tio ou tia como fontes principais de formação, e não mais os pais. A família adquire importância na sociedade moderna por prevenir o isolamento social devido a ausência de trabalho, e também aos riscos da sociedade televisiva/telemática. Ela pode criar um sentimento de pertencimento do indivíduo e igualmente reenergizá-lo existencialmente.

Família e sociabilidade se alteram, mas ela nunca perdeu sua principal função: possibilidades de proteção, socialização e criação de vínculos relacionais. No entanto, devemos reconhecer que a família no capitalismo carece de proteção para que a mesma possa oferecer proteção a seus membros: isso só poderia ser resolvido em uma sociedade completamente autônoma e subsistente.

Os serviços públicos devem colocar mais ênfase às queixas das família. A escuta deve ser empática, e não mecânica (burocrática/fria). Alguns serviços notaram que este processo reduziu o número de internações e de receituário de medicamentos. Nos serviços de assistência social, observou-se aumento da auto-estima e da capacidade em reelaborar e avaliar suas histórias de vida, motivando-os a centrarem-se com maior autoria na alteração de seu cotidiano.

As redes de serviços de apoio psicossocial, cultural e jurídico à família são importantes, pois fornecem serviços de saúde mental, ampliação do universo informacional e de oportunidade de trocas culturais. Há também os serviços advocatícios e de defesa dos direitos familiares.

Programas de complementação de renda são importantes para dar maior autonomia familiar e eficazes para erradicar o trabalho infantil, permanência e sucesso escolar de crianças e adolescentes.

Programas de geração e trabalho de renda, como cestas básicas, permite tirar as famílias da pobreza.

Há alguns princípios fundamentais e estratégicos na condução destes programas, tais como:
- Primazia das dimensões éticas, estéticas e comunicativas
- Desenvolvimento da auto-estima
- Um olhar sobre as fortalezas das famílias e não mais às suas vulnerabilidades
- Fortalecimento dos vínculos relacionais
