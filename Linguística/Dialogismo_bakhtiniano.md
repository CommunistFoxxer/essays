# Dialogismo bakhtiniano
Dialogismo (também escondido muitas vezes por trás de termos como interação, dialética, histórico, materialismo dialético, cronotopologia, tectologia, social, contextual, relacional, processo, dinâmica e ecologia) é uma estrutura epistemológica (ou mesmo ontológica).

Dialogicidade é a essência da condição humana, notadamente que nosso ser no mundo é totalmente interdependente com a existência dos outros. Um ser humano é interdependente com as experiências, ações, pensamentos e pronunciamentos dos outros. A pessoa não é um indivíduo autônomo que pode decidir tudo por si mesma, como o monologismo tende a supor.

Nesse sentido, o dialogismo nega o sujeito autônomo que pensa, fala e age por si mesmo. Cada palavra dirige-se a uma resposta e não pode escapar à influência profunda da palavra de resposta que ela antecipa. As atividades e situações em que participamos não são estáticas, mas sempre em processo de "tornar-se" e nossas ações evoluem de forma dinâmica.

O monologismo em psicologia, para não falar de lógica e filosofia, tem estado relutante em "contaminar" idéias e cognições com avaliações e emoções. O monologismo deseja limpar o pensamento (ao torná-lo "cognição") das emoções, influências culturais e dimensões morais.

Eco psicólogos como Gibson enfatizam que a percepção do ambiente é fundida com avaliações; atribuímos valores, "possibilidades" às coisas. Ver algo como algo é aplicar uma perspectiva, e as perspectivas geralmente são tendenciosas e carregadas de valores. Existe um mundo ao nosso redor que existe independentemente de ser percebido ou reconhecido pelos sujeitos humanos e suas culturas.

O dialogismo não nega a "realidade" das coisas no mundo (exterior).

No realismo agencial de Barad, fenômenos como percebidos/apreendidos são constituídos no processo ativo (do conhecedor) de conhecer no mundo. Quando eu falo com você sobre o mundo real, eu construo o conhecimento dele de forma dialógica.

Esta teoria da onto-epistemologia examina como os fenômenos e “ontologias” são criados, sem negar a realidade dos fenômenos e objetos. A realidade não é composta de coisas-em-si-mesmas ou coisas-por-trás-dos-fenômenos, mas coisas-nos-fenômenos.

Um ser humano deve ser visto como determinado biologicamente, socioculturalmente interdependente e equipado com uma consciência individual. A unidade dialógica de análise é a interação situada. Ao mesmo tempo, o dialogismo foi mapeado como uma metateoria para a mente humana. A mente é realizada amplamente em e por meio de suas interações situadas.

O dialogismo pode, portanto, ser descrito como uma epistemologia geral para as ciências humanas e como uma ontologia da mente humana. As teorias dialógicas trazem significado e criação de sentido de volta às ciências humanas, particularmente à psicologia e à linguística.

As teorias constituintes do monologismo são o modelo de cognição de processamento de informação, o modelo de comunicação de transferência, o modelo de código da linguagem e as teorias do conteúdo como externo à linguagem.

O dialogismo não é sobre comunicação perfeita ou compreensão completa, mas sobre compreensão suficiente para os atuais propósitos práticos, ou seja, as pessoas devem estar satisfeitas em se entenderem suficientemente bem, a fim de prosseguir em suas comunicações ou outras atividades atuais.

No monologismo, cognição e comunicação são considerados processos separados. As teorias dialógicas consideram o pensamento e a comunicação muito mais intimamente relacionados entre si do que o monologismo. Ao contrário do monologismo, o dialogismo considera a distinção entre fazer e pensar (cognição) confusa.

O monologismo retrata outras mentes como um “problema”. O dialogismo pressupõe que os outros são co-constitutivos de si; outras mentes estão envolvidas em toda a vida social e experiências individuais vividas.

Algumas das falhas nos experimentos psicológicos devem-se à configuração descontextualizante, cognitivizante (intelectualização) e confusa (mistificadora) das situações experimentais. Uma ontologia monológica (que é relevante para explicar o processo de criação de sentido) abrange apenas indivíduos e sua cognição e comportamentos.

De acordo com o monologismo, o individual é a origem auto-evidente do conhecimento do mundo e dos outros. No dialogismo, ao contrário, não se pode presumir que os eus individuais existam como agentes e pensadores antes de começarem a interagir com os outros e o mundo. As inter-relações entre os sujeitos, os outros e o meio ambiente existem desde o início, e a consciência de si mesmo e dos outros co-se desenvolve ao longo do tempo; eles são os dois lados do mesmo processo.

Em resumo, o monologismo está intimamente ligado ao individualismo. Mas a ontologia está ao mesmo tempo dividida entre subjetivismo e objetivismo. O que parece ser uma suposição monologista mais profunda, por trás e além do individualismo e objetivismo, é a crença na causalidade unilateral e variáveis 'independentes' vs 'dependentes' em modelos científicos. Isso remonta, pelo menos, à teoria da causalidade de Hume. O indivíduo não desaparece no dialogismo. É um ser social interdependente dos demais, não sujeito autônomo ou cogito cartesiano.

Você nunca pode não estar em uma situação. Os recursos socioculturais pertencem a ‘tradições’, que constituem o contraponto de ‘situações’.

A dupla dialogicidade é a combinação de interacionismo e construcionismo social (ou seja, sócio-histórico). A dupla dialogicidade significa que o diálogo ocorre não apenas em trocas interpessoais entre co-participantes participantes e outras interações situadas, mas também no nível da práxis, ou seja, em práticas socioculturais, comunidades, instituições, etc., que transcendem situações. Eles pertencem a tradições.

Então, o que é primário: situações ou tradições/sistemas, agência ou estrutura? O dialogismo tenta superar esse problemático problema de atribuir primazia a qualquer um deles, insistindo na necessária interação entre interações situadas e recursos e práticas culturais.

Idéias emergentes adotadas de teorias autopoiéticas: os sistemas sociais reproduzem-se a partir de si mesmos; eles são auto-organizados.

Os ambientes construídos, escritos e profissionais têm uma natureza material; são edifícios, objetos e textos inscritos com significado (ou pelo menos ou pelo menos 'recursos' para a construção de significado) que foram construídos socioculturalmente ao longo do tempo. Simplificando, leva tempo para projetar e construir casas, para escrever textos e estabelecer profissões. A materialidade combina bem com a criação de sentido, e isso indica uma maneira pela qual o realismo pode ser combinado com o construcionismo.
>Uma palavra é um ato bilateral. É determinado igualmente por quem é a palavra e para quem se destina. Como palavra, é precisamente o produto da relação recíproca entre falante e ouvinte, destinatário e destinatário. [...] Uma palavra é uma ponte que se joga entre mim e o outro. Se uma extremidade da ponte depende de mim, a outra depende do meu destinatário. Uma palavra é um território compartilhado tanto pelo remetente quanto pelo destinatário, pelo falante e seu interlocutor.<br/>
>– <cite>Voloshinov</cite>

O indivíduo é um outro antes de ser um eu. É dirigindo-se a si mesmo no papel de outro que seu eu se eleva em experiência. Para Bakhtin e semelhantes, falar sobre "consciência individual" equivaleria a uma contradição em termos. Consciência é "conhecer" com os outros. O eu, para Bakhtin, é algo "não finalizável" e continuamente co-construído com os outros.

É precisamente tendo a oportunidade de complementar nossa perspectiva individual em um diálogo com outra, que se torna possível ver todos em vez de realidades individuais fragmentadas e separadas.

É esse encontro com o outro que nos ajuda a evitar o perigo do solipsismo possivelmente espreitando a ideia de perspectividade da experiência.

A intersubjetividade é uma propriedade definidora da comunicação. A comunicação seria impossível sem qualquer suposição de intersubjetividade e semelhança nas mentes dos participantes.

Alteridade é a noção de que o outro muitas vezes vem com uma perspectiva sobre as coisas faladas que é diferente da própria. Tensões e tensões, diferenças entre pessoas e tradições, limites entre comunidades, conhecimento, normas e expectativas em variação.
>Sem temporalidade, sem alteridade; sem alteridade, sem diferença; sem diferença, sem significado; sem sentido, sem mundo.<br/>
>– <cite>Sandywell</cite>

Aprender não é meramente internalização e retenção de algum “input” objetivo, mas implica estabelecer e manter relacionamentos com e dentro do ambiente. A zona de desenvolvimento proximal de Vygotsky é um fenômeno completamente dialógico e mostra que intersubjetividade e alteridade estão intrinsecamente entrelaçadas na interação.

Um elemento na comunicação parece ser o esforço, por parte dos participantes, de buscar a intersubjetividade, a comunhão e o equilíbrio. Kurt Lewin, um psicólogo social que tinha muito em comum com o dialogismo, descreveu a interação como movimentos dentro de um campo, que se esforça - depois de ter sido interferido ou interrompido - para chegar novamente a um estado de equilíbrio. Ou seja, quando o equilíbrio é perturbado, os elementos se esforçam para se reorganizar para chegar a um novo equilíbrio ou homeostase. Isso obviamente lembra a dialética de Hegel. Porém, como vimos, alteridade não é apenas um período transitório entre as situações de homeostase. Serge Moscovici (1979) enfatizou a dinâmica resultante da quebra de simetria e equilíbrio; As tensões são uma fonte de movimento tanto nos indivíduos como no diálogo e na sociedade (Capítulo 12). Frequentemente, há várias opiniões majoritárias e minoritárias, e as últimas frequentemente resultam em mudanças importantes (Marková, 2003). De acordo com dialogistas como Bakhtin e Lévinas, o modelo hegeliano com síntese implicando homeostase, não vale para diálogo e comunicação, que são necessariamente 'incompletos' e, portanto, em certo sentido 'não finalizáveis'.

Aristóteles na Ética a Nicômaco diz que o outro é como eu (Ego) e ao mesmo tempo é diferente (Alter), portanto, uma espécie de "alter ego".

Incorporamos aspectos dos outros (e suas imagens de nós) em nós mesmos e projetamos partes de nós mesmos nos outros.

A palavra latina consciência é derivada do prefixo con ‘(junto) com’ e scio ‘saber’. Ter consciência implica estar ciente de seus próprios pensamentos, sentimentos e comportamentos, quase como se alguém tivesse adotado e internalizado a perspectiva de outra pessoa observando "mim". ‘Cognição’ poderia ser re-gerada de volta ao latim co- ‘junto com’ e gnoscere ‘venha a conhecer, aprender’; pensamos junto com os outros e com o mundo.

O pensamento dos indivíduos é, visto sob esta luz, profundamente social. Sem pertencer a um mundo social, não teríamos nosso conhecimento, moral e ideologia pessoal e identidade individual.
>Uma única consciência é uma contradição in adiecto.<br/>
>– <cite>Mikhail Bakhtin</cite>

Wittgenstein argumentou que não pode haver linguagem privada. Nem podemos ter representações inteiramente privadas do mundo. Ou seja, não pode haver lógica ou semântica inteiramente privada de linguagens e representações. Nosso pensamento individual e, claro, nossa comunicação interpessoal, dependem crucialmente de nosso conhecimento socialmente adquirido da língua e do mundo. Devemos nos apoiar em conhecimentos, normas e rotinas apropriadas dos outros. Mesmo quando os indivíduos desenvolvem ideias altamente originais, eles devem confiar e respeitar sua linguagem social e grande parte de suas relações básicas com o mundo. Na verdade, é apenas quando essas condições são satisfeitas que os indivíduos podem desenvolver ideias altamente originais. Isso não quer dizer que alguns indivíduos, em circunstâncias desviantes ou excepcionais, possam ter visões de mundo relativamente "privadas" e inventar algumas rotinas idiossincráticas de uso da linguagem, mas está muito além do escopo deste tópico entrar nos meandros da psiquiatria.
>O eu dialógico pode ser descrito como uma multiplicidade dinâmica de posições-eu na paisagem da mente. (Uma 'posição-eu' é uma posição que o 'eu', ou seja, o ego, pode ocupar)<br/>
>– <cite>Hermans</cite>

O dialogismo nega a existência do sujeito autônomo, noção resumida na imagem do "ego" cartesiano: o pensador independente, o cogito.

O sujeito dialógico está socialmente inserido e sujeito a restrições corporais, e é um indivíduo consciente e racional com vontade e capacidade - em algumas circunstâncias sociais - de se entregar à ação emancipatória.

A mente/consciência do indivíduo não é dividida em um pacote de eus múltiplos desesperados e socialmente determinados, nem é uma propriedade monolítica. Em vez disso, o eu dialógico, contextualmente interdependente com os outros e com contextos, movendo-se entre diferentes posições, mas ainda parte de continuidades.

Sobre o eu corporificado e a noção de vozes: O conceito e fenômeno de 'voz' envolve pelo menos três dimensões importantes:
1. Material ou incorporação física - prosódia (acentos, entonações) e qualidade de voz
2. Signo pessoal
3. Perspectiva sobre tópicos e questões

Os sentidos como sistema dialógico: as relações entre a mente (percepção e cognição), o corpo e o meio ambiente entrelaçados; não se pode pensar em um deles sem envolver os outros. Nossas mãos, por exemplo, tanto os órgãos manipuladores quanto os perceptuais; eles são produtores e receptores de percepções hápticas (táteis).
>Quando tocamos um objeto, também somos tocados por ele e, ainda mais reflexivamente, quando nos tocamos, somos tanto tocantes quanto tocados em um duplo sentido. É como um instrumento de medição que mede seus próprios estados internos.<br/>
>– <cite>Merleau-Ponty</cite>

A mente geralmente foi vista como uma propriedade de um único indivíduo; isso faz parte da herança cartesiana do pensamento ocidental. O fato das mentes humanas estarem inseridas socioculturalmente nos dá motivos para questionar a ideia consagrada pelo tempo de que a mente é algo "interno" ao indivíduo, inteiramente subjetivo e invisível aos observadores.
>Embora o cérebro em uma cuba seja uma proposição atraente de ficção científica, os cérebros reais não podem existir ou funcionar por conta própria, isolados do mundo da atividade social humana. Cérebros reais criam laços semióticos cognitivos entre o corpo e o mundo.<br/>
>– <cite>Paul Thibault</cite>

>A cognição é distribuída ao longo de todo o ciclo estendido dos sistemas corpo-cérebro, artefatos, recursos semióticos e o mundo material.<br/>
>– <cite>Paul Thibault</cite>

A ideia da mente ampliada tem sua contrapartida na teoria do self dialógico, que é um self estendido aos outros e ao social. A mente é “algo vivo”, não um conjunto de mecanismos. O cérebro é corporificado e distribuído, isto é, caracterizado tanto pela corporificação quanto pela inserção cultural, não principalmente por idéias universais abstratas. O corpo é uma condição necessária (mas não suficiente) para significado e consciência.

O individualismo metodológico é o pressuposto de que devemos estudar os coletivos em e por meio dos indivíduos que os constituem.
>O homem existe nas relações pessoais como um centro mutante de interações em um campo de relações.<br/>
>– <cite>Mikhail Bakhtin</cite>

>A verdade não se encontra na cabeça de um indivíduo, ela nasce entre pessoas em busca coletiva da verdade, no processo de sua interação dialógica.<br/>
>– <cite>Jovchelovitch</cite>

Husserl, Merleau-Ponty e Heidegger propuseram que a epistemologia começa com a intersubjetividade e não no sujeito individual. O significado reside na interface entre o sujeito culturalmente inserido e a própria cultura (que contém outros indivíduos inseridos na cultura). Precisamos evitar ser forçados a escolher entre subjetivismo e objetivismo.

De acordo com o dialogismo, seria um erro de categoria, no sentido de George Ryle, atribuir significados a qualquer um deles (objetividade e subjetivismo): significados não são coisas sobre as quais se pode perguntar se estão fisicamente localizados. Como Putnam insistiu, “os significados não estão na cabeça”. Esforça-se muito para evitar pensar em termos de mundos "interno" e "externo" distintos, uma vez que essas noções estão conosco há muito tempo nas culturas ocidentais.

Um dos pressupostos ontológicos do dialogismo é que há uma diferença entre objetos monológicos e sujeitos dialógicos. Há suporte para isso nos dados de desenvolvimento; as crianças fazem distinções entre entidades vivas e mortas desde muito cedo. Para Bakhtin, os sujeitos têm "voz", enquanto as coisas são "sem voz".

A monoperspectividade é uma forma de monologicidade, o autor tentando autorizar apenas uma interpretação do tema tratado; o texto deve ser, ou pretende ser, inequívoco ou unívoco.

Disseminação (ensino, pregação, etc) é "diálogo suspenso".

As organizações monológicas não são inerentemente más; desejabilidade depende de situações e propósitos.

A ação comunicativa e os projetos devem ser vistos como elos que se entrelaçam em cadeias. Cada elo tem seus aspectos retrospectivos/retroativos, tornando a ação de alguma forma responsiva e relevante em relação a situações, ações e enunciados anteriores. A noção de que enunciados, textos e seus significados são elos em cadeias de outras unidades é um insight dialógico fundamental.

Não pode haver significado nem primeiro nem último; o significado sempre existe entre outros significados como um elo na cadeia de significado, que em sua totalidade é a única coisa que pode ser real.

De acordo com os pragmáticos monólogos, o "ato de fala" de John Searle, por exemplo, os atos de fala não estão essencialmente ligados a seus contextos. Em vez disso, um ato de fala searliano é uma ação descontextualizada: em teoria, um falante cria e finaliza seu enunciado (ato de fala), suas intenções, significado e execução da performance por ele mesmo.

Segundo a teoria dialógica, não basta dizer que os enunciados formam cadeias. Em vez disso, um conceito central na análise dialógica será o de "projeto comunicativo", uma noção parcialmente holística. Os projetos comunicativos são dinâmicos - podem mudar à medida que são executados ou concluídos, às vezes de formas que não foram projetadas desde o início. O significado não está com o falante, nem com o destinatário, nem com o enunciado sozinho, mas sim com o passado interacional, o presente e o próximo enunciado projetado.
>Tudo o que dizemos e fazemos é uma resposta a alguma coisa.<br/>
>– <cite>Mikhail Bakhtin</cite>

Levinas argumenta que resposta, responsividade e responsabilidade são o que nos torna humanos. Tornamo-nos responsáveis porque temos que responder a outras pessoas.

O dialogismo não é uma estrutura social-determinista na qual os indivíduos humanos são reduzidos a pontos de passagem de várias influências sociais. O diálogo é o ambiente para processos nos quais os indivíduos constroem suas identidades (individuais) e se estabelecem como agentes sociais responsáveis. É quando o outro me olha e fala que me responsabilizo (por agir em relação a ele).

Bruner argumenta que o conceito central da psicologia humana é o significado e os processos e transações envolvidos na construção do significado.
>Não estímulos e resposta, não comportamento abertamente observável, não mergulhos biológicos e suas transformações e não informação e processamento de informação, como em um paradigma cognitivista extremo.<br/>
>– <cite>Jerome Bruner</cite>

Em tal teoria [onde o significado é central], os agentes, ou pessoas, são fundamentais na construção do significado. Cognição e comunicação envolvem intervenções no mundo, engajamento com o mundo. A ação no mundo é uma função semântico-pragmática mais básica para a linguagem e a comunicação do que a representação do mundo. A linguagem não é principalmente uma linguagem de representação; em vez disso, representar algo pode ser reanalisado como um tipo de ação.

Uma teoria da ação dialógica não nega a agência dos indivíduos, mas abre uma porta para a agência dos coletivos.

Bartlett argumenta que se pode falar de toda reação cognitiva humana - percepção, imaginação, lembrança, pensamento e raciocínio como um esforço após o significado. Se o significado está ligado à ação e interação, o mesmo se aplica à compreensão, que envolve ser capaz de lidar com as situações.

Garfinkel argumenta que em nenhuma circunstância devemos nos preocupar em “entender tudo”; precisamos de compreensão apenas para “propósitos práticos atuais”.

A criação de sentido é - tanto para o falante quanto para o receptor - um empreendimento dinâmico que envolve atividades comunicativas e movimentos discursivos; isso [fazer sentido] não é uma questão de produzir e perceber mapeamentos da realidade. A criação de sentido é, portanto, em princípio, um processo sem fim. Veja as noções bakhtinianas de "inacabamento" e "impossibilidade de finalização" do significado.

Bateson argumenta que a noção de "compreensão (suficiente) para os propósitos práticos atuais" está relacionada à noção de uma "diferença que faz a diferença". A comunicação em diálogo é uma conquista prática de momento a momento. Assim como os entendimentos são parciais, o conhecimento sobre o mundo não é absolutamente certo. A busca pela certeza absoluta, muitas vezes no topo da agenda dos filósofos (Descartes), é irreal; para agir, precisamos de certeza relativa. O significado não é um fenômeno puramente cognitivo; consequentemente, a cognição pura é um artefato, um produto da idealização normativa dentro de uma tradição racionalista (por "cognição pura" quero dizer cognição completamente separada dos desejos, preocupações e compromissos humanos). Significado e construção de sentido não podem ser entendidos como inteiramente subjetivos, nem como fenômenos completamente objetivos; eles [significado e criação de sentido] pertencem a um "mundo interno" entre os indivíduos e seu ambiente.

As teorias dialógicas são amplamente voltadas para a explicação da criação de sentido - pode-se pensar que o dialogismo faz parte da semiótica. No entanto, as teorias dialógicas tendem a adotar uma abordagem dinâmica à criação de sentido em oposição às teorias relativamente estáticas (Peirce, Saussure) dos signos. As teorias dialógicas enfatizariam a ação-base da criação de sentido e da significação. Os signos são em si mesmos ações. Os signos não possuem expressões estáticas, muito menos significados fixos, e também possuem aspectos responsivos e projetivos.
>O fluxo da razão e dos pensamentos e a evolução temporal das idéias e atitudes são determinados e explicados pela interação íntima, complexa e contínua do cérebro, do corpo e do mundo.<br/>
>– <cite>Andy Clark</cite>

Os pensamentos não são unidades autônomas. Muitas vezes não está claro o quanto "pertence a" um 'pensamento'; por exemplo, quantas pressuposições e implicações devem ser incluídas? O dialogismo rejeita a afirmação geral de Descartes de que pode haver algum tipo de conhecimento do mundo que é absolutamente certo. No entanto, o dialogismo afirma que podemos ter um conhecimento razoavelmente certo, com base em evidências empíricas e reflexivas. A consciência tem uma base sociodialógica e o mesmo se aplica ao conhecimento; o conhecimento é de natureza social e está intimamente relacionado com a comunicação e a ação. A linguagem em uso é sempre indexical, alusiva e incompleta; os enunciados devem basear-se em contextos de vários tipos. A mente, a linguagem e o conhecimento do mundo são produtos em evolução e dinâmicos, nunca acabados. Em suma, no dialogismo a dinâmica é básica. As atividades e situações em que participamos não são estáticas, mas sempre em processo de 'devir' (dialeticamente) e nossas ações evoluem de forma dinâmica.

Bakhtin argumenta que devemos olhar para o mundo como um evento e não como uma existência em forma pronta. Se objetos e entidades parecem ser fundamentais em nossa compreensão do mundo, eles devem ser pensados como fenômenos contextualizados apoiados por uma cultura tipicamente letrada que está constantemente envolvida em práticas de categorização e reificação. O diálogo e o discurso devem ser vistos em termos de "vir a ser", "tornar-se" (e tornar-se iterativo) e na construção (e reforma), em vez de "ser". Uma característica importante do dialogismo é sua insistência na dinâmica, emergência, proveniência, mudança e evolução. Os processos são mais fundamentais do que os produtos (que às vezes podem ser percebidos como objetos). Viver é interagir com o meio ambiente. Pode-se pensar o dialogismo como uma ontologia para organismos vivos, especificamente, para aqueles que possuem uma mente encarnada viva, abrigando processos semióticos.

A ênfase na dinâmica está ligada a uma crença nas teorias dialógicas de que os comportamentos na cognição, interação e uso da linguagem podem ser parcialmente explicados em termos de seus precursores de desenvolvimento (materialismo histórico). A linguagem não é simplesmente um sistema fora do fluxo da história social, as estruturas linguísticas não são temporais, abstratas, espirituais ou mentais, mas organizadas no tempo e incorporadas por pessoas reais. Mente e corpo caminham juntos, uma posição que se opõe tanto ao pressuposto do corpo sem mente (empirismo) quanto à da mente sem corpo (racionalismo).

Potencialidades (capacidades subjacentes que só podem ser atualizadas, ativadas, exploradas e desenvolvidas em contextos) são centrais para o dialogismo. A noção de potencialidade remonta a Aristóteles. Potenciais de significado; recursos para a criação de sentido que são relativamente estáveis, embora parcialmente abertos, multiplamente determináveis ​​no contexto e dinamicamente modificáveis ​​ao longo do tempo sócio-histórico.
>O sistema de linguagem é um potencial que se realiza em enunciados concretos que utilizam os recursos de significado da linguagem.<br/>
>– <cite>Mikhail Bakhtin</cite>

A noção tradicional de "uso da linguagem" é enganosa, uma vez que sugere que primeiro temos idiomas à nossa disposição, em seguida, começamos a "usá-los". Uma teoria dialógica da linguagem deve atribuir primazia à ação (atos, atividades) e não à cognição pura e à transmissão de produtos cognitivos. Uma razão importante pela qual a linguística tradicional virou essas coisas de cabeça para baixo é que ela priorizou a linguagem escrita.

Os artefatos estão profundamente envolvidos na interação humana; muitas formas de cognição e comunicação humanas não podem ocorrer sem artefatos. Os artefatos recebem recursos para a criação de significado e se tornam partes de uma mente ampliada. Artefatos: telefones, calculadoras, PCs, internet, máquinas de anestesia, martelo, tesoura, óculos, óculos, gráficos, linguagem escrita ... Artefatos não são apenas objetos (físicos ou abstratos) a serem conceituados isoladamente de seus usuários humanos. Em vez disso, eles [artefatos] são inscritos com potenciais de significado, ou melhor: recursos.

Recursos são fenômenos relacionais, não propriedades estáticas de objetos; eles têm oportunidades de uso (potencialidades) que são selecionadas e liberadas por agentes humanos que as implantam e entendem de maneiras especiais. Recursos ou potenciais ainda não são significados percebidos para os usuários.

Quando os artefatos estão sendo usados e têm sentido, eles se tornam artefatos em uso ao invés de artefatos “como tais” ou objetos físicos tomados em abstracto. Os artefatos são apropriados pelos usuários de diferentes maneiras em diferentes contextos. A apropriação implica transformar artefatos em algo que os usuários “possuem” e integram às suas atividades. Os artefatos podem ser conceituados como partes da mente estendida (ou distribuída), estendendo-se ao corpo e ao ambiente. Artefatos são "objetos de fronteira" típicos que conectam contextos e situações. Eles podem ser movidos entre situações e eles trazem conhecimento cultural.

A mente humana pode realizar tarefas de maneiras que os computadores não podem e como muitos diriam (Hubert Dreyfus) nunca serão capazes de fazer. As razões pelas quais os computadores não podem por si simular a mente humana têm a ver com as propriedades dialógicas da cognição e comunicação humanas.

Se a dialogicidade é universal em humanos, ela deve ter um fundamento biológico. O cérebro é projetado para controlar e monitorar a interação com o meio ambiente. Os cérebros são interativos em vários aspectos. Essa interatividade é um pré-requisito para a dialogicidade da mente humana. No entanto, muitos estudiosos das ciências humanas e sociais mostram uma clara falta de interesse nas teorias neurobiológicas modernas. Há uma grande lacuna entre a neurobiologia e as teorias, digamos, da criação de sentido e da linguagem.

No entanto, o dialogismo está em ressonância com muitas teorias modernas da neurobiologia. Pelo menos isso se aplica à estrutura e funções no “nível do pico” neuronal (neurônios-espelho) e em um nível global (interação entre diferentes partes do cérebro e suas interconexões com o corpo).

Apesar dos argumentos que falam a favor de uma neurobiologia dialogista, muitas vezes tem sido afirmado que cérebros, por necessidade, posses individuais, devem ser “monológicos”. Para a maioria dos psicólogos, sua disciplina está, por definição, preocupada com o fenômeno mental individual. A mente é (explícita ou implicitamente) entendida como algo alojado dentro do crânio, e as teorias do cérebro humano facilmente convidam a interpretações individualistas e, assim, reforçam o individualismo na psicologia e nas ciências cognitivas. Afinal, apenas os indivíduos têm cérebro.

Do ponto de vista dialógico, este parece ser um argumento enganoso e uma conclusão falaciosa; mesmo que apenas os indivíduos tenham cérebros e mentes, isso não significa que nos entregamos apenas a atividades verdadeiramente individuais, em monologismo ou solipsismo. Na verdade, a suposição mais frutífera é sem dúvida o oposto: o cérebro é dialógico!

A ciência cognitiva por muito tempo foi dominada por abordagens baseadas em suposições de processamento de informações e representações mentais do mundo. Cognição vista como processamento de informações, com "informações" consideradas itens em uma representação do mundo, esses itens sendo privados de agência, incorporação e cultura. Devemos contestar a suposição cognitivista de que o cérebro está principalmente envolvido na modelagem, geração de imagens e imaginação da realidade externa.

Em vez disso, o cérebro é projetado para servir como um meio para o indivíduo se relacionar com seu ambiente, incluindo situações sociais. Os cérebros são interativos (ou em um sentido metafórico: “dialógico”).

Antônio Damásio argumenta que o cérebro interage com o resto do corpo, a mente está incorporada, não apenas incorporada, e o corpo está atento. Devemos ver a mente como surgindo de um organismo, e não de um cérebro desencarnado. Em vez de ser a sede de representações mentais epistemicamente privadas, o cérebro funciona para regular a interação do corpo com seu ambiente ecossocial.

Clark argumenta que a mente não está contida no cérebro individual: em vez disso, ela está inextricavelmente entrelaçada com o corpo, o mundo e a ação. Ao falarmos uns com os outros e com nós mesmos, criamos recursos, oportunidades que convidam o outro a ver e mover-se em certas direções que parecem promissoras.

Damasio argumenta que todos os componentes do cérebro são caracterizados por atividades continuamente em andamento nessas redes neurais, que têm potencial para processamento paralelo. No mesmo espírito, Hodges (2007) aponta para as observações de Gibson sobre a percepção como uma exploração incessante do ambiente. Para Damásio, os processos não são concluídos nem simplesmente ligados e desligados. Podemos comparar isso com a noção bakhtiniana de "impossibilidade de finalização".

Descartes sugeriu apenas uma resposta verdadeiramente ad hoc à questão “como corpo e mente se comunicam?”, em sua teoria da glândula pineal.

A psicologia cognitiva foi por muito tempo dominada por uma teoria mental "sem corpo", enquanto a neuropsicologia foi muitas vezes "sem mente". Até certo ponto, esta é a herança do que Damásio chama de "Erro de Descartes", a separação completa de mente e corpo. Uma estrutura dialógica geral tem algo a oferecer à ciência cognitiva e à psicologia evolucionista, até porque o diálogo e a interação dialógica parecem ser mais fundamentais do que a linguagem.

Bakhtin propôs que as ciências exatas (físicas) (lidando com coisas sem voz) são monológicas. Já as ciências que consideram os humanos como seres humanos, ou seja, sujeitos dublados, são necessariamente dialógicas.

Se olharmos para a história da genealogia das disciplinas científicas, descobrimos que as diferentes (posições mais ou menos “monológicas”) foram esculpidas em uma interação dialógica com posições concorrentes. Portanto, os proponentes de uma teoria (monológica) não podem evitar a influência de seus oponentes, aqueles que aderem a teorias concorrentes.

A história das disciplinas às vezes pode ser explicada como uma luta entre tendências opostas em grande escala. Por exemplo, a tensão entre formas de pensamento sócio-interacionais (hermenêuticas, ‘dialogísticas’) e paradigmas behavioristas ou cognitivistas inspirados nas ciências naturais.

Como já sabemos, as teorias dialógicas ou dialogismo afirmam ser o paradigma acadêmico para a teorização da ação humana, cognição e comunicação.

Mas o dialogismo é em si dialógico?

Há um certo paradoxo em, por um lado, propor uma perspectiva sobre o mundo humano que enfatiza a dinâmica, multiplicidade, contradições parciais e, por outro lado, propor apenas essa perspectiva única, tornando assim a metateoria 'monológica'.

O dialogismo pode ser visto como uma "grande" epistemologia para as ciências humanas, culturais e sociais. Em oposição a tal postura monologizante, pode-se propor, que não devemos tentar lutar por "grandes teorias". (No entanto, observe que, aliás, a teoria de que não deveria haver grandes teorias é em si uma grande teoria!)

Não existe uma maneira simples e fácil de sair desse dilema. Precisamos desenvolver uma alternativa coerente ("dialógica") ao "monologismo". Ao mesmo tempo, esse dialogismo deve atribuir um lugar às "práticas monológicas" e não deve ser confundido com o relativismo pós-moderno extremo.

Aviso importante: não faça suposições de que todo o acadêmico pode ser dividido em monólogos e dialogistas, ou que o trabalho de acadêmicos que não podem ser facilmente colocados em um ou outro “campo” é de alguma forma inerentemente ambíguo e, portanto, até mesmo potencialmente falho. Muitas tradições não são monólogas nem dialogistas por natureza.

Pode-se chamar o monologismo abrangente de “cartesiano”, nomeando Descartes como seu representante por excelência. No entanto, abrange muito mais filosofias europeias, muito mais atrás na história do que Descartes (por exemplo, Platão) e muitos depois dele (por exemplo, Hume, Kant, Frege). Enquanto os dialogistas podem incluir Nietzsche, Wittgenstein (o “posterior”), Heidegger e Merleau-Ponty.

O cartesianismo busca uma discussão ordenada e racional com coerência, lógica e frequentemente normatividade, sem consideração de interesses e compromissos humanos “irrelevantes”, erros e deficiências, poder, ilusões, conluios, etc. A alternativa dialógica reconhece as reais realidades socioculturais no mundo humano: várias tensões e heterogeneidades.

O fundamento metateórico do monologismo é frequentemente a suposição de que as "realidades" subjacentes à superfície desconcertante são simples e, portanto, belas. As teorias dialógicas pressupõem que as realidades socioculturais são ricas e complexas e, a esta luz, a “redução de dados” de muitos relatos científicos parece ser mais uma necessidade prática. Uma regra prática consagrada pelo tempo na ciência monologista é a Navalha de Occam: deve-se pesquisar teorias econômicas ou parcimoniosas e os fenômenos explicativos não devem ser multiplicados além do que é absolutamente necessário. Um princípio relacionado é o do Cânone de Morgan: cada explicação deve proceder de um nível inferior para um nível superior em uma hierarquia de ciências bem conhecida. O assunto das teorias dialógicas, ou seja, principalmente a criação de sentido humano, pensamento, comunicação, linguagem, ação no mundo são complexos e não se prestam necessariamente a explicações que obedecem completamente aos princípios da Navalha de Occam ou do Cânon de Morgan.

Merleau-Ponty argumenta que a perspectiva está relacionada à corporificação; é porque meu corpo está localizado em um determinado lugar que experimento um ponto de vista.

Dialogismo pode ser entendido como uma contra-teoria ao monologismo, por exemplo. modelos individualistas e representacionais de cognição, comunicação e ação. Mas o dialogismo é diferente do construcionismo radical. É possível que o dialogismo incorpore uma forma de construcionismo social. Mas então estamos preocupados com um construcionismo contextual, em vez de um construtivismo radical.

Aqui estão algumas interpretações que foram atribuídas ou associadas ao dialogismo.
1. Relativismo extremo - o mundo (como apareceu e entendido) é inteiramente construído na e através da (dentro) linguagem e discurso.

O dialogismo apóia um "construcionismo contextual" que é compatível com um realismo ontológico. "Realidade" inclui o mundo externo e as mentes dialógicas (e sua interação). Assim, os extremos de viradas "linguísticas"/"discursivas" nas ciências sociais devem ser evitados.

2. Situacionalismo extremo - a criação de sentido ocorre exclusivamente dentro dos limites de situações específicas; portanto, o significado não existe fora das ocasiões em que é criado.

Reconhecer a importância da interação situada na pesquisa não implica ficar com suas especificidades e detalhes, mas podemos e devemos também procurar generalizações e regularidades subjacentes ("quididade" no mesmo jargão filosófico).

3. Determinismo social extremo - o indivíduo é inteiramente determinado por influências sociais.

O dialogismo atribui agência e responsabilidade aos indivíduos, mas é claro que ainda assume que os seres humanos são interdependentes dos outros. O eu é apenas relativamente autônomo, estando parcialmente sujeito a uma ordem social que opera nas suas costas. O indivíduo não é dividido em um feixe de eus múltiplos, socialmente determinados; em vez disso, poderíamos falar de um eu dialógico, contextualmente interdependente com outros e com contextos.

4. Coletivismo extremo (pensamento de "mente de grupo") - pensar e se comunicar em uma sociedade pressupõe a existência de uma mente coletiva.

A suposição de que os indivíduos têm uma mente dialógica não implica que haja uma mente coletiva supra-individual (Volksseele). Uma mente precisa de um corpo (e cérebro) (“hardware”) para sustentá-la.

5. Intersubjetivismo extremo - o diálogo envolve ou implica o empenho em uma comunicação perfeita.

Não há diálogo entre seres humanos mortais que conduza ao entendimento completo em um sentido absoluto. Os entendimentos nunca são completos ou perfeitos, mas geralmente são suficientes para os atuais propósitos práticos.

Essas interpretações falaciosas, injustificadas ou enganosas frequentemente significam reduções ao absurdo. São mal-entendidos que podem desacreditar o dialogismo, a menos que sejam evitados ou refutados.
