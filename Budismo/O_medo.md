# O medo
O medo também tem sua fonte no apego, e que é um erro de percepção: imaginamos um mundo fantástico e um mundo horrível que acabam dominando nossas mentes. Ele é uma forma de rejeitar o sofrimento.

Ao contrário da concepção ocidental, nós não temos paz ou amor, porque tudo é uma realidade transitória. Para ter, eu teria que tê-lo para sempre, mas isso é impossível, pois tudo transita, nada é estático. Quando não aprendemos com isso, surge o apego e com isso surge o medo da perda, e de novo, é um ciclo vicioso: o medo alimenta o apego e o apego alimenta o medo.

Fugir nunca é uma opção para lidar com o medo. A melhor maneira de tratar o medo é a plena concentração no presente e a compaixão.
