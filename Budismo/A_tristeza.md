# A tristeza
A tristeza pode ocorrer por vários motivos, mas ela tem uma solução: se nos lembrarmos que as pessoas, nós, as coisas e a realidade são o que são, e que podemos apreciar o que temos, ao invés de lamentar o que não temos, e com compaixão aprendermos que a dor é passageira e nada é estático, a dor vai-se embora. É o contentamento com a existência, por sermos parte de algo grande, algo belo, onde até mesmo o mal é complexo.
>A pessoa que conhece o contentamento é feliz, mesmo dormindo no chão duro; a pessoa que não conhece o contentamento é infeliz mesmo num palácio celestial.

Como está nossa respiração? Como estão os batimentos cardíacos? Como está a nossa postura? Que pensamentos são esses que me fazem deixar os ombros cair para frente, baixar a cabeça e, quem sabe, chorar? Como se formam as lágrimas? E mesmo em meio a lágrimas, podemos sorrir e perceber que enquanto vivas criaturas temos esta experiência extraordinária e bela de poder ficar triste. Tristeza que vem. Tristeza que vai. E sem se apegar a coisa alguma e sem sentir aversão a coisa alguma descobrimos o verdadeiro sentido da vida.

Abandonar a tristeza é abrir as mãos, o coração, a mente para a emoção seguinte.
