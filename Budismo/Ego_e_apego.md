# Ego e apego
A origem de toda a insegurança é o ego, que é uma concepção equivocada de um "eu" que existe por si próprio. O ego precisa constantemente se ocupar de tarefas para reafirmar ele mesmo, reafirmar a sua existência, e por mais que ele tente, essas relações de reafirmação são instáveis, pois temos medo de falhar na nossa reafirmação do "eu". É um ciclo vicioso sem fim.

Uma prova de que não existe tal "ego" é que somos mutáveis: não há um "eu" permanente; tudo está sujeito à mudanças, nada é permanente, este é o curso da vida (Samsara), e entender isso é se livrar das concepções egoístas que afligem o ser.

Cadê o ego?
- Está no meu nome? A resposta é não. Poderiam me chamar de um jeito ou de outro e isso não teria influência no meu modo de ser.
- Está na minha nacionalidade? Também não. Eu poderia ter nascido em qualquer outro país.
- Está nos meus pensamentos? Este é um ponto delicado. Muitos afirmam que somos o que pensamos porque dos pensamentos surge a ação. Hoje, no entanto, posso pensar em uma coisa e amanhã em outra. Portanto, um pensamento pode ser mais ou menos duradouro, mas eu não sou esse pensamento. Quantas vezes já mudamos de ideia? Quantas vezes já pensamos que temos pouco valor, mas nosso entorno nos faz ver e acreditar exatamente no oposto?
- Está nas minhas ações? Nem sempre realizamos as mesmas ações. Podemos cometer erros e aprender. Podemos repetir a mesma ação algumas vezes, mas temos a capacidade de mudar o nosso comportamento. Portanto, não existe uma ação inerente que nos defina, porque também é variável.
- Está na minha cultura, ou então na minha sociedade? Posso fazer parte de uma ou outra cultura. Isso é aleatório. Além disso, apesar da nossa cultura, também temos nosso modo de ser, nosso próprio modo de pensar. Quando viajamos, lemos, meditamos, estudamos… pode haver uma mudança em nós que muda o nosso condicionamento social e cultural.
- Está no meu corpo? Em que parte do meu corpo está o ego? Eu sou o meu corpo? Se um dia eu sofrer um acidente e ficar sem as pernas, continuo sendo eu? A princípio, sim, mas sem as pernas. O ego não varia, mesmo que não tenha pernas. Portanto, também não está no meu corpo.

Quando nos percebemos de forma estática e invariável, nos apegamos ao nosso ego. O ego e o apego andam de mãos dadas. “Eu sou assim” nada mais é do que uma declaração sobre a pouca consciência de mudança que todos nós possuímos. Quando nos apegamos ao ego, nasce nossa identidade férrea e com poucas possibilidades de mudança. No entanto, tudo muda. Quando libertamos a mente de uma identidade estática, nos abrimos a mudanças e circunstâncias externas. Dessa forma, a intensidade do nosso sofrimento diminui.

Ao perceber o mundo a partir do nosso ego estático, queremos que tudo o que acontece se encaixe nas nossas expectativas, com as ideias que temos do que deveria acontecer. “Eu sou assim e as coisas devem ser como eu acho que deveriam ser”. Do ego surge o egocentrismo. Ou seja, tudo deve ser como eu acho que deveria ser. Se algo é diferente das minhas expectativas, sofro, fico irritado, com raiva, etc. E também surge o egoísmo, ou seja, em nosso mapa mental passamos a ser o Sol, pensando no que nos rodeia como elementos que giram ao nosso redor.

Portanto, o ego vê uma separabilidade entre "eu" e o "mundo", mas que não existe de fato, por causa da vacuidade.
