# O desafio de criar uma nova vida
Ainda não entendemos a vida por completo, pois ela é muito complexa. Aqui um [PDF](https://sites.usp.br/lbbp/wp-content/uploads/sites/464/2019/03/AMN-Metabolismo.pdf) juntando metabolismo e catabolismo, mas essa não é a parte mais complexa do processo. [Aqui](http://biochemical-pathways.com/#/map/1) outro mapa biológico gigantesco.

Uma área interessante é a proteômica, que tem como objeto de estudos, sistemas complexos como este.

O problema de construir um ser vivo é que há milhões de partes móveis e os seres vivos que conhecemos nunca dão arranque, eles apenas se dividem já funcionando.

Porém, uma vantagem é que as moléculas de DNA são construídas e interpretadas igualmente por todos os seres vivos. Um mesmo códon faz sempre o mesmo aminoácido em qualquer ser vivo.
