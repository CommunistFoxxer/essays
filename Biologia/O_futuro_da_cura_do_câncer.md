#;O futuro da cura do câncer
Dá para escrever DNA com funções auto-regenerativas para os órgãos e curar o câncer com programas genéticos extras, também dá para escrever DNA que constrói células que façam computação digital, mas isso é muito primitivo ainda.

Design de macromoléculas por programação no DNA ainda é muito difícil. Existem diversos mistérios e desafios nesse campo. É preciso de muito poder de processamento para projetar um gene, além de equipamento muito caro para colocar hélices de DNA com conteúdo arbitrário dentro de uma bactéria, e a produção é muito lenta, quase artesanal.

Atualmente a técnica mais prática envolve usar fungos especiais para construir as hélices de DNA, mas eles cometem bastante erros e é preciso refazer várias vezes. Existem pesquisadores tentando construir uma "impressora 3D de bactérias", mas é muito difícil. Não só é muito difícil organizar moléculas individuais em uma estrutura coerente, como é difícil "ressuscitar" uma bactéria morta.

Mas no dia em que essa tecnologia for aperfeiçoada, nós teremos bactérias ciborgues e nanocomputadores biológicos feitos à partir de bactérias, ou seja, o que hoje nós imaginamos como nanobôs, provavelmente serão bactérias artificiais.
