# Introdução a eletrônica
Irei dar uma introdução aqui a eletrônica - com foco em eletrônica analógica.

![](images/generic_processor_diagram.png)

Esse é um diagrama genérico de um processador (processadores são chips digitais) - e portanto, não é real, por exemplo, o diagrama da Intel é bem maior.

Bus significa barramento, é uma trilha de circuito que leva dados de um lado para outro. O barramento existe do lado de fora do processador, na placa de circuito (placa-mãe é uma placa de circuito), é por ele que o processador se comunica com o restante da placa.

Lembra como a placa-mãe tem várias peças revestidas de plástico preto, com formato mais ou menos retangular e pouca altura? Aquilo são microchips. O processador é um deles, só que maior e usa mais energia.

![](images/motherboard_microchips.png)

![](images/motherboard_2.png)

As linhas mais claras são trilhas de cobre revestido, elas transportam eletricidade. Transportam tanto a energia que alimenta os chips e componentes quanto os sinais digitais de comunicação do circuito.

Os roxinhos do lado do soquete de cooler se chamam capacitores eletrolíticos, são componentes eletrônicos. Ao lado deles tem limitadores de tensão (as peças pretas com uma parte metálica mais grossa saindo deles) e aquelas coisas verdes com cobre enrolado são indutores toroidais.

Um circuito é o conjunto da placa com todos os componentes dela. Consegue ver os retângulos coloridos que desenhei? O retângulo amarelo são os capacitores. Os retângulos rosas são os limitadores de tensão, e os verdes são os indutores toroidais.

![](images/motherboard_1.png)

Basicamente, indutores criam um campo magnético, capacitores criam um campo elétrico. Os dois armazenam energia em formas diferentes para manipular os componentes da onda de energia. Através do atraso deliberado de uma parte ou outra do sinal, você pode criar filtros de ondas, ou ressonâncias, selecionar frequências, etc.

Um campo elétrico armazena energia com o diferencial de tensão entre duas chapinhas metálicas enroladas. Campo elétrico induz diferença de potencial, ou seja, tensão. Campo magnético induz corrente, porque ele é um campo de energia em movimento. O campo elétrico é composto de energia estática. Quando você tem um sinal de corrente alternada, a energia varia de forma em volta do condutor de eletricidade, ela "para" em determinados nós (campo elétrico) e "corre" entre eles (campo magnético).

Eletricidade é uma manifestação de uma das forças primordiais da natureza, a força eletromagnética. Ela existe em todo o universo, e nós aprendemos a manipular ela usando elementos metálicos.

Existem duas partículas fundamentais por trás da eletricidade: o próton e o elétron. Elas carregam essa forma especial de energia e uma atrai a outra. Todo átomo no universo possui um número fixo de prótons no núcleo, e alguns elétrons livres girando em volta na velocidade da luz. Basicamente os elétrons são repelidos por outros elétrons e atraídos por prótons. Quando nós usamos elementos metálicos de uma determinada forma, conseguimos fazer os elétrons irem em massa em uma direção ou outra, atraindo eles com lacunas (espaços na matéria onde tem mais prótons do que elétrons) e repelindo eles com excesso de elétrons. Por convenção, a carga elétrica do elétron é chamada de negativa, a do próton é chamada de positiva. As pilhas químicas que usamos em controle-remoto e brinquedos usam aqueles símbolos negativo-positivo por isso (obs: isso não está ligado com o sistema binário que é mais matemática do que eletrônica per se, mas o fato de só existirem dois tipos de cargas elétricas influencia sim).

Na natureza é comum a energia elétrica fluir de um lado pra outro o tempo todo, sem controle. Os raios são um exemplo de um monte de eletricidade subindo ou descendo das nuvens de uma vez só. Quando uma quantidade de carga elétrica entra em movimento, nós dizemos que ela criou uma corrente. Existe uma unidade de medida padronizada pra quantidade de corrente elétrica, o ampère, aparece nas baterias de celular, miliampère-hora, ou seja, quanta corrente ela iria gerar se fosse descarregar toda a energia acumulada dela por exatamente uma hora. As baterias de carro têm a capacidade delas medida em ampères-hora porque são muito mais potentes, tem muito mais energia guardada nelas.

Quando em um material você tem mais prótons disponíveis, ou seja, mais lacunas do que elétrons para equilibrar as cargas, por convenção dizemos que o material está positivo, isso pode acontecer se você esfregar uma lã em um pente, ele vai ficar eletricamente carregado, e você pode saber o que vai acontecer caso você esfregue 2 materiais diferentes, basta procurar pela série triboelétrica. Se aproximar esse pente de pedaços pequenos de papel, os átomos do papel serão atraídos pela carga elétrica positiva do pente. Todo átomo no universo tem carga elétrica, é uma energia fundamental da natureza. Quando você tem um material nesse estado no qual o pente ficou, eletricamente carregado, nós podemos dizer que ele tem eletricidade estática. É literalmente energia elétrica em um estado de não-movimento, ou de movimento insignificante.

Já brincou com imãs? Eles têm dois pólos, e com a interação deles eles podem se atrair ou se repelir, essa repulsão ou atração é devida à interação de magnetismo, que é uma forma especial da energia elétrica. Magnetismo é muito parecido com a eletricidade estática, mas é energia em movimento constante. Todo imã tem uma pequena quantidade de eletricidade que fica viajando de um polo para o outro em volta dele, ela é repelida de um lado do imã, e atraída pelo outro lado, por isso ela forma círculos em volta do imã. Essa região no espaço em volta do imã existe porque a natureza transporta a energia dos elétrons do material na forma de fótons virtuais. Mas isso é física de partículas.

O importante é saber que a eletricidade sempre se manifesta de duas formas principais: parada ou em movimento, e a energia elétrica sempre é atraída pelas lacunas (maioria de prótons, falta de elétrons) e repelida pelo excesso de elétrons. Lacunas fazem um polo "positivo", excesso de elétrons fazem um polo "negativo". A eletricidade sempre irá fluir do negativo para o positivo. Quando você tem uma diferença na quantidade de cargas entre dois materiais diferentes, você tem uma diferença de potencial.

Quanto mais diferença de potencial, mais força os elétrons vão fazer indo de um lado pro outro. O caminho dos elétrons raramente é fácil, eles sempre viajam em volta dos átomos dos materiais, e a maioria dos materiais tornam a passagem deles difícil, ou seja, os materiais criam resistência à passagem da corrente. Para a eletricidade ser útil pra nós, ela precisa fazer uma corrente. Quanta corrente vai passar, depende da resistência entre um polo e outro. Quanto maior a resistência, menor a corrente, e quanto maior a diferença de potencial, mais forte será a corrente.

Os metais têm uma grande abundância natural de elétrons livres na camada de valência, por isso os elétrons viajam com muita facilidade neles. A energia que o elétron precisa ganhar para escapar da atração dos prótons dos átomos dos metais é pequena, então os metais tem uma resistência elétrica muito baixa.

Há uma lei que resume isso: lei de Ohm, que estabelece que `U = R . I` e `R = U / I`, onde U é a tensão ou diferença de potencial elétrico, medida em volts, I é a corrente, medida em Ampères e R é a resistência, medida em Ohms. Isso é o básico de eletrônica de corrente direta.

Se a gente modificar a forma de criar a corrente, entramos em uma nova área de estudo: a eletrônica de corrente alternada. Corrente alternada é uma corrente na qual nós fazemos os elétrons irem pra um lado, depois voltarem, irem de novo, voltarem de novo, sem parar, o tempo todo. O tempo todo, invertemos os polos, então eles vão e voltam com bastante força.

Dependendo da quantidade de inversões de polos por segundo, nossa corrente ganha uma frequência. Se forem 2 inversões por segundo, a frequência é de 2 Hz (Heartz), se forem 10, 10 Hz, etc. A eletricidade fornecida nas tomadas do Brasil oscila em mais ou menos 60 Hz.

Essa corrente alternada não simplesmente viaja na velocidade da luz de um lado para outro, sem parar. Ela viaja variando de forma, imitando a variação de força que a mudança de polos está causando, porque nos metais, a camada de valência está mais distante do núcleo.

A eletrosfera é a região em volta das camadas de valência de um material metálico. É uma região com muitos elétrons energizados, que não estão presos nos átomos mas estão sendo atraídos o suficiente para ficarem ali. É ela que torna os metais em condutores muito bons de eletricidade.

Os elétrons ganham energia absorvendo fótons e perdem energia liberando fótons. Os elétrons em si são indestrutíveis, eles nunca "esvaziam". Ees podem perder quase toda a energia deles, mas sobra a energia de um fóton, e eles têm um limite de energia que eles podem adquirir (é uma das constantes físicas estudadas).

Esses fótons são os mesmos fótons que fazem a luz. Fótons são partículas virtuais, elas não têm massa, apenas energia pura. Os elétrons têm massa. A única diferença entre a eletricidade que faz os sinais de rádio e os que fazem a luz é a frequência de vibração dos fótons.

A luz visível vibra em frequências de vários Terahertz. As ondas de rádio vibram muito mais devagar, isso as permite atravessar as paredes, então ondas de rádio são literalmente luz invisível.

A corrente alternada viaja pelo condutor (que pode ser um fio metálico) alternando momentos de campo elétrico e magnético, ou seja, ela "para" em determinados pontos, e "acelera" entre eles. Nesses pontos que ela "para", ela imita o que acontece no pente: energia estática. Nos trechos em que ela "acelera e desacelera", imita o que acontece no imã: magnetismo.

A variação da diferença de potencial (eixo XZ) e variação de corrente (eixo XY) ao longo do tempo. A variação de tensão e de corrente não acontecem ao mesmo tempo, existe sempre uma pequena diferença entre elas, esse pequeno atraso entre elas chamamos de defasagem. Essa defasagem é medida em graus, como na geometria.

A eletrônica alternada usa os componentes eletrônicos para manipular os sinais de tensão e corrente, usando da física ondulatória da energia elétrica para produzir os efeitos.

Lembrando que capacitores armazenam campo elétrico, indutores (bobinas de fio metálico enrolado) armazenam campo magnético, ou seja, os indutores prolongam a fase de movimento da energia, os capacitores prolongam a fase de "parada" da energia manipulando o atraso em graus de cada componente da onda, onde se cria uma série de efeitos que eu não vou explicar agora. São como lentes especiais. O estudo da corrente alternada usa e abusa da física ondulatória.

Tudo o que você imaginar em óptica, acontece na eletrônica alternada, adaptado. Mais ou menos da mesma forma que você usa espelhos e lentes, usa os resistores, indutores e capacitores. Mas na eletrônica digital, nós usamos corrente direta em ultra-baixa tensão, porque é mais fácil de manipular a energia em alta velocidade se ela vier em quantidades minúsculas.

Na eletrônica digital entra em cena o transistor, um componente eletrônico que serve para principalmente duas coisas: amplificar uma onda e ligar-desligar um sinal. Na eletrônica digital só se usa essa última função, para criar as portas lógicas, e é aqui que entram os bits!

Um bit é um sinal eletrônico que representa um estado lógico, verdadeiro ou falso. Nos circuitos existem várias formas de representar bits, a mais usada nos chips hoje em dia é com tensões. Escolheram 3V (ou menos, ou mais, depende do chip) para representar um sinal verdadeiro, 0V para um sinal falso, então usamos circuitos feitos com transistores que fazem uma função lógica, e ligamos eles aos montes para fazer todo tipo de chips, incluindo os processadores.

Geralmente os chips recebem um sinal de sincronia vindo de fora, para manter os chips todos trabalhando na mesma velocidade e com harmonia, esse sinal é chamado de clock.

2 MHz é a frequência de uma onda com 2 milhões de inversões de fase por segundo. 2 GHz é a frequência de 2 bilhões de inversões de fase por segundo. A luz inverte várias trilhões de vezes por segundo, Terahertz. Os raios X e raios gama vibram ainda mais rápido.
