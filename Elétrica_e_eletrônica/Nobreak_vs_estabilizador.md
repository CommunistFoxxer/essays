# Nobreak vs estabilizador
Nobreak possui uma bateria para manter as tomadas com energia por alguns minutos quando a força da rua cai.

Estabilizador é uma das maiores fraudes da indústria eletrônica e tu realmente não deveria usar essa merda. Ao invés de estabilizador, compra uma fonte com filtro PFC ativo e liga direto na tomada ou em uma régua de tomadas.

PFC, em português, significa Correção de Fator de Potência. É preciso estudar muito para calcular, tem uma fórmula terrível de decorar.

Basicamente, o fator de potência é a razão entre energia ativa (que pode ser convertida em potência real) e reativa (energia que escapa da ressonância e não se traduz em potência real no equipamento final). A correção de fator de potência é o processo no qual um circuito especial faz operações que modificam a fase do sinal de energia, de modo que a maior parte do sinal entre em ressonância e consiga produzir potência real. Quanto mais próximo o fator de potência estiver de 1, maior a eficiência energética do aparelho, mas ela nunca pode chegar à 1, porque para chegar em 1, a energia precisaria entrar em ressonância total, e isso faria os transformadores, capacitores e indutores do circuito pararem de funcionar.

Esses componentes precisam recarregar um pouco de energia nos seus campos elétricos antes de enviar um pulso pra frente no circuito, e eles usam a energia reativa para essa recarga, então todo circuito elétrico busca manter o fator de potência o mais próximo de 0,97 possível, entre 0,95 e 0,97, ou seja, para cada 100 VA de energia, 97 Watts de energia ativa e 3 VA de reativa, ou 95 Watts de energia ativa e 5 VA de reativa. A energia tem que vir quase toda na fase de ressonância, e um residual pequeno fora de fase para alimentar esses componentes.

O filtro de correção de fator de potência ativa é um circuito muito interessante. Ele tem uma espécie de osciloscópio e fica analisando o sinal de energia em tempo real, se a componente magnética atrasar, ele injeta capacitância no circuito ligando capacitores (o capacitor atrasa a componente elétrica (corrente)) em paralelo, se a componente elétrica atrasar, ele injeta indutância (o indutor atrasa a componente magnética (tensão)) no circuito ligando indutores em série, e fica o tempo todo nessa brincadeira, tira e põe, tira e põe.

Há um [PDF](https://adm.online.unip.br/img_ead_dp/20787.PDF) sobre o que acontece em um circuito simples de corrente alternada, comparando o resistor, capacitor e indutor. O que eu chamo de componente magnética, eles chamam de sinal de tensão, e componente elétrica de sinal de corrente. É a mesma coisa, mas vista de pontos de vista diferentes.
