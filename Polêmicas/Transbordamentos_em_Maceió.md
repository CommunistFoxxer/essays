# Transbordamentos em Maceió
Toda a sociedade brasileira deve ficar atenta e se indignar com o que acontece em Maceió. Visando apenas lucro, a Braskem vem minerando sal-gema há mais de 40 anos numa área habitada da cidade.

Resultado: 5 bairros estão afundando e 60 mil pessoas tiveram que se mudar em plena pandemia de COVID-19. É um absurdo que não pode ser ignorado, sob pena de voltar a acontecer impunemente em qualquer parte do país. É o maior crime socioambiental em curso em uma área urbana no mundo! Ontem, o povo de Maceió foi para a porta da empresa gritar, basta!
