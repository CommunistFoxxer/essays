# A Igreja Católica e o HIV
A igreja católica está muito presa ao dogmatismo, não se atualizaram e tão pouco ligam para o bem-estar de todos, apenas para a moral (não estou negando que a igreja católica seja a maior rede de caridade do mundo). Eles foram os grandes responsáveis pela contaminação em massa do HIV (erroneamente conhecido como AIDS).

Os jovens de 18 a 22 anos ficam atrasados em formação por conta do programa de missão compulsório, e têm dificuldades de se encaixar no mercado de trabalho quando retornam, sendo obrigados a aceitarem subempregos para outros membros da igreja, o próprio programa de missão é um programa pró-imperialista. O tanto de missionário americano do GOP com quem eu interagi não está no mapa.

Não adianta questionar a igreja católica do ponto de vista religioso, o Papa é político, além de tudo, e aprova todo tipo de ação maluca de seu interesse. O Papa Pio XII que apoiou o fascismo, o mesmo Vaticano que fez revisionismo do nazismo, o mesmo Vaticano que apoiou a operação Gladio, que curiosamente estava editando a Wikipédia juntamente da CIA (também há casos do FBI) secretamente, o mesmo Vaticano que apoiou o golpe de Estado fascista de Pinochet. Mas de que adianta criticar se existe a infalibilidade papal?

Fontes:
- [Catholics for Choice | The Lesser Evil: The Catholic Church and the AIDS Epidemic](http://web.archive.org/web/20210116212633/https://www.catholicsforchoice.org/issues_publications/the-lesser-evil/)
- [WikiLeaks | Pope’s Private Letter Reveals Early Involvement in Power Struggle](https://wikileaks.org/popeorders/)
- [Epoca | Como a Igreja Católica ajudou a consolidar o fascismo](https://epoca.oglobo.globo.com/cultura/noticia/2017/05/como-igreja-catolica-ajudou-consolidar-o-fascismo.html)
- [Wikipedia | Pope Pius XII and the Holocaust](https://en.wikipedia.org/wiki/Pope_Pius_XII_and_the_Holocaust)
- [Covert Action Information Bulletin - Winter 1986 - No. 25 - Nazis, the Vatican, and CIA.pdf (PDFy mirror)](https://archive.org/details/pdfy-xGyFKQCjORitMNOE)
- [The Sidney Morning Herald | CIA and Vatican edit Wikipedia entries](https://www.smh.com.au/national/cia-and-vatican-edit-wikipedia-entries-20070819-gdqwa2.html)
- [Público | El Vaticano colaboró con EEUU apoyando el golpe de Pinochet](https://www.publico.es/internacional/vaticano-colaboro-eeuu-apoyando-golpe.html)
- [Wikipedia | Infalibilidade papal](https://pt.wikipedia.org/wiki/Infalibilidade_papal)
