# Modos gregos
Os modos gregos são 7 modelos para tocar a escala maior natural. São eles:
- Jônico (tom-tom-semitom-tom-tom-tom-semitom)
- Dórico (tom-semitom-tom-tom-tom-semitom-tom)
- Frígio (tom-tom-tom-semitom-tom-tom)
- Lídio (tom-tom-tom-semitom-tom-tom-semitom)
- Mixolídio (tom-tom-semitom-tom-tom-semitom-tom)
- Eólio (tom-semitom-tom-tom-semitom-tom-tom)
- Lócrio (semitom-tom-tom-semitom-tom-tom-tom)

Existem também os modos medievais, persas, índianos, cigano, espanhol, húngaro, napolitano, toscano, Scarlatti, japoneses e árabes e outros infinitos.
