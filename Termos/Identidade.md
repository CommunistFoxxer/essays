# Identidade
A Lei da Identidade é expressa como “A = A”, que na lógica simbólica nada mais é do que um dos axiomas que definem o comportamento do signo "=". Quando aplicado geralmente na Lógica Formal, no entanto, isso diz que todos os conceitos e coisas são “auto-iguais”. Nessa forma, é a base da abordagem “metafísica” para entender o mundo como coisas separadas e imutáveis. Isso contrasta com a abordagem dialética em que o mundo é concebido como coisas e processos interconectados, autocontraditórios e mutáveis.

A Identidade de A só pode ter sentido definindo o que é diferente de A, “não-A”. Descobrimos que, na verdade, tudo é “não-A” incluindo o mesmo 'A' um segundo depois (desde que a premissa de que todas as coisas estão em movimento seja verdadeira), e assim surge a “Máxima da Diversidade”.

Leitura adicional:
- Hegel's shorter logic
- Hegel's science logic
- Crítica de Hegel às ciências naturais de seu tempo como a Filosofia da Identidade e sobre A = A
- Comentários de Lenin sobre o conceito de Hegel da Lei da Identidade
- Explicação de Trotsky da dialética baseada em uma crítica de "A = A"
- C L R James sobre Identidade, Diferença e Contradição
