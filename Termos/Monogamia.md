# Monogamia
A monogamia é uma identidade: a do SER casal, não apenas estar em casal. E apesar da monogamia em série ser de longe a forma mais comum hoje em dia, essa identidade é experienciada como eterna: não se pode admitir a (muito provável) finitude. Além disso, ela é uma identidade por exclusão: nós versus o mundo.
>Monogamia é muito bom eu amo MINHA mulher, vou casar com a MINHA mulher, eu sou o homem DELA, vamos viver JUNTOS para SEMPRE, apenas EU e ELA. Eu amo a monogamia.

Cria-se um território que só pertence àquele núcleo, que é hierarquicamente mais importante que todas as outras relações. E essas relações são vistas como ameaças, portanto há um constante confronto e competição. Pela monogamia ser um sistema, ela procura manter sua hegemonia frente a outras formas de relacionar-se. 

Se coloca como uma forma superior: amor verdadeiro, puro, ideal, natural. As outras possíveis formas são ignoradas, desvalorizadas, criticadas. Acusações/percepções de promiscuidade, superficialidade e inferioridade são algumas das mais comuns:
>Aos poligamicos de plantão, se querem ser corno sabidos, problema de vocês, eu quero minha mulher só para mim.

A assexualidade e a arromanticidade são patologizadas. O sistema mono institui a amatonormatividade: ter uma relação afetivo-sexual central é sinônimo de saúde e sucesso. O ideal do amor romântico estabelece que apaixonar-se e ser feliz para sempre é uma experiência indispensável.

Enfim, através de mecanismos como esses, alguns “negativos” repressores e outros “positivos” idealizadores, o sistema é reproduzido não só institucionalmente mas também pelas próprias pessoas no cotidiano. Passa a ser visto como natural e espontâneo, e se estabeleceu no feudalismo, quando senhores feudais viam tudo como sua propriedade; inclusive, as mulheres.

Leituras mais aprofundadas:
- Monogamia e hierarquia (Brigitte Vasallo)
- Amor romântico (Coral Herrera)
- Família, estado e propriedade privada (Engels)
- LGBT X família (Amanda Palha)
- Crítica marxista à família mono (Sérgio Lessa)
- Bixa ex-monogâmica (Alef Diogo)
