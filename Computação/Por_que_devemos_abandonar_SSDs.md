# Por que devemos abandonar SSDs?
Se você teve badblock em um SSD, ele está morrendo. A área danificada vai aumentar rapidamente. É preciso arrumar um HD ou outro SSD assim que possível para não perder mais dados, ou antes que seu computador pare de funcionar por causa dele.

Badblock em um dispositivo tipo Flash significa que o chip de memória dele está queimando. Essa área queimada vai aumentar rapidamente. Quer dizer que você ultrapassou o limite de regravações que a memória flash do SSD suporta.

É por isso que eu amo HDs, posso regravar infinitamente desde que o motor dele não tenha problemas.

SSDs têm um método de uso específico. Não foram feitos para arquivos que serão regravados várias e várias vezes. SSDs foram feitos para gravar uma ROM e passar muito tempo sem alterar aquela ROM.

HDs possuem discos magneto-resistivos. A informação é gravada na forma de polarização magnética nesses discos. Devido à estrutura especial do material do qual são feitos os discos, eles podem ser regravados e lidos infinitas vezes. Se passar 300 anos desligado, quando vocÊ ligar o HD, vai estar tudo lá.

Já o SSD, se ficar 40 anos desligado, perde toda a informação gravada, porque os SSDs usam chips Flash, quase a mesma tecnologia dos chips de memória SD/micro-SD e dos chips de memória dos nossos celulares. Chips Flash armazenam a informação na forma de eletricidade armazenada em um capacitor isolado dentro da junta de um transistor. É como um capacitor sem terminais, dentro do "útero" do transistor.

Quando aquele transistor é eletricamente polarizado com uma tensão alta o suficiente, a barreira que isola o capacitor se desfaz e o capacitor carrega energia. A leitura não exige muito, porque o capacitor carregado muda o funcionamento do transistor, então a leitura exige quase nada de energia e é muito rápida.

Só é perigoso no caso de um gênio do mal detonar perto do seu HD uma bomba PEM. O pulso eletromagnético vai apagar os dados na superfície do disco, mas se bem que bombas PEM também queimam chips Flash...

Todo chip Flash possui um limite de regravação. A barreira de semicondutor que protege o capacitor dentro do transistor fica desgastada com o tempo, porque os átomos que a compõem se movem e desalinham. Cada gravação de dados aumenta a desordem dos átomos da barreira. Existem diferentes tipos de tecnologias de chips Flash que podem regravar mais ou menos vezes. Os principais tipos são os chips Flash NAND e NOR.

Flash NOR é mais rápido do que o Flash NAND, mas regrava menos vezes antes de queimar, é por isso que os SSDs usam Flash NOR, e os chips de memória usam Flash NAND.

Acho que um chip Flash NOR pode regravar o mesmo setor entre 10 e 100 mil vezes. O chip Flash NAND pode regravar o mesmo setor entre 100 mil e 1 milhão de vezes. Eles obviamente são mais duradouros. SSDs não foram feitos para durar, foram feitos para performance de leitura. SSD é o tipo de dispositivo que você regrava uma vez na vida e lê milhares e milhares de vezes.

Arquivos de usuário que você regrava todo dia não deveriam ir para um SSD, e sim para um HD.

Cold boot ou arranque à frio significa que você acabou de ligar o computador e está com o cache de arquivos vazio. Na verdade, cold boot é usado para falar de um programa que está abrindo com o SO nesse estado. Hot boot ou arranque à quente é quando você já tem um cache disponível para o SO na RAM e cache no chip de controle do seu HD ou SSD. Os HDs também fazem um cache de setores próprio.