# GraphQL: uma crítica construtiva
GraphQL é um servidor na frente da sua fonte de dados que puxa dados dela, organiza e serve para os clientes. Se você não estudar direito o servidor GraphQL que você usa, você não vai saber depois como resolver problemas de performance, ou se a estrutura que você está fazendo vai trabalhar bem com as restrições do seu servidor, ou se ela vai ser fácil de mudar/escalar depois.

APIs REST também não são isentas de problemas, mas a vantagem é que é super simples e você controla tudo que acontece depois do pedido HTTP chegar no servidor.

Eu prefiro REST porque:
- GraphQL não utiliza especificações HTTP, ou seja, o GraphQL desconsidera tudo que o HTTP já resolveu nos últimos 20 anos, como, por exemplo, o cache. 
- HTTP2 resolve os principais benefícios do GraphQL, como por exemplo, múltiplos requests.
- Em GraphQL, o monitoramento de erros é ruim, afinal tudo retorna `200 OK` em GraphQL.
- GraphQL não tem uma boa performance no servidor, pois a flexibilidade nas queries do GraphQL pode custar caro para a infraestrutura do servidor.
- GraphQL tem lock-in com frameworks.
- GraphQL tem más práticas de desenvolvimento, pois o GraphQL incentiva a exposição do seu DB, e sua API se torna apenas uma interface "bypass", sem qualquer inteligência.

Obs: argumentos como "eu gosto" ou "Facebook usa" não são argumentos válidos, esse tipo de argumento é feito por desenvolvedores que nunca ouviram falar de SOA, oData, WCF, estruturas de armazenamento, ponteiros, etc. É a onda do full stack, que "sabe tudo", mas no fundo, não sabe nada, pois começaram a programar nos últimos 4 anos.

Obs 2: a observação acima não foi uma observação elitista, mas ressalta o triste cenário do mundo dev hoje em dia, com os "devs de palco".

A maior vantagem de GraphQL é de forçar um schema e permitir o client de ser gerado "automaticamente", e de separar "query" de "mutação", mas schemas introduzem outros problemas, tipo versionamento.

Na minha análise, o GraphQL parece ótimo em termos de produtividade para o desenvolvedor da API, mas para o consumidor, a experiência é péssima, pois a navegabilidade e explorabilidade da API torna-se complexa, afinal, sintaxes de queries dentro de uma URL não é nada amigável.

Mas há uma notícia boa: há soluções para todos os problemas que eu mencionei. Para começar, o mais simples seria transformar as queries em persisted queries, isso gera um id único para a query no client e no server, dessa forma, ao invés de enviar o body inteiro da request, ele envia apenas um id. Pode consular com esse id usando requests `GET` e colocar um CDN na frente se for necessário, e utilizar boa parte do que o protocolo HTTP já oferece como bem entender.

E os erros? Tudo retorna 200, sim e não. O que temos que entender aqui é que o GraphQL normalmente trabalha com um conceito de falha e entrega parcial, então imagina-se que parte da requisição tenha sido concluída com sucesso. O que o cliente recebe é um 200 com um campo de erros para fazer o tratamento disso e verificar o que falhou e o que não falhou.

A ideia com o GraphQL é tentar resolver os problemas que o HTTP não resolveu. O GraphQL, quando bem implementado, tem um padrão de monitoramento de erros bem eficiente. Claro que isso abre brechas para os erros de implementação e cada um acaba seguindo um padrão próprio, mas dado que isso também acontece em REST, não vejo como um problema específico de GraphQL. Em relação à performance, é intencional. Trazer essa complexidade do client para o servidor é benéfico se pensarmos que o client pode ser um smartphone menos privilegiado. Realmente, existe uma possibilidade de lock-in com frameworks, principalmente se não trabalhar bem as camadas que abstraem a parte do framework. Mas não há lock-in com databases. É possível usar praticamente qualquer database com GrahpQL, inclusive é possível utilizar APIs REST como datasource.