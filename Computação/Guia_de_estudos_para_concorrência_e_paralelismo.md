# Guia de estudos para concorrência e paralelismo
- Queue
- Forks
- Threads
- Local thread
- Pares
- Visualizador de processos de threads (thread profiler)
- Semaphore
- Mutex
- ReadWrite lock
- Async/futures
- Executors
- Event loop
- Concurrency strategy
- Transactional model
- STM (Software Transactional Model)
- Channels/coroutines
- Reatividade (incluindo o TEA - The Elm Architecture)
- Processos ao invés de threads
- Move semantics (affine types) e linear types, além de RAII (ou alternativamente SBRM - scope-based resource management)
- Control Flow Guard (Windows)
- OTP (supervisores, tasks e agentes)
- Callbacks e CPS (Continuation Passing Style)
- Modelos de threads (modelos 1:1, N:1 e M:N)
- Pi-calculus
- Modelo de atores
- Sistemas operacionais (scheduler, troca de contexto, gerenciamento de memória, paging, journaling, swapping, linking, fragmentação, segmentação, stack e heap)
- Programação dinâmica (otimização serial, caching e memoizing e suas diferenças)
- Programação linear
- Análise ciclomática
- Análise de complexidade temporal assintótica
- Green threads
- Sincronização
- Problemas com threads (deadlocks, data race, race condition, callback hell)
- CSP
- Fibras
- Leak memory
- Endianness
- Atomicidade
- Ponteiros inteligentes
- Modelo de memória da JVM, do Forth e da BEAM (Erlang)
- Contadores de referência (e weak vs strong reference, além de seus problemas como referências cíclicas)
- GCs e tipos de GCs (GC paralelo, mark and sweep, bump allocator, stop the world, ZGC, major e minor, barriers, GIL, )
- Modelos de memória (sequencialmente consistente (SeqCst, sequentially consistent), acquire/release, relaxed)
- SIMD (e SSE vs AVX) e auto-vetorização

Você tem CSP (Clojure, Go), atores (Erlang, Elixir, Pony), variáveis de sincronização (Haskell, C++), corrotinas (Python, Lua), promessas e futuros (Haskell, JavaScript, Java, Rust), STM (Erlang, Elixir, Haskell), green threads M:N preemptivas (Java, Erlang, Elixir, Haskell) e green threads M:N cooperativas (Go).

Com isso, você dominará concorrência e paralelismo, e vale ressaltar que: paralelismo é uma questão de executar as coisas em paralelo, concorrência é uma questão de executar as coisas em ordem não-especificada.