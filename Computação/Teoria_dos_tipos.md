# Teoria dos tipos
Sistemas de tipos podem detectar possíveis códigos com mau comportamento. Ele elimina os chamados "runtime type errors".

Um sistema de tipos pode eliminar erros como invocar métodos inexistentes, impor propriedades de modularidade, proteger a integridade de abstrações definidas pelo usuário, proibir violações de ocultação de informações, como acessar diretamente os campos de um valor de dados cuja representação deve ser abstrata, evitar de tratar um inteiro como um ponteiro e usá-lo para travar a máquina, evitar o esquecimento de converter uma string em um número antes de obter sua raiz quadrada, impedir negligenciar uma condição de contorno em uma análise de caso complexa ou confundir unidades em um cálculo científico.

Tipos provêem uma documentação que nunca se torna ultrapassada, além de ser mais claro e direto. Interfaces provêem um contrato entre o programador e a biblioteca/módulo, assim, melhorando o design da aplicação e a tornando mais consistente.
 
Tipos são úteis em teoria de prova automatizada, onde vários assistentes de prova usam tipos (em particular, sua versão mais poderosa: tipos dependentes), como Coq, Agda, Alef, Idris, etc. Novas linguagens para consultar e manipular XML fornecem sistemas de tipo estático poderosos baseados diretamente nessas linguagens de esquema.

Sistemas de tipos também podem aumentar a segurança de um software, como evidenciado por recentes usos no PaP JINI[1], na tecnologia de segurança Proof-Carrying Code[2][3][4], uma prova formal de que sistema de applets tipados são mais seguros do que aqueles não-tipados e o uso de SLam calculus.

Em teoria dos compiladores, tipos são usados frequentemente em análise, por exemplo, no AnnoDomini[5] (uma utilidade do COBOL que resolvia o problema do bug do milênio com teoria dos tipos), análise de alias[6] e análise de exceções[7].

Sistemas de tipos aumentam a eficiência do programa, o tornando mais rápido, assim como fizeram em Fortran, Titanium, Rust e ML Kit Compiler.

O avançado sistema de tipos das linguagens da família ML permitem garantias que são inconcebíveis[8] em linguagens com tipagem dinâmica. No caso de um servidor web, a aplicação consegue rejeitar valores inválidos antes de repassá-los adiante no call stack. Isso deixa a aplicação mais segura também no sentido que é muito mais difícil alguém conseguir invadir um sistema enviando um campo no meio da chamada que dá algum erro lá no final do processamento.

O ATS poderia impedir bugs como o heartbleed do OpenSSL[9]: o bug se deu por causa da omissão de um bound check. Ao fazer a mesma operação que o código C faz, sem o bound check, você não consegue fazer ele compilar. Colocando o check, o sistema de tipos entende que o buffer tem tamanho suficiente e compila o programa.

O grande problema da teoria dos tipos era você só poder garantir segurança à nível de tipos, mas não à nível de valor (por exemplo, não poderíamos garantir que o segundo argumento de div seria diferente de 0, apenas que ele seria um Int), e isso levou à criação de tipos dependentes.

Todo sistema de tipos têm a intenção de provar coisas sobre o código que ele tipa. No caso de um sistema simples como o de C, o que ele prova é que, por exemplo, uma função com o protótipo int f(int) realmente sempre retorna um inteiro e recebe um inteiro. No caso de um sistema como o de Haskell, ele prova que a função id :: a -> a realmente retorna sempre o mesmo tipo que ela recebeu pra qualquer tipo a (polimorfismo paramétrico), etc.

Na hora de analisar um sistema de tipos, uma das coisas mais fundamentais é entender quais promessas o sistema te faz, porque toda e qualquer análise só pode ser feita em cima dessas promessas. Você não pode julgar um sistema de tipos pelo programa não ter uma propriedade que ele não se propôs a provar.

Uma vez que todo sistema de tipos é um provador, a gente pode analisar o sistema em termos do que significa quando ele prova algo. Daí a gente tem duas propriedades:

Soundness: um sistema que é sound nunca mente. Ou seja, se o sistema te diz que seu programa tem uma propriedade, você pode ter certeza que ele tem essa propriedade. No caso de um sistema unsound, ele pode passar um programa que não tem as propriedades que ele garante, mas te dizer que ele tem essa propriedade. Como, em geral, um sistema de tipos ser unsound destrói a maior parte da funcionalidade dele (fazer garantias sobre seu código), a maioria dos sistemas de tipos tentam ser sound. (Alguns já foram descobertos não ser, e.g.: Java).

Completeness: um sistema que é completo deduz todas as proposições possíveis. Ou seja, no contexto de sistemas de tipos, isso significa que um sistema passaria todo programa que tem as propriedades que ele garante. Na prática, o que acontece é que sempre existem programas que têm as propriedades, mas que o sistema de tipos rejeita. É por isso que se diz que todo sistema de tipos rejeita algum programa válido (teorema de Rice). Ele não consegue identificar se de fato o mau comportamento é presente, e portanto, rejeita alguns programas (tipo if test then cond else type error, mesmo que o bloco else nunca seja alcançado, ele se recusará a compilar o programa), portanto, uma linguagem sem tipos é capaz de compilar mais programas do que uma linguagem tipada.

Essencialmente, o teorema de Rice diz que toda propriedade não-trivial de uma máquina de Turing é indecidível, e a ideia de um sistema de tipos é justamente provar propriedades não-triviais de máquinas Turing-equivalentes (i.e. programas). Portanto, não pode existir um sistema de tipos que diferencia exatamente os programas que têm uma determinada propriedade dos que não têm. Ele tem que errar pro lado de barrar programas excessivos (i.e. ele não vai ser completo) ou pro lado de deixar passar programas que não realmente tem a propriedade que ele pretende demonstrar (i.e. ele não vai ser sound).

O Teorema da Incompletude de Gödel diz que todo sistema lógico que seja poderoso o suficiente para representar os números naturais não pode ser sound e complete ao mesmo tempo.

No contexto de sistemas de tipos, isso significa que todo sistema prático tem que escolher entre ser sound ou complete. Pelos motivos que eu já citei, a maioria dos sistemas escolhe ser sound, porque um sistema unsound permite que programas com erros passem pela checagem de tipos. Isso significa que sistemas de tipos práticos não são completos - justificando o motivo pelo qual todo sistema precisa proibir alguns programas válidos. O sistema simplesmente não pode provar que esses programas fazem a coisa certa, mesmo eles fazendo.

Uma vez que um sistema de tipos é um sistema de provas, um erro de tipo significa que o sistema não consegue provar a proposição correspondente a uma parte do programa. Em outros termos, um erro de tipo é o sistema de tipos derivando uma contradição.

Isso pode ser visto como o subtermo mal-tipado tendo um tipo correspondente a False.

Fun facts:
- O sistema de tipos de TypeScript, o sistema de templates de C++ e o sistema de traits de Rust são turing-completos, e portanto, podem ser chamados de linguagens de programação.
- Existe um framework de teoria dos tipos, chamado de teoria dos tipos de homotopia (HoTT), que é um framework, uma fundação matemático-filosófica e uma linguagem ao mesmo tempo!
- Ralf Jung quer especificar matematicamente o modelo de memória de Rust para poder dizer se um código unsafe é unsound ou não. O último modelo que ele produziu foi o [stacked borrows](https://plv.mpi-sws.org/rustbelt/stacked-borrows/paper.pdf). Ele também criou o Miri, que dentre outras coisas durante a execução do programa para detectar UB no momento que ele ocorre.
- Scheme é uma safe language, mesmo não sendo estaticamente checada.
- Scheme deveria ser chamado de estaticamente checado, não estaticamente tipado, pois esta contém type tags em runtime.
- Modula-3 e C# oferecem uma sublinguagem unsafe para permitir coisas unsafe em runtime, como GC.
- Linguagens que oferecem unsafe sob um espaço controlado não são um problema.
- HoTT é feita de uma forma tão original, uma forma tão nova de pensar, que ao contrário de outras lógicas (principalmente teoria dos conjuntos), ela não se baseia na lógica de predicados!

Leituras adicionais:
- [Typing is hard](https://3fx.ch/typing-is-hard.html)
- Types and Programming Languages
- Practical Foundations for Programming Languages
- Programming Language Foundations

Fontes:
[1]: http://library.lol/main/8844F04095C84DAFAB11688DFC109F38 <br/>
[2]: http://www.cs.cmu.edu/~15712/papers//necula96.pdf <br/>
[3]: http://www.cs.cmu.edu/afs/cs/project/pop-10/member/petel/www/publications/lncs98.pdf <br/>
[4]: https://www.cs.jhu.edu/~fabian/courses/CS600.624/proof-carrying-code.pdf <br/>
[5]: https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.35.8206&rep=rep1&type=pdf <br/>
[6]: https://courses.cs.washington.edu/courses/cse503/11au/readings/ocallahan-icse97.pdf <br/>
[7]: https://sci-hub.se/https://doi.org/10.1145/349214.349230 <br/>
[8]: https://lexi-lambda.github.io/blog/2019/11/05/parse-don-t-validate <br/>
[9]: https://bluishcoder.co.nz/2014/04/11/preventing-heartbleed-bugs-with-safe-languages.html
