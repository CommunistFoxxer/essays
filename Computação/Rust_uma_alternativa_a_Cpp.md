# Rust: uma alternativa a C++?
Rust é uma linguagem para sistemas seguros, eficientes, funcionais e type-safe.

Rust consegue evitar data race. Claro, tem como burlar, mas além de ser unsafe Rust, você tem que querer muito.

Rust não deixa você duplicar recursos impropriamente, porque Rust usa RAII (chamado na linguagem de SBRM - ou para os mais acadêmicos, tipos lineares, mais especificamente tipos afins) para limpar recursos. Você "clona" o recurso ou você move ele, ou seja, evita double free. Por esses motivos, as referências têm um tempo de vida (lifetime), por exemplo. O lifetime é até no máximo quando o recurso é válido, até qual escopo (presente até mesmo na notação de tipos da função). As referências têm semântica parecida com travas de leitura/escrita. Se você tem uma referência imutável para o recurso, você pode criar outras referências imutáveis e ler do seu recurso, mas não pode criar referência mutável nem escrever nele. Se você tem referência mutável, não pode criar nenhuma outra referência de qualquer tipo, nem ler, nem escrever, nem nada. A leitura é compartilhada, a escrita é exclusiva. O compilador não deixa você escapar disso, até tem com burlar, mas de novo, tem que querer muito, é unsafe Rust (geralmente por culpa do C), e é UB - automaticamente incluso na linguagem! Você não pode burlá-lo, a menos que tenha um bug no borrow checker (ou alternativamente, simular tal ato com ponteiros inteligentes inclusos no próprio Rust), mas é algo muito difícil, e estão desenvolvendo um borrow checker muito mais superior - o Polonius!

Você "clona" o recurso ou você move ele, ou seja, evita double free. Por esses motivos, as referências têm um tempo de vida (lifetime), por exemplo. O lifetime é até no máximo quando o recurso é válido, até qual escopo (presente até mesmo na notação de tipos da função). As referências têm semântica parecida com travas de leitura/escrita. Se você tem uma referência imutável para o recurso, você pode criar outras referências imutáveis e ler do seu recurso, mas não pode criar referência mutável nem escrever nele. Se você tem referência mutável, não pode criar nenhuma outra referência de qualquer tipo, nem ler, nem escrever, nem nada. A leitura é compartilhada, a escrita é exclusiva. O compilador não deixa você escapar disso, até tem com burlar, mas de novo, tem que querer muito, é unsafe Rust, e é UB.

A parte unsafe da linguagem é separada da parte safe, assim como Haskell separa a monad IO. Até existem ponteiros "crus" (raw pointers - referências brutas como em C) e etc, mas de novo, ponteiro cru faz parte do unsafe Rust.

É teoricamente impossível tomar um segmentation fault usando somente safe Rust. A única exceção que consigo pensar é um stack overflow em modo de release. Nesses verificadores de C, pode muito bem escapar algo, já que código C não fornece informação o suficiente sobre o código.

O código rust carrega mais informação do que código C, isso possibilita mais detecção de erros, porque isso simplesmente faz parte da linguagem. Em C todas essas ferramentas não fazem parte da linguagem e não são capazes de detectar certas coisas justamente porque a linguagem tem um escopo limitado de informações e porque todas essas ferramentas não fazem parte da linguagem.

A única desvantagem é Rust não ter TCO, porque o spec de Rust garante que, quando um tipo tem um destrutor e o tempo de vida de um valor desse tipo é ligado ao escopo de uma função (i.e, ele é local àquela função), o destrutor roda imediatamente após a função retornar. Ou seja, TCO quebraria essa garantia. Então a keyword `become` (que faria o mesmo que `return`, só que os destrutores de valores locais rodariam imediatamente antes do `become`) está reservada para possibilitar TCO, mas ninguém parece muito interessado em pegar o issue para implementar.

Rust não tem null pointers, só isso já o torna 1000x melhor que C (unsafe Rust tem null pointers mas é opt-in), referências nao podem ser nulas, mas raw pointers sim, e existe um ponteiro `NonNull<T>`. Ponteiros crus são só manipuláveis em unsafe Rust, onde dá pra fazer aritmética e etc, mas com referências não, porque referência é um negócio bem strict em Rust e referência é completamente idiomática em Rust.

Ponteiro é só para casos em que safe Rust não cobre, e você raramente expõe ponteiros em API pública.

"Problema do programador", então programa só em assembly, faz uma versão para cada plataforma. O meu ponto é que não dá pra fazer uma linguagem de qualquer jeito e ficar esperando que o programador se vire com qualquer coisa, ainda mais com alternativas melhores, "problema é do programador" é o que os próprios programadores C mais "fanáticos" dizem quando criticam a linguagem deles. Somos seres humanos, nós vamos errar, e quanto mais as ferramentas estiverem preparadas, melhor. É pelo mesmo motivo que se compra um celular resistente.

O problema do C não é a obsolência, o problema é que o sistema de tipos de C é unsound, unsafe, error-prone, limitado. Não tem porque usar C no lugar de Rust, a não ser que seja em um hardware específico.

`atoi` como várias funções do C tem o infame UB quando falha para parsear a string, não tendo nenhum modo de checar o erro. Não entendo porque muitas universidades ensinam programação com C e muita gente aprendendo a programar começa com essa language que é cheia de armadilhas, undefined behavior e funções vulneráveis a centenas de problemas de segurança como buffer overflows.

UB (Undefined Behavior) é um comportamento não definido que acontece quando o comportamento não é garantido pelo standard ou padrão da linguagem, o que acontece depende da implementação ou compilador. Não é só tirar um enderenço inválido; acessar uma array de modo out of bounds ou fora dos seus limites que causam undefined behavior. Também são UB usar um objeto dinamicamente alocado que já foi deletado; deletar um object mais de uma vez, double free; soma de inteiros com sinais signed integers cujo valor passe do limite representável e por ai vai, tem centenas de UB que podem trazer surpresas ruins. No caso da função `atoi`, segundo a documentação, ela tem UB se você entrar com um valor inteiro fora do limite representável (signed integer overflow). O problem do UB é que causam bugs que são difíceis ou quase impossíveis de detectar em tempo de compilação ou em runtime, tudo pode acontecer, o software terminar abruptamente (melhor caso), ou continuar com erro sem que o desenvolvedor note. O UB também pode introduzir falhas de seguranças. Analisadores estáticos e o undefined behavior sanitizer do compilador Clang podem ajudar a detectar o UB.

Digamos que alguma função te retorna um ponteiro (endereço pra alguma coisa na memória). Se você se confundir e achar que você virou o "dono" do ponteiro, você vai desalocar a memória dele depois de usar (vai marcar ela como disponível para outros dados). Se você se enganar e alguém mais tiver usando esses dados, vai usar memória desalocada, que já pode ter outro dado lá. É um problema de segurança. Rust foi feito para impedir que esses problemas de segurança aconteçam.

Então, um objetivo do sistema de ownership em Rust é não ter esse tipo de problema. Tanto `Box<Pessoa>` (Box aloca na heap) quanto `&mut Pessoa` em Rust acabam sendo, por detrás dos panos, "ponteiros para `Pessoa`". C não diferencia entre esses ponteiros, Rust sim. Se você receber um `Box<Pessoa>`, o Rust automaticamente vai dar um free() nele quando você deixar de usar. Não tem como errar (nesse ponto funciona como os smart pointers do C++ moderno — toda a memória de Rust é gerenciada por ponteiros inteligentes). Se você recebe um `&mut Pessoa`, o Rust já sabe que esse ponteiro (que neste caso ele chama de referência ou empréstimo) está sendo usado em outro lugar.

"Empréstimo" porque você só pegou emprestado, você não é o dono da `Pessoa`. `Pessoa` aqui poderia ser um objeto em um jogo que representa um personagem na tela digamos, o "dono" acaba sendo ou uma estrutura de dados que tem o tal `Box<Pessoa>`, ou a parte do código que está com essa pessoa. Você consegue pegar um `&mut Pessoa` emprestado de um `Box<Pessoa>` (emprestado do dono) mas não consegue a partir de um empréstimo pegar um "owned pointer".

```rs
fn pegar_pessoa_emprestado(pessoa: Box<Pessoa>) -> &mut Pessoa;
```

Essa função pode ser escrita (na pratica ela não é, porque é trivial), mas a função:

```rs
fn peguei_emprestado_agora_sou_dono_falows(pessoa: &mut Pessoa) -> Box<Pessoa>;
```

Não pode ser escrito no "safe Rust", isso garante que um objeto (no caso aqui `Pessoa`) só tenha um dono sempre, isso é o que faz Rust impedir que falhas de segurança e etc aconteçam.

C não diferencia o "empréstimo" de "ser dono" do objeto. Rust diferencia e diz: só pode haver um dono!

C++ não tem proteção contra overflow, não algo como em Rust. Não força a validar nada, C++ em casos extremos tem memory leak que nem com Valgrind e afins se consegue detectar. Devs de games amam C++, mas porque existe 1001 libs de muleta. Intrisics da intel colabora se for usar SIMD em álgebra linear, mas isso por conta da ligação com C. Boost da comunidade tem libs péssimas. QT tem coisas legais, mas performance não é boa como wxwidgets e afins.

Rust está cada ano mais melhorando. Em 2018, foram trazidas as seguintes mudanças:
- Adicionaram NLL
- Lifetime elision
- Propagator operator
- Suporte a programação assíncrona
- Dyn traits
- Inclusive ranges
- Lifetime anônimo
- Constantes associadas
- Inteiros de 128 bits (e espero que cheguem floats de 128 bits também algum dia)
- Não é necessário mais usar extern crate (só em alguns casos)
- Operador `?` em macros
- Mudar alinhamento com `repr`
- Compilação incremental
- SIMD por padrão (só com suporte nativo, eu acho)

Ralf Jung quer especificar matematicamente o modelo de memória de Rust para poder dizer se um código unsafe é unsound (viola o modelo) ou não. O último modelo que ele produziu foi o [stacked borrows](https://plv.mpi-sws.org/rustbelt/stacked-borrows/paper.pdf). Ele também criou o Miri, que durante a execução do programa detecta UB no momento que ele ocorre.

Mas o stacked borrows ainda não está integrado em um provador de teoremas para provar que determinado código definitivamente não tem erros de memória, mas o Ralf e o grupo de pesquisa dele já trabalhou nessa questão: a ideia dele é que cada estrutura de dados que implementa unsafe, em geral tem alguns invariantes. Por exemplo, no `Vec`, o `len` não pode ser maior que o capacity (porque o `Vec` lê até o `len` usando código safe, e se o `len` for maior que o capacity - que é o espaço alocado -, ele lê memória não inicializada em código safe)

A partir dessa ideia, a prova se dividiria em duas: de que manter os invariantes são suficientes para manter o soundness, e que determinado código de fato mantém as invariantes. Provar o primeiro já é grande coisa!

E esse projeto de pesquisa (o [Rustbelt](https://plv.mpi-sws.org/rustbelt/popl18/paper.pdf)) produziu uma definição matemática de uma simplificação do Rust (mas que não inclui o borrow checker, que é o que o stacked borrows analisa), e eles aplicaram a análise deles para achar erros no unsafe da stdlib. Isso eles fizeram usando Coq, que é outro provador de teoremas.

Eles tiveram vários follow ups, como apresentado neste [paper](https://people.mpi-sws.org/~dreyer/papers/rbrlx/paper.pdf).

Eu não considero Rust funcional, porque Rust não ajuda nem incentiva transparência referencial ou evitar efeitos colaterais, além de não ter TCO e não ser trivial fazer recursão. Porém, Rust as vezes parece um ML com várias influências de Haskell, mas sem Hindley-Milner (clássico da família ML - o que leva a questões difíceis como dizer se Rust é ou não ML), disfarçado com sintaxe ALGOL-like, sem HKTs como em Haskell e com a comunidade preferindo usar associated types do que unassociated types como em Haskell (sim, em Rust, type families é algo comum) - é incrível como algo assim conseguiu chegar ao mainstream.
