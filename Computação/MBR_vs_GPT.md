# MBR vs GPT
Você só deve usar tabela GPT se seu arranque usa o sistema UEFI. E nesse caso, obrigatoriamente deve usar GRUB versão 2 e um kernel assinado digitalmente.

GRUB versão 1 não suporta arranque UEFI e kernels antigos não possuem assinatura criptográfica para o UEFI.