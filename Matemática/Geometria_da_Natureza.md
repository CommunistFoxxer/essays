# Geometria da Natureza
Há mais de 400 anos o matemático e astrônomo alemão Kepler escreveu um pequeno livro chamado "O Floco de Neve de Seis Pontas". Ele conjecturou que flocos de neve se compõem geometricamente desta forma naturalmente, assim como as bolhas de sabão naturalmente formam finas esferas ou as abelhas formam colmeias hexagonais.

Um passeio na floresta revela uma infinidade de formas e padrões, assim como o desenho nas asas de uma borboleta ou as ondulações da areia. Mas como se desenvolvem esses padrões? Quais são as regras que determinam os padrões que nos cercam?

Uma curva se desenrola de um ponto central que vai progressivamente se afastando desse ponto é chamada de espiral. As espirais fascinam matemáticos desde os tempos de Arquimedes, que aliás, foi o primeiro a escrever um tratado sobre o assunto. Os dois principais tipos de espirais são a espiral de Arquimedes e a espiral logaritmica. Uma espiral de Arquimedes acontece, por exemplo, quando dobramos o tapete.

Em coordenadas polares, a espiral de Arquimedes é dada por r(θ) = aθ, onde «a» é uma constante qualquer e fazemos θ assumir ângulos crescentes no sentido anti-horário (positivo) a partir da origem. Isso significa que depois de uma volta completa (2π radianos; 360 graus), o raio «r» tem comprimento 2πa. Depois de duas voltas (4π radianos), o raio passa para 4πa, e assim por diante, em múltiplos de 2π. Então a cada volta o raio cresce em uma progressão aritmética de razão 2πa. A característica da espiral de Arquimedes é que a distância entre os pontos sucessivos no mesmo raio são constantes.

A espiral logaritmica é obtida substituindo r(θ) por ln(r(θ)), ou seja:<br/>
ln(r(θ)) = aθ<br/>
$`r(θ) = e^{aθ}`$

Quando a > 0, «r» cresce sem limites e a espiral se desenrola indefinidamente. Quando a = 0, obtemos um círculo. Se a < 0, obtemos uma espiral que se enrola em direção ao centro indefinidamente, porque r(θ) vai diminuindo e se torna tão pequeno quanto desejarmos. Veja também que o raio da espiral logaritmica se desenvolve como uma progressão geométrica:<br/>
$`r(2θ) = e^{2aθ}`$

Que forma uma PG de razão $`e^{2aθ}`$.

A propriedade matemática fundamental da espiral logaritmica corresponde exatamente ao princípio biológico que governa o crescimento de moluscos em conchas: a concha do molusco cresce mantendo a sua forma. O molusco cresce e a concha se mantém com formato similar a si própria, crescendo somente em uma extremidade, com cada incremento de comprimento sendo balanceado pelo aumento do raio, que mantém a sua forma inalterada. Observamos este padrão também no crescimento do feto humano, na forma de expansão do universo e formato de galáxias.

Bernoulli publicou um tratado sobre a espiral logaritmica (Spira Mirabilis), e ficou tão impressionado que em seu túmulo está inscrito "embora mudado, ressurgirei o mesmo". Ele se impressionava com duas qualidades das espirais logaritmicas:
1. Qualquer raio da espiral logaritmica intercepta ela em ângulos iguais. Isto faz com que a espiral logaritmica seja um parente próximo do círculo, pois neste o ângulo de intercessão com os raios é sempre 90°, ou seja, o círculo é uma espiral logaritmica de crescimento zero. Isso é chamado de propriedade equiângular.
2. Traçando a espiral de fora para o centro (polo), são necessárias infinitas voltas para chegar ao polo, e o comprimento de um ponto qualquer da espiral logaritmica até o centro pode ser finito ou infinito.

Por que a colméia tem forma hexagonal? Darwin dizia que as abelhas primeiro constroem as colméias em forma cilíndrica até que o cilindro atinja o cilindro vizinho e elas empurram as paredes preenchando os espaços vazios. Mas na verdade, o que ocorre é que as abelhas constroem as células da colméia lado a lado, uma a uma, na forma mais densa: hexagonal. A abelha rainha tem em sua genética o conhecimento para dar a forma mais eficiente a sua colméia.

Já a forma dos flocos de neve é resultado da simetria encontrada na estrutura molecular dos cristais. Um floco de neve se origina a partir de um minúsculo cristal de gelo hexagonal. À medida que cai pela atmosfera, o cristal cresce, mas devido a simetria geométrica, o cristal cresce de forma regular. Na verdade, o cristal tem uma geometria fractal. Já o arranjo dos cristais em sais são perfeitamente cúbicos devido as forças iônicas do sódio e cloreto.

Na margarida, por razões desconhecidas, há o padrão da sequência de Fibonacci. Ela está presente no padrão das sementes de uma flor disposta como dois conjuntos de espirais logaritmicas entrelaçadas.

Cada semente de flor pertence a ambas espirais, uma que se desenvolve em sentido horário e a outra no anti-horário. Contando o número de espirais, dá a sequência de Fibonacci.

Os biólogos atribuíam os padrões encontrados na natureza como herança genética, mas recentemente, biólogos e cientistas da computação passaram a olhar esses padrões de uma nova maneira. Uma observação é que dois indivíduos da mesma espécie são semelhantes, mas não idênticos. Isso os levou a perceber que a resposta, para por exemplo, listras de zebras, talvez não esteja contida no DNA. Os genes seguiriam algumas regras simples que produziriam os padrões.

Implementando regras simples através de simulações de computador, os cientistas criaram padrões similares aos encontrados na Natureza. Isso sugere que mecanismos simples são responsáveis pelo desenvolvimento complexo da Natureza. Uma forma de gerar padrões encontrados na Natureza é criando programas de autômatos celulares ou geometria fractal.
