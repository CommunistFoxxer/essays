# A matemática é real?
A matemática não se confunde com seu objeto de estudo. Matemática é o estudo das quantidades das coisas e suas relações. As coisas já estavam por aí, e não importa se você não sabe quantificar e relacionar ou sabe e precisa melhorar sua matemática para lidar com elas.

É bem provável que você já tenha ouvido alguém perguntar "matemática: descoberta ou invenção?". O que talvez você ainda não sabia é que essa questão é discutida por uma área da filosofia, a filosofia da matemática.

A filosofia da matemática, porém, discute não apenas se objetos matemáticos existem ou são inventados, mas também:
- Como podemos obter conhecimento deles
- Como teorias matemáticas se desenvolvem 
- Qual o estatuto de provas computacionais
- Qual a natureza de explicações matemáticas
- Como dar conta de sua aplicação a fenômenos do mundo real

Vários filósofos como Frege, Gödel, Russell, Quine, Kripke, etc pensaram que "a matemática é real". Isso se uma filosofia chamada “platonismo” for verdadeira: que objetos, conjuntos, funções e estruturas matemáticas existem (ainda que sejam abstratos), o que é negado pelos nominalistas. Onde esses objetos existem? De acordo com os platônicos, eles existem "fora" do espaço-tempo e independentemente de qualquer mente ou estado mental. Em minha experiência, a maioria dos filósofos são "platônicos" por causa de sua análise da linguagem matemática.

Há duas coisas importantes a serem abordadas primeiro para entender o que significa "platonismo":
1. Realismo semântico: várias afirmações matemáticas são verdadeiras ou falsas, independentemente da opinião de qualquer pessoa.
2. Realismo matemático: objetos matemáticos existem.

Um platonista acredita tanto no realismo semântico quanto no realismo de objeto. No entanto, ainda há mais no platonismo do que isso. Há várias maneiras de ser platonista ou nominalista. Seja como for, uma das motivações para ser platonista é o chamado argumento da indispensabilidade da matemática. De maneira bem simplificada, o argumento pode ser apresentado assim. Primeiro, a matemática está de tal modo relacionada a nossas teorias científicas que poderíamos dizer que ela é indispensável para elas; em outras palavras, não haveria como eliminar objetos matemáticos de teorias científicas *e*, ainda assim, possuir boas teorias (isto é, teorias com sucesso empírico etc.). Segundo, assumindo uma postura naturalista, parece plausível defender que devemos apenas admitir a existência de objetos indispensáveis a boas teorias científicas. Mais: como teorias são testadas em bloco, a mesma evidência que confirma a parte empírica de uma teoria também confirma sua parte matemática. Mas, se é assim, então devemos admitir a existência de objetos matemáticos!

Esquematicamente, temos duas premissas e uma conclusão
1. A matemática é indispensável às melhores teorias científicas.
2. Devemos admitir a existência de todos os objetos que sejam indispensáveis às melhores teorias científicas.

A conclusão é que devemos admitir a existência de objetos matemáticos. O argumento é válido, isto é, necessariamente, se as premissas são verdadeiras, a conclusão também o é. Para criticá-lo, resta então analisar se cada premissa é de fato verdadeira. Mas isso é um debate muito mais sério e que levaria à análises mais minuciosas sobre o tema. Talvez seja útil olhar as leituras adicionais.

Como você já deve ter adivinhado, o platonismo tem origem em Platão (mais especificamente, na sua ideia sobre formas). No entanto, nenhum platônico hoje acredita exatamente no que Platão acreditava, ou em suas razões para acreditar nisso.

Em meados do século 20, um movimento denominado "virada linguística" tentou responder à questões tradicionais da metafísica observando a maneira como falamos sobre elas. Por "linguagem" quero dizer o léxico que os filósofos usam na lógica de predicados. A lógica dos predicados foi um avanço na lógica aristotélica para expressar frases semânticas de uma forma pseudo-matemática. Mas o que isso significa? Considere a afirmação "há pelo menos duas cidades maiores do que Hong Kong". O que torna essa afirmação verdadeira? É óbvio que:
1. Há uma cidade chamada "Hong Kong".
2. Há pelo menos duas cidades maiores.

E o que torna verdadeira a afirmação “há pelo menos dois números maiores que 14”? Tem exatamente a mesma estrutura semântica que a última afirmação "há pelo menos três Gs maiores que F". Portanto, deve ter as mesmas [condições de verdade](https://bhalimi.parisnanterre.fr/wp-content/uploads/2014/07/LMP_benacerraf_mathematical_antinomy.pd).

Há uma maneira muito mais simples de expressar esse argumento, mas requer um pouco mais de compreensão sobre a lógica dos predicados e, em particular, um filósofo chamado Willard Quine.

Na lógica, existem dois termos quantificadores: o quantificador universal "para todo x" é o `∀` e o quantificador existencial "existe algum x tal que" é o `∃`. Quine teve a ideia de que, se quantificamos algo em afirmações verdadeiras, então o que quantificamos deve existir. Quine chamou isso de "critério de comprometimento ontológico" e (com algumas exceções) é uma visão bastante padrão. Na lógica de segunda ordem, você pode quantificar conjuntos, funções, relações, etc, e essas afirmações são verdadeiras, o que sugere que talvez o realismo matemático seja verdadeiro.

Leituras adicionais:
- Computadores na prática matemática: um exercício de microhistória (Gisele Dalva Secco)
- The Indispensability of Mathematics (Mark Colyvan)
- Nominalismo na filosofia da matemática (Otavio Bueno)
- Filosofia da matemática (Stewart Shapiro)
