# Divisão por zero resolvida
**Introdução**

A teoria inovadora da força gravitacional de Newton tem sido, e continua sendo, extremamente útil em cálculos e previsões da dinâmica gravitacional de corpos massivos. Mas existem fenômenos de força gravitacional que contradizem os cálculos newtonianos, como o deslocamento do periélio de Mercúrio em suas órbitas, que não é explicada pelo modelo de gravidade de Newton, ou a curvatura gravitacional mensurável das trajetórias dos raios de luz à medida que passam em proximidade suficiente de objetos suficientemente massivos, como estrelas.

A teoria da gravidade da relatividade geral de Einstein foi capaz de explicar quase toda essa discrepância. Pode parecer que a teoria geral da relatividade de Einstein constrói o conceito de gravidade científico válido, enquanto o conceito de força gravitacional de Newton é deficiente, mas o modelo de gravidade de Einstein buga na divisão por zero (chamado de singularidade) ao tentar explicar os fenômenos do colapso gravitacional total de objetos suficientemente massivos, por exemplo, de estrelas.

Os cientistas que se apegam à teoria da relatividade geral neste ponto de seu colapso, começam a mistificar sobre infinitos reais físicos, como densidade de massa e de volume infinitamente pequeno, supostamente existindo no núcleo das estrelas colapsadas (buracos negros).

**Hipótese**

A hipótese é que os buracos negros contêm uma forma completamente finita de substância massa-energia além da matéria degenerada das estrelas anãs brancas e além até mesmo do neutrônio das estrelas de nêutrons.

O resultado de tal colapso gravitacional envolve uma categoria ontológica de massa-energia que não pode ser descrita na linguagem matemática da teoria da relatividade geral de Einstein, por exemplo, dados os compromissos ontológicos restritos e descritividade relativamente abstratas dessa linguagem.

Além disso, a teoria da relatividade geral de Einstein não combina com a mecânica quântica, que é um grande problema pelo qual tanto a teoria da relatividade geral quanto a ideologia da mecânica quântica são provavelmente culpadas, mesmo que as interações gravitacionais das partículas da mecânica quântica, devido a suas massas mínimas, são desprezíveis em magnitude.

**Teoria matemática**

Existe uma certa classe ampla de equações diferenciais que são não lineares, ou seja, sua expressão diferencial do lado direito envolve uma potência [não-unitária] da função desconhecida (não-diferenciável), para a qual tal equação deve ser resolvida, denotada por x(t,...), para uma equação diferencial parcial, ou apenas x(t) para uma equação diferencial total.

Exemplos:
- δx(t,...)/δt = ...x(t,...)ⁿ... tal que n ≠ 1
- dx(t)/dt = ...x(t)ⁿ... tal que n ≠ 1

Este fato por si só implica que deve haver, sob certas condições iniciais, um valor de t ∈ R, denotado por t*, tal que x(t*,...) = ±∞ ou x(t*) = ±∞, apesar do fato de que t* <<< |±∞|. Isso ocorre porque, a partir de t = t*, tanto x(t,...) quanto x(t) se tornam da forma (X/0), onde, por exemplo, X ∈ R: x(t*,...) = (X/0) e x(t*) = (X/0).

Agora descobrimos que, para instâncias fisicamente aplicáveis de tais equações diferenciais, esses momentos t*, de infinita singularidade, estão associados a profundas mudanças qualitativas no mundo físico: com a irrupção de uma nova estrutura (revolução ontológica) no curso da dialética da Natureza. Essa descoberta se aplica a alguns dos melhores - até mesmo para alguns dos mais clássicos - modelos matemáticos do mundo físico, incluindo muitos que são centrais para a física moderna. Quando aplicados, esses modelos prevêem de maneira adequada e com extraordinária precisão o estado quantitativo das coisas físicas até meros momentos antes de um momento tão crucial t*.

Entretanto, naquele momento t*, aquele momento da divisão por zero, a expressão do modelo matemático contendo aquela divisão por zero realmente parece "explodir" - quantitativamente - ao infinito quantitativo. Mas essa parte (pelo menos aquela parte "ao infinito") da previsão quantitativa da equação para aquele momento não é o que realmente acontece no mundo físico real que, até aquele ponto, era tão apropriadamente modelado, em termos quantitativos, por aqueles equações.

O que é real é que essa dessemantificação e falsificação infinita dessas equações em tais momentos de erro infinito, de singularidade infinita e sem sentido, é precisamente o resultado da elisão dos qualificadores - de sua falta de qualificadores aritméticos; de, no mínimo, sua omissão da qualificação métrica. Isto é, seu erro infinito em t = t* é devido à falta de multiplicação [generalizada] por/em uma unidade de medida ou dimensão expressa aritmeticamente; à falta de qualquer fator de unidade métrica como qualificador aritmético.

Quando, pela aritmética dialética, quando se 'requalifica' essas equações, com qualificadores de unidade métrica ao menos, descobrimos que esse "erro infinito" em t = t* é superado. Nós descobrimos, axiomaticamente, que x(t*,...) = 🌑 = x(t*) e que a nova  lógica aritmética - desse novo tipo de número, 🌑, chamado de zero completo, em oposição a 0, chamado de zero vazio, em relação a 🌑, resolve o problema de dessemantificação/desvalidação originada na singularidade. Essa 'requalificação' - essa restauração dos qualificadores - faz todo o sentido das singularidades aparentemente sem sentido e "infinitas" dessas equações diferenciais não lineares, ou seja, que atormentam [metricamente] as formas puramente quantitativas dessas equações não lineares.
