# Monstros matemáticos
No prefácio de seu livro "Fractals Everywhere", Michael Barnsley diz:
>A geometria fractal fará com que você veja as coisas diferente. É perigoso ler mais. Você arrisca perder a visão infantil de nuvens, florestas, flores, galáxias, folhas, penas, rochas, montanhas, torrentes de água, tapetes, tijolos e muito mais. Nunca mais você interpretará estes objetos da mesma forma.

Nas últimas décadas do século 20, os cientistas descobriram uma nova maneira de entender o crescimento e a complexidade da natureza. Para o homem, o mundo funcionava como um relógio de milhões de engrenagens. Muitas engrenagens já haviam sido descobertas, e outras aguardavam somente a sua vez para serem decifradas. Mas uma nova ciência chamada de "caos" mostrou que as engrenagens que acionam a natureza às vezes tornam-se erráticas e imprevisíveis. Por que só agora foi possível observar estes fenômenos? Porque só é possível observá-los usando a grande potência dos computadores.

Segundo a ciência do caos, na natureza coexistem a ordem (o determinismo) com o caos (a imprevisibilidade). À ciência do caos normalmente estão associadas duas áreas novas e interligadas da matemática: sistemas dinâmicos e a geometria fractal. A teoria dos sistemas dinâmicos é o campo que discute analiticamente os fenômenos caóticos, mas não falarei deles aqui.

A geometria fractal é uma linguagem matemática que descreve, analisa e modela as formas encontradas na natureza. Tire uma foto de uma couve-flor e de uma pequena parte de seu corpo, e amplie as duas fotos no mesmo tamanho. Se a foto não tiver fundo, será geralmente impossível dizer qual é a couve-flor inteiro e qual é o pedaço. Isto é assim porque pequenos pedaços de couve-flor são «semelhantes» ao todo. Dizemos então que o couve-flor é «auto-semelhante».

Agora, o que os matemáticos descobriram é que este fenômeno de semelhança está provavelmente muito mais presente na natureza do que você talvez imagine. Isto ocorre nas estruturas das plantas, das montanhas, do cosmos, do cérebro, etc. Na verdade, poucas são as formas regulares na natureza, como por exemplo, a laranja, a melancia e o olho humano.

Mandelbrot é geralmente considerado como o pai da geometria fractal. Mas antes dele outros matemáticos criaram figuras estranhas que desafiavam o enquadramento nas definições convencionais da geometria Euclidiana, e por isso foram chamados de "monstros matemáticos". Vamos examinar três destes monstros.

O conjunto de Cantor é talvez o primeiro objeto reconhecido como fractal. Embora não possua o apelo visual da maioria dos fractais, este conjunto é peça fundamental no estudo dos fractais e dos sistemas dinâmicos. Ele também representa um modelo de imaginação abstrata na matemática. Para construir o Conjunto de Cantor (C) partimos do segmento unitário [0,1], o qual dividimos em 3 partes iguais e removemos a parte do meio, 1/3 < x < 2/3, processo conhecido como "remoção do terceiro meio". Os dois segmentos que sobraram formam o conjunto C₁. Em seguida, usando o mesmo processo de remover o terceiro meio das duas partes que sobraram, chamamos de C₂ os segmentos que sobraram. Repetindo o processo infinitas vezes chamamos de C o conjunto dos pontos que sobraram após remover infinitos segmentos.

De que é composto o conjunto C? Note que no 1° passo removemos 1/3 do comprimento do segmento unitário, no 2º passo removemos 2 segmentos de comprimento (1/3²), isto é, movemos (2/3²), no 3º passo removemos 4 segmentos de comprimento (1/3³), se removemos (2²/3³), e assim por diante. O total removido é 1(1/3) + 2(1/3²) + 2²(1/3³) + 2³(1/3⁴) + ... = 1/3 (1 + 2/3²+ 2²/3³ + 2³/3⁴ + ...).

O termo entre parêntesis é uma PG de razão (2/3), cuja soma é s=1/(1 - 2/3) = 3. Portanto o comprimento total removido é igual a (1/3) - 3 = 1. Então removemos todos os pontos do segmento [0, 1]? Observando, você pode imaginar que certamente sobraram os pontos extremos dos intervalos, isto é, C é o conjunto de todos estes pontos. Entretanto isto não é assim. Na verdade existem muitos pontos que não são extremos e que pertencem a C. Cada vez que removemos um intervalo, ficamos com outros dois intervalos, um na esquerda e outro na direita. Se um ponto está em C, podemos identificá-lo com uma sequência de posições à esquerda e à direita, que indicam a que intervalo este ponto pertence.

Qual é o tamanho de C? Note que os pontos extremos são enumeráveis de cardinalidade א‎₀, pois podemos associar a cada ponto extremo um número natural.  Mas como vimos, existem muitos outros pontos em C que não são extremos. O fato é que os pontos de C são o conjunto dos pontos extremos, mais o conjunto dos pontos que não são extremos. O Conjunto de Cantor é uma poeira destes dois tipos de pontos alinhados. Como saber se o Conjunto de Cantor é enumerável ou não enumerável?

Podemos transformar esta pergunta em "quantos números podemos formar com os conjuntos de E's (esquerda) e D's (direita)?" Naturalmente podemos associar conjuntos de números com E's e D's com qualquer números binários compostos de 0's e 1's usando a representação binária. Portanto, para cada número do intervalo [0, 1], existe um número diferente correspondente em C. Então a cardinalidade de C deve ser no mínimo a cardinalidade c do continuum. Por outro lado C é um subconjunto do continuum.

Pelo teorema de Cantor-Schooder-Berstein podemos remover todos os números enumeráveis de cardinalidade א‎₀ do continuum e ficaremos ainda com os números irracionais que tem cardinalidade c. Então a cardinalidade de C é igual a cardinalidade c do continuum e, portanto C é não enumerável. Como podemos concluir que C é um fractal?

Agora, olhe bem de perto o intervalo 0 < x ≤ 1/3 que restou após o 1º estágio. Se examinarmos esta porção de C com um microscópio e o ampliarmos com um fator de três vezes, o que veremos será uma réplica exata de C. A estas figuras autossemelhantes denominamos fractais.

O segundo fractal que apresentamos é a Curva de Koch, construída da seguinte forma,
1. Desenhe um segmento de reta;
2. Divida-o três partes iguais e retire a parte do meio;
3. Construa na parte central retirada um triângulo equilátero e retire sua base;
4. Repita o processo nos 4 segmentos restantes;
5. Repita o processo indefinidamente.

Novamente, temos uma construção baseada na autosemelhança, portanto um fractal. Uma versão das curvas de Koch é o floco de neve, onde no lugar de começar com um triângulo, começamos com um triângulo equilátero.

Embora esta curva não seja idêntica a um floco de neve, porque entre outras coisas ela é perfeitamente simétrica, podemos considerá-la como um floco de neve "ideal". Uma das motivações de Koch para desenvolver sua curva foi mostrar uma curva que não é diferenciável, isto é, uma curva que não tem tangentes em nenhum de seus pontos. Esta descoberta, já feita anteriormente por Weierstrass, causou uma crise no cálculo que há 200 anos se centrava na ideia diferencial por Newton e Leibniz, pois mostrou que uma curva, embora contínua, não é diferenciável em qualquer de seus pontos.

Nosso terceiro fractal também apresenta características peculiares. Iniciando com um triângulo equilátero no plano, aplique o seguinte esquema repetitivo de operações:
1. Marque os pontos médios dos três lados
2. Em conjunto com os vértices do triângulo inicial, estes pontos definem quatro novos triângulos iguais, do quais eliminamos o triângulo central. 

Então após o 1º passo temos três triângulos iguais com lados iguais a metade do lado do triângulo inicial; após o 2° passo temos nove triângulos iguais com lados de 1/4 do triângulo original, e assim por diante.

Como nos fractais anteriores, a cada nova interação, obtemos figuras indistinguíveis das anteriores numa escala menor, o que caracteriza uma autossemelhança. Mas existe uma maneira surpreendente de construir o triângulo de Sierpinski. Ela se chama "o jogo do caos":
1. Numa folha de papel construa um triângulo equilátero ABC, e marque um ponto inicial P₀;
2. Jogue um dado que, se der 1 ou 2 selecione o ponto A, se der 3 ou 4 selecione o ponto B, e se der 5 ou 6 selecione o ponto C. Jogue o dado e suponha que deu 5 que corresponde a C.
3. Ligue P₀ com C e marque P₁ na metade da distância P₀C;
4. Jogue o dado. Suponha que deu 3 que equivale a B. Ligue P₁ com B e marque P₂ na metade de P₁A;
5. Repita este procedimento diversas vezes sempre marcando os pontos Pᵢ.

Até os primeiros 30 pontos plotados, nada de muito interessante acontece. O que se percebe é que obviamente, a partir de um momento, os pontos P₁ caem dentro do triângulo ABC. Mas depoís de, digamos, 75 jogadas, o triângulo de Sierpinski começa a tomar forma.

Produzimos então uma estrutura extremamente ordenada gerada por um método totalmente aleatório! Isto não é coincidência nem milagre, e é exatamente o que quisemos dizer quando definímos a geometria fractal como uma estrutura que põe ordem no caos.

Considere agora, no lugar do triângulo ABC, um quadrado ACGT e aplicando as mesmas regras, só que com um dado de quatro faces, jogamos "o jogo do caos". Infelizmente neste caso, nada de tão visualmente radical é obtido, mas são gerados pontos com alguma ordenação.

O que isto tem de especial? Ocorre que a molécula de DNA que armazena as informações biológicas é composta de quatro blocos, A, C, G, T e supõe-se que toda informação biológica dos seres vivos está codificada na sequência das letras A, C, G, T, portanto algum padrão nesta estrutura poderá corresponder a características biológicas específicas.

Essa não é uma folha de samambaia, mas um fractal inventado por Barnsley, construído nos mesmos moldes dos exemplos anteriores. Se você observar os ramos que se originam do caule, notará que réplicas reduzidas da samambaia grande. Os ramos da direita e da esquerda têm no caule uma espécie de eixo, e vão reduzindo de tamanho no sentido vertical do caule. Mais uma vez a samambaia foi construída como a repetição de algumas poucas regras de transformação.

Como plotar figuras deste tipo será visto mais adiante. Por hora é importante salientar que figuras como o floco de neve e a samambaia naturais não são evidentemente cópias reduzidas exatas de si mesmas. O que existe nas figuras da natureza é uma autossemelhança aproximada em diferentes escalas. Essa autossemelhança aproximada é chamada de autossemelhança estatística, porque, em diferentes escalas, essa autossemelhança existe em média.

Nos fractais matemáticos as partes são cópias exatas do todo, mas nos fractais naturais as partes são apenas reminiscências do todo. A característica central dos fractais é sua invariância sob mudança de escala. Isto pode ser visto no exemplo inicial da couve-flor, olhando as nuvens céu ou observando o oceano a partir de um avião.

Oceano ou poça d'água? A que distância estão as nuvens? Desfiladeiro ou pedra? Em nenhum dos casos é possível identificar o tamanho real do objeto se não houver outro objeto de tamanho conhecido como referência.
