# Friedman e os Chicago Boys: o fascismo na vanguarda econômica latino-americana
Milton Friedman e a Escola de Chicago são fundamentais para a ditadura de Pinochet, a tal ponto que a própria constituição de 1980 é assinada por discípulos dessa escola como Sergio de Castro, Sergio de la Cuadra, Rolf Lüders, Álvaro Bardón, Hernán Büchi, etc.

Mario Vargas Llosa entrevistou Milton Friedman e perguntou-lhe se tinha alguma dúvida moral ao observar que suas teorias eram geralmente aplicadas em países com governos autoritários, ele respondeu:
>Não [...] não gosto de governos militares, mas procuro o mal menor.

De acordo com Friedman, se para que suas receitas sejam executadas, ele tem que confiar em regimes militares autoritários, então os apoia. E, de fato, foi. Não só na ditadura militar chilena, mas também na ditadura argentina de Videla e na Turquia militar.
