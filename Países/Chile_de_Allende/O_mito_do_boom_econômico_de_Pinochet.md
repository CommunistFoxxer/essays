# O mito do boom econômico de Pinochet
Enquanto o crescimento do PIB foi alto e os 10% mais ricos chilenos viram a renda aumentar em 83%, no final de seu regime 44% da população vivia na pobreza e os abonos de família eram 28% do que eram em 1970. Saúde e educação os orçamentos também caíram em aproximadamente 20%, enquanto a dívida externa cresceu 300%. O blog de economia de Harvard [explica](https://blogs.harvard.edu/guorui/2006/12/16/did-pinochet-following-milton-friedman-really-create-an-economic-mi/) como a nacionalização e a reforma agrária de Allende foram responsáveis pelos sucessos posteriores da economia do Chile. Até economistas burgueses como Paul Krugman [apontam](https://krugman.blogs.nytimes.com/2010/03/03/fantasies-of-the-chicago-boys/) que a ideia de Pinochet ter salvado a economia do Chile é um mito.

A constituição de Pinochet também não salvou o Chile, apenas o limitou. Pode-se argumentar que o Chile atualmente tem os melhores índices econômicos da América do Sul, mas isso só foi possível porque o Chile teve um processo de acumulação de capital massiva durante Pinochet e os últimos governos do Chile eram mais orientados à esquerda, e ajudaram a redistribuir a grande quantidade de capital que estava concentrado nas mãos dos burgueses. Mesmo esses feitos, foram enormes feitos, tendo em vista que a constituição de Pinochet limitava e muito a ação do governo na economia.

![](images/santiago_protest_2019.png)

<em>Protesto em Santiago no Chile, com cerca de 1 milhão de manifestantes, protestando contra as desigualdades sociais.</em>

O Chile em 2014 foi a 7° economia mais livre do mundo, mas em 2019 estourou uma revolta dentro do Chile justamente porque a liberdade econômica não favorecia a todos. Cerca de 15% da população do Chile têm acesso a saúde e existe alta segregação na educação por ser cara. Como relatado em um [artigo](https://noticias.uol.com.br/ultimas-noticias/bbc/2019/10/26/qual-e-o-custo-de-vida-no-chile-em-comparacao-com-o-brasil-e-outros-paises-da-america-latina.htm):
>A mediana dos salários dos chilenos é de US$550 (R$2.220) por mês. Esse é o salário que 50% da população economicamente ativa recebe, enquanto o salário mínimo atual é de US$414 (R$1.671).

O Chile cresceu cerca de 20% de 1990 até 2007, mas crescimento não é sinal de bem estar social e redução da desigualdade, porque atividade econômica cresce, mas o fruto dela não é repassado.
