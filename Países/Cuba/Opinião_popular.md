# Opinião popular
De acordo com um [artigo](https://newrepublic.com/article/121502/cuban-poll-shows-more-political-satisfaction-americans) publicado no New Republic, os cubanos estão significativamente mais satisfeitos com seu sistema político do que os americanos com o deles. O mesmo se aplica aos sistemas de saúde e educação:
>Mais de dois terços dos cubanos - 68% - estão satisfeitos com seu sistema de saúde. Cerca de 66% dos americanos disseram o mesmo em uma pesquisa Gallup de novembro de 2014. 72% dos cubanos estão satisfeitos com seu sistema educacional, enquanto uma pesquisa Gallup de agosto de 2014 descobriu que menos da metade dos americanos - 48% - estão “completamente” ou “um pouco” satisfeitos com a qualidade do ensino fundamental e médio.

O povo cubano também ratificou recentemente uma nova constituição, que reafirma o papel do Partido Comunista e afirma que Cuba é um Estado socialista que avança para o comunismo. A constituição também inclui algumas reformas políticas e econômicas, como o reconhecimento de pequenas empresas e a presunção de inocência no sistema judicial. De acordo com um [artigo](https://www.reuters.com/article/us-cuba-constitution-referendum-idUSKCN1QE22Y) da Reuters, evidências independentes apóiam a contagem oficial de votos (aproximadamente 90% de apoio):
>O jornal online independente El Toque pediu aos leitores que enviassem contagens locais, uma dúzia das quais mostrou apoio esmagador à ratificação.

Yoani Sanchez, a dissidente mais conhecida de Cuba, testemunhou a contagem em sua seção eleitoral local, relatando os resultados como "400 votos sim, 25 votos não e 4 votos em branco". Isso sugere que os resultados oficiais foram corretos e que o povo cubano apoiou de forma esmagadora a nova constituição.

Um [artigo](https://www.independent.co.uk/voices/fidel-castro-death-cuba-embargo-peasants-poor-urban-areas-modernisation-a7441036.html) no Independent, escrito por um autor cuja família vive em Cuba, resume bem esta questão:
>A maioria dos cubanos com quem falo apóia a reformulação da economia e a ampliação dos laços com os Estados Unidos. Assim como nós, eles querem melhorar de vida, querem um celular melhor, uma casa maior, querem viajar. Mas nenhum deles gostaria de viver em uma Cuba, por mais rica que fosse, sem educação gratuita universal, saúde gratuita, transporte público barato e os menores índices de crimes violentos das Américas. Nenhum deles. Este é o legado de Fidel.

Embora o povo cubano apoie amplamente a reforma econômica e a normalização das relações com os EUA, seu apoio geral às conquistas de seu sistema socialista permanece alto. Como afirma o New Republic:
>Indicadores objetivos, como as baixas taxas de mortalidade infantil e analfabetismo do país, há muito mostram que Cuba tem serviços sociais relativamente fortes. Os novos dados da pesquisa sugerem que os cubanos estão bem cientes disso.

Este é um crédito importante para a revolução.
