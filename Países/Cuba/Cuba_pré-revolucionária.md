# Cuba pré-revolucionária
Antes de aprender sobre as conquistas do socialismo cubano, devemos tirar um momento para examinar como era a vida antes da revolução. Os defensores de Fulgencio Batista costumam afirmar que os padrões de vida eram melhores antes da revolução. No entanto, um rápido exame dos fatos mostrará que isso não faz sentido. É verdade que o reinado de Batista viu um crescimento do PIB relativamente alto; no entanto, os indicadores de desenvolvimento humano pintam um quadro muito mais sombrio. De acordo com um [artigo](https://www.coralgablescavaliers.org/ourpages/users/099346/IB%20History/Americas/Cuba/Cuba_s%20Food%20Rationing.pdf) da Cornell University:
>Opiniões à parte, embora Cuba tenha sido classificada como um dos países em desenvolvimento mais prósperos na década de 1950 com base no produto interno bruto (PIB), os indicadores sociais desse período retratam condições sociais sombrias, especialmente entre os camponeses rurais.

O regime de Batista deixou o povo cubano (especialmente a grande população rural) atolado na pobreza e na doença. De acordo com o artigo citado, estudos contemporâneos relataram uma taxa de desnutrição de 91% entre os trabalhadores agrícolas. Embora alguns comentaristas considerem esse número muito alto, "ele transmite a magnitude do empobrecimento rural". As condições de saúde são resumidas por um [estudo](https://caribbean.scielo.org/scielo.php?pid=S0043-31442013000300015&script=sci_arttext&tlng=pt) no West Indian Medical Journal:
>Má higiene, saneamento ineficiente e desnutrição [contribuíram] para a taxa de mortalidade infantil de 60 por 1000 vidas, taxa de mortalidade materna de 125,3 por 1000, [e] uma taxa de mortalidade geral de 6,4 por 1000.

A população rural, em particular, sofria de péssimas condições de saúde; de acordo com um [estudo](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3464859/) publicado no American Journal of Public Health:
>Cuba tinha apenas 1 hospital rural, apenas 11% das famílias de trabalhadores rurais bebiam leite e a mortalidade infantil rural era de 100 por 1000 nascidos vivos.

A infraestrutura também foi lamentavelmente subdesenvolvida sob Batista. De acordo com o artigo do Cornell acima mencionado:
>De acordo com o censo de 1953, 54,1% das casas rurais não tinham banheiro de nenhum tipo. Apenas 2,3% das residências rurais tinham encanamento interno, em comparação com 54,6 das residências urbanas. Nas áreas rurais, 9,1% das casas tinham eletricidade, em comparação com 87% das casas nas áreas urbanas.

O analfabetismo e o desemprego foram generalizados sob Batista:
>Quase um quarto das pessoas de 10 anos ou mais não sabia ler nem escrever, e a taxa de desemprego era de 25%.

A alta taxa de analfabetismo não é surpreendente quando nos lembramos do péssimo estado da educação na Cuba pré-revolucionária. De acordo com um [artigo](https://www.theguardian.com/global-development/poverty-matters/2011/aug/05/cuban-development-model) no The Guardian:
>Em 1958, durante a ditadura de Batista, metade das crianças de Cuba não frequentava a escola.

Tudo isso sem falar da dominação imperialista, do crime organizado e da exploração desenfreada que o povo cubano sofreu durante o reinado de Batista. Com tudo isso em mente, passemos a examinar a revolução cubana e suas conquistas.
