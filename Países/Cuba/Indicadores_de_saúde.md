# Indicadores de saúde
O sistema de saúde de Cuba é uma de suas conquistas mais impressionantes e conhecidas. De acordo com o [artigo](https://www.coralgablescavaliers.org/ourpages/users/099346/IB%20History/Americas/Cuba/Cuba_s%20Food%20Rationing.pdf) acima mencionado da Cornell University:
>Os indicadores de saúde superiores de Cuba - altamente classificados tanto regional quanto globalmente - são atribuídos aos serviços universais de saúde primária do país.

O sistema de saúde cubano é baseado no investimento público e na provisão universal. De acordo com um [relatório](http://www.ecocubanetwork.net/wp-content/uploads/NASW%20CUBA%20REPORT.pdf) da Associação Nacional de Assistentes Sociais:
>Cuba tem o maior número de médicos per capita do que qualquer país do mundo. O país dedica quase um quarto de seu produto interno bruto (PIB) à educação e saúde - quase o dobro da porcentagem do PIB dos Estados Unidos destinada às mesmas despesas. Como resultado, o país garante educação e assistência médica gratuitas para todos os cidadãos, e as mulheres recebem seis semanas de licença-maternidade remunerada e até um ano de licença remunerada após o parto.

Segundo [dados](https://data.worldbank.org/indicator/SP.DYN.LE00.IN?locations=CU-US) do Banco Mundial, a expectativa de vida de Cuba é um pouco maior que a dos Estados Unidos. Compare isso com a era pré-revolucionária, quando a expectativa de vida cubana era aproximadamente 6 anos menor do que a expectativa de vida americana.

Além disso, segundo [dados](https://data.worldbank.org/indicator/SP.DYN.IMRT.IN?locations=CU-US) do Banco Mundial, a taxa de mortalidade infantil de Cuba é aproximadamente 1/3 menor que a dos Estados Unidos. Compare isso com a era pré-revolucionária, quando a taxa de mortalidade infantil cubana era quase o dobro da dos EUA.

De acordo com um [estudo](https://journals.sagepub.com/doi/abs/10.1177/0169796X19826731) de 2019 publicado no Journal of Developing Societies, os indicadores de saúde de Cuba superam os das nações desenvolvidas, apesar dos gastos muito menores:
>Enquanto Cuba gasta cerca de um vigésimo per capita com saúde em comparação com os EUA, as pessoas em Cuba, no entanto, têm uma expectativa de vida mais longa (79 anos) do que as pessoas nos EUA (78 anos). Cuba também tem uma mortalidade infantil superior (o número de mortes até a idade de 5 anos por 1.000 nascidos vivos por ano) de seis, em comparação com oito nos EUA.

Enquanto ao Nobel, quantos países são solidários enviando seus médicos e não recebem devidos créditos? A preferência é central ([fonte](https://gauchazh.clicrbs.com.br/geral/2018/11/ninguem-vem-de-la-enganado-diz-medico-cubano-que-trabalha-no-rs-cjokbkhcw0ee301rx4yd62g3x.html)).
>Ninguém vem de Cuba para cá enganado. Todo mundo sabe o que vai ganhar e a parte com a qual o governo vai ficar.

Cuba também fez alguns avanços surpreendentes na área da saúde. De acordo com a OMS, [Cuba é a primeira nação do mundo a eliminar a transmissão vertical (mãe para filho) do HIV e da sífilis](https://www.who.int/mediacentre/news/releases/2015/mtct-hiv-cuba/en/).

De acordo com o Washington Post, [Cuba desenvolveu uma potencial vacina contra o câncer de pulmão](https://www.washingtonpost.com/news/to-your-health/wp/2016/10/27/in-a-first-u-s-trial-to-test-cuban-lung-cancer-vaccine/?utm_term=.9a6205587548), com resultados muito promissores, e está sendo testada nos Estados Unidos. Em Cuba, um transplante de rim custa cerca de 5 mil dólares. Nos EUA, custa cerca de 50 mil USD.

A experiência cubana oferece um modelo importante a ser seguido por outras nações. De acordo com um [estudo](https://academic.oup.com/ije/article/35/4/817/686547) no International Journal of Epidemiology (publicado pela Oxford University Press):
>Cuba representa um importante exemplo alternativo onde investimentos modestos em infraestrutura combinados com uma estratégia de saúde pública bem desenvolvida geraram medidas de estado de saúde comparáveis ​​às dos países industrializados. Se a experiência cubana fosse generalizada para outros países pobres e de renda média, a saúde humana seria transformada.

Um [artigo](https://www.theguardian.com/news/gallery/2007/jul/17/internationalnews) no The Guardian resume este tópico muito bem:
>Quer se trate de uma consulta, dentadura ou cirurgia cardíaca aberta, os cidadãos têm direito a tratamento gratuito. Como resultado, a empobrecida ilha ostenta melhores indicadores de saúde do que sua vizinha exponencialmente mais rica, a 145 quilômetros do estreito da Flórida.

Esta é uma conquista extremamente impressionante.
