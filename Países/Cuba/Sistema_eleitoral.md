# Sistema eleitoral
O modelo eleitoral de Cuba é muito mais democrático que o gerrymandering racista e colonial dos EUA e seu sistema injusto de swing state.

Yoani Sanches, famosa por ser a grande expositora das "crueldades" de Cuba, anti-comunista, famosa por seu blog, até hoje não perdeu seus direitos políticos, entra e sai de Cuba, tem casa em Havana e pode votar e se candidatar.

Em Cuba, você pode votar e se candidatar à partir dos 16 anos. Não precisa estar filiado ao partido, mais de 50% dos candidatos à cargos provinciais e regionais não estão filiados ao partido, e ocorre eleição de 2 em 2 anos. Também não há qualquer restrição de voto, independente da religião, sexo ou crenças. Há até uma bancada evangélica forte, e uma bancada de oposição ao presidente. Os cargos executivos são escolhidos de 5 em 5 anos. E o número de vereadores é proporcional ao número de habitantes por cada região. O Partido Comunista não indica candidatos à eleição para não atrapalhar a autonomia cubana e os interesses de cada região.

O modelo cubano é parlamentarista, onde se escolhe 605 delegados e 1 desses é escolhido pelo Conselho para ser presidente. Os poderes do presidente são menores do que o Conselho de Estado, aliás. Em Cuba, se você é novo para ser prefeito ou algo do tipo, você pode se alinhar à sindicatos e eles terão papel importante na sua divulgação, e ganha o que o povo achar que tem os melhores projetos que atendem às necessidades da região. A fiscalização é feita pelos próprios cidadãos, eles podem fazer um mini-curso de como fiscalizar, contar e gerir os votos, então eles passam a serem as autoridades. Quase 172 mil cubanos se voluntariaram nas últimas eleições para contarem os votos e fiscalizarem.

O Partido Comunista não tem poder executivo, ele apenas tem papéis em:
- Consciência de classe (propaganda) e cursos políticos
- Educação
- Mobilização da sociedade
- Fiscalizar o Estado

Vale lembrar que os parlamentares em Cuba não têm privilégio ou auxílio algum como aqui no Brasil, então ao mesmo tempo em que eles cumprem seus papéis políticos, eles também trabalham. Tudo isso para garantir a igualdade social e que também não recebam de fundos estrangeiros.
