# Desenvolvimentos de infraestrutura
Em 1959, aproximadamente 50% dos lares cubanos tinham acesso à eletricidade. De acordo com um [relatório](https://www.edf.org/sites/default/files/cuban-electric-grid.pdf) do Fundo de Defesa Ambiental, em 1989, mais de 95% das famílias tinham acesso à eletricidade, inclusive nas áreas rurais, que antes eram quase totalmente privadas desta novidade. Cuba também superou muitos de seus vizinhos em termos de geração de eletricidade:
>Em 1990, Cuba tinha cerca de 1,8 vezes mais capacidade de geração por pessoa do que a República Dominicana e 1,3 vezes mais do que a Jamaica.

Em Cuba, [o acesso à água potável e ao saneamento melhorou muito](http://data.un.org/en/iso/cu.html) desde a revolução. De acordo com dados das Nações Unidas, em 2018, 96,4% da população urbana e 89,8% da população rural tinham acesso a água potável, enquanto 94,4% da população urbana e 89,1% da população rural tinham acesso a serviços de saneamento melhorados.

Um excelente [artigo](https://www.independent.co.uk/voices/fidel-castro-death-cuba-embargo-peasants-poor-urban-areas-modernisation-a7441036.html) do Independent discutiu muito bem esse assunto:
>Este é o legado de Fidel. Água potável e eletricidade para todos. E educação e saúde universais e gratuitas. Os cubanos costumam brincar que são mais saudáveis ​​e têm melhor educação do que os americanos, apesar do bloqueio americano de mais de 50 anos.
>
>Então, para mim, a Cuba rural é a Cuba de Fidel. Seus ideais continuam vivos aqui - e os pobres rurais de Cuba foram os que mais se beneficiaram com suas políticas do berço ao túmulo. Aqui, os netos dos camponeses realmente se tornam cirurgiões consultores e pilotos de aviões comerciais.

Vale lembrar também que Cuba é um país pobre em recursos naturais. Não produz grande variedade agrícola e também não tem grandes fontes de energias, como quedas d'água para construir hidrelétricas ou petróleo em abundância. Você vê as paredes descascando? Sim, mas você não vê mais crianças esmolando, a prostituição em Havana, pessoas vão para a escola à noite, deixam seus notebooks em praças sem medo de serem roubadas. Apesar de todo o embargo e sem recursos naturais, Cuba superou muitos problemas.

Este é um enorme crédito para a revolução.
