# Fidel
Fidel tem um lugar muito especial para mim, não apenas por suas incríveis realizações na América Latina e na África, mas também por sua formação jurídica. Ele era um advogado dedicado ao povo, onde, depois de se formar em direito, abriu um escritório especificamente para servir aos pobres, mas falhou. Sua educação jurídica continuou a servi-lo bem, embora, como sua primeira tentativa de revolta e sendo preso, ele fez seu discurso extremamente inspirador do tipo ["a história vai me absolver"](https://www.marxists.org/history/cuba/archive/castro/1953/10/16.htm) como argumento final.
>Uma causa justa, mesmo nas profundezas de uma caverna, pode fazer mais do que um exército.

Lembra daquela frase "Ganhei com 80 homens, mas preferia ter 40 para começar, desde que fossem dedicados"? Na verdade, [ele só acabou com 19](https://en.wikipedia.org/wiki/Fidel_Castro#Guerrilla_war:_1956%E2%80%931959).
