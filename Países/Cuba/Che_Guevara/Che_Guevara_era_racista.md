# Che Guevara era racista?
É verdade que Che Guevara fez declarações racistas quando era jovem, o que os especialistas infelizmente afirmam "não seria incomum vindo de um argentino de 24 anos na época" (veja citação abaixo); no entanto, à medida que crescia, Che Guevara passou a rejeitar essas declarações e ideias. Citando um [artigo](https://www.politifact.com/florida/statements/2013/apr/17/marco-rubio/did-che-guevara-write-extensively-about-superiorit/) do Politifact sobre o assunto:
>Em manifestações de trabalhadores nessa época, Guevara e Raul Castro falaram sobre a necessidade de "avançar o programa anti-discriminação da revolução", escreveu Alejandro de la Fuente, professor de história da Universidade de Pittsburgh em seu livro A Nation for All: Race, Inequality and Politics in 20th Century Cuba. No discurso em Santa Clara, Guevara pediu que a universidade "se pinte de preto, se pinte de mulata" alunos e professores, escreveu Fuente.
>
>Em 1964, Guevara falou perante as Nações Unidas e criticou a intervenção "racista" do Ocidente no Congo, escreveu Anderson em seu livro.
>
>Pessoas que estudaram Guevara disseram que ele era racialmente inclusivo em suas ações.

Esses sentimentos são apoiados por vários outros especialistas no assunto, como Jonathan Benjamin-Alvarado:
>Jonathan Benjamin-Alvarado, professor de ciências políticas da Universidade de Nebraska que ensina política latina, disse que Guevara passou por uma transformação desde seus primeiros dias na Argentina.
>
>"Embora haja evidências para apoiar a alegação de que Ché fez tais declarações, ou as guardou em sua mente quando jovem, seu papel na revolução foi aquele em que ele defendeu abertamente ideais anti-racistas e igualitários", disse Benjamin-Alvarado ao PolitiFact em um e-mail.

Talvez a declaração mais explícita de Guevara contra o racismo tenha ocorrido em 11 de dezembro de 1964, em um [discurso](https://www.marxists.org/archive/guevara/1964/12/11.htm) que proferiu nas Nações Unidas:
>Aqueles que matam seus próprios filhos e os discriminam diariamente por causa da cor de sua pele; aqueles que deixam os assassinos de negros permanecerem livres, protegendo-os e, além disso, punindo a população negra por exigir seus direitos legítimos de homens livres - como aqueles que fazem isso podem se considerar guardiães da liberdade? Compreendemos que hoje a Assembleia não está em condições de pedir explicações sobre estes atos. Deve ficar claramente estabelecido, entretanto, que o governo dos Estados Unidos não é o campeão da liberdade, mas sim o perpetrador da exploração e opressão contra os povos do mundo e contra grande parte de sua própria população.

E, claro, Che lutou ao lado de revolucionários negros no Congo, o que ele não teria feito se nutrisse ódio racial contra eles. Em suma, é claro e óbvio para qualquer pessoa honesta que Che Guevara não era racista e que condenava a discriminação racial, estando ao lado de revolucionários de todas as origens raciais.
