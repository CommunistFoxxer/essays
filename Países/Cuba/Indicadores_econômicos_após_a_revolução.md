# Indicadores econômicos e nutricionais após a revolução
Desde o início, a revolução cubana esteve comprometida com a melhoria de vida do povo, tanto no plano econômico como no social. De acordo com um [relatório](https://web.archive.org/web/20090303221405/http://www.oxfamamerica.org/newsandpublications/publications/research_reports/art3670.html/OA-Cuba_Social_Policy_at_Crossroads-en.pdf) da Oxfam America:
>Quando a revolução de Cuba chegou ao poder em 1959, seu modelo de desenvolvimento visava vincular o crescimento econômico aos avanços na justiça social.

Segundo dados das Nações Unidas, a [taxa de desemprego em Cuba continua abaixo de 3%](http://data.un.org/en/iso/cu.html), há décadas. As taxas não oficiais podem ser um pouco mais altas, mas mesmo o dobro dessa taxa ainda colocaria Cuba bem abaixo da média regional (e muito mais baixa do que sob Batista):
>De acordo com o Índice de Fome Global de 2019, Cuba é uma das dezessete nações da Terra (e apenas quatro na América Latina) a ter uma pontuação inferior a 5, o que significa níveis impressionantemente baixos de fome. A taxa de desnutrição de Cuba é inferior a 2,5%.

De acordo com um [relatório](https://www.globalhungerindex.org/cuba.html) do Our World in Data (baseado na Universidade de Oxford), os americanos têm duas vezes mais chances de morrer de desnutrição do que os cubanos.

Segundo um [relatório](http://www.fao.org/ag/agn/nutrition/cub_en.stm) da FAO, "percentagens notavelmente baixas de desnutrição infantil colocam Cuba na vanguarda dos países em desenvolvimento".

Segundo um [relatório](https://web.archive.org/web/20131105150934/http://www.fas.usda.gov/itp/cuba/CubaSituation0308.pdf) do Departamento de Agricultura dos Estados Unidos, o cubano médio consome aproximadamente 3300 calorias por dia, muito acima da média da América Latina e do Caribe, e apenas um pouco menor do que nos Estados Unidos. Aproximadamente 2/3 das necessidades nutricionais são atendidas por rações alimentares mensais, enquanto o restante é comprado de forma independente. O relatório também afirma:
>A economia cubana fez um progresso notável em direção à recuperação do desastre econômico gerado pelo colapso do Bloco Soviético.

Em seu [relatório](https://www1.wfp.org/countries/cuba) sobre Cuba, o Programa Mundial de Alimentos (o braço de assistência alimentar das Nações Unidas) afirma que:
>Nos últimos 50 anos, programas abrangentes de proteção social erradicaram amplamente a pobreza e a fome. As redes de segurança social baseadas em alimentos incluem uma cesta básica mensal para toda a população, programas de alimentação escolar e programas de saúde materno-infantil.

Isso é especialmente impressionante quando Cuba é comparada a outros países em desenvolvimento e considerando as décadas de bloqueio econômico que a nação sofreu. O relatório também afirma:
>Maior ilha do Caribe, Cuba ocupa a 72ª posição entre 189 países no Índice de Desenvolvimento Humano de 2019 e é uma das mais bem-sucedidas em alcançar os Objetivos de Desenvolvimento do Milênio (ODM).

Um [artigo](https://www.theguardian.com/global-development/poverty-matters/2010/sep/30/millennium-development-goals-cuba) do The Guardian aborda este tópico:
>...as evidências sugerem que Cuba fez um excelente progresso em direção aos ODM na última década, com base no que já é universalmente reconhecido como realizações notáveis ​​em padrões equitativos de saúde e educação.
>
>De acordo com um novo Relatório dos ODMs do Overseas Development Institute, Cuba está entre os 20 países com melhor desempenho no mundo.

O artigo também inclui uma declaração de um economista cubano sobre como esse progresso é feito:
>A economia cubana é planejada e redistribuímos as receitas dos setores mais dinâmicos, que mais geram divisas, para os menos dinâmicos, mas necessários para o país. É assim que mantemos um orçamento para manter a saúde e a educação de alta qualidade e gratuitas para o usuário.

A revolução melhorou muito a situação habitacional em Cuba e também trouxe um desenvolvimento urbano significativo. De acordo com a Oxfam America:
>As iniciativas nas cidades não foram menos ambiciosas. A reforma urbana trouxe uma redução pela metade dos aluguéis para os inquilinos cubanos, oportunidades para os inquilinos terem suas próprias casas e um ambicioso programa de construção de moradias para os que vivem em favelas marginais. As novas moradias, junto com a implementação de medidas para criar empregos e reduzir o desemprego, especialmente entre as mulheres, transformaram rapidamente as antigas favelas.

Finalmente, o sistema de seguridade social e pensões em Cuba melhorou drasticamente desde a revolução, como evidenciado por esta declaração do relatório da Oxfam América acima:
>Tanto a cobertura quanto a distribuição melhoraram significativamente desde a revolução. Com sistema previdenciário desde a década de 1930, Cuba foi um dos primeiros países latino-americanos a instituí-lo. Consistia em fundos de pensão independentes e em 1959 cobria cerca de 63% dos trabalhadores, mas o sistema variava muito em termos de benefícios e dependia quase exclusivamente das contribuições dos trabalhadores. Desde 1959, o programa foi totalmente financiado pelo governo. Em 1958, cerca de 63% da força de trabalho estava coberta por seguro de velhice, invalidez e sobrevivência; hoje, a cobertura é universal.
