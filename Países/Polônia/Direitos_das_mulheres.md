# Direitos das mulheres
As mulheres obtiveram grandes ganhos na República Popular da Polônia. Os direitos reprodutivos e o aborto são um grande exemplo disso. Antes da era comunista, o aborto só era legal em casos de atividade sexual criminosa. De acordo com a Brown Political Review:
>No início do século 20, o aborto era ilegal em qualquer circunstância na Polônia. Mas em 1932, a Polônia promulgou um código que legalizou o aborto nos casos de um ato criminoso, ou seja, estupro, incesto e sexo com menores. Esta foi a primeira lei de aborto que perdoou o aborto em caso de crime. A lei permaneceu nos livros de 1932 a 1956.

No entanto, foi apenas na era comunista que o aborto se tornou totalmente legal, bem como disponível gratuitamente:
>Em 1956, o Sejm polonês (a câmara dos deputados do parlamento), de acordo com a ortodoxia do Partido Comunista, legalizou o aborto quando as mulheres expressarem “difíceis condições de vida”. Durante as décadas de 60 e 70, o aborto tornou-se disponível gratuitamente em hospitais públicos e clínicas privadas. Embora o sistema soviético encorajasse as mães a engravidar, a lei deixava que os médicos decidissem se o aborto deveria ser realizado e, em grande parte, garantia fácil acesso à operação.

Até mesmo comentaristas reacionários reconheceram a igualdade de gênero na era comunista. De acordo com o The Guardian:
>Estampado no DNA dessa sociedade, desde os anos do pós-guerra até 1991, estava que todos tinham que trabalhar; para isso, deveria haver igualdade de acesso à educação, cuidados infantis (principalmente ligados aos locais de trabalho) e cuidados aos idosos.

O emprego para mulheres era extremamente alto na era comunista, e caiu drasticamente depois:
>Ao longo dos anos comunistas, a participação feminina na força de trabalho era incrivelmente alta, frequentemente citada em 90%. Com o colapso do comunismo, a participação caiu para 68% e agora está em 45%.

Uma mulher polonesa é citada como tendo dito:
>O regime não fazia absolutamente nenhuma distinção entre homens e mulheres. Eu nunca pensei sobre a divisão - todo avanço na sociedade estava aberto a homens e mulheres igualmente.
>
>No que diz respeito à educação era absolutamente igual, na medida em que nas universidades técnicas - as de alto padrão de engenharia - acho que 30% dos alunos eram mulheres (isso foi na década de 1960 - cursos de engenharia no Imperial College Londres ainda tem uma proporção de homens para mulheres de 5:1 hoje).

Tenha em mente que este artigo do The Guardian foi escrito de uma perspectiva firmemente anticomunista, e mesmo assim reconhece que "o fim do comunismo na Polônia não ajudou as mulheres polonesas". Isso demonstra as melhorias nos direitos das mulheres feitas durante os comunistas.
