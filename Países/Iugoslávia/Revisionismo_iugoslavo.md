# "Revisionismo iugoslavo"
Infelizmente, muitos MLs chamam apoiadores da Iugoslávia de revisionistas, pelo fato de que o modelo iugoslavo descobriu que o marxismo-leninismo não era a única forma de se organizar, e travaram duras críticas ao leninismo, assim como o fizera décadas passada a Rosa Luxemburgo.

Muitos ainda citam um [artigo](https://www.novacultura.info/post/2019/01/26/revisionismo-iugoslavo-produto-da-politica-imperialista) muito famoso chamado "Revisionismo iugoslavo: produto da política imperialista", e aqui irei elaborar uma crítica a este artigo.

O artigo começa apontando o auxílio que Tito de fato recebeu dos EUA, mas atribui erroneamente esse fato com a política interna e externa que Tito «por contexto» foi condicionado a seguir. Tito recebeu de fato um auxílio que compôs 4% da renda da Iugoslávia, mas não, isso não “implementou” um estilo de vida americano, ou pelo menos os EUA não conseguiram o implementar.

Primeiro de tudo, é bom ressaltar a tendência de todo período pós-revolução em o país receber auxílio do tipo, tratado Sino-Soviético que o diga; a aliança ter acontecido justamente com os Estados Unidos (note, alianças "pragmáticas" do tipo sempre ocorreram em países do tipo, incluindo na Rússia Soviética, como vou abordar mais para frente) só relata a atuação mais óbvia considerando o longo histórico Tito-Stalin.

O texto cita uma notícia do Washington Post e argumenta sobre como em 1957 o país balcânico teria adotado um "estilo de vida americano". Será? Na verdade, em 1957, uma grande empresa queria justamente atacar países socialistas - de forma típica como os EUA faziam; discursava sobre uma farta variedade de produtos e marcas, e usava formas de mídia como Washington Post para mostrar as "maravilhas" que um país capitalista poderia oferecer. Não obstante, esse tipo de atuação ocorreu na mesma época contra a União Soviética. Longe de uma implementação do "padrão de vida americano" vinda de Tito, isso foi um boicote contra ele - de origem estadunidense, mirando tanto o bloco soviético quanto a Iugoslávia.

Ainda tentando relacionar o auxílio com a ruptura entre a Iugoslávia e a URSS, o artigo cria uma falsa dicotomia entre ser vassalo da União Soviética ou ser um país do bloco imperialista, e desconsidera todo o caminho do líder iugoslavo até onde chegou. A ruptura entre Tito e a URSS vem de antes da Iugoslávia sequer se tornar independente, e objetiva muito além de "ficar acima dos soviéticos e trair o proletariado internacional".

De forma teórica, o Titoísmo sequer tem motivos para se espelhar na URSS, como, por exemplo, a China o faria. Dentre as várias tendências do marxismo, as idéias de centralismo e de vanguarda sequer fazem parte do pensamento marxiano, quem dirá parte do Titoismo. Existe uma vasta cultura de autores (do Jovem Lukács ao conselhismo de Korsch) que, seguindo a tendência da Terceira Internacional, rompeu totalmente com a ideia de que a única atuação prática possível é a vanguardista. Não diferentemente, a característica fundante do socialismo iugoslavo foi justamente o sistema de autogestão e a oposição ao centralismo - o texto sequer critica as premissas básicas da economia Kardeljiana. Em uma máxima, o artigo pode ser usado para apontar as diferenças entre os dois sistemas; do contrário, usar o fato de existirem diferenças entre experiência X ou Y como crítica o torna um argumento vazio, além de confirmar dogmatismo leninista que o texto contesta.

No âmbito concreto, supondo que fosse de interesse de Tito se aliar com a URSS, não podemos também desconsiderar o longo histórico entre Tito e Stalin: Em 1921, após o Partido Comunista Iugoslavo ser exilado e dissolvido de seu país, Tito e membros do Partido Comunista da Iugoslávia viveram no exílio e na ilegalidade até se reorganizarem em Moscou, onde se tornaram representantes do Partido no novo país. A ruptura, objetivamente falando, ocorreu durante o Grande Expurgo, quando Tito viu um a um de seus aliados serem mortos; foi exilado e caçado até retornar ao seu país. Esse tipo de narrativa contra a Iugoslávia anseia por pintar um herói - e desdobra os acontecimentos usando uma falsa dicotomia entre bem e mau. Pinta um mundo que nunca existiu onde os movimentos operários coexistem em harmonia sob a guarda do grande líder Stalin e seu conhecimento revolucionário.

Fora dessa concepção idealista, o movimento comunista internacional nunca foi unido de tal forma, e conflitos do tipo sempre fizeram parte: desde a Oposição Operária do partido Bolchevique, passando por Kronstadt, revolta na Grécia e várias outras que ocorreram impulsionadas pela revolução de 1917. Note, a maioria dos conflitos é, se não consequência prática de todo sistema vanguardista, uma ilustração de como a URSS atuou, as vezes, de forma contra-revolucionária perante o movimento comunista (autônomo) internacional.

O artigo critica várias vezes o auxílio EUA-Iugoslávia, mas o mesmo «que pinta o modelo soviético como modelo a ser seguido» não cita alianças como a Churchill-Stalin que, não só se aliou com potências imperialistas, como interferiu de forma direta contra a classe operária Grega - contexto onde o KKE (Partido Comunista da Grécia) após conquistar quase todas as cidades, teve sua revolta desarmada pela aliança soviética.

Alguns companheiros de guerra do Tito, como o Kardelj, concluiriam que uma relação mais informal com a OTAN era necessária. Isso se daria por dois motivos: o primeiro é, como apontado, por Kardelj, a oposição interna à tendência de um órgão como a ONU agir como instrumento dos países imperialistas; e pelo fato de a Iugoslávia ser um país pequeno que provavelmente teria dificuldades para se defender de um país como a URSS. No entanto, essa medida não foi necessária por muito tempo. Após a morte de Stalin, Tito criou o Movimento Não-Alinhado, protagonizando a luta internacional contra a dependência da URSS e dos EUA.

O “grupo de Tito” sempre reconheceu a luta de países como a URSS e sua importância na luta contra o imperialismo; na verdade, o vice-presidente da Iugoslávia, Kardelj, até escreveu um artigo apontando a luta de forças democráticas do tipo como ferramenta importante para a destruição do mundo capitalista – longe de afirmar que o desenvolvimento do socialismo viria de forma pacífica; e longe de negar o imperialismo como categoria importante para o capitalismo.

O texto repete frases de Lenin, as pinta como verdades absolutas e reivindica sua posição dogmatista; desconsidera que cada contexto cria uma realidade concreta diferente; e assim, desconsidera que toda experiência revolucionária terá, naturalmente, suas próprias características.

Já um outro [artigo](https://www.revolutionarydemocracy.org/archive/yugoslav.htm) do Hoxha chamado "The Yugoslav Revisionist Leaders — Dangerous Enemies of the International Communist and Workers’ Movement" também é muito citado.

O texto do Hoxha começa usando um trecho de Marx que ele mesmo desmente em "guerra civil na França" e nos prefácios do manifesto depois de viver a comuna. 
>A Comuna, nomeadamente, forneceu a prova de que a classe operária não pode simplesmente tomar posse da máquina de Estado [que encontra] montada e pô-la em movimento para os seus objec0tivos próprios.

Marx contrapõe o estado centralizado, Hoxha pega um Marx parado no tempo e toma ele como sendo a totalidade de seu pensamento.

O termo "socialismo de mercado" também é bem desexplicativo quando pinta o "socialismo real" em um governo que literalmente reproduz forma-mercadoria. Além de não criticar as bases da autogestão que estão, como eu disse, nos escritos de Marx sobre a Comuna e nem falo só sobre o Kardelj.

A "associação livre dos produtores" vai ser um conceito de Marx importante na maioria dos teóricos da autogestão, tanto no pessoal do CCI, quanto nos iugoslavos e nos franceses (que vão criticar os iugoslavos).

Há uma obra não traduzida de Edvard Kardelj, chamada "Socialismo e guerra" onde o mesmo responde às críticas chinesas. Leninismo e marxismo não são sinônimos, e nem Kardelj nem nenhum autogestionário jurava fidelidade dogmática ao seu criador.

Outra gama de "denunciantes" apontam que os problemas do defensor da Iugoslávia estão no livro "Esquerdismo: A doença infantil do comunismo" de Lenin, mas o livro é uma crítica à uma fração muito específica do movimento comunista, e ele nem cita o Bordiga nesse livro, e mais, o "esquerdismo" é um livreto pequeno, passa longe da imensidão das denúncias que o CCI fez.

Um fato interessante - e engraçado - é que Amadeo Bordiga, muito conhecido mesmo por julgar experiências socialistas como burocráticas e semi-capitalistas, disse que ao contrário das tendências do Leste Europeu, a da Iugoslávia era a única que parecia se descentralizar.

A autogestão tem como base: a organização de comunas/conselhos - a propriedade social dos meios de produção (isso inclui a de orgãos de assistência social, tanto os de educação/saúde como os de regulação), e a negação de qualquer forma de dirigência burocrática (sendo essa uma dirigência onde representantes da classe operaria ou orgãos centralizados como o estado tem autoridade sobre essa). As empresas eram reguladas pelos orgãos e sindicatos, e esses não eram necessariamente parte do partido. Horvat inclusive ataca o que ele chama de "partidocracia" e é crítico de toda forma de dirigência do tipo. O modelo de mercado de cooperativas nunca foi um fim em si, e foi analisado por Bourdet como "autogestão limitada", não é à toa que a Iugoslávia sempre se descentralizou, talvez depois da constituição de 74 o caráter de "forma-mercadoria" fosse caindo gradativamente (embora seja difícil analisar seu verdadeiro impacto, já que a crise do petróleo ocorreu em 1977).

É importante ressaltar que Bourdet chamava a experiência iugoslava de "autogestão limitada".

Fontes:
- [The Washington Post | The great American supermarket lie](https://www.washingtonpost.com/news/made-by-history/wp/2018/04/27/the-great-american-supermarket-lie/)
- [Amazon | Bourrinet - The Dutch and German Communist Left](https://www.amazon.com.br/Dutch-German-Communist-Left-1900-1968/dp/1608468216)
- [Editoria Em Debate | José Carlos Mendonça - Karl Korsch: Crítico Marxista do Marxismo](https://editoriaemdebate.ufsc.br/catalogo/karl-korsch-critico-marxista-do-marxismo/)
- [Amazon | Stevan Pavlowitch - The improbable survivor: Yugoslavia and its problems](https://www.amazon.com/Improbable-Survivor-Pavlowitch/dp/0814204864), p. 36
- [Marxists | Edvard Kardelj - O Desenvolvimento e as Perspectivas da Situação Internacional.](https://www.marxists.org/portugues/kardelj/ano/mes/desen.htm)
- [International Churchill Society | A Regency Fit for a King: Churchill Visits Athens](https://winstonchurchill.org/publications/finest-hour/finest-hour-180/churchill-visits-athens-greek-government/)

