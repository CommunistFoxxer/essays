# Os problemas da Albânia comunista
Apesar das grandes conquistas do governo comunista, houve enormes falhas que acabaram por derrubar a nação. Estes foram:

1. O rígido anti-revisionismo de Hoxha, que se recusou a permitir qualquer desvio do modelo stalinista, mesmo quando certas políticas não faziam mais sentido para as condições atuais.
2. O isolamento auto-imposto da nação, enquanto Hoxha queimava pontes com outras nações socialistas que não compartilhavam sua interpretação doutrinária do marxismo.
3. A paranóia que resultou deste isolamento, incluindo a infame campanha de bunker, realizada para evitar uma temida invasão da URSS ou da Iugoslávia. Em média, cada bunker custa tantos recursos quanto um apartamento de dois quartos, e os recursos totais dedicados a construí-los poderiam ter resolvido facilmente o déficit habitacional do país.
4. Quase entrou em falência por causa do FMI.

Esses problemas deveriam servir como um alerta para todas as gerações subsequentes de socialistas e comunistas, pois mesmo as grandes conquistas do socialismo na Albânia não foram suficientes para evitar que as falhas acima mencionassem tudo.
