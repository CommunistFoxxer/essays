# Conclusão
A República Socialista Popular da Albânia é um caso complicado para analisarmos. A seguinte avaliação da revolução é dada pelo estudo LSE:
>No geral, como podemos caracterizar a Albânia comunista? Claramente, apesar do considerável desenvolvimento econômico em relação ao seu nível anterior, permaneceu pobre e principalmente rural. No entanto, a agenda social do desenvolvimento avançou muito mais. A educação e a seguridade social tornaram-se virtualmente universais e os cuidados de saúde estavam disponíveis para todos. Além disso, a discriminação tradicionalmente severa contra as mulheres foi bastante reduzida.

Algumas conquistas dos comunistas são reconhecidas até pelos reacionários:
>A redução das tradições de longa data de discriminação contra as mulheres e o fornecimento de educação universal e atenção primária à saúde foram conquistas que até mesmo os inimigos do regime reconhecem.

Os comunistas também desenvolveram muito a economia do país, desenvolveram sua infraestrutura e a libertaram do domínio estrangeiro. Um dos melhores resumos das conquistas do socialismo na Albânia é dado pela Enciclopédia Britânica, em seu [artigo](https://www.britannica.com/biography/Enver-Hoxha) sobre Enver Hoxha:
>A economia da Albânia foi revolucionada sob o longo governo de Hoxha. As terras agrícolas foram confiscadas de ricos proprietários de terras e reunidas em fazendas coletivas que eventualmente permitiram que a Albânia se tornasse quase totalmente autossuficiente em plantações de alimentos. A indústria, que antes era quase inexistente, recebeu enormes quantias de investimento, de modo que na década de 1980 havia crescido e contribuído com mais da metade do produto interno bruto. A eletricidade foi levada a todos os distritos rurais, as epidemias de doenças foram eliminadas e o analfabetismo tornou-se uma coisa do passado.

Apesar de tudo isso, no entanto, as falhas do isolacionismo e do dogmatismo rígido impediram a nação de avançar após 1975 e, por fim, causaram sua queda. Que isso sirva de aviso: o idealismo e o isolamento são fatais para qualquer revolução.

Fontes:
- [Columbia University Press | "A Coming of Age: Albania Under Enver Hoxha" by James O'Donnell](https://espressostalinist.files.wordpress.com/2010/12/a-coming-of-age.pdf)
- [London School of Economics | Mortality Transition in Albania, 1950-1990](http://etheses.lse.ac.uk/2870/1/U615819.pdf)
- [Google Books | "The Albanians: An Ethnographic History from Prehistoric Times to the Present" by Edwin Jacques](https://books.google.com/books/about/The_Albanians.html?id=IJ2s9sQ9bGkC)
- [The Telegraph | Growing Up Under Communist Rule Made Me the Tough Feminist I Am Today](https://www.telegraph.co.uk/women/life/growing-up-under-communist-rule-made-me-the-tough-feminist-i-am/)
- [Encyclopedia Britannica | Enver Hoxha](https://www.britannica.com/biography/Enver-Hoxha)
