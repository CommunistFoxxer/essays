# Reformas sociais e conquistas da Bolívia
O governo Morales também conseguiu implementar reformas sociais muito bem-sucedidas, especialmente nas áreas de educação e questões raciais. A Bolívia conseguiu se tornar apenas a terceira nação da América Latina (depois da Venezuela e Cuba) a ser declarada ["livre do analfabetismo" pela UNESCO](https://www.telesurenglish.net/news/UNESCO-Declares-Bolivia-Free-of-Illiteracy-20140721-0048.html). Isso foi alcançado por meio de programas educacionais inspirados no modelo cubano. Além disso, o governo boliviano fez grandes avanços na luta contra o racismo contra a população indígena da Bolívia (o próprio Morales é o primeiro presidente da Bolívia entre a população indígena). Por isso, eles têm recebido elogios da ONU.

De acordo com um [artigo](https://www.telesurenglish.net/news/UN-Praises-Bolivia-for-Progress-in-Fight-Against-Racism-20161018-0012.html) da Telesur:
>O governo da Bolívia fez "grandes avanços" nos últimos dez anos na luta contra o racismo e a discriminação institucional e estrutural, anunciou terça-feira em nota oficial o Escritório do Alto Comissariado das Nações Unidas para os Direitos Humanos.

O governo também [tem procurado combater a violência e a discriminação contra as mulheres e promover a igualdade de gênero](https://www.telesurenglish.net/news/Bolivias-Evo-Morales-Outlines-Govt-Measures-to-Curb-Violence-Against-Women-Promote-Equality-20190309-0009.html).

O governo Morales conseguiu fazer grandes avanços em várias questões sociais na Bolívia.
