# Questões sociais (raça, gênero, trabalho forçado, etc) dos naxalitas
Os naxalitas fizeram conquistas extremamente positivas e importantes em termos de erradicação da discriminação e combate à opressão social. Vamos consultar o relatório do governo indiano para obter mais informações.

Como mencionado acima, os naxalitas lutaram contra a discriminação contra os adivasis (povos indígenas):
>Sempre que houver uma base para discrição por parte dos funcionários do governo, o pessoal florestal teve que ser apaziguado de diferentes maneiras para evitar o assédio. Foi só depois que os naxalitas entraram em cena que os adivasis conseguiram proteção contra esse assédio, que era bem conhecido da administração, mas normalmente ignorado.

Os naxalitas também fizeram muito para combater a opressão de casta e gênero, que continua sendo um grande problema na Índia. Isso inclui acabar com a exploração sexual de mulheres da classe trabalhadora por homens da casta superior:
>Formas de tratamento indelicado a que os dalits foram submetidos, e a proibição de usar roupas e calçados limpos na presença de castas superiores, ou ao passar por suas localidades, e a compulsão de se dirigir a eles como dora ou malik e outros semelhantes práticas opressivas, em geral, foram encerradas em suas áreas de trabalho. A humilhação diária e a exploração sexual de mulheres trabalhadoras de comunidades dalit por homens de casta superior é outra forma de opressão que tem sido combatida com sucesso.
>
>Em Bihar, houve muitos casos em que dalits sofreram opressão social e, recentemente, vítimas dos massacres perpetrados por castas senas como Ranbir Sena. As vítimas receberam essa ajuda dos naxalitas.

Uma de suas conquistas mais importantes é acabar com o trabalho forçado nas comunidades rurais, que floresceu por décadas:
>Não existe nenhuma lei que penalize o trabalho forçado em outras formas. Portanto, floresceu nas formas mais medievais no distrito de Telangana em Karimnagar, e foi necessário um grande aumento liderado pelos naxalitas no final dos anos setenta e início dos anos oitenta do século passado para pôr fim a ele.

Crucialmente, os naxalitas também conseguiram impedir o despejo forçado e a apreensão de recursos naturais pelo governo indiano, bem como por empresas privadas:
>Os habitantes da floresta (estavam) perpetuamente à beira da expulsão de seu próprio habitat. O medo da resistência armada naxalita dissuadiu os movimentos repressivos e depredadores das autoridades.

Talvez a mais inspiradora de todas essas conquistas seja o senso de orgulho, humanidade e poder que os naxalitas incutiram nas pessoas. Isso é reconhecido até pelo governo indiano:
>Além das questões concretas empreendidas pelos naxalitas contra a opressão social, o fato de que o quadro e também a maioria dos líderes locais das organizações naxalitas consistem em aldeões pobres de castas consideradas humildes, dotou os oprimidos de muita força. Uma sensação de impotência é uma característica da constituição psicológica das classes oprimidas. O quadro Naxalita típico, entretanto, é um adolescente confiante (provavelmente empunhando uma arma) dessas mesmas classes. Ver meninos e meninas de suas próprias aldeias e sua própria classe/casta ativa no movimento Naxalita, e exercendo poder sobre os 'grandes' homens da aldeia e o alto e poderoso tahsildar deu um sentimento de empoderamento aos oprimidos que tem valor inestimável.
