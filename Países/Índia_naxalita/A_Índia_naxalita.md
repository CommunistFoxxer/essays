# A Índia naxalita
De todos os vários grupos socialistas e comunistas que atualmente travam a luta armada contra o capitalismo e o imperialismo, poucos são mais significativos e conhecidos do que o movimento maoísta da Índia, comumente conhecido como Naxalitas. Frequentemente demonizados e retratados como terroristas, eles continuam a lutar (com notável sucesso, como será mostrado em breve) contra a exploração implacável e o assassinato das pessoas mais pobres da Terra.

Antes de mergulhar na análise difícil, eu recomendo que todos vocês leiam Walking With the Comrades, de Arundhati Roy, uma análise brilhante da situação na Índia, bem como um relato de primeira mão da época em que Roy visitou os maoístas. Ela é uma das autoras mais conhecidas do mundo hoje, e seu trabalho é geralmente aceito e aclamado até mesmo por criadores de tendências liberais; ela até conseguiu ganhar alguma atenção positiva para os naxalitas em publicações convencionais, como a Paris Review:

- [The Paris Review | Arundhati Roy em "Walking With the Comrades"](https://www.theparisreview.org/blog/2011/11/01/arundhati-roy-on-walking-with-the-comrades/)

Se você quiser entender a horrível exploração e o terrorismo de estado que tornaram o movimento Naxalita necessário em primeiro lugar, leia esse livro. Com isso esclarecido, vamos mergulhar nas conquistas do movimento Naxalita.
