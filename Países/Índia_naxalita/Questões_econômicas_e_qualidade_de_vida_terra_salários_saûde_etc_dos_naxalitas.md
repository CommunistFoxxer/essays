# Questões econômicas e qualidade de vida (terra, salários, saúde, etc) dos naxalitas
Uma de nossas fontes mais importantes sobre este tópico vem, ironicamente, do próprio governo indiano. Seu relatório, intitulado "Desafios de Desenvolvimento em Áreas Afetadas Pelo Extremismo", nos dá uma grande quantidade de informações sobre o sucesso dos Naxalitas:

- [Indian Planning Commission | Development Challenges in Extremist Affected Areas](https://tribal.nic.in/writereaddata/AnnualReport/DevelopmentChallengesinExtremistAffectedAreas.pdf)

O governo reconhece o imenso problema da distribuição desigual de terras, ao mesmo tempo que admite que os naxalitas fizeram muito para redistribuir terras aos sem-terra:
>Embora não haja estimativas precisas disponíveis, é um fato que em alguns casos o movimento naxalita conseguiu ajudar os sem-terra a ocupar uma extensão substancial das terras do governo, seja para propriedades rurais ou para cultivo. Em Bihar, todos os partidos naxalitas tentaram ajudar, em suas respectivas áreas de influência, os Musahars sem-terra, os mais baixos entre os dalits, a tomar posse de uma extensão considerável dessas terras.

Eles lutaram para acabar com a exploração implacável do trabalho adivasi (indígena):
>Os naxalitas garantiram aumentos na taxa de pagamento pela colheita da folha de tendu, que é usada para rolinhos de plantas, nas áreas florestais de Andhra Pradesh, Chhattisgarh, Orissa, Maharashtra e Jharkhand. Essa era uma fonte muito importante de exploração da mão de obra adivasi e, embora o governo a ignorasse conscientemente, os naxalitas acabaram com ela. A exploração foi tão severa que as taxas aumentaram ao longo dos anos até cinquenta vezes o que os empreiteiros tendu patta costumavam pagar antes dos naxalitas intervirem.

Os naxalitas também têm lutado para aumentar os salários em geral, com sucesso significativo:
>A Lei do Salário Mínimo continua sendo uma lei no papel em grande parte da Índia rural. Nas áreas de sua atividade, é relatado que os naxalitas garantiram o pagamento de salários decentes, embora eles geralmente não tenham seguido os salários mínimos legais. As taxas que eles garantiram são às vezes mais altas e às vezes mais baixas do que a taxa legal. Sua orientação para os direitos em geral não é regida por direitos legais, mas pelo que eles consideram justo e justo, levando em consideração todos os fatores que consideram relevantes.
>
>Existem também grandes áreas de trabalho não regidas pela Lei do Salário Mínimo. Uma vez que os Naxalitas, em qualquer caso, não se importam se existe ou não uma lei que rege o direito que defendem, eles intervieram e determinaram salários justos em suas percepções em todos os processos de trabalho em suas áreas de influência. Isso inclui salários para lavar roupas, fazer potes, cuidar do gado, consertar implementos, etc.

Os naxalitas também lutaram para garantir a proteção da propriedade comum e para evitar sua exploração por proprietários privados:
>O usufruto dos recursos de propriedade comum como um direito tradicional por pastores, comunidades pesqueiras, chapéus de coco, pedreiros, tornou-se vulnerável devido à apropriação desses recursos pelos setores dominantes da sociedade ou pelos demais com seu apoio. Os naxalitas tentaram garantir a proteção desse direito onde quer que estejam ativos.

A pressão dos naxalitas forçou os funcionários do governo nas áreas de saúde e educação a cumprirem adequadamente seus deveres para com o povo:
>Em algumas ocasiões, os naxalitas conseguiram pressionar os administradores de nível inferior para que realizassem seu trabalho com eficácia. A pressão exercida pelo movimento Naxalita teve algum efeito em garantir o atendimento adequado de professores, médicos etc.

Na verdade, os próprios naxalitas conseguiram melhorar os cuidados de saúde nas áreas onde atuam. Em seu livro Hello, Bastar: The Untold Story of India's Maoist Movement, Rahul Pandita (vencedor do Prêmio Internacional da Cruz Vermelha) escreve:
>Também no campo da saúde, os maoístas costumam preencher grandes lacunas deixadas pelo estado. Suas unidades médicas móveis cobrem grandes distâncias para oferecer cuidados básicos de saúde aos tribais... Vários campos de treinamento são realizados regularmente em medidas preventivas contra doenças como diarreia ou malária. Os médicos de base nas equipes médicas podem administrar vacinas, identificar várias doenças por meio de sintomas e tratar ferimentos que não são graves. Alguns podem até realizar exames de sangue simples para chegar a um diagnóstico. Esta é uma vantagem significativa nessas áreas.

É evidente que os naxalitas fizeram grandes avanços na melhoria da qualidade de vida das pessoas nas áreas em que operam.
