# Conclusão sobre os naxalitas
Apesar da enorme campanha de propaganda dirigida contra eles, bem como da resposta totalmente perversa e violenta do governo indiano (documentada na obra de Arundhati Roy, já mencionada, Walking With The Comrades), o movimento naxalita conseguiu fazer grandes avanços pelo povo. Talvez isso seja melhor resumido pelo próprio governo indiano, em um comentário do relatório:
>O movimento oferece proteção aos fracos contra os poderosos e leva a sério a segurança e a justiça para os fracos e os socialmente marginais.

Em uma das nações mais pobres e exploradas da Terra, isso em si é uma grande conquista e algo a ser valorizado.
