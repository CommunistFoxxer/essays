# Conclusão sobre a Nicarágua
Embora os sandinistas tenham inegavelmente cometido grandes erros nos últimos anos (como sua crescente afinidade com as políticas neoliberais, austeridade, etc), suas conquistas não podem ser negadas. Isso é especialmente verdadeiro quando consideramos as terríveis condições por que passaram durante a guerra e a hostilidade do governo dos Estados Unidos desde então.

Fontes:
- [Oxfam America | Nicaragua: The Threat of a Good Example?](https://oxfamilibrary.openrepository.com/handle/10546/121188?show=full)
- [University of Wisconsin-Madison | Agrarian Reform in Nicaragua](https://minds.wisconsin.edu/bitstream/handle/1793/56510/ltc122.pdf?sequence=1)
- [Food and Agricultural Organization (UN) | Nicaragua Profile](http://www.fao.org/faostat/en/#country/157)
- [World Bank | Nicaragua](https://data.worldbank.org/country/nicaragua)
- [Critical Sociology | A Report on the Health of the Nicaraguan Revolution](https://journals.sagepub.com/doi/abs/10.1177/089692058901600105)
- [American Journal of Public Health | Health Services Reforms in Revolutionary Nicaragua](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1651882/pdf/amjph00633-0082.pdf)
- [UNESCO | Nicaragua's Literacy Campaign](https://unesdoc.unesco.org/ark:/48223/pf0000146007)
- [University of Munich | An Investigation Into the Reported Closing of the Nicaraguan Gender Gap](https://ideas.repec.org/p/pra/mprapa/86769.html)
