# A Nicarágua
>O que vemos é um governo diante de enormes problemas, alguns aparentemente insuperáveis, empenhado em uma grande experiência que, embora precária e incompleta em muitos pontos, dá esperança aos setores pobres da sociedade, melhora as condições de educação, alfabetização e saúde, e pela primeira vez oferece ao povo da Nicarágua um mínimo de justiça para todos, em vez de uma sociedade que oferece privilégios exclusivamente aos ricos... e aos poderosos.<br/>
>– <cite>Conselho Mundial de Igrejas, Relatório sobre a Nicarágua, 1983.</cite>
