# Política social e igualdade de gênero na Nicarágua
De acordo com o Fórum Econômico Mundial, a igualdade de gênero na Nicarágua melhorou muito sob os sandinistas. Isso é observado em um [estudo](https://ideas.repec.org/p/pra/mprapa/86769.html) da Universidade de Munique:
>Nos últimos cinco anos, o Índice Global de Diferenças de Gênero do Fórum Econômico Mundial tem relatado que a Nicarágua é um dos países com maior igualdade de gênero no mundo. Este é o ápice de um notável aumento na igualdade de gênero na Nicarágua durante a última década, mapeado pelo mesmo índice.

As mulheres também se beneficiaram muito de vários programas sandinistas, como a já mencionada campanha de alfabetização. De acordo com o [relatório da UNESCO](https://unesdoc.unesco.org/ark:/48223/pf0000146007):
>Para muitas mulheres, a campanha de alfabetização representou uma oportunidade de emancipação: 60% dos brigadistas eram mulheres, assim como cerca de 50% dos alfabetizadores. A separação do corpo docente (Exército Popular de Alfabetização) segundo homens e mulheres aumentou as oportunidades de liderança para as brigadistas.

São avanços impressionantes, que deixam clara a dedicação que as revolucionárias sandinistas tiveram para melhorar a condição feminina, assim como seu envolvimento na atividade política e de luta.
