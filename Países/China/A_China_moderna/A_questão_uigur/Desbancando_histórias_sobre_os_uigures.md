# Desbancando histórias sobre os uigures
Em primeiro lugar, é importante compreender o fato de que os depoimentos são evidências anedóticas, fortemente sujeitas a vieses de percepção. A sociedade hoje sofre de falsa vitimização porque as pessoas tendem a acreditar na suposta vítima e desconsiderar qualquer coisa vinda do réu. No caso de falsidade de depoimentos, o réu é quem mais sofre.

**Uigures desaparecidos**

É importante ressaltar que os jornais anti-China procuram qualquer brecha para acusar a China, como [neste caso](https://www.globaltimes.cn/content/1174468.shtml) em que um uigur supostamente desapareceu, mas foi encontrado posteriormente vivendo normalmente em sua casa.

Também houve outro caso de um uigur que foi dado como morto mas tempos depois apareceu "ressuscitado".
![Alive uyghur](https://pbs.twimg.com/media/E3Zkv_xWEAIG77A?format=jpg&name=900x900)

**A China não te deixa entrar nos campos de reeducação**

A China não deixam entrarem nesses “campos de concentração”? Falso. Muitos conspiradores da internet fizeram o mesmo ponto, isto é, se realmente não há nada de errado com o que eles estão fazendo nesses campos, então por que eles não permitiriam a entrada nesses campos? Eles permitem, permitiram muitas vezes.

1. Janeiro de 2019: [a China convidou observadores da ONU](https://www.dawn.com/news/1456065).
2. Junho de 2019: [a China convidou o alto comissário da ONU](https://www.telesurenglish.net/news/China-Invites-UN-to-Visit-Education-Centers-in-Xinjiang-20190613-0004.html).

Os próprios americanos em 2005 [investigaram](https://web.archive.org/web/20090620050738/http://www.america.gov/st/washfile-english/2006/None/20060416141157uhyggep0.5443231.html) os Falun Gong (grupo que assim como os uigures, tem denunciantes sobre “abusos de direitos humanos” na China) e os uigures e não encontraram nenhuma irregularidade. A China também [já recebeu mais de mil fiscalizadores e diplomatas em Xinjiang](https://www.globaltimes.cn/content/1180917.shtml), e diz estar completamente aberta para receber a ONU na região para analisar o ocorrido e as acusações. [2](https://www.chinadaily.com.cn/a/201909/04/WS5d6f2e32a310cf3e35569a8d.html).

Muitos meios de comunicação como a BBC e a Vice também fizeram documentários sobre o assunto, com a BBC tendo a oportunidade de realmente entrar nas escolas de treinamento vocacional. O resultado? Não importa o quanto a BBC tente retratar as escolas como o inferno na terra, incluindo descaradamente [palavras mal-traduzidas do entrevistado](https://medium.com/@sunfeiyang/breaking-down-the-bbcs-visit-to-hotan-xinjiang-e284934a7aab), usando imagens de satélite fora do contexto e inventando mentiras sobre um "graffiti que eles por acaso encontraram em uma parede", o documentário não mostra nada das acusadas de “violações dos direitos humanos”, o que, é claro, perturba os defensores da conspiração. Apesar dos esforços da China para limpar seu próprio nome, os conspiradores do Ocidente ainda se recusam a aceitar a realidade e afirmam que essas "filmagens internas" são planejadas e falsificadas para enganar os telespectadores. Basta dar uma olhada na seção de comentários do [documentário da BBC](https://www.youtube.com/watch?v=WmId2ZP3h0c). É difícil acreditar que existe de alguma forma uma coexistência entre pessoas que pensam que a China tem algo a esconder para não permitir o acesso interno e pessoas que pensam que a China falsificou o acesso interno. A verdade sempre esteve lá, mas pode não ser a verdade que algumas pessoas querem ver.

Também é mentira que a [ONU reportou a China como tendo campos de concentrações](https://thegrayzone.com/2018/08/23/un-did-not-report-china-internment-camps-uighur-muslims/) para uigures. Eles nunca conseguiram achar nenhuma prova.

O [Relatório Final](https://www.govinfo.gov/content/pkg/CRECB-2005-pt18/html/CRECB-2005-pt18-Pg24156.htm) da Comissão do 11 de setembro:
>Claro, como todos sabemos, um dos maiores desastres de politização da inteligência que ocorreu antes mesmo da guerra do Iraque foi sobre a politização da inteligência na União Soviética... Levivier também entrevistou um analista da CIA sobre o papel dos Mujahedin. Este agente da CIA disse: “A política de guiar a evolução do Islã e de ajudá-los contra nossos adversários funcionou maravilhosamente bem no Afeganistão contra o Exército Vermelho. As mesmas doutrinas ainda podem ser usadas para desestabilizar o que resta do poder russo e, especialmente, para conter a influência chinesa na Ásia Central."

Uma [resolução](https://undocs.org/pdf?symbol=en/A/HRC/41/G/17) das Nações Unidas para as nações do sul global:
>Expressamos nossa firme oposição à prática de países relevantes de politizar questões de direitos humanos, denunciando e envergonhando, e exercendo publicamente pressões sobre outros países. Elogiamos as notáveis realizações da China no campo dos direitos humanos ao aderir à filosofia de desenvolvimento centrado nas pessoas e proteger e promover os direitos humanos por meio do desenvolvimento. Também apreciamos as contribuições da China para a causa internacional dos direitos humanos.

[Investigação](https://www.worldbank.org/en/news/statement/2019/11/11/world-bank-statement-on-review-of-project-in-xinjiang-china) do Banco Mundial em Xinjiang:
>Quando as alegações são feitas, o Banco Mundial as leva a sério e as analisa minuciosamente. Em linha com a prática padrão, imediatamente após receber uma série de alegações graves em agosto de 2019 em conexão com o Projeto de Educação e Treinamento Técnico e Profissional de Xinjiang, o Banco Mundial lançou uma análise de apuração de fatos e os gerentes seniores do Banco Mundial viajaram para Xinjiang para coletar informações diretamente. A equipe realizou uma revisão completa dos documentos do projeto, envolveu-se em discussões com a equipe do projeto e visitou escolas diretamente financiadas pelo projeto, bem como suas escolas parceiras que foram objeto de denúncias. A revisão não fundamentou as alegações.

A Organização de Cooperação Islâmica (organização islâmica internacional na qual mantém [estreita colaboração](http://www.inp.net.pk/china-lauds-oics-resolution-on-xinjiang/) com a China) elogia o manejo chinês de Xinjiang:
>Congratula-se com os resultados da visita realizada pela delegação do Secretariado-Geral a convite da República Popular da China; louva os esforços da República Popular da China na prestação de cuidados aos seus cidadãos muçulmanos; e espera uma maior cooperação entre a OIC e a República Popular da China.

Delegados da mídia egípcia [visitaram](https://www.thenews.com.pk/latest/430738-egyptian-media-delegates-provide-a-detailed-insight-of-the-situation-in-xinjiang) Xinjiang:
>O relatório recentemente publicado também traz alguns fatos interessantes relacionados à liberdade religiosa em oposição à propaganda ocidental. O relatório fornece um forte testemunho dos delegados visitantes que afirmam claramente, “nas casas de culto, como a Mesquita Id Kah em Kashgar, abundam as instalações modernas, fornecendo água, eletricidade e ar condicionado. Clérigos locais disseram aos visitantes que suas atividades religiosas estavam muito bem protegidas”. "As condições aqui são muito boas", disse Abdelhalim Elwerdany, do jornal egípcio Al-Gomhuria. "Pude sentir que os muçulmanos locais desfrutam plenamente da liberdade religiosa."

**Pompeo e seu relatório de extermínio uigur**

Quase 100 estudiosos e líderes religiosos em Xinjiang [refutaram Pompeo](https://www.globaltimes.cn/content/1158473.shtml) com uma carta conjunta:
>Você disse que Xinjiang está massacrando sistematicamente a cultura uigur, mas onde está sua evidência? A constituição da República Popular da China estipula que o estado protege os direitos e interesses legais de cada grupo étnico e ajuda as regiões de minorias étnicas a alcançar um ritmo mais rápido de desenvolvimento econômico e cultural. Xinjiang tem se empenhado muito na proteção, herança e promoção da cultura de cada minoria étnica. Cursos em línguas de minorias étnicas são oferecidos por todas as escolas sob o sistema educacional obrigatório... Suas alegações de que Xinjiang está encerrando as crenças islâmicas e que o governo chinês persegue severamente os crentes de várias religiões não são baseadas em fatos. É uma política básica de longa data do governo chinês respeitar e proteger a liberdade de crença religiosa. Xinjiang nunca associou a repressão ao terrorismo e extremismo a nenhum grupo étnico ou religião específica. O governo local de Xinjiang protege as atividades religiosas normais e cumpre as exigências religiosas razoáveis ​​dos crentes de acordo com a lei. Em Xinjiang, existem 24.400 mesquitas e 29.000 clérigos religiosos. Existem 10 faculdades religiosas, incluindo o Instituto Islâmico de Xinjiang, matriculando mais de 1.300 alunos anualmente. Em Xinjiang, para cada 530 muçulmanos há uma mesquita, um número que ultrapassa muitos países muçulmanos.

**Tursunay Ziyawundun e a castração química**

Tursunay Ziyawundun é a pessoa que iniciou a [alegação de esterilização forçada/castração química](https://apnews.com/269b3de1af34e17c1941a514f78d764c), que é muito comum na internet hoje. O problema? Ela afirmou em sua [entrevista com a Buzzfeed](https://www.buzzfeednews.com/article/meghara/china-uighur-xinjiang-kazakhstan) que "não foi espancada ou abusada". Por que ela mudou seu testemunho? Uma explicação simples seria que, se ela alegasse ter sido abusada fisicamente, ela teria sido solicitada a mostrar evidências corporais, algo que desmascararia imediatamente sua alegação. Agora, 7 meses depois, ela pode reivindicar o que quiser e pode simplesmente ignorar qualquer pedido de apresentação de evidências corporais. A Global Times explica com mais detalhes em um [artigo](https://www.globaltimes.cn/content/1193454.shtml) sobre as falhas no método de Adrian Zenz.

**Sayragul Sauytbay e o conto de fadas**

Sayragul Sauytbay está em um nível totalmente novo de criatividade e desonestidade. Suas [alegações](https://www.dailymail.co.uk/news/article-7599941/Whistleblower-escaped-Chinese-education-camp-reveals-horrors.html) foram inspiradas em ficções de fantasia sombrias, incluindo:
>"Ver uma velha tendo sua pele esfolada."<br/>
>“Morreram de fome durante uma semana inteira, mas foram alimentados à força com carne de porco de churrasco às sextas-feiras.“<br/>
>“Estuprada pessoalmente na frente de 200 presidiários.”

Tudo isso parece horrível até que você olhe para o [depoimento original](https://www.theglobeandmail.com/world/article-everyone-was-silent-endlessly-mute-former-chinese-re-education/) dela, sendo que "Eu não vi nenhuma violência".

**Merdan Ghappar e a gravação do campo de concentração**

Ghappar, um modelo uigur, teria supostamente roubado um celular e mantido esse celular dentro de um campo de concentração de uigures. Ele manteve esse celular escondido por mais de duas semanas sem carregar, entrando em contato com terceiros. Tudo isso enquanto estava supostamente preso em um campo de concentração e algemado na cama.

[A BBC analisou as mensagens enviadas](https://www.bbc.com/news/world-asia-china-53650246) por Ghappar no aplicativo WeChat para publicar sua história, mas os chineses notaram que as capturas de tela diferiam dos originais quando a história apareceu em março, já que partes do texto haviam sido removidas, como ofensas.

**Campos de concentração vistos via satélite**

Você provavelmente já viu a imagem de satélite dos supostos campos que abrigam esses 2 milhões de detidos. Pegue uma das áreas mais movimentadas do mundo, Nova York Manhattan. Este lugar acomoda regularmente 1,6 milhão de pessoas e é, sem dúvida, um lugar bem lotado. Como não há arranha-céus nessas imagens de satélite, a área de ocupação dos 2 milhões de acampamentos uigures deve ocupar outra metade de um Manhattan extra. Onde estão as imagens de satélite dos campos do tamanho de uma cidade ao redor de Xinjiang? Se isso não dá uma ideia de quão ridículo é o número 2 milhões, a população total de presidiários dos EUA é de 2,3 milhões. Existem 1.719 prisões estaduais, 102 penitenciárias federais, 942 penitenciárias juvenis, 3.283 penitenciárias locais e 79 penitenciárias indianas, além de prisões militares, centros de detenção de imigração, centros de detenção civil e prisões. As prisões dos Estados Unidos também são conhecidas por operar constantemente em sua capacidade máxima. Deveria haver mais de 6 mil instalações de tamanhos diferentes na província de Xinjiang, não apenas algumas dezenas de pequenas estruturas do tamanho de escolas primárias.

A própria BBC também recrutou especialistas em arquiteturas de presídios que usando uma lista de imagens aéreas da região, [identificaram 44 prédios](https://www.bbc.com/portuguese/internacional-45999030) que poderiam ser centros de detenção. Contudo, para ter 1 milhão de presos nesses 44 prédios, daria mais ou menos 23 mil pessoas por prédio. Analisando um prédio só, eles disseram que haveria cerca de 11 mil pessoas e que seria um dos maiores que eles viram nas imagens.

Há [vídeos](https://www.youtube.com/watch?v=v_XI-aiCa34&ab_channel=ChinaDaily) no YouTube sobre isso, [threads](https://twitter.com/_tchiek/status/1299386623390617601?s=20) no Twitter e até mesmo [sites](https://www.globaltimes.cn/content/1208288.shtml) destinados a oferecer imagens reais e atualizadas de Xinjiang (a maioria das imagens que se vê pela internet são de 2014 ou até mesmo de antes de 2009). Também há filmagens sobre a região:
- [244集 从美食美女到西域风情，走进新疆喀什的真实生活 | 冒險雷探長Lei's adventure](https://www.youtube.com/watch?v=0vvk7z8rvGY&ab_channel=冒险雷探长)
- [246集 最被误解的中国城市？走进新疆之窗乌鲁木齐国际大巴扎 | 冒險雷探長Lei's adventure](https://www.youtube.com/watch?v=Xz1HuV-wNIk&ab_channel=冒险雷探长)
- [256集 新疆八卦城探秘柯尔克孜族的古老传说｜冒险雷探长Lei's adventure](https://www.youtube.com/watch?v=FTIwST9kIuk&ab_channel=冒险雷探长)

![](images/debunking_concentration_camps_photos.jpg)
<em>A mídia ocidental pega fotos desatualizadas da construção de escolas, parques e casas de repousos, e alegam ser "campos de extermínio" para uigures.</em>

**Adrian Zenz e o número de uigures presos**

A **Newsweek** japonesa disse que os números vieram de uma organização uigur sediada na Turquia, que afirma ter recebido esses números de uma fonte pública chinesa. Entretanto, Adrian não possuía os dados de toda a província de Xinjiang, ele se baseou nos dados de alimentação de 27 condados dos 64 que existem na província e, nesses dados, chegou à conclusão de que 600.000 pessoas estariam presas, dentre essas pessoas, seriam cazaques, uigures, ou uma das outras 53 etnias que existem na China - de uma população total de 4 milhões e meio de pessoas. Após a ‘descoberta’ desses números, ele os arredondou para 10% da população total e aplicou para o resto de Xinjiang.

O problema é que em Xinjiang, os uigures são apenas 45,84% da população (censo de 2020), então é impossível saber quais são os dados de prisões comuns, o que são dados de supostos campos de concentração e qual a porcentagem de cada etnia que realmente está presa. Em um [evento em Genebra](https://www.youtube.com/watch?v=tWey4fZWZAI), Zenz disse que havia 1 milhão e meio de presos. Já na entrevista para a **Radio Free Asia**, ele falou em **1.000.800 presos**. Coincidentemente, o estudo foi publicado pouco antes da câmara dos Estados Unidos passar os **Atos de direito de Xinjiang**, que autoriza o governo norte-americano a impor sanções à região.

Já o [Instituto Australiano de Política Estratégica](https://www.aspi.org.au/) diz que [analisou 28 campos](https://www.aspi.org.au/report/mapping-xinjiangs-re-education-camps) e que eles poderiam conter cerca de 500 mil pessoas, mas eles nunca publicaram os estudos, nem os dados e muito menos a matemática usada.

**Adrian Zenz e os abortos forçados**

Pelo que eu posso rastrear, existem atualmente 2 fontes para números na casa dos milhões, o Congresso Mundial Uyghur e o “estudo” de Adrian Zenz. O número do Congresso Mundial Uyghur é fácil de desmascarar (não dizendo que o de Zenz é difícil). Há um [vídeo](https://www.youtube.com/watch?v=A8uZZjB4kfM) do presidente do WUC, Omer Kanat, respondeu à alegação de 1 milhão de uigures.

O estudo de Adrian Zenz é um pouco mais complicado de desmascarar porque há um "estudo" real para se olhar. Depois de olhar para o estudo real e fazer algumas pesquisas sobre Adrian Zenz, pode-se descobrir rapidamente que ele não tem um histórico na condução de trabalhos aprovados academicamente. Ele não recebeu educação em assuntos relacionados à estatística e tem um longo histórico de fazer algumas afirmações muito “questionáveis”. Ele alegou que foi "enviado por Deus para punir Pequim" e conecta ativamente a homossexualidade, a igualdade de gênero e a proibição de punições corporais ao poder do "Anticristo". Desde então, sua página da Wikipedia removeu qualquer conteúdo sobre suas crenças “não convencionais”, mas os arquivos ainda podem ser encontrados na internet.

Adrian Zenz divulgou um relatório sobre a esterilização de uigures. Este artigo de Adrian foi somente publicado em uma revista ligada à **OTAN** e mais em lugar nenhum. Com base nos dados que ele obteve do governo chinês. A China má está cometendo o genocídio uigur, ao contrário da luz brilhante da retidão e pureza moral dos Estados Unidos (que nunca, jamais, jamais faria qualquer coisa para prejudicar qualquer muçulmano).

Em seu [relatório](https://web.archive.org/web/20200629192118/https://jamestown.org/wp-content/uploads/2020/06/Zenz-Internment-Sterilizations-and-IUDs.pdf?x60014), Zenz afirma que 80% dos DIUs na China foram feitos em Xinjiang:
>Em 2018, 80% de todas as novas colocações de DIU [Dispositivo intrauterino] na China foram realizadas em Xinjiang, apesar do fato de que a região representa apenas 1,8% da população do país.
>
>Em 2014, 2,5% dos DIUs recém-colocados na China foram colocados em Xinjiang. [38] Em 2018, essa parcela subiu para 80%, muito acima da parcela de 1,8% de Xinjiang na população da China. [39]

Sempre o pesquisador diligente, Zenz fornece a seguinte fonte primária chinesa:
>[38] Fonte: Anuários estatísticos de saúde e higiene de 2015 e 2019, tabela 8-8-2.

Este é o [anuário estatístico chinês de saúde e higiene de 2019](https://web.archive.org/web/20200712091001/https://s2.51cto.com/oss/201912/05/1822362d5f7ccc8ff5d87ecdba23e64c.pdf). É um documento e tanto, com várias centenas de páginas.

Se você seguir pela árdua tarefa de rolar para a página 228, encontrará a tabela 8-8-2 de Zenz:

![](images/zenzs_table.png)

A coluna relevante é 放置节育器例数, o número de DIUs implantados. Temos um total (总计) de 3,8 milhões, com Xinjiang (新疆) respondendo por 328.475. Assim, 8,7% dos DIUs da China ocorreram em Xinjiang. (Uma observação, mas o que realmente se destaca nesta tabela não é Xinjiang, mas Henan. Em toda a China, 86% das vasectomias e 26% dos litígios tubários aconteceram em Henan. Ao contrário do DIU, esses são procedimentos de esterilização reais que não podem ser revertidos)

Assim, parece que os assistentes chineses ajudando Zenz adicionaram uma casa decimal por engano. Ou Zenz está mentindo. Mas tenho certeza de que um camarada sênior "Vítimas do Comunismo", "liderado por Deus" para destruir a China, não faria nada furtivo assim. Afinal, não é como se ele [tivesse convencido as testemunhas uigures a mudar suas histórias](https://twitter.com/DanielDumbrill/status/1277704695222603776) ou algo assim.

P.S: Alguns observadores mais astutos podem notar que baseei minha pesquisa em uma investigação do Global Times. Mas todos nós sabemos que todo mundo acredita que o Global Times é propaganda chinesa. Portanto, decidi confirmar a verdade mostrando a você a própria fonte primária.

Em relação ao estudo real, não há nada de substancial. A maioria dos parágrafos é dedicada a fornecer informações sobre as informações geológicas e geográficas de Xinjiang. A conclusão de “milhões de detidos” é feita entrevistando apenas 8 supostos ex-detentos. Não há mais informações úteis no estudo de Zenz sobre este tópico. É justo concluir, com uma relação com suas visões cristãs fundamentalistas e histórico de crenças questionáveis ​​pelos padrões de hoje, que o estudo de Adrian Zenz é, em última análise, [impróprio para ser citado por qualquer artigo](https://thegrayzone.com/2019/12/21/china-detaining-millions-uyghurs-problems-claims-us-ngo-researcher/).

**Drone filmando uigures sendo capturados**

Há um [vídeo](https://www.youtube.com/watch?v=xU_w5UzdZEs) sobre imagens do drone de homens com os olhos vendados, algemados e barbeados que supostamente seriam uigures. A filmagem específica do drone geralmente vem com alegações sobre a natureza do conteúdo. A maioria diria que se trata de uma transferência de detidos uigur para os campos de concentração. No entanto, uma mera inspeção de perto das costas dos coletes desses supostos detidos mostra que essas pessoas pertencem à prisão provisória de 喀什市看守所, Kashgar. Isso significa que esses são presidiários, prisioneiros, criminosos literais que agora estão sendo retratados como vítimas inocentes para se adequar à agenda política do comentarista. Usar essa filmagem para argumentar que o campo de concentração existe é mal intencionado e tem como objetivo explorar os telespectadores que não sabem ler chinês simplificado.

Note que na [matéria](https://www.cnn.com/2019/10/06/asia/china-xinjiang-video-intl-hnk/index.html) da CNN, eles afirmam:
>A CNN não pode verificar de forma independente a autenticidade deste vídeo ou a data em que foi filmado. Autoridades do Ministério das Relações Exteriores da China não responderam aos repetidos pedidos de comentários sobre o vídeo. Em um comunicado à CNN em 4 de outubro, as autoridades de Xinjiang disseram que "reprimir crimes de acordo com a lei é a prática comum de todos os países. A repressão de crimes de Xinjiang nunca foi ligada a etnias ou religiões", acrescentou o comunicado. "Transporte de presos por autoridades judiciais (está relacionado) para atividades judiciais normais."

**Uigures presos em Douban**

Há outro [vídeo](https://www.douban.com/group/topic/106094686/) (eu tinha o vídeo, mas só consegui achar a foto) de prisioneiros uigures vendados e amarrados. Este vídeo retém algumas informações, já que os homens no vídeo são inexpressivos. Eles nem mesmo eram uigures. Então, o que é este vídeo? Este é um vídeo capturado em agosto de 2017 em Douban. Os homens vendados são missionários do esquema de pirâmide (MLM), criminosos literais que enganam as pessoas com seu dinheiro suado e literalmente destroem vidas e famílias. Eles são vitimados para se adequar à agenda de quem usa isso como "evidência" de campos de concentração uigures.

**PodNext e a prisão de Luopu**

Um podcast brasileiro no Twitter - chamado PodNext - fez [propaganda anti-china](https://twitter.com/Opodnext/status/1302610017665810432). Eles postaram imagens de uma prisão comum em Luopu, e um [evento religioso](https://web.archive.org/web/20180821032854/https://baijiahao.baidu.com/s?id=1564669932542581) realizado com a presença das famílias inclusive, e este evento era contra o extremismo religioso da região. Ele também publicou sobre uma mulher que supostamente estaria presa em um campo e estava arrumando um jeito de ver a filha atrás das grades, mas na verdade ela era uma funcionária da saúde que estava lutando contra a COVID. Também há uma imagem de várias pessoas algemadas, vendadas e sentadas no chão. Foi anunciada como uigures sendo transportados, mas na verdade é de uma prisão comum em Kashgar e não tem como saber se são hans, cazaques, uigures ou uma das 53 etnias minoritárias chinesas, e metade das pessoas nessas imagens são policiais. Xinjiang tem mais que o dobro de mesquitas da Turquia, e há 20 anos, haviam apenas 4.000 (crescimento de 1000%).

**Cabelo falso feito com cabelo uigur**

O prêmio de acusação mais ridícula vai para a alegação de que o cabelo falso é trançado com "Cabelo de detido Uigur". Alguns meios de comunicação cobriram o tópico de que os EUA apreenderam recentemente $800.000 em produtos sintéticos para cabelo chineses, e com alguma estranha conexão com o fato de que as cabeças dos prisioneiros chineses são raspadas (assim como os prisioneiros de todos os outros países, assim a alegação nasceu). No entanto, o desmascaramento da alegação está nos artigos que as reportam. Por que eles usam palavras como “suspeito”, “alegado” e “supostamente” quando é bastante simples dizer a diferença entre cabelo real e cabelo sintético? Eu perguntei a muitas pessoas que têm experiência com extensões de cabelo falsas se o cabelo sintético tem uma sensação ou toque diferente do cabelo real. Minha irmã também possui um, que ela me deixou tocá-lo. O resultado é quase certo que extensões de cabelo e fios de cabelo sintéticos são muito diferentes do cabelo real. Se as pessoas que apreenderam a carga de cabelo não são idiotas, por que os relatórios ainda usam palavras como "suspeito", quando alegremente chegariam à conclusão de "sim, com certeza têm cabelo de verdade"? Em segundo lugar, a extensão de cabelo e o cabelo falso trançado com cabelo real existem, mas são comparativamente muito caros. Eu pude ver os palhaços chegando à conclusão de que "oh, isso deve significar que as extensões de cabelo são feitas de cabelo de detidos para ganhar muito dinheiro." Você pode vender seu cabelo comprido sem tingimento por um bom preço, isso é um fato conhecido se você tiver experiência com a indústria da beleza ou de cosplay, e na maioria das barbearias chinesas, você pode vender seu cabelo na hora, se quiser. Assim, mesmo que o cabelo apreendido seja trançado de cabelo real, isso não é prova de que seja feito de cabelo de prisioneiros detidos que ninguém tem a prova de que eles existam.

**Uigures banidos por exporem a verdade sobre Xinjiang**

![](images/Azeem_Khan_Uyghur.png)

O Twitter suspendeu a conta de um uigur que descrevia a experiência do povo na China. 2 amigas uigures divulgavam a situação dos uigures na China no Twitter. As duas foram banidas. O Google também baniu o [documento](https://docs.qq.com/doc/DRHJYRVNpeFB6VEVs) de um amigo meu explicando a situação dos uigures, e todo mundo que repostava o documento. Eu mesmo já fui banido por postar vídeos do ETIM na minha conta - ao mesmo passo em que os EUA descriminalizava o grupo terrorista. É só mais um caso dentre vários outros.

Pelo menos, ainda há [alguns uigures](https://twitter.com/GulnarNorthwest) no Twitter, que diariamente postam coisas de suas maravilhosas vidas (como um internauta comum faria).
