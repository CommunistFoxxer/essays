# As acusações sobre os uigures
Algumas das supostas proibições ao grupo seriam a proibição de:
- uso de véus
- idiomas e dialetos populares próprios
- barba
- recitar o alcorão em funerais
- realização de chamadas internacionais
- porte de livros sobre a religião e a cultura uigur
- utilização de roupas com símbolos muçulmanos
- orar em sua residência, caso visitas estejam presentes

Contudo, apenas vemos as mídias ocidentais - como a Reuters - espalharem esse tipo de acusação, nunca uma fonte primária sequer - como um documento, vídeos ou projetos legislativos - mas, apenas uma “confiança na palavra”. Portanto, é impossível chegar a uma conclusão sem ser da China, ou mais especificamente um uigur.
