# Fatos sobre os uigures e a China
Uma pequena fração de uigures exigem seu próprio país, chamado de "Turquistão" (já que eles se dizem provenientes dos turcos, apesar de haver controvérsias históricas) cobrindo toda a província chinesa de Xinjiang. Nenhum país na história mundial tolerou o separatismo e a secessão. Os uigures representam **0,7% da população chinesa**, e os separatistas querem Xinjiang, região que possui **4 vezes o tamanho da Alemanha** e **16% da área terrestre chinesa**. **Só Xinjiang tem mais de 20.000 mesquitas, além de que todas as grandes cidades da China tem grandes mesquitas, algumas construídas há mil anos**.

Há um [interesse enorme](https://rainershea612.medium.com/u-s-xinjiang-propaganda-is-about-preparing-for-a-ukraine-style-fascist-coup-in-eastern-china-733763348058) dos americanos em Xinjiang, e há [vídeos](https://youtu.be/LE9aZXqw1r4) que explicam porque. Também há mais vídeos interessantes:
- [CGTN Exclusive: Western propaganda on Xinjiang 'camps' rebutted](https://www.youtube.com/watch?v=Wb-MNi8E-TA)
- [A look at vocational education and training programs in Xinjiang](https://www.youtube.com/watch?v=Hb4v7g6yM0Y&ab_channel=CGTN)
- [CGTN finds Mihrigul Tursun's claims false](https://www.youtube.com/watch?v=rsQxjFCh5zU&ab_channel=CGTN)
- [What's China's 're-education camp' in Xinjiang really about?](https://www.youtube.com/watch?v=U3YBomwuB10&ab_channel=CGTN)
- [What you probably don't know about Xinjiang](https://www.youtube.com/watch?v=THTmagqjhNQ)
- [CGTN Exclusive: Inside Xinjiang's boarding schools 新疆寄宿制學校真相](https://www.youtube.com/watch?v=x99g4kua5s0&ab_channel=CGTN)
- [What changes have boarding schools brought to students in Xinjiang?](https://www.youtube.com/watch?v=XRorvkxIDPg&ab_channel=CGTN)
- [Qiao Collective | The U.S. is Set on a Path to War with China. What Is to be Done?](https://www.qiaocollective.com/en/articles/what-is-to-be-done)
- [Uyghur 'unrest' was a CIA narrative planned to destabilize China, top US army Chief admits. 2018](https://www.youtube.com/watch?v=00Cvx0R8iDo)

Resumindo, é de grande interesse dos americanos desestabilizarem a China, mas mais importante: impedir o projeto Belt & Road de ser finalizado (já que o mesmo passa por Urumqi - capital de Xinjiang). Também há outros motivos, como a CIA mesmo já liberou um [documento](https://www.cia.gov/readingroom/docs/CIA-RDP04T00907R000200170001-3.pdf) de 1987 mostrando que poderia haver grandes reservas de petróleo em Xinjiang, e no qual foi [provado recentemente](https://news.cgtn.com/news/2021-06-18/China-discovers-900m-tonne-oil-and-gas-field-in-Xinjiang-11csCgsqbmg/index.html). 1987 é um ano estranho, o terrorismo em Xinjiang começou a aumentar poucos anos depois disso, começando com o primeiro atentado ao ônibus de Urumqi em 1992.

Essa afirmação parece ser corroborada por Sibel Edmonds, ex-funcionária do FBI, que em 2011 [afirmou](https://www.youtube.com/watch?v=EoZSAtl7GvQ):
>Entre 1996 e 2002, nós, os EUA, planejamos, financiamos e ajudamos a executar cada uma das revoltas e esquemas terroristas em Xinjiang.

Os uigures tradicionalmente seguem o sufismo, uma versão bastante moderada do islamismo, diferentemente do wahabismo da Arábia Saudita. As mulheres uigures usam roupas coloridas lindas, adoram dançar e cantar e não usam burcas. A tradição uigur inclui até bebidas alcoólicas leves (chamadas de museles). Você pode observar isso no artigo da [celebração da cultura uigur](http://www.xinhuanet.com/english/2017-07/19/c_136455823.htm). Mas se você observar as mulheres separatistas uigures, elas estão sempre encobertas como as seitas ultraortodoxas. Isso mostra que quem está financiando esses radicais são grupos fundamentalistas reacionários, como o ISIS.

A população uigur **dobrou nos últimos 40 anos**. Eles praticamente foram isentos da política de filho único até 2013, **além do PIB de Xinjiang ter crescido 10 vezes nos últimos 20 anos**. Existem muitos atletas, cantores, dançarinos, políticos que são uigures. Os uigures recebem [tratamento preferencial](https://www.tandfonline.com/doi/full/10.1080/24761028.2019.1625178) para admissão na faculdade e em outras áreas, além de que escolas de Xinjiang obrigatoriamente oferecem cardapio da culinária Halal. Ao contrário da França, que baniu véus em escolas e ao sair de casa, e tem uma ampla campanha islamofóbica de extrema-direita (a hipocrisia é tão grande que tenho um amigo liberal que mora na França e ele cai em propagandas ocidentais sobre os uigures e acha que a França e todos os outros países europeus são "exemplos de democracia" - talvez não seja de se espantar, já que o mesmo trabalha na ANSSI). Há uma [análise mais aprofundada](#políticas-chinesas-sobre-minorias) sobre as políticas da China em relações às minorias.

O Twitter [censurou e baniu a conta](https://twitter.com/Jingjing_Li/status/1395620548802011136) de duas garotas uigures mostrando a vida em Xinjiang (elas têm 1,82 milhão de seguidores em Douyin). O Google vem fazendo a [mesma coisa](https://twitter.com/andraydomise/status/1364082274195673088).

O público dos EUA obtém a [realidade alternativa](https://www.peoplesworld.org/article/in-xinjiang-controversy-china-is-the-target-of-u-s-violence-against-muslims/) dos eventos:
>Um exame cuidadoso revela que essas acusações estão intimamente ligadas a um ataque multifacetado, de décadas, do imperialismo dos EUA contra muçulmanos e supostos muçulmanos em toda a Ásia, norte da África e os EUA. Essa estratégia incluiu o fornecimento de armas a grupos islâmicos ultradireitistas, treinamento militar e apoio financeiro em países de maioria muçulmana como uma política de desestabilização, caracterizando todos os muçulmanos como uma ameaça terrorista e, finalmente, usando isso como justificativa tanto para a opressão de muçulmanos em casa, quanto para a invasão e ocupação da maioria dos países muçulmanos no exterior.

A China também [proíbe linguagem anti-islâmica](https://www.globaltimes.cn/content/1067405.shtml) nas redes sociais:
>"É necessário remover oportunamente as frases radicais que discriminam o Islã e são tendenciosas contra os muçulmanos para evitar o agravamento do ódio online contra o grupo. Essas frases minam gravemente a harmonia religiosa e a unidade étnica", disse Xiong Kunxin, professor da Universidade Minzu em Pequim da China. Para alcançar a unidade nacional e a estabilidade social, as minorias étnicas, incluindo o povo Hui e Uigur, desfrutam de políticas favoráveis, incluindo o recebimento de pontos extras nos exames de admissão à faculdade na China, políticas de planejamento familiar mais brandas e garantia de uma certa proporção de cargos no governo.

A Reuters em um [artigo](https://www.reuters.com/article/uk-mideast-crisis-syria-china/syria-says-up-to-5000-chinese-uighurs-fighting-in-militant-groups-idUSKBN1840UP) afirma:
>Até 5.000 uigures étnicos da região de Xinjiang, que é propensa à violência, estão lutando em vários grupos militantes na Síria, disse o embaixador sírio na China na segunda-feira, acrescentando que Pequim deveria estar extremamente preocupada com isso.

Além disso, apenas o "primeiro mundo" condena a China em relação aos uigures - nem os países árabes o fazem.
![Xinjiang supporters](https://pbs.twimg.com/media/E3bPFu-XwAEl6Or?format=png&name=small)

Há um [PDF](http://english.www.gov.cn/archive/whitepaper/201908/17/content_WS5d57573cc6d0c6695ff7ed6c.html) que cobre totalmente a vida dos uigures locais.
