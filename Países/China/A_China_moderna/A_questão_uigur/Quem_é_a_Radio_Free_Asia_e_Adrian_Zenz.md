# Quem é a Radio Free Asia e Adrian Zenz?
A **Radio Free Asia** foi criada nos anos 50 como mídia anti-comunista pela **Fundação Ásia** como projeto da CIA para fortalecer o anti-comunismo no continente asiático. Hoje, a Radio Free Asia é bancada diretamente pelo congresso americano e sediada em Washington. Eles foram os primeiros a afirmarem a existência de campos de concentração, na qual tinha associação com **Gay McDougall**, a primeira pessoa a denunciar na ONU as barbaridades do estado chinês, sem nenhuma prova ou fonte primária. Os EUA também financia separatistas uigures através de organizações [como a NED](https://www.ned.org/wp-content/themes/ned/search/grant-search.php?organizationName=&region=Asia&projectCountry=China&amount=&fromDate=&toDate=&projectFocus%5B%5D=&search=&maxCount=10&orderBy=CountryR&start=1&sbmt=1), que [financia grupos terroristas uigures](https://www.ned.org/uyghur-human-rights-policy-act-builds-on-work-of-ned-grantees/) e [interfere em eleições de outros países](https://www.youtube.com/watch?v=NzIJ25ob1aA).

**Adrian Zenz**, antropólogo alemão, é o maior líder desses estudos sobre barbaridades contra os uigures, no qual levantou relatórios incompletos em 2019 sobre a quantidade de uigures nesses campos de concentração.

Adrian também é teólogo, cristão e diz ter sido enviado por Deus para investigar as minorias chinesas, entretanto, ele não fala chinês, nem o dialeto uigur, nunca pisou em Xinjiang e, além disso, é ligado à **Fundação Memória Ás Vítimas Do Comunismo**.

O mesmo também justifica o fato de haver campos de concentrações na Alemanha Nazista e o Ocidente não se preocupar porque Hitler "salvou" a Alemanha.

![](images/adrian_nazi_zenz.png)

O mesmo também [acredita que os judeus](https://books.google.com.br/books?id=lRtSQB3HHJcC&q=jews&redir_esc=y#v=onepage&q=fiery%20furnace&f=false) que se recusam a se converter ao cristianismo serão "eliminados" e colocados em uma "fornalha ardente" (soa familiar?).

![](images/adrian_antisemite_zenz.png)
