# Terrorismo uigur
Durante a década de 80, os Estados Unidos **recrutaram, financiaram e armaram vários militantes islâmicos** - os mujahideens - para lutar no Afeganistão contra a antiga União das Repúblicas Socialistas Soviéticas. Não é de se surpreender que os uigures aderiram a essa jiha, tendo em vista que o Afeganistão faz fronteira com Xinjiang. Militantes uigures agiram com Bin Laden e criaram o grupo **ETIM**, ou **Movimento Islâmico do Turquistão Oriental**, que é designado como um grupo terrorista pela ONU.

Conselho de Segurança da ONU [sobre o ETIM](https://www.un.org/securitycouncil/sanctions/1267/aq_sanctions_list/summaries/entity/eastern-turkistan-islamic-movement):
>O Movimento Islâmico do Turquestão Oriental (ETIM) é uma organização que tem usado a violência para promover seu objetivo de estabelecer um chamado “Turquistão Oriental” independente dentro da China. Desde a sua criação, o ETIM manteve laços estreitos com o Talibã, a Al-Qaeda (QDe.004) e o Movimento Islâmico do Uzbequistão (QDe.010). Foi fundada por Hasan Mahsum de Xinjiang, China, que foi morto por tropas paquistanesas em outubro de 2003. A ETIM é atualmente liderada por Abdul Haq (QDi.268), que também era membro do Conselho Shura da Al-Qaeda em 2005.

O ETIM espera iniciar um etnostado muçulmano em Xinjiang. Durante anos, eles foram financiados pessoalmente por Osama Bin Laden e seu líder, Abdul Haq, está no conselho de anciãos da Al-Qaeda. Ao longo dos anos 2000 e 2010, quando as relações EUA/China eram boas, a mídia noticiava como milhares de extremistas uigures estavam indo para o Oriente Médio para serem treinados na jihad contra a China - e como estavam de fato realizando ataques terroristas em Xinjiang:
- [Associated Press | AP Exclusive: Anger with China drives Uighurs to Syrian war](https://apnews.com/article/1a7aad978ad5470bb6450ef13f86469e)
- [CNN | Xinjiang violence: Does China have a terror problem?](https://edition.cnn.com/2015/12/02/asia/china-xinjiang-uyghurs/index.html)
- [BBC News | Urumqi attack kills 31 in China's Xinjiang region](https://www.bbc.com/news/world-asia-china-27502652)
- [The Guardian | Xinjiang attack: four 'terrorists' and one bystander killed, says China](https://www.theguardian.com/world/2016/dec/29/xinjiang-attack-four-terrorists-and-one-bystander-killed-says-china)

Os ataques do ETIM foram o catalisador para uma resposta massiva de Pequim, colocando Xinjiang sob bloqueio e reduzindo drasticamente as liberdades. O governo dos EUA era neutro quando começou, mas a ascensão da China os levou a condenar a China. Recentemente os EUA [os retirou da lista de grupos terroristas](https://www.federalregister.gov/documents/2020/11/05/2020-24620/in-the-matter-of-the-designation-of-the-eastern-turkistan-islamic-movement-also-known-as-etim-as-a). [2](https://moderndiplomacy.eu/2020/12/13/turkestan-islamic-party-desires-to-be-a-national-liberation-movement-after-us-de-blacklist/). Mas em 2018, eles estavam em guerra com eles no Afeganistão, onde em um [vídeo](https://www.youtube.com/watch?v=RrNHRQLyzxo) o comandante da OTAN no Afeganistão detalha como eles estão em guerra com o ETIM, bombardeando-os para reduzir o terrorismo na província de Xinjiang. A remoção do ETIM da lista de terroristas foi saudada por muitos como um passo para ajudar os uigures em Xinjiang contra um ataque chinês.

Em última análise, tudo isso é político - O ETIM foi colocado na lista por causa da Guerra ao Terror dos Estados Unidos, agora ele foi retirado por causa da iminente guerra contra a China.

Os uigures que vivem em áreas urbanas como **Urumqi** são seculares e definitivamente não querem viver em um estado islâmico administrado por "extremistas" religiosos. Além disso, existem milhares de uigures no Afeganistão. E adivinha? Os EUA os matam, capturam e enviam para Guantánamo. Essas ações são melhores do que a China enviando jihadistas para programas de educação e treinamento profissional? Além de que a Malásia tem seus próprios campos de desradicalização que [recebem elogios](https://www.youtube.com/watch?v=poTDSEQwoZA) nas notícias da BBC.

Entretanto, vale ressaltar a hipocrisia dos Estados Unidos em relação ao assunto. O país gastou [quase 2 trilhões de dólares em fundos diretos com finalidade de bombardear países muçulmanos](https://www.crfb.org/blogs/government-has-spent-2-trillion-wars-middle-east), causando [mais de 50 milhões de refugiados muçulmanos](https://www.unicef.org/press-releases/nearly-50-million-children-uprooted-worldwide) nos últimos vinte anos. Jihadistas uigures de países como Afeganistão ou Síria [são considerados terroristas quando ameaçam ou entram em conflito com os Estados Unidos e a OTAN](https://www.nbcnews.com/news/ncna845876) (Organização do Tratado do Atlântico Norte) porém, quando voltam ou viajam para a China, se tornam supostas vítimas. Esses mesmos terroristas incentivaram vários ataques na China desde a década de 90, que variam de ataques com facas e carros até tumultos massivos e ataques suicidas, e [inclusive um atentado terrorista a um aeroporto](https://www.bbc.com/news/world-asia-28305109). Dezenas de milhares de uigures foram para a China e [se juntaram com o ISIS e Al Qaeda](https://smallwarsjournal.com/jrnl/art/china-xinjiang-and-uyghurs-global-jihadist-propaganda). Há também um [documentário](https://www.youtube.com/watch?v=BjgSOYRZqIo) sobre o terrorismo em Xinjiang. Sem análise histórica, há a predominância do argumento de que eles simplesmente odeiam os uigures sem motivo.

Depois de 25 anos de luta, a China reprimiu os separatistas e terroristas uigures. O resultado foi que desde 2017 não houve sequer nenhum atentado terrorista uigur. Nenhum país no mundo encontrou uma solução fácil, ou não-autoritária, para lidar com o jihadismo e mudá-los, como disse, não há análise sem contexto histórico-social. Embora a China pudesse fazer um trabalho melhor com mais transparência, os esforços gerais são razoáveis.

Eu compilei uma série de vídeos e textos sobre o ETIM e outros grupos terroristas (atenção: NSFW +18, imagens/vídeos fortes):
- Vídeo antigo de propaganda da ETIM fazendo [lavagem cerebral em crianças e pregando-lhes o ódio e a violência](https://www.mediafire.com/file/wqgxjqcka8oq29a/ETIM+old+propganda+video+showing+them+brainwashing+children+and+preaching+them+hate+and+violence+1.mp4/file). [2](https://www.mediafire.com/file/thulew7dmjq8iog/ETIM+old+propganda+video+showing+them+brainwashing+children+and+preaching+them+hate+and+violence+2.mp4/file). [3](https://www.mediafire.com/file/b4lkvmgch6bne1w/ETIM+old+propganda+video+showing+them+brainwashing+children+and+preaching+them+hate+and+violence+3.mp4/file). [4](https://www.mediafire.com/file/ccgzfqz3xg8t4w9/ETIM+old+propganda+video+showing+them+brainwashing+children+and+preaching+them+hate+and+violence+4.mp4/file). [5](https://www.mediafire.com/file/ndjrs9lmw63wn5b/ETIM+old+propganda+video+showing+them+brainwashing+children+and+preaching+them+hate+and+violence+5.mp4/file). [6](https://www.mediafire.com/file/w2iscd10wbgesca/ETIM+old+propganda+video+showing+them+brainwashing+children+and+preaching+them+hate+and+violence+6.mp4/file).
- ETIM: ["Os chineses são os mais impuros e sujos entre os povos do mundo"](https://imgur.com/a/ZNxfycc)
- [Propaganda anti-China do ETIM](https://www.mediafire.com/file/l8g2ijfym3bvup0/Im+sure+Adrian+zenz+will+love+this+ETIM+propganda+video+.mp4/file)
- Observatório Sírio para os Direitos Humanos: [Velho e sua esposa mortos com suas gargantas cortadas depois que a área ficou sob o controle da ETIM e da Al Qaeda e a maioria da população cristã fugiu temendo por suas vidas](https://imgur.com/a/3drVcbZ)
- ETIM: ["Corona e sinofobia é um castigo de Deus aos chineses por seus crimes"](https://imgur.com/a/MmrHaPt)
- Um clipe do vídeo de propaganda do ETIM que foi lançado recentemente mostrando abou qatada alfalstiny um de seus também estudiosos, de acordo com o clipe, jihad é como qualquer outro ritual islâmico e é [obrigatório no Islã](https://www.mediafire.com/file/yfwuhbl8m52k2xw/A+clip+of+ETIM+propganda+video+that+was+released+recently+showing+abou+qatada+alfalstiny+one+of+their+too+scholars,+according+to+the+clip+jihad+is+lik.mp4/file)
- [Grupo jihadista uigur na Síria anuncia "pequenos jihadistas"](https://www.longwarjournal.org/archives/2015/09/uighur-jihadist-group-in-syria-advertises-little-jihadists.php)
- Parte do ETIM ensina separatistas a [praticar táticas de lobo solitário](https://www.mediafire.com/file/2lw5i4f23q24nk6/Part+of+ETIM+teach+separatists+how+to+practice+lone+wolf+tactics...mp4/file)
- [Vídeo que documenta crianças soldados sendo recrutadas por jihadistas na Síria do campo de refugiados Atmeh na fronteira com a Turquia](https://vimeo.com/264488781)
- [Vídeo recente de propaganda](https://www.mediafire.com/file/6f29ze9q0vn8xvn/recent+ETIM+propaganda+video+accusing+saudi+arabia+of+launching+a+war+on+islam+for+fighting+wahhabism,+and+attacking+muslims+living+in+the+west..mp4/file) do ETIM acusando a Arábia Saudita de lançar uma guerra contra o Islã por combater o wahhabismo e atacar os muçulmanos que vivem no oeste
- [Vídeo interessante](https://www.mediafire.com/file/tac2r10utq5x088/Interesting+western+propganda+video+goes+back+around+1941...mp4/file) de propaganda ocidental remonta a 1941 que acaba ficando [mais interessante](hhttps://www.mediafire.com/file/epiivufcfe99ttv/Gets+more+interesting+.mp4/file)
- Vídeo de propaganda do ETIM mostrando eles [queimando a bandeira palestina](https://www.mediafire.com/file/918g946u5cmq9c5/ETIM+propganda+video+showing+them+burning+the+Palestinian+flag...mp4/file)
- [Criança uigur chinesa submetida a lavagem cerebral por separatistas para executar civis sírios](https://imgur.com/a/3lfp7XQ)
- [Ataque do ETIM contra a polícia local em 2008, que resultou em 17 vítimas e 15 feridos](https://tw.appledaily.com/international/20080819/SE7L6JZGLETSZCWYZSAUZXUXFA/)
- Terroristas do ETIM em sua revista "Turquestão oriental islâmica": ["Os chineses comem porcos, gatos, bebês, embriões humanos e fervem ovos na urina das crianças, pois não acreditam em Deus"](https://imgur.com/a/UPt9zai)
- [Vídeo antigo de abou qatadah](https://www.mediafire.com/file/8ba4o158i20vbb8/Old+video+of+abou+qatadah+who+is+considered+a+top+religious+leader+of+ETIM+speaking+about+fatwa+he+issued+to+target+women+and+children,+his+fatwas+cau.mp4/file) que é considerado um dos principais líderes religiosos do ETIM falando sobre a fatwa que ele emitiu para mulheres e crianças, suas fatwas causaram a morte de 200 mil apenas na Argélia, então a melhor resposta para os ocidentais quando perguntam por que os muçulmanos não apoiam separatistas é: Vai se foder
- Vídeo de propaganda separatista mostrando o comandante da Al Qaeda na Síria [pedindo aos capitalistas muçulmanos](https://www.mediafire.com/file/70mwvxbbdv66fhl/Separatists+propganda+video+showing+alqaeda+commander+in+Syria+asking+Muslim+captalists+to.mp4/file) que apoiem os separatistas em Xinjiang e espalhem sua propaganda como recompensa pelo que fizeram na Síria
- Ex-líder do ETIM: ["o islã nos proíbe de ver pecados e imoralidade e não fazer nada, a China permite que as pessoas abram bares perto das mesquitas para provar que praticam a democracia e a liberdade, e quem quiser obedecer a allah pode e quem quiser desobedecer a allah ninguém fará pará-los."](https://www.mediafire.com/file/llat1m57xi8jgzf/ETIM+late+leader+islam+forbid+us+to+see+sins+and+immorality+and+do+nothing,+china+let+people+open+bars+near+mosques+to+prove+that+they+practice+democ.mp4/file)
- Declaração da ETIM datada de 2013, adotando o [ataque a uma boate com facas que deixou 4 vítimas e 8 feridos](https://imgur.com/a/zznSxey)
- Declaração da ETIM datada de 2013 adotando o [ataque a um mercado que deixou 15 vítimas e 2 feridos](https://imgur.com/a/yIVsm5N)
- Artigo na 9ª edição do Turquestão Islâmico de 2011 (porta-voz dos separatistas) acusando os paquistaneses de serem ["exército de ladrões"](https://imgur.com/a/gGTgNAi)
- ["Poderíamos ter vivido uma vida boa em nosso país, mas escolhemos, mas imigramos para viver sob a lei da sharia."](https://www.mediafire.com/file/iw60h5ca7dfchrf/ETIM+we+couldve+lived+a+good+life+in+our+country+but+we+chose+but+we+immigrated+to+live+under+sharia+law..mp4/file) (áreas sob controle do Talibã e da Al Qaeda)
- Vídeo de propaganda da ETIM mostrando-os [queimando itens religiosos islâmicos](https://www.mediafire.com/file/9o6isl59kufrca5/ETIM+propganda+video+showing+them+burning+islamic+religious+items...mp4/file)
- Em 2015, os jihadistas uigures do ETIM [lançaram carros-bomba suicidas contra a sede do governo sírio](https://www.mediafire.com/file/sgj1tjxct32iaw4/Combat+vid+18++-+-+In+2015+Uyghur+jihadists+org+Turkistan+Islamic+Party+launched+suicide+car+bombs+against+Syrian+gov+HQ+during+their+take+over+of+.mp4/file) durante a tomada de Jisr Al Shughur, Idlib, Síria. Agora, o NYT critica a ofensiva russa apoiada pela Síria para limpar essa sujeira. Em 31 de outubro de 2016, jihadistas uigures da ETIM lançaram um carro-bomba suicida contra as forças do governo sírio em Aleppo no projeto “1070”. “Rebeldes da Síria” afiliados da Al Qaeda, jihadistas uigures do ETIM [atacaram a posição do governo da Síria](https://www.mediafire.com/file/q3vcio6o8y0v0di/Combat+vid++18+-+-+Al+Qaeda+affiliate+Syrian+Rebels+Uyghur+jihadists+org+Turkistan+Islamic+Party+storming+Syrian+gov+position+during+their+takeover+.mp4/file) durante a aquisição de Jisr Al Shughur em 2015
- [BBC espalhando fake news](https://www.mediafire.com/file/rjuzy7w4tyn7hew/What+makes+a+qualified+BBC+reporter+Chinese+netizens+video+mocking+@BBCs+made-up+fake+news+went+viral+on+Chinas+social+media..mp4/file)
- Filmagens do Waziristão, onde o ETIM, a Al Qaeda e o Talibã do Paquistão lutaram contra o exército paquistanês e [expulsaram civis acusados de entrar em contato com o governo do Paquistão](https://archive.org/details/etaliban-pakistan)
- [ETIM lutando contra o exército paquistanês no Waziristão](https://www.academia.edu/32064823/Eastern_turkistan_movement_in_Pakistan_docx)
- ["Disfarçados em Idlib, conseguimos entrar em áreas isoladas onde jihadistas uigures chineses vivem com suas famílias. 1ª vez que uma câmera entra."](https://www.mediafire.com/file/cna0io1o6382px4/@akhbar+20+Undercover+in+Idlib,+we+managed+to+enter+sealed+off+areas+where+Chinese+Uyghur+jihadists+live+with+families.+1st+time+a+camera+gets+in..mp4/file), diz Jenan Moussa
- [Alzawahry em seu último vídeo pedindo ataques contra os chineses usando invenções da BBC para justificá-lo](https://imgur.com/a/a1scSc8)
- Todas as mentiras da mídia ocidental sobre Xinjiang vêm da China derrotando [esses caras](https://www.mediafire.com/file/l0zm17kwesc2yni/All+the+western+media+lies+about+Xinjiang+come+from+China+defeating+these+guys.+-+-+CW.+Viewer+discretion+is+advised,+Footage+taken+from+@CGTNOffic.mp4/file). ["Tínhamos que mirar nas multidões com facas ou explosivos... Tínhamos que fazer isso onde as pessoas estavam concentradas, não avisaríamos ninguém de antemão. Não importava se você era um muçulmano ou um uigur, não havia distinção"](https://www.mediafire.com/file/aeq9qa4mtbydobr/We+had+to+target+crowds+either+with+knives+or+explosives...We+had+to+do+it+where+people+are+concentrated,+we+would+warn+no+one+beforehand,+It+didnt+m.mp4/file). [A resposta do ETIM](https://www.mediafire.com/file/6nuqhpqpu7fp6b0/ETIM+Response.mp4/fileg).
- Terroristas do ETIM na revista "Turquestão islâmica" em 2013: ["Graças a Deus, as operações terroristas em Xinjiang e outras partes da China humilharam o governo chinês nos últimos anos, antes que houvesse um grande ataque a cada década, mas nos últimos 5 anos há um grande ataque a cada dois meses, louvor a Deus"](https://imgur.com/a/CiNGyv8)
- Terroristas da ETIM na revista "Turquestão Islâmica" de 2013: ["A China ficou nervosa quando a chamada Primavera Árabe começou, que resultou na derrubada de governadores infiéis como Kadafi e hosni Mubarak, e agora teme que o mesmo aconteça em Xinjiang"](https://imgur.com/a/4B428AM)
- Terroristas do ETIM na revista "Turquestão Islâmica" de 2013: ["Os chineses insultaram a religião e a cultura dos muçulmanos de Xinjiang ao permitir boates, e é por isso que Noor Mohamed atacou um desses clubes em Kashgar e matou duas prostitutas chinesas em 09/07/2012"](https://imgur.com/a/g2h0wRS)
- Terroristas da ETIM na revista "Turquestão Islâmica" de 2013: ["Os malvados estudiosos islâmicos anunciaram uma reunião como resultado do ataque terrorista de Kunming, incluindo estudiosos de Xinjiang e o perverso estudioso abd-alhameed Ahmed tokhti, surpreendentemente discutiram questões de diferentes religiões"](https://imgur.com/a/gX5QdSU)
- Entrevista com terrorista ETIM que emigrou para o Afeganistão na revista "Turquestão Islâmica" 2018: [- por que você escolheu o Afeganistão? - Escolhemos o Afeganistão porque sabíamos que era a terra em que Abdellah azzam e Osama bin Laden viviam, e a ETIM é ativa lá então decidimos juntar-nos a eles, enquanto estávamos na China, vimos vídeos das batalhas e conquistas do ETIM, e ouvimos a sua transmissão na rádio, depois decidimos apoiar o Emirado Islâmico a juntar-se à sua luta até à vitória ou morrer na luta ](https://imgur.com/a/2fXIglL)
- Entrevista com terrorista ETIM que emigrou para o Afeganistão na revista "Turquestão Islâmica" 2018: [- quais são os benefícios de imigrar para o Afeganistão? - um dos melhores benefícios que alcançamos a terra da glória e da jihad ao invés de ir para a Europa, a terra da blasfêmia e desorientação, e louvor a Alá, agora podemos fazer cumprir a lei Sharia e lutar contra os inimigos de Deus, tanto descrentes quanto ateus](https://imgur.com/a/6Mvbu99)
- Terroristas da ETIM na revista "Turquestão Islâmica" 2018: ["Quem para de lutar por causa da dor que enfrenta, ou para ter paz interior, ou porque as pessoas os rejeitam, ou por pensar que há boas ações melhores do que a jihad, é um soldado de Satan"](https://imgur.com/a/ZKd0m9r)
- Os separatistas do ETIM ameaçam os muçulmanos uigures com o inferno se eles não aderirem ou financiarem grupos terroristas, e alegando que [a "jihad" é tão obrigatória quanto a oração e os muçulmanos que a denunciam não são muçulmanos](https://www.mediafire.com/file/p6j1at37b00xpky/ETIM+separatists+threatening+Uyghur+Muslims+with+hell+if+they+didnt+join+or+fund+terrorist.mp4/file)
- Thread no Twitter mostra protestos contra o ódio contra os asiáticos sendo [interrompidos por grupos pró-uigures gritando "Fuck China"](https://twitter.com/NicXTempore/status/1373691318208245764)
- [Mamat Juma, o imã da mesquita Id Kah em Kashgar. Seu pai era o imã antes dele, mas foi tragicamente morto em um ataque brutal de faca em 30 de julho de 2014 por terroristas](https://news.cgtn.com/news/2020-09-29/Mamat-Juma-More-than-the-imam-of-Kashgar-s-Id-Kah-Mosque-U9XP4uap6o/index.html)
- [ISIS dizendo que eles atacaram a comunidade Hazara como um castigo pelo Paquistão se opor ao Ocidente e apoiar os comunistas e as minorias paquistanesas](https://www.mediafire.com/file/0jl8wdp8neewr3f/Isis+saying+that+they+attacked+the+Hazara+community+as+a+punishment+for+Pakistan+opposing+.mp4/file)
- Vídeo de propaganda do ETIM lançado em abril: ["nossa jihad não é sobre o povo sírio ou líbio, é uma luta sectária entre sunitas e outras seitas/religiões"](https://www.mediafire.com/file/nl94ckhobm7nfep/ETIM+propganda+video+released+a+week+ago+our+jihad+isnt+about+Syrian+or+Libyan+people,+its+a+secterian+fight+between+Sunnis+and+other+sectsreligio.mp4/file)
- Um vídeo de propaganda separatista visando os uigures dentro da China diz: ["levar uma vida boa é trair seu senhor, a morte - na jihad - é melhor do que ter uma vida boa e pedir aos uigures para se juntarem a grupos terroristas separatistas"](https://www.mediafire.com/file/c12ml5tifh3g9ci/Separatists+propganda+video+released+4+months+ago+targeting+Uyghurs+inside+china+says+leading+a+good+life+is+betrayal+of+ones+lord,+death+-+in+jihad.mp4/file)
- Vídeo recente de propaganda do ETIM, lançado meses atrás, [pedindo aos uigures que se juntem a grupos terroristas separatistas, "livrem-se" dos descrentes e apliquem a lei Sharia](hhttps://www.mediafire.com/file/gwxudkj3vorqcrm/Recent+ETIM+propganda+video+released+months+ago+asking+Uyghurs+to+join+separatist+terroris.mp4/file)
- Terrorista ETIM na Síria [convocando ataques terroristas](https://www.mediafire.com/file/v6glteeeka4xkw5/ETIM+terrorist+in+Syria+calling+for+terrorist+attacks+against+The+Chinese+in+islamic+countries+around+china,+days+before+the+assassination+attempt+of+.mp4/file) contra chineses em países islâmicos ao redor da China, dias antes da tentativa de assassinato do embaixador da China no Paquistão
- Fotos publicadas por terroristas da ETIM em 2010 mostrando seus ataques contra [companhia petrolífera em Urumqi, edifício residencial explodido em Karamay, explosão de laboratório químico em Shandong e explosão de um restaurante uigur em Pequim](https://imgur.com/a/uOukhlr)
- Propaganda do ETIM: ["Deixamos a China porque viver com não-muçulmanos nos levaria para o inferno, e amar sua casa, seus filhos e sua família não é islâmico"](https://www.mediafire.com/file/77o0m4e5gafjak3/ETIM+propganda+we+left+china+because+living+around+non-muslims+would+get+us+to+hell,+and+l.mp4/file)

**Nota:** Se não conseguir acessar um dos links (muito provavelmente irá cair por ser +18), copie o link e vá na [Wayback Machine](https://web.archive.org) e procure pela cópia.

Aqui está a lista de ataques terroristas uigures na China:
>28 de agosto de 2012
>
>Vários terroristas em Xinjiang usaram facas e machados para atacar cidadãos comuns, causando 15 mortes e 14 feridos. A polícia matou oito terroristas no local e prendeu outro terrorista.
>
>28 de outubro de 2013
>
>Oito terroristas, todos uigures, usaram veículos para colidir com os cidadãos na Praça Tiananmen, causando cinco mortes e 40 feridos. Eles compraram gasolina, máscaras anti-venenosas, facas e machados como armas de ataques terroristas. Três terroristas morreram no local, enquanto outros cinco suspeitos foram presos dez horas após o ataque. Três dos terroristas presos foram posteriormente condenados à morte pelo tribunal. Antes dos ataques, os organizadores foram várias vezes observar a situação na Praça Tiananmen.
>
>30 de dezembro de 2013
>
>O condado de Shache, em Xinjiang, sofreu um ataque terrorista no qual a polícia matou oito suspeitos de terrorismo no local.
>
>24 de janeiro de 2014
>
>Uma bomba explodiu em um mercado e um cabeleireiro em Xinjiang, causando a morte de duas pessoas e dois feridos. A polícia prendeu três pessoas, mas duas delas suicidaram-se ao detonar uma bomba amarrada aos seus corpos.
>
>14 de fevereiro de 2014
>
>Uma dúzia de terroristas atacou veículos da polícia em Xinjiang. Quatro policiais ficaram feridos e oito suspeitos de crimes foram mortos. Três outros suspeitos acionaram as bombas amarradas a seus corpos e cometeram suicídio. Um foi a prisão. Cinco viaturas policiais foram destruídas, mostrando um novo padrão de ataques suicidas que visavam viaturas policiais. Os terroristas usaram bombas de gás natural feitas por eles mesmos em seus ataques de 24 de janeiro e 14 de fevereiro.
>
>1 de março de 2014
>
>A estação de Kunming testemunhou o ataque de terroristas que usaram facas e machados para atacar cidadãos, causando a morte de 31 pessoas e 141 feridos. O governo da RPC lidou rapidamente com o ato terrorista matando cinco terroristas e prendendo outros quatro. Segundo rumores, dois dos terroristas eram mulheres com idades entre 18 e 19 anos apenas.
>
>2 de março de 2014
>
>Um terrorista imitou o ataque de Kunming em uma cidade em Xinjiang, matando um cidadão e ferindo outro.
>
>17 de março de 2014
>
>Um terrorista atacou uma delegacia de polícia em Xinjiang e feriu um policial, mas ele foi morto.
>
>23 de abril de 2014
>
>Um grupo de 25 terroristas atacou a polícia em Xinjiang, causando a morte de 15 pessoas. Os policiais, que foram a uma residência para investigar a suspeita de posse de armas, foram presos pelos terroristas que atearam fogo no apartamento. Depois de cinco noites de confrontos graves, a polícia acabou matando seis terroristas no local e prendendo outros oito, enquanto 11 cúmplices foram capturados posteriormente. Antes do confronto, os membros do grupo terrorista aprenderam a fazer bombas e operavam no subsolo desde setembro de 2010.
>
>27 de abril de 2014
>
>O presidente Xi Jinping chegou a Xinjiang para uma visita de três dias, mas durante o primeiro dia três oficiais Han em Xinjiang que estavam pescando foram mortos por terroristas.
>
>30 de abril de 2014
>
>O presidente Xi encerrou sua visita a Xinjiang e deixou Urumqi para Pequim. Na mesma noite, dois terroristas na Estação Ferroviária Sul de Urumqi explodiram seu dispositivo de bomba que matou três pessoas e feriu outras 79. Um minuto após a explosão da bomba, a polícia foi ao local e atacou os terroristas, seguida por 150 membros das equipes de resgate e do comitê de gestão. Em resposta, Xi ordenou uma abordagem linha-dura para penalizar os elementos terroristas e “conter sua arrogância”. Descobriu-se que o ataque de 30 de abril foi iniciado pelo movimento do Turquestão Oriental, que também admitiu abertamente sua responsabilidade em 11 de maio por meio de um vídeo na Internet.
>
>6 de maio de 2014
>
>A estação ferroviária de Guangzhou testemunhou um terrorista usando uma faca e um machado para ferir seis pessoas.
>
>8 de maio de 2014
>
>A polícia matou um terrorista e prendeu outro que tentava atacar uma delegacia de polícia em Xinjiang.
>
>22 de maio de 2014
>
>Um grupo de pelo menos oito terroristas usou dois veículos e mergulhou em uma multidão no centro cultural Urumqi, atirando granadas ao mesmo tempo e levando a graves explosões que mataram 43 pessoas e feriram 94 outras. Quatro terroristas morreram durante a explosão da bomba e a polícia matou três outros terroristas no local. Um terrorista foi preso, enquanto cidadãos capturaram três outros suspeitos de terrorismo que possuíam 12 bombas feitas por ele mesmo. A polícia perseguiu imediatamente dois suspeitos de terrorismo que estavam foragidos.
>
>23 de maio de 2014
>
>A polícia de Xinjiang prendeu um residente uigur que possuía 12 facas e machados. Ele era suspeito de usar as armas para atingir um jardim de infância. Após sua prisão, todas as escolas em Xinjiang elevaram seu status de segurança por medo de qualquer ataque terrorista.
>
>25 de maio de 2014
>
>Moradores de uma vila em Xinjiang descobriram cinco suspeitos de terrorismo que dormiam com espadas e facas. Os moradores relataram o ocorrido à polícia, que foi prender os suspeitos. Os suspeitos gritaram o slogan do uso da jihad e brigaram com a polícia, que matou três deles e prendeu outro.]
>
>27 de maio de 2014
>
>A polícia de um condado de Xinjiang foi avisada por residentes que encontraram alguns extremistas religiosos que se treinaram fisicamente. A polícia prendeu sete suspeitos de terrorismo.
>
>2 de junho de 2014
>
>A polícia de uma cidade de Xinjiang prendeu um homem que usava a Internet para propagar idéias terroristas depois que moradores relataram suas atividades à polícia.
>
>4 de junho de 2014
>
>A polícia de um condado de Xinjiang matou quatro terroristas que lançaram um ataque, mas um policial morreu.
>
>5 de junho de 2014
>
>Os tribunais de seis cidades de Xinjiang condenaram 81 terroristas em 23 casos. Doze pessoas receberam sentenças de morte e três outras foram condenadas a penas suspensas. Dois outros suspeitos usaram a Internet e blogs para propagar idéias extremistas e terroristas e foram condenados a 5 e 10 anos de prisão.
>
>21 de junho de 2014
>
>Um quartel da polícia tornou-se alvo de ataques terroristas e a bomba, que causaram três feridos policiais e a morte de 13 terroristas após a reação da polícia.
>
>9 de julho de 2014
>
>Seis terroristas lançaram um ataque no condado de Xinjiang e dois deles escaparam, levando à implantação e mobilização imediata da polícia para tentar prender os que estavam foragidos.
>
>28 de julho de 2014
>
>O condado de Shache, na província de Kashgar, testemunhou dez locais sofrendo simultaneamente ataques terroristas daqueles que usaram facas e machados para atacar a polícia e que também usaram veículos para colidir com cidadãos, levando à morte de 37 cidadãos e à prisão de 215 pessoas. A polícia matou 59 terroristas e o ataque destruiu 31 veículos. Uma gangue usou a jihad e para fazer dois oficiais uigur participarem dos ataques terroristas, mas os dois foram mortos pelos terroristas quando se recusaram a fazê-lo. O movimento do Turquestão Oriental admitiu a responsabilidade pelos ataques. O governo de Xinjiang iniciou imediatamente uma caça ao homem usando 300 milhões de yuans como recompensa aos informantes.
>
>11 de agosto de 2014
>
>Um homem de Xinjiang de 22 anos foi preso por espalhar boatos falsos de Urumqi sobre a polícia matando 50 cidadãos do condado de Shache. Ele chegou mesmo a afirmar que os aviões militares usaram bombas para matar 3.000 cidadãos. Após sua prisão, ele admitiu que “Eu queria atrair a atenção de mais pessoas e criar um impacto na opinião pública”.
>
>21 de setembro de 2014
>
>Um shopping center, uma delegacia de polícia e um mercado em Bazhou, em Xinjiang, sofreram ataques a bomba, causando a morte de quatro policiais, 40 terroristas, dez outros cidadãos e 54 feridos.
>
>28 de novembro de 2014
>
>Um grupo de terroristas detonou uma bomba na rua de alimentação do condado de Shache, causando a morte de quatro pessoas e 14 feridos. A polícia matou 11 terroristas.

Isso não inclui os distúrbios que abalaram a capital de Xinjiang, Urumqi, em 2009, matando centenas e ferindo milhares.

Por último mas não menos importante, fique longe do Louis Proyect. Ele [espalha fake news](https://bennorton.com/louis-proyect-liar/), além de [apoiar a "Revolução Verde" do Irã, a guerra da OTAN contra a Líbia, a guerra contra a Síria e o Euromaidan](https://www.wsws.org/en/articles/2015/06/08/proy-j08.html) (golpe neofascista apoiado pelos EUA na Ucrânia).

Fontes:
- [Asia Times | Chinese Uyghur colonies in Syria a challenge for Beijing](https://asiatimes.com/2017/05/chinese-uyghur-colonies-syria-challenge-beijing/)
