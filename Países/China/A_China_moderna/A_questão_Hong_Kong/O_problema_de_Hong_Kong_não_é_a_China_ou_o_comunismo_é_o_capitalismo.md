# O problema de Hong Kong não é a China ou o comunismo, é o capitalismo
Vamos analisar as [taxações de Hong Kong](https://en.wikipedia.org/wiki/Taxation_in_Hong_Kong):
>Além disso, de acordo com o Artigo 106 da Lei Básica de Hong Kong, Hong Kong goza de finanças públicas independentes e nenhuma receita tributária é transferida para o Governo Central da China.

Agora analisaremos [a economia de Hong Kong](https://en.wikipedia.org/wiki/Economy_of_Hong_Kong):
>A política econômica de Hong Kong tem sido frequentemente citada por economistas como Milton Friedman e o Cato Institute como um exemplo de capitalismo laissez-faire, atribuindo o sucesso da cidade à política.

Mas... Nem tudo são flores:
>Hong Kong lidera a tabela como o mercado imobiliário mais caro do mundo pelo nono ano consecutivo.

Esses são os sinais reveladores do capitalismo. Moradia, padrão de vida, corrupção, sistema de leilão confuso para maximizar os financiamentos, sistema de arrendamento de terras e magnatas dos negócios elevando os preços para seu próprio benefício. No entanto, a mídia e os manifestantes de HK estão cada vez mais mal direcionados (com os dedos do Ocidente na balança) para culpar a China. As demandas opacas são assustadoras e suspeitas, na melhor das hipóteses. Eles pediram:
- Uma retirada completa das dívidas
- A retirada da caracterização de “motim” dos protestos de 12 de junho
- A libertação incondicional de todos os manifestantes presos
- A formação de uma comissão independente de inquérito sobre o comportamento policial
- Sufrágio universal

Nenhum clama por independência ou mesmo "mais autonomia" como a mídia tem levado muitos a acreditar. A exibição nojenta dessa narrativa não pode ser exemplificada mais aqui, à medida que os ocidentais se deleitam na falsa glória dos manifestantes HK dispensando a bandeira, afirmando que "merecem sair do comunismo", algo QUE ELES NUNCA TIVERAM. A questão do sufrágio universal é explicada muito bem [aqui](https://www.scmp.com/comment/opinion/article/3045901/why-we-dont-have-universal-suffrage-2020).

Eis aqui o pobre de Hong Kong:
![](https://i.pinimg.com/originals/5f/37/ec/5f37ec88aa430bddc08ba5f37a39aeba.jpg)
![](https://i.pinimg.com/originals/53/3e/33/533e33c68dbf1f6889f26eb260ae7c34.jpg)

Os protestos estão diminuindo um pouco e as autoridades locais estão tendo mais controle da situação. Isso está longe de terminar? Absolutamente não. O que isso mostrou é a feiura e o racismo do Ocidente, flexionando seu poder de mídia. No entanto, assim como na Venezuela, ele falhou. Como o Ocidente estava implorando por outro evento da Praça Tiananmen, as sociedades socialistas/comunistas estão restringindo o Ocidente de forma extremamente eficaz agora. Chega dessa besteira da Guerra Fria de derrubar ditadores por meio da CIA e da agitação. O povo permanecerá forte contra tais mentiras, engano e manipulação.

Os protestos de HK deveriam ser contra os corporativistas, capitalistas e infiltrados. Não a China.

Fontes:
- [South China Morning Post | Hong Kong tops the table as world’s most expensive housing market for 9th straight year](https://www.scmp.com/business/article/2182980/nothing-be-proud-hong-kong-tops-table-worlds-most-expensive-housing-market)
- [South China Morning Post | Why the wealth gap? Hong Kong’s disparity between rich and poor is greatest in 45 years, so what can be done?](https://www.scmp.com/news/hong-kong/society/article/2165872/why-wealth-gap-hong-kongs-disparity-between-rich-and-poor)
- [South China Morning Post | Causeway Bay retail landlord offers to cut rent by 44 per cent as Prada closes flagship store next year amid dwindling foot traffic](https://www.scmp.com/business/article/3023267/causeway-bay-retail-landlord-offers-cut-rent-44-cent-prada-closes-flagship)
- [Hong Kong Free Press | Hong Kong’s wealth gap is a much greater threat to its security than separatists](https://hongkongfp.com/2018/10/06/hong-kongs-wealth-gap-much-greater-threat-security-separatists/)
- [National Geographic | Life Inside Hong Kong’s ‘Coffin Cubicles’](https://www.nationalgeographic.com/photography/article/hong-kong-living-trapped-lam-photos)
- [The Guardian | My week in Lucky House: the horror of Hong Kong's coffin homes](https://www.theguardian.com/world/2017/aug/29/hong-kong-coffin-homes-horror-my-week)
- [South China Morning Post | Cage homes 'worse than living on street'](https://www.scmp.com/article/968615/cage-homes-worse-living-street)
- [Greater China Journal | Hong Kong Housing Problem – From The 1950s To The Present](https://china-journal.org/2017/11/16/hong-kong-housing-problem-from-the-1950s-to-the-present/)
- [Trade Finance Global | Hong Kong’s Housing Problem](https://www.tradefinanceglobal.com/posts/hong-kong-housing-problem/)
- [Land for Hong Kong | Insufficient Land Supply leading to Imbalance in Supply-Demand](https://web.archive.org/web/20200213070103/https://www.landforhongkong.hk/en/demand_supply/index.php)
- [Civil Engineering and Development Department of Hong Kong | Land Usage Distribution in Hong Kong](https://www.cedd.gov.hk/filemanager/eng/content_954/Info_Sheet2.pdf)
- [Al Jazeera | A lack of affordable housing feeds Hong Kong’s discontent](https://www.aljazeera.com/economy/2019/08/11/a-lack-of-affordable-housing-feeds-hong-kongs-discontent/)
