# Hong Kong e o ensino de mandarim
Em Hong Kong há muito ativismo contra o ensino de mandarim/普通话. Dizem que estão tentando "apagar" o ensino de cantonês. Essa propaganda também é reproduzida no ocidente, para dizer que a China está colonizando Hong Kong. Isso tá bem longe dos fatos.

Até 1997, apenas 20% das escolas secundárias em Hong Kong ensinavam cantonês. Inglês era a língua obrigatória. Com o governo chinês de volta ao poder, em 1998, o número de escolas que ensinam o cantonês passou pra 75%.

Mandarim também é língua obrigatória, o que é natural. É a língua padrão do país. A comunicação nacional é feita em mandarim. Não há "apagamento" do cantonês, o que há é uma educação do mandarim em todo o território chinês como língua universal na China. Ninguém é reprimido por se comunicar em outra língua.

De fato, há 14 línguas oficiais na China, todas reconhecidas por lei. A questão do mandarim é a questão da língua universal falada nacionalmente.

O Brasil, por exemplo, tem várias línguas faladas em todo o território. Mas há só duas línguas oficiais: o português e libras. E eu não preciso dizer que libras sequer é ensinada na maior parte das escolas do país.

Então essa crítica de que o mandarim de alguma forma esteja apagando o cantonês é completamente infundada. O que aconteceu foi justamente o contrário. O cantonês adquiriu um protagonismo que não tinha na Hong Kong britânica, quando o inglês era a única língua oficial da ilha.
