# Desbancando histórias sobre o sistema de crédito social chinês
Como a blitz da mídia contra o sistema de crédito social da China está em pleno andamento mais uma vez, vamos examinar criticamente as reportagens feitas por todos os meios de comunicação tradicionais. Lembre-se de que tudo neste tópico ainda é verdade e as publicações estão incluindo invenções completas em suas histórias, mais notavelmente aquela peça que circulou esta semana. O governo não está penalizando você por comprar cerveja. Mas isso é coisa padrão, já desmascarada. As pessoas que acreditam nesse absurdo acreditarão, não importa o que aconteça. Estou mais interessado nas fontes dessas histórias. Ou, para ser mais preciso, sua fonte.

Quase todos os artigos sobre o sistema de crédito social que vi incluíram a história de Liu Hu. Ele afirma ter sido colocado em uma lista negra de crédito e foi citado bem mais de uma dúzia de vezes nos principais relatórios da mídia sobre o sistema. Aqui estão os princípios básicos. Um tribunal ordenou que Liu pagasse uma multa após perder um caso de difamação. Nesta versão, o pagamento era exigido se ele não se desculpasse publicamente.
>Liu Hu passou duas décadas pressionando duramente os limites da censura na China. Jornalista talentoso, ele usou um blog para acusar funcionários de alto escalão de corrupção e irregularidades e para publicar detalhes de má conduta das autoridades.
>
>No final de 2013, ele foi preso e acusado de "fabricar e espalhar boatos". No final de 2016, em um caso separado, um tribunal o considerou culpado de difamação e ordenou que ele se desculpasse por sua conta nas redes sociais, que na época tinha 740.000 seguidores. Se ele não quisesse fazer isso, disse o tribunal, ele poderia dar o veredicto em uma agência de notícias autorizada. Liu pagou ao tribunal $115, uma quantia que ele acredita que cobriria os custos de publicação.
>
>Então, ele disse, o juiz disse a ele que o veredicto inteiro precisava ser publicado, a um custo de pelo menos $1.330.

Aqui está outra versão. Desta vez, o pagamento não foi recebido. Nenhuma menção a um aumento nas taxas.
>Em 2013, o jornalista investigativo Liu Hu publicou um artigo alegando que alguém era um extorsionário. 2 anos depois, o homem processou com sucesso o Sr. Liu por difamação. O tribunal disse que apresentaria um pedido de desculpas em um jornal em seu nome e às suas custas. Ele transferiu dinheiro para o tribunal, mas meses depois o Sr. Liu descobriu abruptamente que não poderia comprar passagens de avião. Só então um oficial do tribunal disse que o pagamento não havia sido recebido. O Sr. Liu corrigiu imediatamente o erro.

Neste, não há menção a nenhuma multa ou taxa. Apenas uma reclamação de que o tribunal considerou seu pedido de desculpas "falso".
>Quando Liu Hu recentemente tentou reservar um voo, disseram-lhe que ele foi proibido de voar porque estava na lista de pessoas não confiáveis. Liu é um jornalista que foi condenado por um tribunal a se desculpar por uma série de tweets que escreveu e, em seguida, foi informado que suas desculpas eram falsas.
>
>"Não posso comprar uma propriedade. Meu filho não pode ir para uma escola particular", disse Liu. "Você sente que está sendo controlado pela lista o tempo todo."

Aqui, a taxa inicial foi de $1.400, em vez dos $115 informados em outros lugares. O escalonamento (taxa) ainda acontece, dando-nos três taxas diferentes e totais de multas, dependendo do relatório que você está lendo.
>O jornalista Liu Hu foi processado em 2015 por repostagem de uma mensagem nas redes sociais. O tribunal determinou que o material era difamatório e ordenou que Liu pagasse uma indenização de $1.400 mais outras taxas. Mesmo assim, Li é considerado um dos que tiveram mais sorte.
>
>“Eu transferi o dinheiro para a conta errada, então o tribunal não recebeu o dinheiro. Ninguém ali me contou. Então o tribunal me colocou na lista negra”, disse Liu.
>
>Ele corrigiu o erro, mas o juiz disse que o valor precisava ser aumentado em pelo menos 1.000 dólares.

Outro relatório faz a reclamação de conta incorreta em vez da reclamação de escalonamento gratuito. Este inclui um apelo pessoal de Liu ao juiz.
>Algumas pessoas já se sentem presas na rede de crédito social da China. O repórter investigativo Liu Hu publicou em 2013 um artigo alegando que alguém era um extorsionário. O homem processou Liu por difamação e venceu. O tribunal ordenou que o jornalista pagasse uma multa, o que ele afirma ter feito. No entanto, quando Liu tentou reservar uma passagem de avião usando um aplicativo de viagens, ele foi notificado de que a transação não poderia ser realizada porque ele havia sido incluído na lista negra da suprema corte.
>
>Ele contatou o tribunal local e soube que havia transferido o dinheiro para a conta errada. Ele se apressou em pagar a multa e enviou ao juiz uma foto de sua transferência. Ele não teve resposta. Posteriormente, por meio de ligações, ele conseguiu se encontrar com o juiz e pleitear sua retirada da lista negra, mas até agora nada aconteceu. Através de uma brecha, Liu pode comprar passagens de avião usando seu passaporte, mas ele sente que não há nada que possa fazer para sair da lista negra. “É indefeso”, diz ele.

Este é semelhante ao anterior. Sem taxas, sem alegações de falta de sinceridade. Um número de conta errado novamente.
>Os chineses que foram considerados não confiáveis ​​estão tendo o primeiro vislumbre do que um sistema unificado pode significar. Um dia, em maio passado, Liu Hu, um jornalista de 42 anos, abriu um aplicativo de viagens para reservar um voo. Mas quando ele inseriu seu nome e número de identidade nacional, o aplicativo o informou que a transação não seria realizada porque ele estava na lista negra do Supremo Tribunal Popular. Essa lista - literalmente, a Lista de Pessoas Desonestas - é a mesma que está integrada ao Zhima Credit. Em 2015, Liu foi processado por difamação pelo assunto de uma história que ele escreveu, e um tribunal ordenou que ele pagasse $1.350. Ele pagou a multa e até fotografou o boleto de transferência bancária e enviou a foto para o juiz do caso. Perplexo sobre o motivo de ainda estar na lista, ele contatou o juiz e soube que, ao transferir sua multa, ele havia digitado o número da conta errada. Ele correu para transferir o dinheiro novamente, para se certificar de que o tribunal o havia recebido. Desta vez, o juiz não respondeu.

A história da ABC retorna ao pedido de aumento de taxas. Nenhuma menção de um número de conta incorreto.
>Liu Hu é um jornalista investigativo que descobriu a corrupção nos altos escalões do Partido e resolveu casos de assassinato em série.
>
>Ele diz que o governo o considera um inimigo.
>
>Hu perdeu seu crédito social quando foi acusado de um crime de fala e agora se encontra excluído da sociedade devido à sua baixa pontuação.
>
>Em 2015, Hu perdeu um caso de difamação depois de acusar um oficial de extorsão.
>
>Ele foi obrigado a publicar um pedido de desculpas e pagar uma multa, mas quando o tribunal exigiu uma taxa adicional, ele se recusou.

Devemos ser muito céticos quando tantos veículos de notícias - mais de uma dúzia, ao que parece - usam a mesma fonte para o que afirmam ser uma experiência de longo alcance em controle social. Devemos ser duplamente céticos quando os fatos do caso dessa fonte mudam tão radicalmente de um relatório para outro. Isso lança dúvidas sobre toda a narrativa quando as informações básicas e fundamentais mudam assim.

O problema é que os princípios básicos da reportagem vão pela janela afora quando se trata de um país como a China. Quando seus leitores acreditarem em qualquer coisa que você diga, graças a décadas de propaganda orientalista, não há necessidade de ser minucioso. Por que se preocupar em acompanhar inconsistências quando sua história sobre China's Black Mirror tem garantia de um milhão de acessos e milhares de comentários crédulos no Facebook como "Uau... assustador!!!"

Fontes:
- [The Globe and Mail | Chinese blacklist an early glimpse of sweeping new social-credit control](https://www.theglobeandmail.com/news/world/chinese-blacklist-an-early-glimpse-of-sweeping-new-social-credit-control/article37493300/)
- [Human Rights Watch | China’s Chilling ‘Social Credit’ Blacklist](https://www.hrw.org/node/312510/printable/print)
- [CBS News | China's behavior monitoring system bars some from travel, purchasing property](https://www.cbsnews.com/news/china-social-credit-system-surveillance-cameras/)
- [Marketplace | Inside China’s "social credit" system, which blacklists citizens](https://www.marketplace.org/2018/02/13/social-credit-score-china-blacklisted/)
- [Foreign Policy News | Life Inside China’s Social Credit Laboratory](https://foreignpolicy.com/2018/04/03/life-inside-chinas-social-credit-laboratory/)
- [Wired | Inside China's Vast New Experiment in Social Ranking](https://www.wired.com/story/age-of-social-credit/)
- [ABC News | Leave no dark corner](https://www.abc.net.au/news/2018-09-18/china-social-credit-a-model-citizen-in-a-digital-dictatorship/10200278?nw=0)
