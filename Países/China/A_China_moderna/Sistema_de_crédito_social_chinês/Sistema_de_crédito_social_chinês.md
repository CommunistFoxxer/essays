# Sistema de crédito social chinês
A República Popular da China está refinando um novo método para a autorregulamentação das empresas privadas por meio da pontuação do Crédito Social.

![](https://pbs.twimg.com/media/E3Lk-viXoAIQqgh?format=png&name=small)

Embora ainda em debate e uma questão de controvérsia; este sistema é denunciado no Ocidente como uma "maquinação orwelliana" também porque foi planejado para ser implementado principalmente por empresas privadas. Esse sistema de controle tem como objetivo fornecer incentivos importantes para que as empresas respeitem a lei e seus funcionários, ao mesmo tempo em que evita a corrupção e a má gestão nas estatais. A história que circula sobre a China proibindo pessoas de andarem aviões e trens por causa do "crédito social" é outro caso de notícias falsas que atacam a China e que são veiculadas pela mídia ocidental com pouca investigação. 

Alguns pontos que gostaria de ressaltar:
1. As pessoas que estão sendo restringidas são principalmente [pessoas de negócios que infringiram a lei](https://www.nytimes.com/2017/12/13/business/china-blacklist-jia-yueting-leeco.html), se recusam a pagar multas, etc. [2](https://www.nytimes.com/2017/12/13/business/china-blacklist-jia-yueting-leeco.html). Se você quiser ler a lista de pessoas na lista negra, o governo chinês a publica publicamente.
2. As restrições são especificamente sobre luxos, não sobre qualquer coisa essencial para a vida cotidiana.

Como cita um [artigo](https://chinacopyrightandmedia.wordpress.com/2016/09/25/opinions-concerning-accelerating-the-construction-of-credit-supervision-warning-and-punishment-mechanisms-for-persons-subject-to-enforcement-for-trust-breaking/) sobre o caso:
>Restringir as pessoas sujeitas à execução por quebra de confiança e o representante legal, as principais pessoas responsáveis, as pessoas que realmente controlam, bem como as pessoas diretamente responsáveis que influenciam o desempenho da dívida em pessoas sujeitas à execução por quebra de confiança por andarem com travessas macias em trens, todos os assentos em conjuntos de trens de classe G e todos os assentos de primeira classe em outros conjuntos de trens, aeronaves civis e outros consumíveis que não sejam necessários para a vida ou para o trabalho.

Os artigos acima também listam hotéis de 3 estrelas ou mais, assentos de primeira classe em trens de alta velocidade, etc. Pense por um segundo que classe está sendo afetada por essa restrição, quem são as pessoas que usam esses serviços?

Esperançosamente, isso ajudará a compreender o que está acontecendo na China:
1. Não existe uma "pontuação única" que se aplique a indivíduos. A mídia ocidental confunde os números de identificação usados no sistema com uma pontuação.
2. O sistema está focado principalmente em empresas e corp. que enganam as pessoas ou evitam a corrupção, a proteção ao consumidor ou as leis ambientais.
3. Quando a lei se aplica a pessoas físicas, elas são aplicadas apenas a pessoas que já foram condenadas por crimes como fraude, ameaças de bomba, difamação, sonegação fiscal e corrupção.
4. As penas incluem a impossibilidade de adquirir bilhetes de avião ou de comboio de 1ª classe até um ano e estas penas só podem ser aplicadas após condenação e esgotamento dos recursos e o tribunal provar que o culpado pode pagar a multa, mas recusa.
5. A pena é suspensa assim que o culpado tentar pagar a multa.
6. O Sistema de Crédito Social é totalmente diferente do sistema "Crédito Sésamo". A mídia ocidental confunde esses dois o tempo todo.

Primeiro, algumas informações básicas sobre por que o sistema existe. Na China, um sistema do tipo FICO, como o que existe nos Estados Unidos, não seria muito útil, já que pouquíssimos chineses são detentores de dívidas. A maioria das pessoas não pega empréstimos para grandes compras; eles economizam em vez disso.

Além disso, e provavelmente o mais importante, antes do sistema existir, não havia um conjunto unificado de consequências para as empresas que deixavam de pagar multas, cumprir acordos ou geralmente obedecer a padrões de conduta aceitáveis. A menos que a ofensa fosse criminosa, poucas punições existiam.
