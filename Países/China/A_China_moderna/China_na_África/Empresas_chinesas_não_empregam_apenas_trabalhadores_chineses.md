# Empresas chinesas não empregam apenas trabalhadores chineses
O ex-presidente Barack Obama e seu vice-presidente Joe Biden apontaram para a China durante a reunião Africa-EUA de 2014 reivindicando esse ponto, que foi repetido muitas vezes desde então. Joe Biden disse:
>A América está orgulhosa da medida que nossos investimentos na África andam de mãos dadas com nossos esforços de contratar e treinar os moradores locais para promover o desenvolvimento econômico e não apenas extrair o que está no chão.

O Secretário de Estado John Kerry prosseguiu com uma questão retórica:
>Quantos chineses vieram fazer o trabalho?

Essas acusações condizem com a realidade?

Depois de pesquisar 1,000 empresas chinesas na África, a consultoria da McKinsey observou:
>89% dos funcionários eram africanos, somando mais de 300,000 empregos para os trabalhadores africanos. Escalados em todas as 10,000 empresas chinesas na África, esses números sugerem que os negócios chineses já empregam vários milhões de africanos.

De acordo com uma pesquisa conduzida pela China-Africa Research Initiative (Iniciativa de Pesquisa China-África):
>Os habitantes locais são mais de 4/5 dos funcionários em 400 empresas e projetos chineses, perpassando mais de 40 países africanos. [...] pesquisas de emprego em projetos chineses na África mostram, repetidamente, que 3/4 ou mais dos trabalhadores são, de fato, locais.

Os fatos simplesmente não sustentam esse ponto principal da mídia mainstream sobre as relações sino-africanas. Existem, sem dúvidas, injustiças e abusos em locais de trabalho de negócios pertencentes a cidadãos chineses – assim como de capitalistas de qualquer outra nação que faz negócios na África; isso se verifica sempre que o capital e o trabalho se confrontam com interesses irreconciliáveis.

Além disso, o discurso em torno desta questão trata da capacidade dos governos africanos de restringir ou reduzir as proporções de contratações locais e de gerentes chineses. Nos casos de Angola e da República Democrática do Congo, por exemplo, o nível de contratações chinesas depende das políticas do governo local, e não das políticas chinesas.
