# Alegações de que Friedman aconselhou a economia da China
A China, após a ascensão ao poder de Deng Xiaoping, iniciou uma série de reformas para direcionar setores de sua economia para o mercado e o comércio internacional. Nesse contexto, Friedman e outros economistas dão palestras nas chamadas "zonas especiais", como Xangai.

Em 1988, Zhao Ziyang, que foi o secretário-geral mais popular do PCCh na história da RPC, realizou uma reunião com Milton Friedman. Se Zhao Ziyang aplicaria ou não qualquer medida em relação a qualquer recomendação (certamente sobre a inflação e nada mais) é irrelevante, já que em 1990 ele seria removido como contra-revolucionário (por causa do episódio em Tiananmen) pelo resto do PCCh.
