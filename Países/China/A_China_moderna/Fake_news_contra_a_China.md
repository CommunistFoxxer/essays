# Fake news contra a China
Eric Feigl-Ding é muito sensacionalista. Qualquer notíciazinha, um dado sem contexto, e ele [aumenta em 10 vezes](https://undark.org/2020/11/25/complicated-rise-of-eric-feigl-ding/). Ele já virou uma piada entre os próprios pares.

Já vi um vídeo dizendo que uma médica tinha sido presa por divulgar coisas sobre a pandemia. Na verdade, ela estava em uma ala especial do hospital pois ela tinha pegado COVID e tinha que ficar longe da família.

Já vi um vídeo de uma suposta chinesa comendo morcego, mas na verdade era em Macau. Esse tipo de comércio é proibido na China e é mais fácil você conseguir nos EUA do que na China (como diversos YouTubers fazem todos os anos).

Uma vez recebi um suposto vídeo da China executando cristãos. Eram 2 vídeos, na verdade. O primeiro era uma campanha anti-corrupção em Pequim, e o segundo era uma mulher condenada a morte por matar os pais junto das amigas.

Também outro suposto professor que foi preso por criticar Xi Jinping. Mas ele foi preso 6 meses depois de criticar. Por que as autoridades demorariam tanto? Simples: porque ele tinha sido preso por abusar de meninas.

Já vi vídeo de uma mulher saudando e dançando em volta das fotos de Mao, bolsonaristas disseram que ela estava sendo doutrinada. Na verdade ela fazia parte dos Falun Gong, grupo """religioso""" (só que não) contra o governo.
