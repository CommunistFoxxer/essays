# O grande salto para frente
Stalin, em 1931, declarou que seu país estava 100 anos atrasado em relação a outros países avançados e que a lacuna deveria ser fechada em 10 anos para que não fossem destruídos pelo inimigo. Quase profeticamente, a Alemanha invade a União Soviética em 1941. Mas, àquela altura, graças aos muito bem-sucedidos Planos Quinquenais, a URSS deixara de ser o homem doente da Europa para se tornar o maior produtor de aço e industrial do mundo.

Mais tarde, em 1958, Mao anuncia o Grande Salto para a Frente, no qual planejam superar a produção industrial da URSS e da Inglaterra em um curto período de tempo igualmente aparentemente ridículo. E na maior parte, falha miseravelmente. Porque? Alguns dizem que as promessas de Mao estavam muito distantes da realidade, mas essas mesmas promessas não foram provadas de fato ser possíveis 30 anos antes na União Soviética? Outros dizem que a China era muito pobre e atrasada, mas a União não estava em um estado semelhante em 1930?

Eu não diria que "falhou miseravelmente", mas que as condições se agravaram com uma política ambiciosa demais em tão pouco tempo, e coincidindo com o mau tempo que realmente afetou o Grande Salto para a Frente e levou à fome. Lembre-se de que as fomes eram uma ocorrência comum entre 108 aC e 1911 dC, havia nada menos que 1.828 fomes registradas na China, ou quase uma vez por ano em uma província ou outra. Com quatro fomes ocorrendo durante a era KMT. Notavelmente, depois da fome durante o GSF, nunca mais houve fome na China desde então.

Antes do GSF, que era seu Segundo Plano Quinquenal, a China experimentou seu Primeiro Plano Quinquenal (1953-1957), que foi, sem dúvida, um grande sucesso. A produção agrícola e industrial aumentou anualmente para 18,7% em 1957. (A Índia em condições semelhantes atingiu uma taxa de crescimento de menos de 2% durante os anos 1950). A produção industrial cresceu 83%, a renda nacional cresceu a uma taxa média anual de 8,9%, a expectativa expectativa aumentou de 36 para 57 anos, e a frequência à escola primária dobrou. Isso foi feito por meio de planejamento central e investimento na indústria com a ajuda da URSS, que forneceu maquinário, comércio e treinamento.

As condições para o segundo plano de cinco anos eram diferentes. Mao Zedong testemunhou Khrushchev denunciar Stalin e as revoltas internas resultantes que enfraqueceram severamente a União Soviética. Para evitar o enfraquecimento da China, Mao lançou a campanha Anti-Direita que tinha como alvo os liberais e dissidentes do PCC. Esse par com o fato de Mao ver Khrushchev como um revisionista levou à decisão de Mao de se tornar mais autossuficiente e não depender da União Soviética (o que foi difícil porque a China foi sancionada e bloqueada pelo resto do mundo).

O plano para o Segundo Plano Quinquenal ou Grande Salto para a Frente era diferente do primeiro porque, em vez de planejamento central, Mao descentralizou o governo (no início de 1958, existiam 26.500 comunas), resultando em uma grande diminuição do controle econômico. Isso, junto com o desejo de Mao de se tornar autossuficiente vindo da URSS e sem controle econômico centralizado, tornou-se mais difícil se adaptar aos desequilíbrios da política.

A própria política teve problemas por três motivos principais:
1. O governo desviou muito do setor agrícola para a produção industrial. (A força de trabalho agrícola já foi reduzida em 38 milhões entre 1957-1958.)
2. Aquisição excessiva de grãos que não foi responsável pela redução da produção de grãos.
3. Mau tempo reduzindo a colheita de alimentos disponível e coincidindo com o colapso da produção de grãos. Em 1961, Mao corrigiu o curso e mandou 20 milhões de volta ao campo, centralizou a economia, implementou um programa de estabilização de compras e reestruturou e consolidou comunas.

As pessoas ignoram os sucessos do Grande Salto para a Frente. Nos três anos, a produção de carvão aumentou 36% e os têxteis 30%, a geração de eletricidade aumentou 26% e os ativos fixos nacionais 40%. Construiu um sistema de irrigação de reservatórios (nove dos dez maiores reservatórios da China hoje construídos), represas e canais para a agricultura prepararam a população que dobrou em apenas um quarto de século. A descoberta do campo de petróleo Daqing tornou-se a tábua de salvação para a indústria chinesa. O sistema de comunas fez com que mais mulheres ingressassem na força de trabalho. A produção de aço em 1958 atingiu 11 milhões de toneladas, de mais de 5,35 milhões de toneladas em 1957. Em 1959, atingiu 13,4 milhões de toneladas e chegou a 18,7 milhões de toneladas em 1960.

Fontes:
- O curso de curta duração de Stalin e a transformação econômica socialista de Mao. Li, Huayu.
- Oito Crise: Lições da China, 1949-2009 - Wen Tiejun's
- A ECONOMIA CHINESA: Transições e Crescimento - Barry Noughten
h Lições históricas da Revolução Cultural da China - Cynthia Lai
- O grande salto em frente: anatomia de um desastre de planejamento central - Wei Li e Dennis Tao Yang
- “Recuperação econômica e o primeiro plano de cinco anos.” - Lardy, Nicholas R
- “On the 10 Major Relationships.” Mao Zedong
