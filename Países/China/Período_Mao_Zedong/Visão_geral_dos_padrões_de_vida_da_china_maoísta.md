# Visão geral dos padrões de vida da China maoísta
NÃO, a China não começou a crescer após a abertura econômica de Deng em 1978. De 1949 à 1976, a expectativa de vida chinesa aumentou em 31 anos, mais que um ano por ano cronológico. Só para fins de comparação, a Índia após a independência de 47, aumentou sua expectativa de vida em 19 anos. Os índices da Índia não são negativos, o da China que é extraordinário. De 1949 à 1978, teve expectativa de vida maior que os EUA (0,9%), Reino Unido (1%) e Japão (1,3%). A China teve um aumento de 2,3% ao ano. Os números de pobreza calculado pela ONU, que mostra os critérios de pobreza de menos que $1,90 por dia, reajustado aos diferentes padrões de preços de cada país, em 1981, 2 bilhões de pessoas viviam na pobreza, em 2010, esse número caiu para 1.000.120.000. O número de pobreza foi reduzido em 880 milhões, e 728 milhões eram da China. 30 milhões dessas pessoas também eram do Vietnã, país também socialista. Os países socialistas juntos contribuíram 85% para a redução da pobreza, enquanto os capitalistas apenas 15%.

Nossa fonte principal nesta seção será um estudo aprofundado conduzido por Amartya Sen, Professor de Economia na Universidade de Harvard e membro do Trinity College na Universidade de Cambridge. Sen recebeu o Prêmio Nobel de Ciências Econômicas por seu trabalho comparando os padrões de vida na República Popular da China (especialmente durante o período maoísta) com os da Índia:
- [Harvard University | Perspectives on the Economic and Human Development of India and China](https://books.google.com/books?id=IZUPXjnGnhEC&printsec=frontcover&dq=inauthor:%22Amartya+Kumar+Sen%22+china+india+famine&hl=en&sa=X&ved=0ahUKEwjGsY2Lx5fjAhVjdt8KHTRIDSoQ6AEIKjAA#v=onepage&q&f=false)

Os resultados do estudo podem ser resumidos pela seguinte observação, na qual Sen discute as conquistas decididamente superiores da China e as atribui diretamente à ideologia socialista do período maoísta:
>Por causa de seu compromisso radical com a eliminação da pobreza e com a melhoria das condições de vida - um compromisso no qual as idéias e ideais maoístas e marxistas desempenharam um papel importante - a China alcançou muitas coisas que a liderança indiana falhou em pressionar e buscar com qualquer vigor. A eliminação da fome generalizada, analfabetismo e problemas de saúde se enquadra solidamente nesta categoria. Quando a ação do Estado opera na direção certa, os resultados podem ser bastante notáveis, como é ilustrado pelas conquistas sociais do período pré-reforma [maoísta].

Outro comentário importante que resume os resultados do estudo é o seguinte:
>Argumentamos, em particular, que as conquistas relacionadas à educação, saúde, reformas agrárias e mudança social no período pré-reforma [maoísta] deram contribuições significativamente positivas para as conquistas do período pós-reforma. Isso não ocorre apenas em termos de seu papel na alta expectativa de vida sustentada e nas conquistas relacionadas, mas também no fornecimento de apoio firme para a expansão econômica com base em reformas de mercado.

Sen afirma aqui que o período maoísta testemunhou enormes aumentos na qualidade de vida do povo chinês, bem como desenvolvimentos econômicos importantes, sem os quais a expansão econômica após as reformas de mercado de 1979 muito provavelmente não teria ocorrido.

Sen observa que durante o período maoísta, uma "redução notável na desnutrição crônica ocorreu", atribuindo isso às políticas socialistas implementadas pelo governo de Mao:
>Os processos casuais pelos quais a redução da desnutrição foi alcançada envolveram ampla ação do Estado, incluindo políticas redistributivas, suporte nutricional e, claro, cuidados de saúde (uma vez que a desnutrição é frequentemente causada por parasitas e outras doenças).

Sen concentra mais atenção nos avanços notáveis ​​na área da saúde durante o período maoísta:
>As conquistas da China no campo da saúde durante o período pré-reforma incluem uma redução dramática da mortalidade infantil e infantil e uma notável expansão da longevidade.

Observa-se também que a expectativa de vida na China aproximadamente dobrou durante o período maoísta, de aproximadamente 35 anos em 1949, a 68 anos em 1981 (quando as reformas de Deng começaram a surtir efeito). Isso é mais elaborado na próxima fonte.

Sobre a questão da educação, Sen observa que as enormes melhorias (incluindo aumentos dramáticos na alfabetização) podem ser atribuídas principalmente ao período maoísta pré-reforma:
>O avanço da China no campo da educação elementar já havia ocorrido antes do início do processo de reforma econômica no final dos anos setenta. Os dados do censo indicam, por exemplo, que as taxas de alfabetização em 1982 para o grupo de 15-19 anos já eram tão altas quanto 96% para homens e 85% para mulheres.

Vamos examinar a questão da saúde pública com mais detalhes.

Outra excelente fonte sobre saúde pública na China maoísta vem do jornal Population Studies, em um estudo conduzido por pesquisadores da Universidade de Stanford e do National Bureau for Economic Research:
- [Population Studies | An Exploration of China's Mortality Decline Under Mao: A Provincial Analysis, 1950-1980](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4331212/)

Um comentário importante é o seguinte:
>O crescimento da expectativa de vida ao nascer da China de 35 a 40 anos em 1949 para 65,5 anos em 1980 está entre os aumentos sustentados mais rápidos na história global documentada.

Isso por si só já mostra os enormes benefícios alcançados pelas políticas socialistas de Mao Zedong. Informações mais importantes são fornecidas no estudo, tratando de recursos hospitalares e médicos:
>O suprimento de médicos e hospitais cresceu dramaticamente sob Mao devido a uma variedade de fatores (incluindo aumentos no financiamento do governo, a introdução de seguro social para funcionários públicos urbanos e o lançamento do Sistema Médico Cooperativo Rural da China em meados da década de 1950). Os Esquemas Médicos Cooperativos Rurais (CMS) foram vigorosamente promovidos e se espalharam no final dos anos 1960 como parte da Revolução Cultural.

O estudo confirma a análise de Sen da educação:
>A China fez grandes avanços na educação primária e secundária com Mao.

Ele também cita outra pesquisa que descobriu que os ganhos rápidos na saúde chinesa podem ser atribuídos às políticas socialistas específicas implementadas:
>O declínio da mortalidade na China entre 1953 e 1957, que se assemelha ao dos EUA entre 1900 e 1930, foi "principalmente devido à organização social única das práticas de saúde pública chinesas".

Observe que a China alcançou em quatro anos o que os Estados Unidos levaram trinta anos para realizar, devido aos seus sistemas diferentes (ou seja, socialismo versus capitalismo). O estudo também confirma o imenso sucesso dos programas de vacinação maoísta:
>Esforços sistemáticos para vacinar a população contra poliomielite, sarampo, difteria, tosse convulsa, escarlatina e cólera foram rápidos e supostamente bem-sucedidos (a China quase erradicou a varíola em apenas três anos, com os últimos casos documentados ocorrendo no Tibete e em Yunnan, em 1960).

Citações adicionais para as reivindicações nas citações acima são fornecidas no estudo original.
