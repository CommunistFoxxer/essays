# Burkina Faso
Dentre todos os socialistas, talvez aquele mais amado (ou menos odiado) pelos liberais seja Thomas Sankara, frequentemente referido como "Che Guevara da África", um exímio cristão.

Sankara era um capitão do exército do Alto Volta, uma ex-colônia francesa na África Ocidental. Ele foi fundamental no golpe que destituiu o coronel Saye Zerbo da presidência em 1982, tomou o poder do major Jean-Baptiste Ouedraogo em uma luta interna pelo poder e tornou-se presidente em agosto de 1983. Desde então, ele adotou políticas radicais de esquerda e buscou reduzir a corrupção governamental, mudou o nome do país de Alto Volta para Burkina Faso, que significa “a terra dos homens íntegros” e foi morto em circunstâncias misteriosas por um grupo de soldados em outubro de 1987.

A liderança visionária de Sankara transformou seu país de uma sonolenta nação da África Ocidental com a designação colonial de Alto Volta a um dínamo do progresso sob o orgulhoso nome de Burkina Faso ("Terra do Povo Honrado"). Ele liderou um dos programas mais ambiciosos de reformas radicais já vistos na África. Procurou reverter fundamentalmente as desigualdades sociais estruturais herdadas da ordem colonial francesa.

Sankara concentrou os recursos limitados do Estado na maioria marginalizada no campo. Quando a maioria dos países africanos dependia de alimentos importados e assistência externa para o desenvolvimento, Sankara defendeu a produção local e o consumo de produtos feitos localmente. Ele acreditava firmemente que era possível para os burquinenses, com muito trabalho e mobilização social coletiva, resolver seus problemas: principalmente a escassez de alimentos e água potável.

Na Burkina de Sankara, ninguém estava acima do trabalho agrícola ou das estradas de cascalho - nem mesmo o presidente, ministros do governo ou oficiais do exército. A educação intelectual e cívica foi sistematicamente integrada ao treinamento militar e os soldados foram obrigados a trabalhar em projetos de desenvolvimento da comunidade local.

Sankara desdenhou a pompa formal e baniu qualquer culto à sua personalidade. Ele podia ser visto casualmente andando pelas ruas, correndo ou esgueirando-se visivelmente no meio da multidão em um evento público. Ele era um orador empolgante que falava com franqueza e clareza incomuns e não hesitava em admitir erros publicamente, castigar camaradas ou expressar objeções morais a chefes de nações poderosas, mesmo que isso o colocasse em perigo. Por exemplo, ele criticou o presidente francês François Mitterand durante um jantar oficial por receber o líder do Apartheid na África do Sul. 

Aqui algumas dos feitos de Sankara:
- Ele vacinou 2,5 milhões de crianças contra meningite, febre amarela e sarampo em questão de semanas.
- Ele iniciou uma campanha nacional de alfabetização, aumentando a taxa de alfabetização de 13% em 1983 para 73% em 1987.
- Ele plantou mais de 10 milhões de árvores para prevenir a desertificação.
- Ele construiu estradas e uma ferrovia para unir a nação, sem ajuda externa.
- Ele nomeou mulheres para altos cargos governamentais, encorajou-as a trabalhar, recrutou-as para o serviço militar e concedeu licença-gravidez durante os estudos.
- Ele proibiu a mutilação genital feminina, os casamentos forçados e a poligamia em apoio aos direitos das mulheres
- Ele vendeu a frota do governo de carros Mercedes e fez do Renault 5 (o carro mais barato vendido em Burkina Faso naquela época) o carro de serviço oficial dos ministros.
- Reduziu os salários de todos os servidores públicos, inclusive os seus, e proibiu o uso de motoristas do governo e passagens aéreas de 1ª classe.
- Ele redistribuiu as terras dos senhores feudais e as deu diretamente aos camponeses. A produção de trigo aumentou em três anos de 1700 kg por hectare para 3800 kg por hectare, tornando o país autossuficiente em alimentos.
- Ele se opôs à ajuda externa, dizendo que “Quem te alimenta, te controla”.
- Ele falou em fóruns como a Organização da Unidade Africana contra a penetração neocolonialista contínua da África por meio do comércio e das finanças ocidentais.
- Sankara achou que as garotas deveriam ter a mesma educação que os meninos.
- Ele apelou a uma frente única das nações africanas para repudiar a sua dívida externa. Ele argumentou que os pobres e explorados não têm a obrigação de devolver dinheiro aos ricos e exploradores.
- Em Ouagadougou, Sankara converteu a loja de abastecimento do exército em um supermercado estatal aberto a todos (o primeiro supermercado do país).
- Caçou o imperialismo francês e retirou o Burkina Faso do FMI.
- Ele forçou os funcionários públicos a pagar o salário de um mês para projetos públicos.
- Ele se recusou a usar o ar condicionado de seu escritório, alegando que esse luxo só estava disponível para um punhado de burkinabes.
- Como presidente, ele reduziu seu salário para $450 por mês e limitou seus bens a um carro, quatro bicicletas, três guitarras, uma geladeira e um freezer quebrado.
- Ele próprio um motociclista, ele formou uma guarda pessoal de motociclistas só para mulheres.
- Ele exigia que os funcionários públicos usassem uma túnica tradicional, tecida com algodão burkinabe e costurada por artesãos burquinenses. (A razão é confiar na indústria e identidade locais, ao invés da indústria e identidade estrangeiras).
- Quando questionado por que ele não queria que seu retrato fosse pendurado em lugares públicos, como era a norma para outros líderes africanos, Sankara respondeu: "Há sete milhões de Thomas Sankaras."
- Um guitarrista talentoso, ele escreveu o novo hino nacional sozinho.
- Uma vida muito produtiva que foi brutalmente interrompida em 15 de outubro de 1987, crime patrocinado e orquestrado pelas potências ocidentais (serviço secreta da França, CIA e seu vice-presidente mais especificamente) porque se recusou a se ajoelhar e trair o seu país e a sua população.

Tudo isso sem ler Marx, apenas Lenin! Isso prova o quanto a prática e teoria leninista são poderosas!

Em 15 de outubro de 1987, Sankara foi morto por um grupo armado com doze outros oficiais em um golpe de Estado organizado por seu ex-colega Blaise Compaoré. A deterioração das relações com os países vizinhos foi um dos motivos invocados, tendo Compaoré afirmado que Sankara prejudicou as relações externas com a ex-potência colonial França e a vizinha Costa do Marfim. O príncipe Johnson, um ex-senhor da guerra liberiano aliado de Charles Taylor, disse à Comissão da Verdade e Reconciliação da Libéria (TRC) que foi arquitetado por Charles Taylor. Após o golpe e embora Sankara fosse conhecido por estar morto, alguns CDRs montaram uma resistência armada ao exército por vários dias.

O corpo de Sankara foi desmembrado e ele foi rapidamente enterrado em uma sepultura sem marca, enquanto sua viúva Mariam e dois filhos fugiram do país. Compaoré reverteu imediatamente as nacionalizações, anulou quase todas as políticas de Sankara, reuniu-se ao Fundo Monetário Internacional e ao Banco Mundial para trazer fundos "desesperadamente necessários" para restaurar a economia "despedaçada", e, por fim, rejeitou a maior parte do legado de Sankara. A ditadura de Compaoré permaneceu no poder por 27 anos até ser derrubada por protestos populares em 2014.
