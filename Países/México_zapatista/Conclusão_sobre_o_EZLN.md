# Conclusão sobre o EZLN
O EZLN conseguiu inúmeras grandes conquistas desde sua revolta em 1994; a recente expansão de seu território dá esperança de que sua revolução cresça e melhore. Embora ainda não tenham conseguido estabelecer o socialismo desenvolvido, suas conquistas já são suficientes para garantir-lhes um lugar na história dos grandes movimentos revolucionários proletários e antiimperialistas.

Fontes:
- [The Nation | The Zapatista Revolution is Not Over](https://www.thenation.com/article/zapatista-chiapas-caracoles/)
- [Enlace Zapatista | Political Economy from the Perspective of the Communities: Words of Subcomandante Insurgente Moises](http://enlacezapatista.ezln.org.mx/2015/05/24/political-economy-from-the-perspective-of-the-communities-i-words-of-subcomandante-insurgente-moises-may-4-2015/)
- [National University of Ireland, Cork | Understanding Zapatista Autonomy: An Analysis of Healthcare and Education](https://www.academia.edu/33163010/Understanding_Zapatista_Autonomy_An_Analysis_of_Healthcare_and_Education)
- [World Health Organisation (WHO) | Health and Autonomy: The Case of Chiapas](https://www.who.int/social_determinants/resources/csdh_media/autonomy_mexico_2007_en.pdf)
- [Waging Nonviolence | Zapatista Women Inspire Fight Against Patriarchy](https://wagingnonviolence.org/2018/04/zapatista-women-inspire-fight-against-patriarchy/)
- [Indigenous Anarchist Federation | Zapatista Response to "The EZLN is NOT Anarchist"](https://iaf-fai.org/2019/05/05/a-zapatista-response-to-the-ezln-is-not-anarchist/)
