# Desenvolvimento econômico e padrão de vida
Antes do estabelecimento da República Popular da Hungria, a Hungria era uma nação semifeudal, com muito pouco desenvolvimento industrial e notavelmente baixa qualidade de vida. No entanto, apesar das condições turbulentas, o sistema socialista ainda conseguiu desenvolver rapidamente a economia da Hungria. De acordo com a Divisão de Pesquisa Federal dos EUA:
>Apesar da guerra, depressão, revolução, ocupação estrangeira e períodos de quase caos, a economia da Hungria avançou no século XX de um estado quase feudal para um estágio de desenvolvimento industrial de nível médio.

As notas da Enciclopédia Britânica:
>Historicamente, antes da Segunda Guerra Mundial, a Hungria era principalmente agrária. A partir de 1948, uma política de industrialização forçada baseada no padrão soviético mudou o caráter econômico do país. Foi introduzida uma economia de planejamento centralizado e milhões de novos empregos foram criados na indústria (principalmente para mulheres) e, posteriormente, nos serviços.

Isso é especialmente impressionante quando se considera a grave falta de recursos naturais que a economia húngara enfrenta, o que a forçou a depender quase inteiramente das importações soviéticas. A Divisão de Pesquisa Federal dos EUA declara:
>A falta geral de matérias-primas no país tornou necessário o comércio exterior, uma preocupação que dominou as políticas econômicas dos governos húngaros desde 1918, quando o país perdeu grande parte do território que detinha antes da Primeira Guerra Mundial [...] A União Soviética era o principal fornecedor de matérias-primas da Hungria.

Imediatamente após o estabelecimento da República Popular da Hungria, a nação instituiu uma economia planejada de estilo soviético tradicional. No entanto, no final da década de 1960, um plano de reforma econômica (conhecido como Novo Mecanismo Econômico, ou NEM) foi instituído, o que restaurou de alguma forma o papel das forças de mercado, ao mesmo tempo que retinha a propriedade estatal sobre os meios de produção, distribuição e troca.

A década de 1960 viu o estado se concentrando na expansão da indústria. De acordo com a Divisão de Pesquisa Federal dos EUA:
>Durante a década de 1960, o governo deu alta prioridade à expansão dos ramos de engenharia e química do setor industrial. A produção de ônibus, máquinas-ferramenta, instrumentos de precisão e equipamentos de telecomunicações recebeu a maior atenção no setor de engenharia. O setor químico concentrava-se na produção de fertilizantes artificiais, plásticos e fibras sintéticas. Os mercados húngaro e Comecon eram os principais alvos do governo, e as políticas resultaram no aumento das importações de energia, matérias-primas e produtos semiacabados.

Após este período, a economia mudou o foco para a produção de consumo com a introdução do NEM:
>Em meados da década de 1960, o governo percebeu que a política de expansão industrial que vinha seguindo desde 1949 não era mais viável. Embora a economia estivesse crescendo de forma constante e o padrão de vida da população estivesse melhorando, fatores-chave limitaram o crescimento... O governo introduziu o NEM em 1968 para melhorar a eficiência da indústria e tornar seus produtos mais competitivos nos mercados mundiais.

Esta época foi marcada por altas taxas de crescimento econômico. A Hungria atingiu o nível de uma nação de desenvolvimento médio:
>De 1968 a 1972, o NEM e um ambiente econômico favorável contribuíram para o bom desempenho econômico. A economia cresceu de forma constante, nem o desemprego nem a inflação eram aparentes, e a balança de pagamentos em moeda conversível do país estava em equilíbrio, pois as exportações para os mercados ocidentais cresceram mais rapidamente do que as importações. As fazendas e fábricas cooperativas aumentaram rapidamente a produção de bens e serviços que faltavam antes da reforma. Por volta de 1970, a Hungria alcançou o status de país de desenvolvimento médio. Sua indústria estava produzindo de 40% a 50% do produto interno bruto, enquanto a agricultura contribuía com menos de 20%.

Em meados da década de 1980, a Hungria alcançou um padrão de vida muito alto em comparação com a era pré-comunista. A disponibilidade de alimentos era alta e a seleção era relativamente diversa. Relatórios da Divisão de Pesquisa Federal dos EUA:
>Em 1986, o consumo per capita de carne da Hungria era o mais alto da Europa Oriental, enquanto o consumo de ovos estava entre os mais altos. O consumo per capita de carne, peixe, leite e produtos lácteos, ovos, vegetais, batatas, café, vinho, cerveja e bebidas destiladas aumentaram significativamente entre 1950 e 1984.

Os húngaros também tinham altas taxas de propriedade de vários bens de consumo, e a qualidade dos bens também estava aumentando:
>Em 1984, 96 em cada 100 residências possuíam uma máquina de lavar, todas as residências possuíam uma geladeira e a proporção de aparelhos de televisão para residências era de 108 para 100. A qualidade e a variedade dos bens de consumo duráveis ​​à venda também haviam melhorado.

A Hungria dos anos 80 era auto suficiente e exportava o mesmo tanto que a França.

Apesar dessas boas estatísticas, a economia húngara não estava livre de problemas; um excesso de confiança na ortodoxia stalinista (particularmente nos primeiros anos) fez com que a Hungria se apoiasse fortemente no desenvolvimento da indústria pesada, ignorando outras formas de produção que teriam se adaptado melhor às condições materiais da nação. Além disso, as empresas e fazendas do país sofreram com os baixos níveis de produtividade relativa, o que contribuiu para uma desaceleração do crescimento na década de 1980. A Enciclopédia Britânica resume essas questões:
>Embora a modernização econômica do tipo soviético gerasse rápido crescimento, ela se baseava em um padrão estrutural do início do século 20 e em tecnologia ultrapassada. As indústrias pesadas de ferro, aço e engenharia receberam a mais alta prioridade, enquanto os serviços de infraestrutura e comunicação modernos foram negligenciados. Novas tecnologias e indústrias de alta tecnologia foram subdesenvolvidas e ainda mais prejudicadas pelas restrições ocidentais (o Comitê Coordenador para Controles Multilaterais de Exportação) à exportação de tecnologia moderna para o bloco soviético.

Finalmente, em 1982, a Hungria se tornou a segunda nação do Bloco de Leste a ingressar no FMI. Este foi um erro tremendo, pois várias condições de empréstimo durante a década de 1980 acabaram contribuindo para o colapso do sistema socialista e sua substituição por uma tensão brutal do capitalismo.

No geral, a economia húngara oferece várias lições para socialistas e comunistas modernos:
1. O desenvolvimento econômico deve se concentrar na vantagem comparativa de uma dada nação, ao invés da dependência dogmática da indústria pesada.
2. A produção de bens de consumo e um padrão de vida crescente são essenciais para ganhar (e manter) o apoio do povo.
3. O sistema econômico socialista, quando adequadamente dirigido, é de fato capaz de transformar estados semifeudais em países desenvolvidos. As outras nações do Bloco de Leste (junto com a China) também provam isso.
