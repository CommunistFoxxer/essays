# A revolução húngara
Vários reacionários anteriormente reabilitados (serventes do reino húngaro fascista e da Alemanha Nazista) de vários crimes tentaram tomar o poder em uma contra-revolução. Eles aproveitaram a fragilidade de comunicação do novo regime e fizeram discursos populistas e se disfarçaram de comunistas para atrair as massas.

Neste momento, vários libertários também tentaram dar um golpe contra os comunistas no poder ("burocráticos" e "parasitas", segundo os mesmos). Um erro. Como comunistas, sabemos que nunca devemos nos movimentar junto da classe reacionária (pequeno-)burguesa, ainda mais contra um governo de esquerda com apoio popular! Um puro ato oportunista, que confinou toda a militância e organização do movimento.

"Lealdade" de um dos principais revolucionários da Revolução (um claro oportunista):

![](images/Pál_Maléter_allegiance.png)

Os mesmos realizavam pogroms. Não dá para negar o caráter fascista, anti-semita e oportunista do Levante de 1956.

![](images/hungarian_fascists_1956.png)

Király depois fugiu para os EUA. E curiosamente os EUA trata a Revolução que foi supostamente feita por "comunistas" como um EXEMPLO DE LIBERTAÇÃO NACIONAL. O presidente Bush declarou seu total apoio a revolução. Um apoiador do regime pró-nazista húngaro antes da libertação pelos soviéticos e, claro, lutou ao lado de Hitler na guerra. Após a vitória, ele recebeu o perdão de Stalin e até foi autorizado a entrar no Partido Comunista do país.

![](images/Király_prize.png)

O ex-nazista, ex-comunista e ex-fascista tornou-se um apoiador do Ocidente. Depois de 1991, este covarde, vira-casaca nazista, hipócrita e traidor, retornou para a Hungria, com várias recompensas. Ele foi proclamado “herói” e nomeado o líder da rebelião do comitê de celebração.

Hoje em dia, a Hungria tem um feriado nacional da Revolução, ocorre o processo de renazistificação, os fascistas mergulham novamente no luxo que outrora dispuseram e perseguem qualquer um que tenha [traços de comunismo](https://en.wikipedia.org/wiki/Bans_on_communist_symbols#Hungary).

Nagy, o maior culpado pela revolução que levantou a bandeira do socialismo é hoje tratado como um herói nacional, tendo até mesmo sua própria estátua. Por que um regime anti-comunista apoiaria um comunista?

![](images/Nagy_statue.png)

Todos os líderes da revolução eram apoiadores do antigo regime húngaro. Eles são os ricos, burgueses e proprietários de terras que financiaram a ascensão de Hitler e forneceram tudo o que era necessário para o imperialismo alemão.

Nagy era um fascista disfarçado de comunista. Ele permitiu que as estátuas comunistas fossem vandalizadas, os símbolos comunistas fossem profanados, as bandeiras soviéticas fossem queimadas e até disse que o fazia em nome do socialismo.

Fontes:
- [Marxists - Enver Hoxha | The Khruschevites](https://www.marxists.org/reference/archive/hoxha/works/1976/khruschevites/10.htm)
- [The Counter-Revolution in Hungary in the Light of Marxism-Leninism by Gyula Kállai](https://archive.org/details/Hungary1956Kallai)
- [The Hungarian Soviet Republic by Tibor Hajdu](https://archive.org/details/HungarianSovietRepublic)
- [Historical events of the international revolutionary movement | Hungarian Counter-Revolution in 1956](http://ciml.250x.com/archive/events/english/1956_hungary/1956_counterrevolution_in_hungary.html)
