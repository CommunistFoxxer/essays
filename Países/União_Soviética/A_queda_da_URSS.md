# A queda da URSS
Dizer que a URSS caiu porque o comunismo falhou é uma falácia, e eu vou explicar isso com clean code.

Clean code é uma arquitetura de software onde você segue normas restritivas para manter a legibilidade do software. Isso tem diversas implicações no software final em si: Um código bagunçado pode levar a mais bugs, pode prejudicar a qualidade do software, pode triplicar o tempo de desenvolvimento do software, e consequentemente, levar a empresa à falência.

Robert Cecil Martin explica nas primeiras páginas do livro, que ele se lembrava de uma empresa da década de 80 que fazia bastante sucesso, mas logo ela sumiu do mapa literalmente, após cada vez mais estar demorando na entrega do software e vários bugs.

Note, o problema não foi o software (que era muito bom por sinal, fazia sucesso), mas sim como ele foi escrito. O comunismo soviético também. A diferença entre a URSS e os poucos países socialistas que sobreviveram, é que eles conseguiram ter mais disciplina ideológica que os soviéticos, que já em 70s instituíram várias reformas revisionistas, aquelas quais instituíram a segunda economia (AKA economia informal).

A URSS faliu após as reformas. O programa de descentralização do Gorby, de 1987, desequilibrou totalmente a economia ao permitir a retenção dos lucros por parte das empresas.

Para Terry Martin, a URSS foi "o primeiro império de bem-estar/ação positiva do mundo"
>Criou-se não só dezena e meia de grandes repúblicas nacionais, mas também dezenas de milhares de territórios nacionais espalhados por toda a vastidão do país. Novas elites nacionais foram instruídas e promovidas para cargos de liderança no governo, escolas e empresas industriais desses novos territórios. Em cada território, a língua nacional adquiriu estatuto de língua oficial do governo. Em dezenas de casos isso implicou a criação de uma língua escrita, que não existia. O Estado soviético financiou a produção em massa de livros, revistas, jornais, filmes, óperas, museus, música tradicional e outras produções culturais em línguas não russas. Nada de comparável tinha sido tentado anteriormente (…) e nenhum Estado multiétnico igualou ulteriormente a escala da ação positiva soviética.<br/>
>– <cite>Terry Martin</cite>

A URSS não estava longe de se aproximar dos EUA, mesmo em 1985, e a visão de que os EUA estava anos-luz à frente da URSS é racista e liberal, pois a URSS não incentivava a comercialização de suas realizações tecnológicas (patentes, ex).

Pode-se argumentar que a URSS tinha um grande fosso computacional com os EUA, mas a CIA revelou em 1986 que em 10 ou 15 anos, as instituições científicas de topo teriam provavelmente equipamentos comparáveis aos melhores que dispuseram os laboratórios nacionais dos EUA na época.
>Os Estados Unidos continuam à frente da União Soviética em termos de produção bruta e per capita, de consumo e nível de vida. Mas a União Soviética tem vindo a aproximar-se gradualmente dos Estados Unidos.<br/>
>– <cite>Harry Shaffer</cite>, 1984

Diz Harpal Brar:
>Na URSS, que tinha uma economia gigantesca antes de 1985, a produção industrial caiu 40% desde então; a taxa de inflação alcançou a cifra de 2.500%; a moeda chegou à ruína, com o rublo, que costumava valer mais do que o dólar, tendo agora uma taxa de câmbio de 800 rublos para um dólar (março de 1993).
