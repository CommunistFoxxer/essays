# Conclusão sobre os trabalhadores na URSS
A União Soviética se desenvolveu sob condições de extrema pressão, enfrentando a invasão das potências capitalistas, a invasão nazista e a espionagem do Ocidente. Dadas as dificuldades que enfrentou, é notável que a URSS tenha conseguido desempenhar um papel político positivo para os trabalhadores, especialmente em um momento em que os trabalhadores do mundo capitalista ainda lutavam por direitos sindicais básicos.

A URSS era um Estado operário legítimo, no qual o proletariado detinha o poder no local de trabalho e exercia uma influência significativa nas decisões de política nacional. Compare isso com a total falta de influência popular nos estados burgueses, e isso é ainda mais fácil de avaliar.

Fontes:
- [University of Oxford | Farm to Factory: A Reassessment of the Soviet Industrial Revolution](https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.507.8966&rep=rep1&type=pdf)
- [Williams College | Reassessing the Standard of Living in the Soviet Union: An Analysis Using Archival and Anthropometric Data](https://web.williams.edu/Economics/brainerd/papers/ussr_july08.pdf)
- [University of Warwick | Review of "Farm to Factory"](https://www.jstor.org/stable/25149595)
- [Oxford University Press | The Health Crisis in the USSR](https://academic.oup.com/ije/article/35/6/1384/660149)
- [University of Munich | Declining Life Expectancy in a Highly Developed Nation: Paradox or Statistical Artifact?](https://link.springer.com/chapter/10.1007%2F978-3-642-95874-8_22)
- [University of Melbourne | The Scale and Nature of German and Soviet Mass Killings and Repressions](http://sovietinfo.tripod.com/WCR-German_Soviet.pdf)
- [American Historical Review | Victims of the Soviet Penal System in the Pre-War Years](http://sovietinfo.tripod.com/GTY-Penal_System.pdf)
- [CIA (Freedom of Information Act) | Report on Soviet Gulags](https://www.cia.gov/readingroom/docs/CIA-RDP80T00246A032000400001-1.pdf)
- [CIA (Freedom of Information Act) | Report on Stalin](https://www.cia.gov/readingroom/docs/CIA-RDP80-00810A006000360009-0.pdf)
- [Yale University Press | Life and Terror in Stalin's Russia, 1934-1941](https://www.jstor.org/stable/j.ctt32bw0h)
- [Slavic Review (Cambridge University Press) | On Desk-Bound Parochialism, Commonsense Perspectives, and Lousy Evidence: A Response to Robert Conquest on the USSR](https://www.jstor.org/stable/2499177?seq=1#metadata_info_tab_contents)
- [Slavic Review | Fear and Belief in the USSR's "Great Terror": Response to Arrest, 1935-1939](https://www.cambridge.org/core/journals/slavic-review/article/fear-and-belief-in-the-ussrs-great-terror-response-to-arrest-19351939/748662F55C0CCEE8096BDA2BB2CCCEE0)
- [History News Network | Historian James Harris Says Russian Archives Show We've Misunderstood Stalin](https://historynewsnetwork.org/article/163498)
- [Village Voice | In Search of a Soviet Holocaust: A Fifty Year-Old Famine Feeds the Right](https://msuweb.montclair.edu/~furrg/vv.html)
- [EH Reviews | The Years of Hunger: Soviet Agriculture, 1931-1933](https://eh.net/book_reviews/the-years-of-hunger-soviet-agriculture-1931-1933/)
- [International Council for Central and East European Studies | Reassessing the History of Soviet Workers: Opportunities to Criticize and Participate in Decision-Making, 1935-1941](https://www.docdroid.net/t9gG4jQ/thurston-robert-reassessing-the-history-of-soviet-workers-opportunities-to-criticize-and-participate-in-decision-making.pdf)
- [Japanese Political Science Review | Rethinking Soviet Democracy](http://jpsa-web.org/eibun_zassi/data/pdf/JPSA_Kawamoto_final_July_9_2014.pdf)
