# Participação do trabalhador no local de trabalho
Uma das alegações mais comuns levantadas contra a URSS (e os estados socialistas em geral) por anticomunistas de esquerda é que não era "socialismo real", porque os trabalhadores não tinham controle direto sobre a produção. Essa afirmação pode ser encontrada nos escritos de Noam Chomsky, Murray Bookchin, Alexander Berkman, Emma Goldman e vários outros esquerdistas anti-soviéticos. Alega-se que os ganhos indiscutíveis obtidos pela classe trabalhadora nos estados socialistas (como [grandes melhorias em sua saúde e bem-estar](https://journals.sagepub.com/doi/abs/10.2190/B2TP-3R5M-Q7UP-DUA2)) são irrelevantes, porque essas revoluções foram "burocráticas" e, portanto, ilegítimas.

Ao discutir este tópico, é útil começar no nível do local de trabalho individual. O professor Robert Thurston (Miami University em Ohio) afirma que "nos níveis mais baixos da sociedade, nos assuntos do dia-a-dia e na implementação de políticas, [o sistema soviético] era participativo". Ele observa que os trabalhadores eram frequentemente incentivados a participar da tomada de decisões:
>O regime regularmente instava seu povo a criticar as condições locais e seus líderes, pelo menos abaixo de um certo nível exaltado. Por exemplo, em março de 1937, Stalin enfatizou a importância dos "laços do partido com as massas". Para mantê-los, era necessário "ouvir atentamente a voz das massas, a voz dos membros da base do partido, a voz dos chamados 'pequenos', a voz do povo comum".

Não eram palavras vazias ou propaganda barata; embora houvesse limites para as críticas, o professor Thurston observa que "tais limites permitiam muitas coisas que eram profundamente significativas para os trabalhadores, incluindo alguns aspectos das normas de produção, taxas e classificações salariais, segurança no trabalho, moradia e tratamento por parte dos administradores". Os trabalhadores tiveram voz em diversos órgãos oficiais e, em geral, tiveram suas demandas atendidas:
>O Comissariado da Justiça também ouviu e respondeu aos apelos dos trabalhadores. Em agosto de 1935, o promotor da cidade de Saratov relatou que de 118 casos relacionados a salários recentemente administrados por seu escritório, 90% ou 73,6%, haviam sido resolvidos em favor dos trabalhadores.

Os trabalhadores também participaram da supervisão direta dos gerentes:
>Os trabalhadores participaram às centenas de milhares em inspetorias especiais, comissões e brigadas que fiscalizavam o trabalho de gestores e instituições. Essas agências às vezes detinham um poder significativo.

Os direitos dos trabalhadores soviéticos foram frequentemente observados em relatos posteriores da era socialista:
>Um emigrado lembrou que sua madrasta, uma operária de fábrica, 'muitas vezes repreendia o patrão' e também reclamava das condições de vida, mas nunca foi presa. John Scott, um americano empregado por anos no final dos anos 1930 como soldador em Magnitogorsk, participou de uma reunião em uma fábrica de Moscou em 1940, onde os trabalhadores puderam 'criticar o diretor da fábrica, fazer sugestões sobre como aumentar a produção, aumentar a qualidade, e custos mais baixos.'

Esses fatos são ainda mais impressionantes quando lembramos o estado sombrio dos direitos dos trabalhadores nas nações capitalistas neste momento:
>Isso ocorreu em uma época em que os trabalhadores americanos, em particular, lutavam por um reconhecimento sindical básico, que mesmo quando conquistado não proporcionava muita influência formal no local de trabalho.

Thurston faz a seguinte observação:
>Longe de basear seu domínio nos meios negativos de coerção, o regime soviético no final dos anos 1930 promoveu um papel político limitado, mas positivo para a população... Conceitos anteriores do estado soviético requerem repensar: os trabalhadores que destituíram os administradores, alcançaram a prisão de seus alvos, e ganhou a reintegração nas fábricas o fez por meio de organizações que constituíam parte do aparelho de estado e exerciam poderes de estado.

Ele também observa que "não existia nenhuma divisão nítida entre o Estado e a sociedade", embora diferentes níveis do Estado exerçam diferentes poderes.

Em suma, embora a União Soviética tivesse elementos autoritários (como era inevitável dadas as condições; a URSS havia sido devastada pela guerra civil e invadida por várias nações capitalistas), havia também um forte elemento de controle dos trabalhadores, dando à URSS uma reivindicação legítima de ser um Estado operário.
