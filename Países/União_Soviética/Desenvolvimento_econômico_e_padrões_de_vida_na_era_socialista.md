# Desenvolvimento econômico e padrões de vida na era socialista
Em 1928 (depois que Stalin chegou ao poder como chefe do Partido Comunista), a Rússia Soviética instituiu uma economia totalmente planejada e o primeiro [Plano Quinquenal](https://en.wikipedia.org/wiki/Five-year_plans_of_the_Soviet_Union) foi aprovado. Isso resultou em rápido crescimento econômico. De acordo com Robert Allen:
>O PIB soviético aumentou rapidamente com o início do primeiro Plano Quinquenal em 1928... A expansão da indústria pesada e o uso de metas de produção e orçamentos flexíveis para direcionar as empresas eram adequados às condições da década de 1930, eles foram adotados rapidamente, e levaram a um rápido crescimento do investimento e do consumo.

Os economistas burgueses frequentemente alegavam que esse rápido crescimento veio às custas do consumo per capita e dos padrões de vida. No entanto, pesquisas mais recentes mostraram que isso é falso. Allen afirma:
>Não houve nenhum debate de que o "consumo coletivo" (principalmente serviços de educação e saúde) aumentou acentuadamente, mas a visão padrão era que o consumo privado diminuiu. Pesquisas recentes, no entanto, colocam essa conclusão em questão... Embora o investimento certamente tenha aumentado rapidamente, pesquisas recentes mostram que o padrão de vida também aumentou rapidamente.

O consumo de calorias aumentou rapidamente durante este período:
>As calorias são a dimensão mais básica do padrão de vida, e seu consumo era maior no final dos anos 1930 do que nos anos 1920... Em 1895-1910, a disponibilidade de calorias era de apenas 2100 por dia, o que é muito baixo para os padrões modernos. No final da década de 1920, a disponibilidade de calorias avançou para 2.500... No final da década de 1930, a recuperação da agricultura aumentou a disponibilidade de calorias para 2.900 por dia, um aumento significativo em relação ao final da década de 1920. A situação alimentar durante a Segunda Guerra Mundial era grave, mas em 1970 o consumo de calorias aumentou para 3.400, o que estava no mesmo nível da Europa Ocidental.

No geral, o desenvolvimento da economia soviética durante o período socialista foi extremamente impressionante. De acordo com Robert Allen:
>A economia soviética teve um bom desempenho... O planejamento levou a altas taxas de acumulação de capital, rápido crescimento do PIB e aumento do consumo per capita mesmo na década de 1930.

O crescimento da URSS durante o período socialista excedeu o das nações capitalistas:
>A URSS liderou os países não pertencentes à OCDE e, de fato, alcançou uma taxa de crescimento neste período que excedeu a regressão de catch-up da OCDE, bem como a média da OCDE.

Isaac Deutscher, um trotskista, escreve sobre o final do período Stalin:
>Resistiu às pressões ocidentais com firmeza suficiente para deter qualquer projeto norte-americano de disseminar a guerra; e a indústria nuclear soviética progrediu com grande rapidez e produziu sua primeira bomba de hidrogênio em 1953, logo após os norte-americanos terem realizado esta façanha. Os setores básicos da economia soviética, tendo atingido seu nível de produção do pré-guerra em 1948-1949, teve um crescimento de 50% nos últimos anos de Stalin. A modernização e urbanização da União Soviética foi acelerada. Somente no início dos anos 50, sua população urbana cresceu cerca de 25 milhões de pessoas. As escolas secundárias e as universidades estavam educando duas vezes mais alunos do que antes de 1940. Dos destroços da guerra mundial foram refundadas as bases para renovada ascendência industrial e militar da Rússia, que depois assombraria o mundo.

Esse sucesso também é atribuído especificamente à revolução e ao sistema socialista. Como afirma Allen:
>Esse sucesso não teria ocorrido sem a revolução de 1917 ou o desenvolvimento planejado da indústria estatal.

Os benefícios do sistema socialista são óbvios após um estudo mais detalhado. Como Simon Clarke coloca:
>...uma economia capitalista não teria criado os empregos industriais necessários para empregar o trabalho excedente, uma vez que os capitalistas só empregariam trabalho enquanto o produto marginal do trabalho excedesse o salário. A industrialização patrocinada pelo Estado não enfrentou tais restrições, uma vez que as empresas foram incentivadas a expandir o emprego de acordo com as demandas do plano.

O crescimento econômico também foi auxiliado pela liberação das mulheres e o consequente controle sobre a taxa de natalidade, bem como a participação das mulheres na força de trabalho. Allen afirma:
>O rápido crescimento da renda per capita estava condicionada não apenas sobre a rápida expansão do PIB, mas também sobre o lento crescimento da população. Isso se deveu principalmente a uma rápida transição da fertilidade, e não a um aumento na mortalidade por coletivização, repressão política ou Segunda Guerra Mundial. A queda nas taxas de natalidade deveu-se principalmente à educação e ao emprego das mulheres fora de casa. Essas políticas, por sua vez, foram resultados da ideologia iluminista em sua variante comunista.

As críticas ao trabalho de Allen corroboraram suas declarações. De acordo com Simon Clarke:
>Allen mostra que a estratégia stalinista funcionou, em termos estritamente econômicos, até por volta de 1970... O livro de Allen convincentemente estabelece a superioridade de uma economia planejada sobre uma capitalista em condições de excedente de trabalho (que é a condição na maior parte do tempo).

Outros estudos confirmaram as descobertas de que os padrões de vida da URSS aumentaram rapidamente. De acordo com a economista [Elizabeth Brainerd](https://www.ebrainerd.com/) (ex-Williams College, agora na Brandeis University):
>Melhorias notavelmente grandes e rápidas na altura da criança, estatura adulta e mortalidade infantil foram registradas de aproximadamente 1945 a 1970... As estimativas ocidentais e soviéticas do crescimento do PIB na União Soviética indicam que o PIB per capita cresceu em cada década na era pós-guerra, às vezes ultrapassando de longe as taxas de crescimento das economias ocidentais desenvolvidas... As medidas convencionais de crescimento do PNB e do consumo das famílias indicam uma longa e ininterrupta escalada do padrão de vida soviético de 1928 a 1985; mesmo as estimativas ocidentais dessas medidas apoiam essa visão, embora em uma taxa de crescimento mais lenta do que as medidas soviéticas.

Infelizmente, após a introdução de reformas de mercado e outras políticas revisionistas, os padrões de vida começaram a se deteriorar (embora algumas medidas continuassem a aumentar, embora mais lentamente). Brainerd afirma:
>Três medidas diferentes de saúde da população mostram uma melhoria consistente e grande entre aproximadamente 1945 e 1969: altura da criança, altura do adulto e mortalidade infantil, todas melhoraram significativamente durante este período. Essas três medidas biológicas do padrão de vida também corroboram a evidência de alguma deterioração nas condições de vida a partir de 1970, quando a mortalidade infantil e adulta estava aumentando e a estatura infantil e adulta parou de aumentar e em algumas regiões começou a diminuir.

O crescimento econômico também começou a desacelerar nessa época. De acordo com Robert Allen:
>Após a Segunda Guerra Mundial, a economia soviética voltou a crescer rapidamente. Em 1970, a taxa de crescimento estava caindo e a produção per capita estava estática em 1985.

A Guerra Fria foi outro fator que contribuiu para desacelerar as taxas de crescimento:
>A Guerra Fria foi um fator adicional que reduziu o crescimento soviético depois de 1968. A criação de armamentos de alta tecnologia exigiu uma alocação desproporcional de pessoal e recursos de Pesquisa & Desenvolvimento para os militares. A inovação em máquinas e produtos civis diminuiu em conformidade. Metade da redução na taxa de crescimento do PIB per capita deveu-se ao declínio no crescimento da produtividade, e essa redução fornece um limite superior para o impacto da corrida armamentista com os Estados Unidos.

Em suma, a URSS alcançou resultados econômicos maciçamente positivos até os anos 1970, quando as políticas revisionistas e a Guerra Fria começaram a causar uma estagnação. Agora, vamos passar do desenvolvimento econômico e falar sobre os padrões de saúde da população soviética.
