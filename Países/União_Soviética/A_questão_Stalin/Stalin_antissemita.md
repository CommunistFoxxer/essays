# Stalin antissemita?
Em primeiro lugar, todas as nacionalidades sob Stalin tinham direitos iguais, não importando a raça que pertenciam. Isso pode ser visto no Artigo 123 da Constituição Soviética de 1936. Afirma que:
>A igualdade de direitos dos cidadãos da URSS, independentemente de sua nacionalidade ou raça, em todas as esferas da vida econômica, estatal, cultural, social e política, é uma lei indestrutível. Qualquer restrição direta ou indireta dos direitos de, ou, inversamente, qualquer estabelecimento de privilégios diretos ou indiretos para os cidadãos em razão de sua raça ou nacionalidade, bem como qualquer defesa de exclusividade racial ou nacional ou ódio e desprezo, é punível por lei.

Esta lei não exclui a população judaica, já que o próprio Stalin disse isso sobre o anti-semitismo respondendo a um pedido da Agência Telegráfica Judaica da América:
>O anti-semitismo, como uma forma extrema de chauvinismo racial, é a mais perigosa remanescência do canibalismo. Na URSS, o anti-semitismo é estritamente perseguido por lei como um fenômeno profundamente hostil ao sistema soviético. Os anti-semitas ativos são punidos pelas leis da URSS.

Sob Stalin, toda a população soviética era capaz de ler e escrever, ou seja, alfabetizou-se. O império russo tinha uma taxa de alfabetização de 21% (1897), enquanto sob Stalin, 100% da população era totalmente alfabetizada em 1950, independentemente da raça ou sexo.

A política de Korenizatsiya começou em 1913, mas o efeito total disso foi sob o camarada Stalin. Stalin afirma que o chauvinismo russo é um grande perigo para a União Soviética e insiste na criação do Soviete das Nacionalidades, que se tornou uma das 2 câmaras do Soviete Supremo. Conforme declarado no artigo 37 da constituição soviética de 1936:
>Ambas as Câmaras do Soviete Supremo da União das Repúblicas Socialistas Soviéticas, o Soviete da União e o Soviete das Nacionalidades, têm direitos iguais.

Enquanto:
>O Soviete das Nacionalidades é eleito pelos cidadãos da URSS de acordo com a União e Repúblicas Autônomas, Regiões Autônomas e áreas nacionais com base em vinte e cinco deputados de cada República da União, onze deputados de cada República Autônoma, cinco deputados de cada Região Autônoma e um deputado de cada área nacional.

Nos termos do artigo 35 da Constituição Soviética. Isso mostra que os russos não étnicos desempenharam um grande papel na URSS e na criação de leis, políticas e na eleição de altos funcionários.

No entanto, para descobrir o quanto Stalin era um “nacionalista russo do mal”, devemos olhar para o número de pessoas enviadas aos gulags. Portanto, ao todo, em 1939, havia 810 mil russos nos gulags. Além disso, a população russa em 1939 era de 108.377.000, portanto, podemos ver que 0,75% da população russa estava em gulags (prisões). Agora, vamos examinar os outros grupos étnicos, como ucranianos e cazaques. A população ucraniana em 1939 era de 33.425.000, enquanto o número deles nas gulags era de 182.536, ou seja, 0,55% da população. A população do Cazaquistão era de 6.081.000, enquanto o número de gulags era de 17.333, ou seja, 0.28% da população. Assim, podemos dizer que mais russos estavam em gulags em comparação com outros grupos étnicos durante a época de Stalin.

Podemos aplicar o mesmo aos judeus (Stalin até ajudou a criar Israel), alguns dos amigos mais próximos de Stalin eram judeus como - Lazar Kaganovich, enquanto muitos judeus soviéticos famosos receberam o "Prêmio Estatal de Stalin". Isso inclui:
- Sergei Eisenstein
- Yuli Raizman
- Mikhail Romm

Portanto, no geral, podemos dizer que Stalin era de fato um verdadeiro comunista soviético e não era um nacionalista russo e não odiava o povo judeu.
