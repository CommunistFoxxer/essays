# A fome de Bengala e Stalin
Os britânicos engendraram uma fome que matou 2,5 milhões de pessoas na Índia. Mas quando a Índia apelou a Stalin por comida, ele a mandou imediatamente. Ele disse que "os documentos podem esperar, mas a fome não". Ainda hoje, as pessoas em Bengala chamam seus filhos de "Stalin".

![](images/hunger_stalin-india.png)
