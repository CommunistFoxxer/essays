# Stalin vs Trotsky
Stalin foi o líder escolhido pelo povo soviético (a pessoa que ofereceu um projeto que de fato foi de interesse popular e venceu a disputa). Trotsky também concorreu mas o projeto dele não era atrativo e nem atendia interesses populares de certa forma, e ele em parte não aceitava ter perdido a disputa dentro do partido.

Trotsky vivia fazendo dano mas nesse meio tempo a URSS entrou em guerra, há até registro fotográfico de Stalin um tanto desolado quando soube disso. Aqui, Trotsky cumpriu um grande papel como comissário de guerra, era ele quem decidia quem vivia e quem morria.

Lenin passava 99% do tempo bravo com Trotsky, porque mesmo ele tendo um grande conhecimento sobre guerra (inclusive, tinha um livro por chegar em pt-br sobre isso), ele tinha posições um tanto pós-política, cosmopolita e até contraditórias. Nos escritos de Stalin direcionados ao comitê, ele defendia fortemente Lenin de "equivocos de Trotsky", porque este vivia atacando o outro.

A própria Krupskaya (esposa de Lenin) relembra em um escrito que o próprio Lenin era um crítico de Trotsky e ela mesma guardava frustração com os seguidores e aliados de Trotsky.

A "carta testamento" não é falsa, mas o fato de nela constar a indicação de Trotsky como sucessor sim é uma mentira. Lenin na verdade estava criticando e falando mal de todos os bolcheviques importantes, até como forma de tentar baixar a bola de cada um deles.
