# Deportações
Em seu discurso secreto, Nikita Kruschev afirmou que Stalin realizou deportações em massas com base nas etnias, mas ele omitiu o fato de que haviam exceções, elas foram deportadas por motivos militares e não foi uma questão de racismo.

Nikolai Bulgay, um anti-stalinista russo especialista nas deportações em massa de Stalin afirmou que haviam exceções para veteranos e afirma:
>O governo soviético, em geral, alocou suas prioridades corretamente, baseando-as em seu direito de manter a ordem atrás das linhas de frente, e no Norte do Cáucaso em particular.

Ann Applebaum, uma escritora anti-comunista, em seu famoso livro intitulado "Gulag" nega que houveram, de facto, grandes rebeliões e deserções entre as minorias étnicas do Cáucaso. No entanto, Furr em seu livro "Antistalinskaia Podlost'" prova usando dados públicos e bem conhecidos por acadêmicos historiadores, que, de fato, havia uma taxa imensa de deserção entre essas etnias. Por exemplo, cerca de 90% dos tártaros desertaram e se juntaram aos Nazistas.

Otto Pohl, acadêmico e pesquisador, mostra a partir de fontes alemãs que nem todos esses tártaros que desertaram, se juntaram aos Nazistas - mas mesmo sendo verdade ou não, a URSS não tinha como saber e esses desertores poderiam se juntar à grupos bandidos e anti-soviéticos. 93% dos homens chechenos e inguches convocados para o serviço militar em 1942 desertaram, esconderam-se, juntaram-se aos nazis ou juntaram-se a grupos rebeldes ou bandidos. Em fevereiro de 1943, nacionalistas chechenos pró-nazistas lideraram uma grande rebelião pró-Nazista.

Grigori Tokaty, um russo, historiador e ferrenho crítico dos períodos stalinistas sabia da existência das rebeliões, mas ele achava que elas eram justificáveis. O mesmo trabalhou para o Information Research Department (Departamento de Pesquisa de Informação), um organismo secreto do Foreign Office (Ministério dos Negócios Estrangeiros) criado em 1948 pelo governo britânico para estudar o comunismo e combater ativamente a sua influência interna e externamente, promovendo um eficaz relacionamento com jornalistas dos principais jornais, dirigentes sindicais, etc. Robert Conquest é um dos que trabalharam para o IRD.

Segundo Gomov e Tokaty:
>Os registros do NKVD atestam a existência de 180 trens de comboios transportando 493.269 cidadãos chechenos e inguches e membros de outras nacionalidades apreendidos ao mesmo tempo. 50 pessoas morreram durante a operação e 1.272 morreram durante a viagem.

Isso representa apenas 0.27% das mortes. 0.26% se você descontar os 50 mortos durante a operação. Como aconteceu no inverno, durante a guerra mais violenta da história mundial, esse número não parece muito alto. Provavelmente é muito menor do que a taxa sofrida pelos civis soviéticos nas áreas ocupadas. No caso Chechenio-Ingush e dos tártaros da Crimeia, a colaboração com os nazistas foi massiva, envolvendo a maior parte da população. Tentar isolar e punir “apenas os culpados” seria dividir a nação. Isso provavelmente teria destruído a nação e haveria muito poucos rapazes para as moças se casarem. Em vez disso, o grupo nacional foi mantido junto e sua população cresceu.

O historiador Zemskov estima que 151.720 tártaros crimeanos foram deportados, dos quais 191 morreram durante o curso da deportação. Isso é 0.13% - curiosamente as fontes ocidentais erram a casa decimal frequentemente, retratando como 1.13% ou 13% das mortes.

As deportações foram o resultado da colaboração nazista em grande escala. A mídia ocidental tenta apresentar à você a colaboração Nazista como justificável e as deportações soviéticas como injustificáveis. O governo soviético fez o melhor que pode, sem dividir uma nação.

Sobre os tártaros, eles foram reassentados no Uzbequistão sob a categoria especial de "colonos". Beria declarou que os tártaros tinham direito a levar consigo objetos pessoais, roupas, objetos domésticos, pratos e utensílios, e até 500 quilos de alimentos por família bem como propriedades, edifícios, móveis e terras de fazendas deixadas para trás foram assumidas pelas autoridades locais; todos os bovinos e aves foram assumidos pelo Comissariado do Povo das Indústrias de Carne e Laticínios, toda a produção agrícola pelo Comissariado do Povo da URSS, cavalos e outros animais de tração pelo Comissariado da Agricultura do Povo da URSS, e criação de gado pelo Comissariado das Fazendas Estaduais de Grãos e Pecuária do Povo da URSS.

Até 1 de julho, o NKVD e os Comissariados que tomaram posse apresentaram uma proposta de reembolso para os colonos, com base nas receitas de câmbio dos rebanhos, aves e produção agropecuária deles recebidos. Para facilitar o recebimento de gado, grãos e produção agrícola dos colonos especiais, Benediktov, Subbotin, Smirnov e Lobanov despacharam o número necessário de trabalhadores para a Crimeia, em coordenação com Gritsenko. O pagamento do transporte era baseado na taxa de transporte dos prisioneiros. A cada comboio de colonos especiais, o Comissariado do Povo da Saúde Pública da URSS (Miterev) atribuiu, em prazo a coordenar com o NKVD da URSS, um médico e duas enfermeiras, bem como um abastecimento adequado de medicamentos, e para fornecer cuidados médicos e de primeiros socorros a colonos em trânsito.

O assentamento dos colonos especiais ocorreu em comunidades agrícolas estaduais, fazendas coletivas existentes, fazendas afiliadas a empresas e em comunidades de fábricas, para emprego na agricultura e na indústria. Foram realizados empréstimos de 7 anos de até 5.000 rublos por família, para a construção e instalação de casas, concedidos pelo Banco Agrícola (Kravtsov) a colonos especiais enviados para a SSR do Uzbequistão, em seus locais de assentamento.

Todos os meses durante o período de junho a agosto de 1944, quantidades iguais de farinha, sêmola e vegetais foram alocadas pelo Comissariado do Povo da URSS (Subbotin) ao Conselho Uzbeque de Comissários do Povo da SSR para distribuição aos colonos especiais.

Farinha, sêmola e legumes foram distribuídos gratuitamente aos colonos especiais durante o período de junho a agosto, como retribuição pela produção agrícola e pecuária deles recebida nas áreas de onde foram expulsos. Para aumentar a capacidade de transporte automotivo das tropas do NKVD, guarnecidas nos raions de assentamento nos SSRs do Uzbeque, do Cazaquistão e do Kirgiz, o Comissariado do Povo de Defesa (Khrulev) forneceu 100 veículos motorizados “Willys”, 3 consertados e 250 caminhões durante o período de maio a junho de 1944. Até 20 de maio de 1944, a Administração Principal para o Transporte e Abastecimento de Petróleo e Produtos Petrolíferos (camarada Shirokov) deveria alocar e fornecer 400 toneladas de gasolina para locais especificados pela NKVD, e 200 toneladas de gasolina deveriam ser colocadas à disposição do Conselho de Comissários do Povo do Uzbequistão.

Até 15 de maio deste ano, a Administração Principal de Abastecimento do Ministério Florestal da URSS e o Conselho de Comissários do Povo da URSS (Lopukhov), entregaram 75.000 placas de vagões de 2,75 metros ao Comissariado do Povo das Ferrovias. Em maio deste ano, o Comissariado do Povo das Finanças (camarada Zverev) transferiu 30 milhões de rublos do fundo de reserva do Conselho de Comissários do Povo da URSS para a NKVD, para a implementação de medidas especiais.

Os bálticos celebram os colaboradores nazistas (Noreika, Cukurs) e negam seus crimes durante e após a Segunda Guerra Mundial. Em Salaspils, um dos maiores campos de extermínio, os nazistas mataram pelo menos 100.000 pessoas. Este número é abertamente negado pela Letônia, alegando que apenas "2.000 morreram lá". Só na Letônia, pelo menos 5 frentes colaboradoras foram fundadas pelos nazistas para cometer atos de terror nos países bálticos soviéticos:
- Organização Nacional de Guerrilheiros Letões
- Organização Partidária da Couland do Norte
- União Partidária Nacional da Letônia do Nordeste da Letônia
- União dos Guardiões Nacionais
 - Falcões da Pátria

Por exemplo, os Falcões foram fundados pelo tenente da 19ª Divisão SS, Alfreds Tīlibs. Esses bandidos eram apoiados pelos kulaks (aristocracia rural, barões cinzentos), pela Igreja Católica e por ex-membros dos regimes ultranacionalistas entre guerras.
 
O Vaticano deu ordens explícitas às suas congregações bálticas para "resistir à propagação da ameaça bolchevique e sua infiltração no Ocidente". Esta ordem foi executada por Antonijs Springovičs, arcebispo de Riga. A Igreja Católica abertamente chamada a apoiar os colaboradores armados. Somente no verão de 1945, a Estônia teve 201 ataques de bandidos fascistas. A entrega de salários, jornais e mercadorias foi interrompida por meses (por exemplo, muitas pessoas em comunidades isoladas não faziam ideia de que a guerra havia terminado). Roubos e estupros por bandidos fascistas eram comuns. Em 1944-1947, 544 pessoas foram assassinadas na Estônia pelos bandidos.  4 foram sequestrados. Durante o mesmo período, 862 pessoas foram assassinadas na Letônia pelos bandidos. 125 foram sequestrados. A situação na Lituânia foi muito pior, onde 5.870 pessoas foram mortas e 727 foram sequestradas.

Fontes:
- [Library of Congress | On the crimean tatars](https://www.loc.gov/exhibits/archives/trans-l2tartar.html)
- Salaspils nāves nometne. K. Sausnītis. Rīga, Liesma, 1973.
- Латышские "лесные братья" и немецкие спецслужбы. М. Ю. Крысин. Изд. Вече, 2016.
- От национализма к коллаборационизму: Прибалтика в годы Второй мировой войны.
- Докумиенты в двух томах. А. В. Репников. 2018.
- Patiesība ir dzīva. Dokumenti liecina. Alfrēds Rubiks. Rīga. 2010.
