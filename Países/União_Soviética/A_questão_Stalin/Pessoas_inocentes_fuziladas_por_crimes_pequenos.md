# Pessoas inocentes fuziladas por crimes pequenos
Para desmascarar esses mitos, usarei artigos do Código Penal da RSFSR 1926 (algumas edições de 1956).

>Mas a minha avó roubou pão e foi condenada perpétua no Gulag.

Este é o tipo de declaração que um capitalista ou anti-stalinista faz. No entanto, se olharmos para o Código Penal, ele afirma:
>Para combater os crimes mais graves que ameaçam as fundações do poder soviético e do sistema soviético, até que o Comitê Executivo Central da União da República Socialista Soviética cancele, nos casos especialmente indicados nos artigos deste Código, a execução é utilizada como medida excepcional para proteger o estado dos trabalhadores
>
>Artigo 21.º do Código Penal da RSFSR

>Uma criança roubou alguma coisa, mas foi condenada à morte mesmo assim, porque Stalin e a URSS são maus.

Outra declaração falsa que ouvi muitas vezes. Em contrapartida, afirmo sempre que:
>Os menores de dezoito anos à data do crime e as mulheres grávidas não podem ser condenadas à morte
>
>Artigo 22.º do Código Penal da RSFSR

Estes são os únicos crimes sob os quais um tribunal pode condená-lo à morte (crimes contra-revolucionários):
1. Trair a pátria-mãe, ou seja, aqueles que - ameaçam a independência do Estado ou a inviolabilidade do seu território, tais como espionagem, emissão de segredos militares ou de Estado, passagem para o lado do inimigo, fuga ou fuga para o estrangeiro. Artigo 58, parágrafo 1a, do Código Penal da RSFSR.
2. Qualquer tipo de levante armado ou invasão de fins contra-revolucionários no território soviético por gangues armadas, tomando o poder no centro ou no terreno para os mesmos fins e, em particular, com o objetivo de arrancar à força qualquer parte de seu território a URSS e uma república sindical separada ou a rescisão dos prisioneiros da União SSR com tratados de estados estrangeiros. Artigo 58, parágrafo 2, do Código Penal da RSFSR.
3. Prestar assistência à burguesia internacional em qualquer forma ou formato. Artigo 58, parágrafo 4
4. Espiar, isto é, transferir, abduzir ou coletar informações ultrassecretas (contadas como secretas) com o objetivo de entregá-las a organizações contra-revolucionárias ou indivíduos privados. Artigo 58 parágrafo 6 do Código Penal da RSFSR.
5. Sabotagem contra-revolucionária, ou seja, o não cumprimento deliberado de certos deveres ou seu desempenho deliberado e descuidado com o propósito especial de enfraquecer o poder do governo e as atividades do aparelho de estado. Artigo 58, parágrafo 14, do Código Penal da RSFSR.

Estes são os únicos crimes em que um tribunal pode condená-lo à morte (crimes perigosos):
1. Motins em massa, acompanhados de pogroms, a destruição de ferrovias ou outros meios de comunicações, assassinatos e outras ações semelhantes. Artigo 59 parágrafo 2 do Código Penal da RSFSR.
2. Atividade criminosa, ou seja, a organização de gangues armadas e sua participação nos ataques organizados a instituições soviéticas e pessoais ou a cidadãos individuais, paradas de trens e destruição de ferrovias e outros meios de comunicação e centros de comunicação. Art 59 parágrafo 3 do Código Penal da RSFSR.
3. Roubo de armas, peças de armas e munições. Art 59 parágrafo 3a do Código Penal da RSFSR.
4. Propaganda ou agitação destinada a incitar o ódio ou ódio étnico ou religioso, bem como a distribuição ou produção e armazenamento de literatura da mesma natureza. Art 59 parágrafo 7 do Código Penal da RSFSR.
5. Falsificação ou venda de moedas metálicas falsificadas, bilhetes do estado, bilhetes do Banco do Estado da URSS, papéis do governo, bem como falsificação ou venda de moeda estrangeira falsificada. Aer 59 parágrafo 8 do Código Penal da RSFSR.
6. Contrabando de qualquer forma. Art 59 parágrafo 9 do Código Penal da RSFSR


Este é o único crime em que um tribunal pode condená-lo à morte (indivíduos):
1. Matar outro indivíduo intencionalmente. Art. 136 do Código Penal da RSFSR

Tenham em mente que se você infringir alguma dessas leis, o tribunal decide se vocês seriam ou não condenados à morte. No entanto, em algumas circunstâncias (o tribunal decide), você poderia ser mandado para a prisão em vez de ser baleado, de 3 meses a 10 anos (o que não era raro). Assim, podemos concluir que a URSS de Stalin não atirou em pessoas aleatoriamente sem motivo, pois os tribunais precisavam de legislação adequada para poder condenar pessoas à morte, nem atirou em pessoas por crimes menores. As sentenças de morte seriam aplicadas aos crimes mais severos contra o Estado e a classe trabalhadora.
