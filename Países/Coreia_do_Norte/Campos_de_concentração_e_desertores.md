# Campos de concentração e desertores norte-coreanos
Então, qual é o problema com esses campos? Eles existem? Se sim, as coisas horríveis que ouvimos por desertores profissionais, ou seja, desertores que são pagos para mentir, são verdadeiras? As três principais fontes das mentiras que ouvimos são a Amnistia Internacional, os desertores e o Comité dos Direitos Humanos da Coreia do Norte. Sim, você adivinhou, essas organizações e esses desertores são pagos e estão baseados nos Estados Unidos e não são nada mais do que ferramentas de propaganda. Razões, óbvias.

Portanto, os dois principais relatórios e livros das organizações são estes por estes:
- [Gulag oculto](https://www.hrnk.org/uploads/pdfs/HRNK_HiddenGulag2_Web_5-18.pdf)
- [Relatório de anistia](https://www.amnesty.org/download/Documents/28000/asa240012011en.pdf)

Um fato engraçado sobre esses, é que eles não têm evidência. Estão todos apoiados em testemunhos que não podem ser verificados e muitas vezes se desfazem e os desertores falam outra coisa na segunda-feira e outra coisa no sábado. E há as famosas imagens de satélite. Você sabe, essas imagens que podem muito bem ser prisões... Existe realmente um país que não tem prisões? Eu poderia mostrar a você uma imagem de satélite de uma prisão na Noruega, você não faria a distinção de onde veio e o que realmente é.

Portanto, não há evidências. Não é um vídeo, imagens, fontes oficiais. Nada. Apenas testemunhos de pessoas, que muitas vezes se desfazem. As prisões na RPDC são tão duras? Provavelmente, como na maioria dos países do mundo. Os guardas estão alimentando cães com crianças? Você teria que ser racista ou uma pessoa muito doutrinada para acreditar nisso sem evidências. Além disso, há um grande número de desertores que querem voltar para a RPDC, mas você não ouvirá suas histórias com frequência, não é? Eu realmente me pergunto por quê!

**E quanto aos desertores nos quais precisamos dar-lhes um oscar de melhor atuação?**
- [Shin Dong-hyuk exposto](https://www.youtube.com/watch?v=P82aad8QQW4)
- [Yeonmi Park: uma mentirosa comprovada](https://thediplomat.com/2014/12/the-strange-tale-of-yeonmi-park/) e [Yeonmi Park: a desertora que enganou o mundo](https://jooparkblog.blogspot.com/2014/12/yeonmi-park-defector-who-fooled-world.html)
- Então, que tal [este dramático soldado](https://www.youtube.com/watch?v=0fNy_tWYkd4) norte-coreano que desertou no sul, e os "malignos" soldados da Coreia do Norte ousaram atirar??? [Bem, ele era um assassino. É por isso que ele desertou.](https://www.sbs.com.au/news/north-korean-defector-confesses-to-murder-report)
- [Por que os desertores da Coreia do Norte continuam mudando suas histórias?](https://www.youtube.com/watch?v=XkegD7V9E6g)
- [Coreia do Sul paga US$860,00 a desertores para mentir](https://www.bbc.com/news/world-asia-39170614)
- [O Sul quadruplicou o dinheiro em 2017, uma época de calor político com a Coreia do Norte e o Ocidente. Eu me pergunto por que](https://www.theguardian.com/world/2017/mar/05/kim-jong-un-south-korea-quadruples-reward-north-korea-classified-information)
- [Os incentivos em dinheiro e o apetite sem fim da mídia ocidental por histórias chocantes encorajam os refugiados a exagerar, argumenta Jiyoung Song](https://www.theguardian.com/world/2015/oct/13/why-do-north-korean-defector-testimonies-so-often-fall-apart)
- [Shin dong hyuk relembra partes de sua história depois que seu pai apareceu na TV](https://www.cbsnews.com/news/prominent-north-korean-defector-shin-dong-hyuks-story-questioned/)
- [Empresário nascido na Suíça que viveu e trabalhou na Coreia do Norte por sete anos até 2009, ele frequentemente questiona as representações da mídia sobre a situação dos direitos humanos no país](https://thediplomat.com/2014/10/north-korea-defectors-and-their-skeptics/)
- [12 garçonetes da Coréia do Norte sequestradas pela agência de espionagem seoul](https://au.news.yahoo.com/seoul-tricked-n-korea-waitresses-defecting-manager-080159062--spt.html?guccounter=1&guce_referrer=aHR0cHM6Ly9kcHJrc3R1ZHlndWlkZS53b3JkcHJlc3MuY29tLzIwMTcvMTAvMTMvZHByay1zdHVkeS1ndWlkZS8&guce_referrer_sig=AQAAAAdpgLiRKHlrR-vRT8ByQMn8XN3Ey9YpdBnHLaJrKqXnoX07l_BH0NHleGosUYoZzVf-Jux1EBKLwl0KdCJ7YC9YYkeDTuqFLXa_z8-uhuVDqWUnkEzwj8cfnFgeYEQCx0F32YRzWHRUN3dOPG4xrKXle883fr11xWecrPlLpEEQ)
- [E o sul-coreano que foi morto por soldados sul-coreanos tentando desertar no Norte?](https://www.europapress.es/internacional/noticia-muere-tiroteado-surcoreano-trataba-cruzar-nado-rio-imjin-corea-norte-20130916105528.html)

**E quanto aos desertores que falam a verdade e muitas vezes (isso mesmo) voltam, ou querem, de volta ao dprk?**

**O que dizer dos desertores que não são mentirosos profissionais?**
- [Desertora norte-coreana e seu filho de seis anos morreram de fome na Coreia do Sul depois de ter sua previdência privada negada](https://edition.cnn.com/2019/09/21/asia/north-korean-defector-funeral-intl-hnk/index.html)
- [Mulher da RPDC desertou no sul. Ela percebeu que no sul o estado não cobre moradia, tratamento médico, etc. Ela agora quer voltar para a RPDC, de volta para sua família, mas ela não é permitida pelo sul](https://edition.cnn.com/2015/09/23/asia/north-south-korea-defector-family/index.html)
- [Cidadãos leais a Pyongyang](https://www.youtube.com/watch?v=ktE_3PrJZO0)
- [Os desertores sofrem com dívidas](https://www.bbc.com/news/blogs-news-from-elsewhere-31904466)
- [Os desertores querem voltar para a RPDC](https://www.theguardian.com/world/2014/apr/22/defector-wants-to-go-back-north-korea)
- [“Não me sinto à vontade morando na República da Coreia, pois acredito que o coração das pessoas, e não o dinheiro, deve ser priorizado [...] meu desejo de ir para o Norte não mudará, mesmo que o Sul me dê ouro.”](https://www.nknews.org/2017/09/why-two-north-korean-defectors-want-to-go-back/)
- [Desertores são tratados como lixo](https://www.nytimes.com/2017/08/05/world/asia/north-korea-defector-south-korea.html)
- ["Tudo o que eu disse na TV foi planejado... Para fazer os norte-coreanos parecerem bárbaros, ignorantes e estúpidos."](https://www.dw.com/en/north-korea-defector-returns-home-calling-south-capitalist-hell/a-39745918)
- [Coreia do Sul rejeita pedido de desertores para voltar ao norte](https://www.upi.com/%E2%80%A6/Seoul-rejects-North-Kor%E2%80%A6/7441442934149/)
- [Desertor da Coreia do Norte orgulhoso de armas nucleares, diz que Kim prefere morrer a desistir](https://www.upi.com/Top_News/World-News/2017/09/08/North-Korea-defector-proud-of-nukes-says-Kim-would-rather-die-than-give-up/9701504547982/)
- [Desertor da Coreia do Norte para o sul, preso por elogiar Kim Jong-Un](https://www.upi.com/Top_News/World-News/2016/11/03/North-Korea-defector-arrested-for-messages-praising-Kim-Jong-Un/9711478193917/)
- [Agências de inteligência da Coreia do Sul atraem pessoas do norte para desertar](http://www.koreaherald.com/view.php?ud=20180715000122)
- [Alguns desertores na RPDC depois de terem sido enganados para irem para a Coreia do Sul](https://www.nknews.org/2013/01/third-group-of-north-korean-refugees-re-defect-back-to-dprk)
- ["Embora a Coreia do Norte seja mais pobre, me senti mais livre lá. Vizinhos e pessoas se ajudam e dependem umas das outras."](https://www.abc.net.au/news/2017-12-15/north-korean-defectors-returning-to-the-hermit-kingdom/9254654)
- [Choe in-guk: filho de desertores da Coreia do Sul muda-se para o Norte](https://www.bbc.com/news/world-asia-48905026)
- ["Eu queria ficar na Coreia do Norte", diz americano](https://www.theguardian.com/world/2014/nov/20/-sp-north-korea-matthew-miller)
- [Ex-presidente liberal do Brasil, responsável por um dos maiores pacotes de privatização em décadas, diz que o ocidente produz apenas fake news sobre a Coreia Popular](https://twitter.com/Collor/status/1275628169798856705)
- [Depois de fugir da RPDC, alguns desertores querem voltar](https://www.abc.net.au/news/2017-12-15/north-korean-defectors-returning-to-the-hermit-kingdom/9254654)
- [Problema oculto do Sul: desertores suicidas](https://www.bbc.com/news/magazine-34710403)
- [Desertor da Coreia do Norte interrompe evento de direitos humanos da ONU e pede para voltar à RPDC](https://www.telegraph.co.uk/news/2017/12/14/north-korean-defector-interrupts-un-human-rights-event-plead/)
