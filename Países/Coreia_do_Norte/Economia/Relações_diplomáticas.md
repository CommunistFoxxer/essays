# Relações diplomáticas
Relações com Cuba:
- [Leftist Critic | “Advancing the cause of socialism”: The DPRK and the Cuban Revolution](https://leftistcritic.wordpress.com/2018/05/22/advancing-the-cause-of-socialism-the-dprk-and-the-cuban-revolution/)
- [Cartaz de propaganda coreano dos anos 60](https://i.pinimg.com/originals/77/ae/c7/77aec7970c4248419c2bb83f806b1176.jpg)
- [Miguel Diaz Canel e Kim Jong-Un de mãos dadas](https://web.archive.org/web/20181120060523/https://cdn.asiancorrespondent.com/wp-content/uploads/2018/11/000_1AL01G-940x580.jpg)
- [NK News | Revolutionary Solidarity: Castro’s cozy relationship with North Korea](https://www.nknews.org/2016/11/revolutionary-solidarity-castros-cozy-relationship-with-north-korea/)
- [Yonhap News Agency | N.K. declares 3-day mourning over ex-Cuban leader Castro's death](https://en.yna.co.kr/view/AEN20161128002852315)
- [Latin Times | Fidel Castro Says North Korea Sent Cuba Free Weapons During Cold War](https://www.latintimes.com/fidel-castro-says-north-korea-sent-cuba-free-weapons-during-cold-war-130279)

Relações com os panteras negras:
- [The Guardian | How Black Panthers turned to North Korea in fight against US imperialism](https://www.theguardian.com/world/2014/jun/19/black-panthers-north-korea-us-imperialism)
- [The Asia-Pacific Journal Japan Focus | Juche in the United States: The Black Panther Party’s Relations with North Korea, 1969-1971](https://apjjf.org/2015/13/12/Benjamin-Young/4303.html)

Relações com Angola:
- [NK News | Angola discusses public security cooperation with North Korea](https://www.nknews.org/2016/04/angola-discusses-public-security-cooperation-with-north-korea/)
- [NK News | North Korea: Opponents of Apartheid](https://www.nknews.org/2013/12/north-korea-opponents-of-apartheid/)
- [Agência Angola Press | Angola/North Korea relations considered excellent](https://web.archive.org/web/20200118135050/https://www.angop.ao/angola/en_us/noticias/politica/2013/10/47/Angola-North-Korea-relations-considered-excellent,5342751a-5697-4949-9bdc-80346c0e4330.html)

RPDC - Relações com a Síria:
- [Deutsche Welle News | North Korean leader offers support to Assad](https://www.dw.com/en/north-korean-leader-offers-support-to-assad/a-16392804)
- [United Press International | Kim Jong Un congratulated Syria's Assad before U.S. strike](https://www.upi.com/Top_News/World-News/2017/04/07/Kim-Jong-Un-congratulated-Syrias-Assad-before-US-strike/5141491572060/)
- [Newsweek | War in Syria: Assad Thanks Iran and North Korea For Help In Letters To Two Supreme Leaders Opposed to U.S](https://www.newsweek.com/war-syria-assad-thanks-iran-and-north-korea-support-letters-two-supreme-665992)
- [Huffington Post | Syria Names A Park After Kim Jong Un’s Grandpa](https://www.huffpost.com/entry/syria-north-korea-park-kim-il-sung_n_55e4821de4b0c818f6188e25)
- [Syria Times | N. Korean Diplomat: Syrian Army's Achievements against Terrorism Make Us Feel Happy](http://syriatimes.sy/index.php/news/local/30732-n-korean-diplomat-syrian-army-s-achievements-against-terrorism-make-us-feel-happy)
- [Syrian Arab News Agency | Syria and DPRK: An excellent example of strong bilateral cooperation](https://sana.sy/en/?p=132377)
- [Military Watch Magazine | North Korean Special Forces in Syria; A Look at Pyongyang’s Assistance to Damascus’ Counterinsurgency Operations](https://militarywatchmagazine.com/article/70673)
- [Syrian Arab News Agency | Syria supports DPRK, condemns sanctions targeting its leadership](https://sana.sy/en/?p=82642)

Relações com a Argélia:
- [The Arab Weekly | Algeria defends ties with North Korea despite US pressure](https://thearabweekly.com/algeria-defends-ties-north-korea-despite-us-pressure)
- [NK News | North Korean delegation meets with Algerian officials](https://www.nknews.org/2017/04/north-korean-delegation-meets-with-algeria-officials/)

RPDC para a luta palestina:

![](images/korea-palestine.jpg)

<em>Restaurante em Gaza: “quaisquer clientes coreanos são bem-vindos a um desconto de 80% em gratidão à posição do líder da RPDC em nossa causa palestina”. A Coreia do Norte é grande amiga e aliada da causa palestina.</em>

- [NK News | How North Korea has been arming Palestinian militants for decades](https://www.nknews.org/2014/06/how-north-korea-has-been-arming-palestinian-militants-for-decades/)
- [The Palestiner Poster Project Archives | Palestine - North Korea](https://www.palestineposterproject.org/poster/palestine-north-korea)
- [Israel National News | North Korea: There is an Israel Connection](https://www.israelnationalnews.com/Articles/Article.aspx/20887)
- [Business Insider | Israeli F-4s Actually Fought North Korean MiGs During the Yom Kippur War](https://www.businessinsider.com/israel-north-korea-dogfight-yom-kippur-war-2013-6?IR=T)
- [Jewish Virtual Library | Military Threats to Israel: North Korea](https://www.jewishvirtuallibrary.org/north-korea-military-threat-to-israel)
- [The Telegraph | Hamas and North Korea in secret arms deal](https://www.telegraph.co.uk/news/worldnews/middleeast/palestinianauthority/10992921/Hamas-and-North-Korea-in-secret-arms-deal.html)
- [The National Committee on North Korea | DPRK relations with PLO since 60s](https://www.ncnk.org/resources/publications/NCNK_Issue_Brief_DPRK_Diplomatic_Relations.pdf)
- [Country Data | North Korea - Relations with the Third World](http://www.country-data.com/cgi-bin/query/r-9642.html)
- [Juche Ireland | The Importance of International Solidarity: DPRK – Palestine](https://jucheireland.wordpress.com/2016/07/29/the-importance-of-international-solidarity-dprk-palestine/)
- [Juche Ireland | DPRK on Palestine](https://jucheireland.wordpress.com/2014/07/12/dprk-on-palestine/)

Relações com Madagascar:
- [Country Studies | Foreign relations - Madagascar](http://countrystudies.us/madagascar/29.htm)

Relações com a Líbia:
- [NK News | North Korea and Libya: friendship through artillery](https://www.nknews.org/2015/01/north-korea-and-libya-friendship-through-artillery/)
- ["O vencedor de inimigos, Abu Ali Al-Kim"](https://web.archive.org/web/20210525010037/https://pbs.twimg.com/media/E2MeYURXsAA-xPM?format=png&name=small)

Relações com Moçambique:
- [Rua chamada Kim Il-Sung em Moçambique](https://www.wikidata.org/wiki/Q27464910)
- [North Korea Leadership Watch | Ri Su Yong Meets with Mozambique Delegation](https://web.archive.org/web/20161013045141/https://nkleadershipwatch.wordpress.com/2016/05/26/ri-su-yong-meets-with-mozambique-delegation/)

Relações com o Vietnã:
- [BBC News | N Korea admits Vietnam war role](http://news.bbc.co.uk/2/hi/asia-pacific/1427367.stm)

Relações com a Irlanda:
- [Independent.ie | Official IRA's terror trip to North Korea](https://www.independent.ie/irish-news/official-iras-terror-trip-to-north-korea-29760564.html)
- [NK News | Rocky road to Pyongyang: DPRK-IRA relations in the 1980s](https://www.nknews.org/2013/05/the-rocky-road-to-pyongyang-dprk-ira-relations-in-the-1980s/)

Relações com o Zimbabue:
- [NK News | Zimbabwe and North Korea: Uranium, elephants, and a massacre](https://www.nknews.org/2013/10/zimbabwe-and-north-korea-uranium-elephants-and-a-massacre/)

Relações com os naxalitas:
- [India Today | Maoists building weapons factories in India with help from China](https://www.indiatoday.in/india/north/story/chinese-intelligence-training-and-funding-maoists-in-india-100359-2012-04-26)

Relações com o Hezbollah:
- [Juche Ireland | The Importance of International Solidarity: DPRK’s Support for Hezbollah](https://jucheireland.wordpress.com/2017/01/16/the-importance-of-international-solidarity-dprks-support-for-hezbollah/)

Relações com a Turquia:
- [Medium | North Korea in the eyes of Turkey’s Socialists](https://medium.com/@bcloud__/north-korea-in-the-eyes-of-turkey-s-socialist-s-c673510e64db)

Relações com o Chile:
- [Juche Ireland | Kim Il Sung and Salvador Allende](https://jucheireland.wordpress.com/2011/07/08/kim-il-sung-and-salvador-allende/)

Relações com a Venezuela:
- [Yahoo News | Seoul 'tricked' N. Korea waitresses into defecting: manager](https://au.news.yahoo.com/seoul-tricked-n-korea-waitresses-defecting-manager-080159062--spt.html?guccounter=1&guce_referrer=aHR0cHM6Ly9kcHJrc3R1ZHlndWlkZS53b3JkcHJlc3MuY29tLzIwMTcvMTAvMTMvZHByay1zdHVkeS1ndWlkZS8&guce_referrer_sig=AQAAAAdpgLiRKHlrR-vRT8ByQMn8XN3Ey9YpdBnHLaJrKqXnoX07l_BH0NHleGosUYoZzVf-Jux1EBKLwl0KdCJ7YC9YYkeDTuqFLXa_z8-uhuVDqWUnkEzwj8cfnFgeYEQCx0F32YRzWHRUN3dOPG4xrKXle883fr11xWecrPlLpEEQ)
- [NK News | Venezuela opens first embassy in North Korea](https://www.nknews.org/2019/08/venezuela-to-open-embassy-in-north-korea-on-wednesday-afternoon/)

Também surgiram [fake news](https://www.reuters.com/article/idUSKCN1BS0MQ) publicadas pela Reuters dizendo que o embaixador kuwaitiano fugiu da Coreia do Norte. Primeiro que a RPDC não tem relações diplomáticas com o Kuwait desde 2017. Tão distorcido que ele supostamente fugiu há 16 meses, mas as notícias só circulam agora.
