# Apoio popular
Gostaria de concluir com um exame de Kim Il Sung e o suposto “culto da personalidade” que o cerca. A dor em massa em torno de seu funeral é considerada evidência de que ele é adorado como um deus na RPDC. Na verdade, essa dor resultou do imenso apoio popular que ele desfrutou como líder, durante e após a revolução. Os japoneses o consideravam um líder guerrilheiro altamente capaz e perigoso, chegando ao ponto de estabelecer uma unidade especial de insurgência anti-Kim para caçá-lo. A guerrilha era uma força independente, inspirada pelo desejo de recuperar a península coreana para os coreanos, e não era controlada nem pelos soviéticos nem pelos chineses. Embora muitas vezes se retirassem através da fronteira com a União Soviética para escapar das forças de contra-insurgência japonesas, eles receberam pouca ajuda material dos soviéticos. Ao contrário dos Estados Unidos, que impôs um governo militar e reprimiu os Comitês do Povo, os soviéticos adotaram uma abordagem bastante indireta em sua zona de ocupação, permitindo que uma coalizão de combatentes da resistência nacionalista e comunista fizesse seu próprio show. Em sete meses, o primeiro governo central foi formado, com base em um Comitê do Povo interino liderado por Kim Il-sung.

Ao contrário da mitologia popular, Kim não foi escolhido a dedo pelos soviéticos. Ele gozava de considerável prestígio e apoio como resultado de seus anos como líder guerrilheiro e seu compromisso com a libertação nacional. Na verdade, os soviéticos nunca confiaram totalmente nele. Oito meses após a ocupação, iniciou-se um programa de reforma agrária, com os latifundiários despojados de suas terras sem indenização, mas livres para migrar para o sul ou para trabalhar em lotes iguais aos alocados aos camponeses. Depois de um ano, o Partido dos Trabalhadores de Kim se tornou a força política dominante. As principais indústrias, a maioria pertencentes a japoneses, foram nacionalizadas. Colaboradores japoneses foram expulsos de cargos oficiais. Os cidadãos da RPDC apóiam Kim Il-sung por causa de seu corajoso desafio à dominação dos EUA, seu compromisso com a reunificação e as verdadeiras conquistas do socialismo. Diante daqueles que fazem guerra pela exploração e opressão, as decisões de Kim representaram as aspirações dos trabalhadores, camponeses, mulheres e crianças coreanos - a nação coreana unida - pela liberdade. O apoio de Kim não foi derivado de um culto à personalidade ou levado à força. Ao contrário, ele conquistou o apoio de seu povo na luta.

Na verdade, não havia mecanismos para forçar o povo coreano a apoiar Kim Il-Sung durante seu governo. Lankov escreve:
>Os norte-coreanos na era Kim Il-Sung não eram autômatos de lavagem cerebral cujo passatempo favorito era andar de ganso... nem eram dissidentes secretos... nem eram escravos dóceis que timidamente seguiam qualquer ordem de cima.

A RPDC de Kim Il-sung não era um estado policial, mas sim um país democrático e socialista travando uma guerra valente contra o imperialismo. O povo coreano estava - e continua a ser - unido na luta e a apoiar seus líderes nesta base. Uma pesquisa com desertores estima que mais da metade do país que eles deixaram para trás aprova o trabalho que o líder Kim Jong Un está fazendo. O Instituto de Estudos para a Paz e a Unificação de Seul, conforme relatado pela agência de notícias Yonhap, pediu a 133 desertores que arriscassem um palpite sobre o índice de aprovação real de Kim no país, o que, pelo menos, comprou publicamente o culto absoluto da personalidade em torno de sua liderança. Em uma pesquisa semelhante em 2011, apenas 55% acreditavam que o pai e antecessor de Kim, Kim Jong Il, tinha o apoio da maioria do país.

Como escreve a BBC:
>Os especialistas atribuem a popularidade de Kim Jong-un aos esforços para melhorar a vida cotidiana dos cidadãos, com ênfase no crescimento econômico, nas indústrias leves e na agricultura em um país onde acredita-se que a maioria não tem alimentos, diz Yonhap. Não há pesquisas de opinião no fechado estado comunista, onde - pelo menos externamente - o líder goza de total apoio. Embora não seja diretamente comparável, o índice de aprovação percebido supera o dos líderes ocidentais. Uma pesquisa recente da McClatchy sugeriu que apenas 41% dos americanos apóiam o desempenho do presidente Barack Obama, enquanto o primeiro-ministro do Reino Unido, David Cameron, marcou 38% em uma pesquisa recente do YouGov.

O Wall Street Journal, citando a pesquisa, diz que mais de 81% dos desertores disseram que as pessoas estavam recebendo três refeições por dia, contra 75% do lote anterior pesquisado:
>Isso aponta para uma consolidação de poder bem-sucedida para o jovem líder, que assumiu com a morte de seu pai, Kim Jong Il, em dezembro de 2011. Isso parecia incerto há um ano, pelo menos com base no relatório anterior do instituto sobre entrevistas com desertores. Falando então a 122 pessoas que fugiram da Coreia do Norte entre janeiro de 2011 e maio de 2012, descobriu que 58% estavam insatisfeitos com a escolha do jovem Sr. Kim como sucessor. (É claro que as pessoas que fogem do país tendem a ficar mais insatisfeitas com ele do que as que permanecem)
>
>O novo líder parece estar apertando seu controle, com 45% dizendo que a sociedade está totalmente sob controle, contra 36% no relatório anterior. Folhetos e pichações anti-regime são um pouco menos comuns (mas talvez esse seja o alto índice de aprovação no trabalho): 66% do último grupo disse ter visto essas coisas, ante 73% na pesquisa de 2012 e 70% em 2011 Viajar para outras partes do país tornou-se mais difícil. A porcentagem que relatou ter feito isso, após aumentar por cinco anos consecutivos - para 70% entre os desertores entrevistados em 2012, de 56% entre os entrevistados em 2008 - recuou para 64%.

A mídia burguesa continua a retratar a RPDC como um pesadelo totalitário, habitado exclusivamente por cidadãos pacificados e amedrontados. Como mostrei, isso está longe de ser o caso. O povo norte-coreano tem muito mais voz na forma como suas vidas são estruturadas do que os cidadãos mesmo dos países capitalistas mais “democráticos”. Eles não são forçados a aderir a uma linha partidária transmitida do alto, mas são encorajados a participar na gestão da sociedade. A RPDC é um excelente exemplo de socialismo, que se concentra no desenvolvimento da classe trabalhadora - e da humanidade - em seu potencial máximo. É somente através do socialismo que podemos realizar nosso sonho coletivo de uma sociedade livre e próspera. A RPDC está marchando em direção a este sonho, mesmo em face de uma agressão imperialista sem paralelo. É em parte com base nisso que devemos prometer solidariedade para com o país. Para reiterar o ponto que apresentei em meu último post, no entanto, a RPDC deve ser apoiada independentemente de ser ela própria socialista. Está contra o imperialismo, que é o maior inimigo do socialismo. Indireta ou diretamente, a RPDC trabalha no interesse do socialismo.

"É um país muito tranquilo, muito limpo, não há criminalidade", afirma este ex-atacante norueguês em entrevista à AFP desde Tóquio, onde sua equipe disputa a Copa da Ásia Oriental, torneio bienal que neste ano é sediado pelo Japão. "Vivo num hotel em Pyongyang, numa grande suíte ao lado de minha esposa. Lá, vivo como vivia na Europa", explica Jorn Andersen, 54 anos, com seu cabelo loiro e olhos azuis que escancaram sua origem. Na opinião do treinador, "muitas das coisas negativas" do país vêm do exterior e contrastam com a situação real em Pyongyang. ([Matéria](https://www.nsctotal.com.br/noticias/jorn-andersen-um-noruegues-muito-feliz-por-treinar-a-coreia-do-norte) de 2017).


A licença-maternidade na Coreia do Norte é de 240 dias com salário integral. A Coreia do Norte está no ranking mundial dos países com maior licença-maternidade com salário integral.

Médicos por mil habitantes:
- Coreia do Norte – 4
- Reino Unido – 3
- EUA – 3
- Coreia do Sul – 2
- Japão – 2
- Brasil – 2

**Fonte**: Index Mundi

- Um modelo de planejamento socialista democrático e participativo
- Socialismo e democracia na RPDC
- Lalkar: A estrutura democrática da RPDC
- Uma breve história econômica da RPDC
- Compreendendo a RPDC à luz da tradição marxista
- Protesto em massa na RPDC contra propaganda da ONU
- Democracia: EUA vs RPDC
- Eleições locais na RPDC, 2019
- A RPDC é socialista?
- Como funcionam as eleições na RPDC?
- O que exatamente é juche?
- O sistema parlamentar da RPDC
- O sistema de trabalho taean
- A bomba dos povos!
- Eleições no estado juche
- Algo que os países desenvolvidos invejariam: o sistema de saúde da RPDC
- Coreia resiliente: socialismo na coreia democrática
- Rumo a uma análise concreta da RPDC
- O mito da dinastia Kim
- Governo transfere vítimas das enchentes para novas casas, em um ritmo que levaria três anos para os países em desenvolvimento
- Direitos humanos na RPDC
- Política da RPDC
- Visão da RPDC sobre o nacionalismo
- Bruce cumings sobre a economia da Coréia do Norte
- RPDC: um campeão na luta contra as mudanças climáticas
- Apoiadores na África Ocidental
- Juche na Nigéria
- "o atendimento médico da Coreia do Norte é algo que os países desenvolvidos invejariam" Organização Mundial da Saúde
- A origem da RPDC
- Pessoas com deficiência na RPDC
- 7º congresso do partido
- Por que a RPDC está construindo bombas nuclear
- A contribuição imortal de Kim Il-Sung para a libertação africana
- Por que os latinos devem apoiar a RPDC
- O governo dos EUA é um clube racista de bilionários, de acordo com a RPDC
- Socialismo na Coreia: um estudo de caso
- Funcionários da RPDC vão à China para estudar reformas
- Meu país socialista
- Por que a RPDC possui armas nucleares? Olhe para a Líbia
- Parenti na RPDC
- Por que os coreanos do norte não são loucos
- Por que a Coreia do Norte precisa de armas nucleares
- RPDC responde à retirada americana do acordo de Paris
- Se bbc repostado da mesma forma, reporta para dprk, para a família real
- Violações dos direitos humanos: RPDC vs EUA
- A RPDC nunca removeu o marxismo de sua constituição
- A RPDC convidou Obama e outras autoridades para uma visita em vez de caluniá-la de antemão
