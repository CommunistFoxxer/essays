# A crise da Venezuela
De fato, a Venezuela foi um pouco burra de concentrar toda sua fonte de renda no petróleo, sem aumentar a complexidade econômica e industrial.

O Departamento de Estado vazou documento sobre as metas dos EUA para a Venezuela. "Interesse fundamental na Venezuela; (1) Que a Venezuela continue a fornecer uma parte significativa de nossas importações de petróleo..."

![](images/venezuela_DoS.jpg)

ONGs ocidentais canalizam ajuda financeira para levantes de oposição, com [documentos](http://telesurtv.net/english/analysis/NGOs-A-New-Face-of-Destabilization-in-Latin-America-20160605-0025.html) da WikiLeaks.

A crise da Venezuela pode ser devido à queda dos preços do petróleo (os preços do petróleo caíram cerca de 70% depois de 2008), mas não completamente. Os EUA estão [despejando $49 milhões](http://alwaght.com/en/News/97900/US-Spent-$49M-for-Venezuelan-Regime-Change-Since-2009-Report) na oposição para que seu governo retire Maduro do poder. Esta não é a única ruína do governo. Isso tem muita [infiltração dos EUA](https://wikileaks.org/plusd/cables/04CARACAS501_a.html), com um propósito declarado pelo Departamento de Estado.

Pessoas estão morrendo de falta de alimentos e medicamentos. Desde 2014, o governo venezuelano tem encontrado suprimentos médicos e alimentares acumulados e enterrados pelos burgueses: [suprimentos médicos](https://www.reuters.com/article/us-venezuela-economy-idUSKCN0ID00A20141024), burgueses [acumulando alimentos](http://aljazeera.com/indepth/features/2014/03/hoarding-causing-venezuela-food-shortages-20143210236836920.html) para vender com maior lucro na Colômbia, [50 toneladas de alimentos enterrados](http://telesurtv.net/english/news/Venezuelas-Economic-War-Tons-of-Food-Found-Buried-Underground-20150817-0024.html), manifestantes da oposição [queimando 40 toneladas de alimentos](http://telesurtv.net/english/news/Venezuela-Protesters-Set-40-Tons-of-Subsidized-Food-on-Fire-20170630-0017.html) para famílias pobres. [Tendência de desnutrição na Venezuela](https://data.worldbank.org/indicator/SN.ITK.DEFC.ZS?page=4&year_high_desc=false) de 1991 a 2015.

Gráfico do jornalista americano Michael Prysner, mostrando que a oposição é responsável pela maioria das mortes:

![](images/michael_prysner_graphic.jpg)

Infográfico das mortes:

![](images/death_infographic_venezuela.jpg)

Há [ataques com motivação racial da oposição](http://telesurtv.net/english/news/Venezuelan-Youth-Burned-for-Being-Chavista-Dies-from-Injuries-20170604-0011.html).

[Pesquisa recente](https://www.laiguana.tv/articulos/54333-87-venezolanos-guarimbas/) da organização de votação mais respeitada e neutra venezuelana mostra que 87% dos venezuelanos rejeitam as manifestações contra o governo.

[A Assembleia Nacional não foi dissolvida](http://web.archive.org/web/20170614090021/http://telesurtv.net/english/news/Fake-News-Claims-of-Coup-in-Venezuela-Debunked-20170330-0026.html); eles são detidos por desacato até que a oposição cumpra as decisões de que 3 legisladores da oposição não poderiam ser empossados devido a fraude eleitoral. Se a Assembleia Nacional concordar, então será revertido, eles não o farão, porque é exatamente disso que os imperialistas precisam após um terrível revés na OEA.

>Na terça-feira, os chavistas tomaram as ruas de Caracas em massa após uma tentativa de instaurar um processo de impeachment contra o presidente venezuelano Nicolas Maduro pela legislatura da oposição... Os chavistas expressaram sua rejeição às mais de 53 mil assinaturas fraudulentas coletadas pela oposição anteriormente este ano, como parte do pedido inicial para iniciar o processo de referendo contra Maduro. Eles também denunciaram a recusa da direita em se sentar para um diálogo mediado pelo Vaticano com o governo, apesar de ter exigido a inclusão do Papa em primeiro lugar. [link](https://venezuelanalysis.com/images/12752)

Foto de protesto pró-governo:

![](images/pro_government_protest_in_Venezuela.jpg)

O próprio CEO diz literalmente no vídeo que é [quase impossível falsificar informações](http://telesurtv.net/english/news/Venezuela-Election-Head-Smartmatic-Allegations-Have-No-Basis-20170802-0036.html) e, se houver adulteração, seria óbvio. Se está vindo da boca do CEO, e ele é o único a dizer isso, então é óbvio que algo está errado quando ele diz que o governo "calculou mal" seus números.

Uma autoridade eleitoral venezuelana pediu uma [auditoria investigatória imediata](http://telesurtv.net/english/news/Venezuela-Asks-Electoral-Council-to-Check-Vote-Tally-20170802-0045.html). Um sindicalista norte-americano agiu como observador internacional e afirma que ["a mídia de massa mente sobre a Assembleia da Venezuela (recém-eleita)"](http://telesurtv.net/english/news/US-Trade-Unionist-Unmask-Mass-Media-Lies-on-Venezuela-Assembly-20170806-0014.html).

Quando Maduro pediu à ONU em 2018 que fosse à Venezuela verificar as eleições, a oposição venezuelana [pediu](https://www.reuters.com/article/us-venezuela-politics-un-idUSKCN1GO2J0) que não, porque isso "validaria o regime".

Imperialismo contra a Venezuela: [Reino Unido confisca mais de US$1 bilhão em ouro da Venezuela](https://www.diariodepernambuco.com.br/noticia/mundo/2020/08/reino-unido-confisca-mais-de-us-1-bilhao-em-ouro-da-venezuela.html).
