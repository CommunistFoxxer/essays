# Transição demográfica
A seguir, notas sobre a transição demográfica, que fiz quando estava na escola. Me desculpem pela explicação meio "mecanicista", a relação para mim estava tão complexa que eu traduzi para lógica formal para que tudo ficasse mais fácil de ver, principalmente com a tabela verdade/tableaux semântico, e depois traduzi da lógica formal de volta para linguagem natural, porque o processo parecia mais claro para mim deste jeito.

Se há uma variação na taxa de mortalidade, então a taxa de natalidade vai (quase sempre) mudar simetricamente, o que necessariamente e simetricamente irá alterar a variação do crescimento vegetativo.

Exemplo: se a mortalidade diminuir, quase sempre ela implicará em uma diminuição da natalidade, e sempre que isso ocorrer, o crescimento vegetativo irá diminuir também (note, diminuição da natalidade causa em primeira instância, um aumento do crescimento vegetativo).

Se há uma variação na taxa de mortalidade, então sempre haverá uma variação simétrica no número de jovens.

Exemplo: se a taxa de mortalidade aumentar, o número de jovens sempre aumenta.

Se há uma variação na quantidade de mulheres no mercado de trabalho, então assimetricamente quase sempre isso implicará em uma variação do crescimento vegetativo, que sempre implicará simetricamente em uma variação do número de jovens que implicará assimetricamente no número de idosos.

Exemplo: se a quantidade de mulheres no mercado de trabalho aumentar, então quase sempre o crescimento vegetativo irá diminuir, e assim o número de jovens irá aumentar, assim necessariamente o número de idosos irá cair.

Eu fiz umas simplificações só para ficar fácil de compreender, além de ter pressuposto um modelo demográfico estático para facilitar as coisas.
